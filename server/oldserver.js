import SearchInput from "../src/components/UI/Input/Input";
import Spinner from "../src/components/UI/Spinner/Spinner";
import TemplateTable from "../src/components/TemplateTable/TemplateTable";
import Aux from "../src/hoc/Aux/Aux";
import React from "react";

app.post("/sendqrcode", (req, res) => {

  const {out_trade_no, plans, doctorId, clientId, clientEmail} = req.body

  const newPlanObject = plans[plans.length-1]

  const transporter = nodemailer.createTransport({
    host: "smtp.qy.tom.com",
    secureConnection: true,
    port: 465,
    auth: {
      user: 'pay@voin-cn.com',
      pass: 'voin.2019',
    }
  });

  const newCodeUrlString = "http://www.voin-cn.com/getnewqrcode/"+out_trade_no+"/"+doctorId+"/"+clientId
  const getPdfUrlString = "http://www.voin-cn.com/getpdf/"+out_trade_no+"/"+doctorId+"/"+clientId

  const output = `
    <p>亲爱的患者:<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好，我们已经为您生成了适合您的运动康复处方。您如果想要得到它，请扫描下方二维码，或保存在手机本地相册并识别该二维码。<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;快来得到您的专属计划吧!
    </p>
    <p><b>如果二维码过期，请按此<a href=${newCodeUrlString}>链接</a>获取新的二维码</b></p>
    <img src="cid:${out_trade_no}"/>
    <p><b>如果已完成支付，请按此<a href=${getPdfUrlString}>链接</a>获取您的运动康复处方</b></p>
    <p style="color: #1b76b9">为了更好地观看处方，请下载"WPS OFFICE"APP</p>
    <p><b>苹果手机: </b>在app store搜索"WPS OFFICE"或点击链接<a href="https://apps.apple.com/cn/app/wps-office/id599852710">
    https://apps.apple.com/cn/app/wps-office/id599852710</a></p>
    <p><b>安卓手机: </b>请在应用商店搜索"WPS OFFICE"或扫描下方二维码获得</p>
    <img src="cid:${"android"}"/>
    <p>有任何问题，请联系客服。客服微信号：15611409403（电话同号）</p>
    <p>沃衍健康</p>
    <img src="cid:${'logo'}"/>
  `

  let mailOptions = {
    from: 'pay@voin-cn.com',
    to: clientEmail,
    subject: '沃衍康复功能锻炼计划',
    html: output,
    attachments: [{
      filename: out_trade_no + '.png',
      path: __dirname + '/Qr/' + out_trade_no + '.png',
      cid: out_trade_no
    }, {
      filename: 'logo.png',
      path: __dirname + '/logo.png',
      cid: 'logo'
    }, {
      filename: 'android.png',
      path: __dirname + '/android.png',
      cid: 'android'
    }]
  };

  let total_fee = 28000
  if (doctorId === "5b68bb0fde7068270a29d0fe") total_fee = 1

  wxpay.createUnifiedOrder({
    body: 'VOIN '+newPlanObject.name+"计划费用",
    out_trade_no,
    total_fee: total_fee,
    spbill_create_ip: '192.168.2.210',
    notify_url: 'http://wxpay_notify_url',
    trade_type: 'NATIVE',
    product_id: '123456789'
  }, (err, result) => {
    const qr_svg = qr.image(result.code_url, { type: 'png' });
    qr_svg.pipe(fs.createWriteStream(`${__dirname+"/Qr/"+out_trade_no}.png`))
    setTimeout(() => {
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log("not sent")
          console.log(error)
          return res.json({code: 0, msg: "后端出错了，请刷新"});
        } else {
          console.log("sent")
          console.log(info)
          return res.json({code: 1, doc: info})
        }
      })
    }, 1500)

  })
})


app.get("/getnewqrcode", (req, res) => {
  const out_trade_no = req.query.no
  const planId = out_trade_no.substring(6,30)
  const currentNumber = parseInt(out_trade_no.substring(30, 32), 10)
  const doctorId = req.query.did
  const clientId = req.query.cid

  Plans.findOne({
    doctorId,
    clientId,
    'plans._id': planId,
  }, {
    'plans.$' : 1
  }, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
    } else {
      if (doc) {
        let numbers = doc.plans[0].numbers
        const dateCreated = doc.plans[0].date.getTime()
        const dateNow = new Date().getTime()
        if (numbers === 99 || dateNow-dateCreated > 259200000) {
          return res.json({code: 0, msg: "该计划已过期，请联系医生重新制定计划"})
        }
        if (numbers > currentNumber) {
          return res.json({code: 0, msg: "请在最新邮件索取新二维码"})
        }
        const newGuyDate = doc.plans[0].date.toISOString()
        let stringNumber = ""
        if (numbers < 10) {
          stringNumber = "0"+numbers
        } else {
          stringNumber = ""+numbers
        }
        let out_trade_no = newGuyDate.substring(2,4)+newGuyDate.substring(5,7)
          +newGuyDate.substring(8,10)+doc.plans[0]._id+stringNumber

        wxpay.queryOrder({ out_trade_no: out_trade_no }, function(err, order){
          if (err) {
            return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
          }
          if (order.trade_state === 'SUCCESS') {
            return res.json({code: 1, msg: "此计划已付款"})
          } else {
            wxpay.closeOrder({ out_trade_no: out_trade_no}, err => {
              if (err) {
                return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
              } else {
                Clients.findById(clientId, (err, clientDoc) => {
                  if (!err) {
                    numbers = numbers + 1
                    if (numbers < 10) {
                      stringNumber = "0" + numbers
                    } else {
                      stringNumber = "" + numbers
                    }
                    out_trade_no = newGuyDate.substring(2, 4) + newGuyDate.substring(5, 7)
                      + newGuyDate.substring(8, 10) + doc.plans[0]._id + stringNumber

                    const transporter = nodemailer.createTransport({
                      host: "smtp.qy.tom.com",
                      secureConnection: true,
                      port: 465,
                      auth: {
                        user: 'pay@voin-cn.com',
                        pass: 'voin.2019',
                      }
                    });

                    const newCodeUrlString = "http://www.voin-cn.com/getnewqrcode/" + out_trade_no + "/" + doctorId + "/" + clientId
                    const getPdfUrlString = "http://www.voin-cn.com/getpdf/" + out_trade_no + "/" + doctorId + "/" + clientId

                    const output = `
                          <p>亲爱的患者:<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好，我们已经为您生成了适合您的运动康复处方。您如果想要得到它，请扫描下方二维码，或保存在手机本地相册并识别该二维码。<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;快来得到您的专属计划吧!
                          </p>
                          <p>如果二维码过期，请按此<a href=${newCodeUrlString}>链接</a>获取新的二维码</p>
                          <img src="cid:${out_trade_no}"/>
                          <p>如果已完成支付，请按此<a href=${getPdfUrlString}>链接</a>获取您的运动康复处方</p>
                          <p style="color: #1b76b9">为了更好地观看处方，请下载"WPS OFFICE"APP</p>
                          <p><b>苹果手机: </b>在app store搜索"WPS OFFICE"或点击链接<a href="https://apps.apple.com/cn/app/wps-office/id599852710">
                          https://apps.apple.com/cn/app/wps-office/id599852710</a></p>
                          <p><b>安卓手机: </b>请在应用商店搜索"WPS OFFICE"或扫描下方二维码获得</p>
                          <img src="cid:${"android"}"/>
                          <p>有任何问题，请联系客服。客服微信号：15611409403（电话同号）</p>
                          <p>沃衍健康</p>
                          <img src="cid:${'logo'}"/>
                        `

                    let mailOptions = {
                      from: 'pay@voin-cn.com',
                      to: clientDoc.email,
                      subject: '沃衍康复功能锻炼计划',
                      html: output,
                      attachments: [{
                        filename: out_trade_no + '.png',
                        path: __dirname + '/Qr/' + out_trade_no + '.png',
                        cid: out_trade_no
                      }, {
                        filename: 'logo.png',
                        path: __dirname + '/logo.png',
                        cid: 'logo'
                      }, {
                        filename: 'android.png',
                        path: __dirname + '/android.png',
                        cid: 'android'
                      }]
                    };

                    let total_fee = 28000
                    if (doctorId === "5b68bb0fde7068270a29d0fe") total_fee = 1

                    wxpay.createUnifiedOrder({
                      body: "VOIN 计划费用",
                      out_trade_no,
                      total_fee: total_fee,
                      spbill_create_ip: '192.168.2.210',
                      notify_url: 'http://wxpay_notify_url',
                      trade_type: 'NATIVE',
                      product_id: '1234567890'
                    }, (err, result) => {
                      const qr_svg = qr.image(result.code_url, {type: 'png'});
                      qr_svg.pipe(fs.createWriteStream(`${__dirname + "/Qr/" + out_trade_no}.png`))
                      setTimeout(() => {
                        transporter.sendMail(mailOptions, (error, info) => {
                          if (error) {
                            console.log("not sent")
                            return res.json({code: 0, msg: "邮件未发送，请重新点击链接"});
                          } else {
                            console.log("sent")
                            Plans.findOneAndUpdate({
                              doctorId,
                              clientId,
                              'plans._id': planId,
                            }, {
                              $inc: {
                                'plans.$.numbers': 1
                              }
                            }, err => {
                              if (!err) {
                                return res.json({code: 1, msg: "新二维码已发送，请您及时付款", doc: info})
                              } else {
                                return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
                              }
                            })

                          }
                        })
                      }, 1500)
                    })

                  } else {
                    return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
                  }
                })
              }
            })
          }
        })
      } else {
        return res.json({code: 0, msg: "此计划不存在"})
      }
    }
  })
})


app.get("/getpdf", (req, res) => {
  const out_trade_no = req.query.no
  const planId = out_trade_no.substring(6,30)
  const currentNumber = parseInt(out_trade_no.substring(30, 32), 10)
  const doctorId = req.query.did
  const clientId = req.query.cid

  Plans.findOne({
    doctorId,
    clientId,
    'plans._id': planId,
  }, {
    'plans.$' : 1,
  }, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请重新点击链接1"})
    } else {
      if (doc) {
        if (doc.plans[0].pdfSent) return res.json({code: 0, msg: "计划PDF己发送，请不要重复获取"})
        let numbers = doc.plans[0].numbers
        if (numbers > currentNumber) {
          return res.json({code: 0, msg: "请在最新邮件获取运动康复处方"})
        }
        const newGuyDate = doc.plans[0].date.toISOString()
        let stringNumber = ""
        if (numbers < 10) {
          stringNumber = "0"+numbers
        } else {
          stringNumber = ""+numbers
        }
        let out_trade_no = newGuyDate.substring(2,4)+newGuyDate.substring(5,7)
          +newGuyDate.substring(8,10)+doc.plans[0]._id+stringNumber
        wxpay.queryOrder({ out_trade_no: out_trade_no }, function(err, order) {
          if (err) {
            return res.json({code: 0, msg: "后端出错了，请重新点击链接2"})
          }
          if (order.trade_state === 'SUCCESS') {
            const timeEnd = order.time_end
            const timeString = timeEnd.substring(0,4)+"-"+timeEnd.substring(4,6)+"-"+timeEnd.substring(6,8)+" "+
              timeEnd.substring(8,10)+":"+timeEnd.substring(10,12)+":"+timeEnd.substring(12,14)
            const payDate = new Date(timeString).getTime()
            const now = new Date().getTime()
            if (now-payDate > 259200000) {
              return res.json({code: 0, msg: "该计划已过期，请联系医生重新制定计划"})
            }
            const plan = doc.plans[0]
            Clients.findById(clientId, (err, clientDoc) => {
              if (!err) {
                Doctors.findById(doctorId, (err, doctorDoc) => {
                  if (!err) {
                    const order = plan.moveList.map(move => move.move)
                    Movements.find({ _id: { $in: order } }, (err, moveDoc) => {
                      if (!err) {
                        moveDoc.sort((a, b) =>  {
                          return order.indexOf(a._id) - order.indexOf(b._id);
                        });
                        const planDetail = moveDoc.map(move => {
                          return {"_id": move._id, name: move.name, url: move.url, info: move.info}
                        })
                        const info = {
                          planDetail,
                          planName: plan.name,
                          clientName: clientDoc.name,
                          allDay: plan.allDay,
                          infos: plan.moveList.map(move => move.info),
                          sets: plan.moveList.map(move => move.set),
                          timesPerSets: plan.moveList.map(move => move.timesPerSet),
                          times: plan.moveList.map(move => move.time),
                          weights: plan.moveList.map(move => move.weight),
                          parameter1s: plan.moveList.map(move => move.parameter1),
                          parameter2s: plan.moveList.map(move => move.parameter2),
                          value1s: plan.moveList.map(move => move.value1),
                          value2s: plan.moveList.map(move => move.value2),
                          extra: plan.extra,
                          doctorName: doctorDoc.name
                        }
                        console.log(info)
                        pdf.create(pdfTemplate(info), {}).toFile('./pdf/'+plan.name+plan._id+'.pdf', (err) => {
                          if(err) {
                            console.log(err)
                            return res.json({code: 0, msg: "后端出错了，请重新点击链接3"})
                          } else {
                            console.log(err)
                            const transporter = nodemailer.createTransport({
                              host: "smtp.qy.tom.com",
                              secureConnection: true,
                              port: 465,
                              auth: {
                                user: 'pay@voin-cn.com',
                                pass: 'voin.2019',
                              }
                            });

                            const output = `
                          <p>亲爱的患者:<br>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您的运动康复处方已生成，请在附件中查收。在练习过程中，您如果有任何不适，请立刻停下来，并联系您的医生。<br>
                          </p>
                          <p style="color: #1b76b9">为了更好地观看处方，请下载"WPS OFFICE"APP</p>
                          <p><b>苹果手机: </b>在app store搜索"WPS OFFICE"或点击链接<a href="https://apps.apple.com/cn/app/wps-office/id599852710">
                          https://apps.apple.com/cn/app/wps-office/id599852710</a></p>
                          <p><b>安卓手机: </b>请在应用商店搜索"WPS OFFICE"或扫描下方二维码获得</p>
                          <img src="cid:${"android"}"/>
                          <p>有任何问题，请联系客服。客服微信号：15611409403（电话同号）</p>
                          <p>沃衍健康</p>
                          <img src="cid:${'logo'}"/>
                        `

                            let mailOptions = {
                              from: 'pay@voin-cn.com',
                              to: clientDoc.email,
                              subject: '沃衍康复功能锻炼计划',
                              html: output,
                              attachments: [{
                                filename:plan.name+plan._id+'.pdf',
                                path: './pdf/'+plan.name+plan._id+'.pdf',
                                cid: out_trade_no
                              }, {
                                filename: 'logo.png',
                                path: __dirname + '/logo.png',
                                cid: 'logo'
                              }, {
                                filename: 'android.png',
                                path: __dirname + '/android.png',
                                cid: 'android'
                              }]
                            };
                            transporter.sendMail(mailOptions, (error, info) => {
                              if (error) {
                                console.log("not sent")
                                console.log(error)
                                return res.json({code: 0, msg: "邮件未发送，请重新点击链接"});
                              } else {
                                Plans.findOneAndUpdate({
                                  doctorId,
                                  clientId,
                                  'plans._id': planId,
                                }, {
                                  'plans.$.pdfSent': true
                                }, err => {
                                  if (err) {
                                    return res.json({code: 0, msg: "后端出错了，请重新点击链接5"});
                                  } else {
                                    console.log("sent to "+clientDoc.email)
                                    return res.json({code: 1, msg: "运动处方已发送到邮箱，请查收", doc: info})
                                  }
                                })
                              }
                            })
                          }
                        });

                      } else {
                        return res.json({code: 0, msg: "后端出错了，请重新点击链接6"})
                      }
                    })
                  } else {
                    return res.json({code: 0, msg: "后端出错了，请重新点击链接7"})
                  }
                })
              } else {
                return res.json({code: 0, msg: "后端出错了，请重新点击链接8"})
              }
            })
          }
          else {
            return res.json({code: 0, msg: "此计划未付款，请付款后获取运动康复处方"})
          }
        })

      } else {
        return res.json({code: 0, msg: "此计划不存在"})
      }
    }
  })
})

app.post('/updateplan', (req, res) => {
  const {doctorId, clientId, planId, name, allDay, times, sets, timesPerSets, weights, parameter1s, parameter2s,
    value1s, value2s, infos, ids} = req.body
  const moveList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    moveList.push(oneMove)
  }
  Plans.findOneAndUpdate({
    doctorId,
    clientId,
    'plans._id': planId,
  }, {
    $set: {
      'plans.$.name': name,
      'plans.$.allDay': allDay,
      'plans.$.moveList': moveList
    }
  }, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/create-pdf', (req, res) => {
  console.log(req.body)
  pdf.create(pdfTemplate(req.body), {}).toFile(`${__dirname}/pdf/${req.body.planName}.pdf`, (err) => {
    if(err) {
      res.send(Promise.reject())
    }
    res.send(Promise.resolve())
  });
})

app.get('/fetch-pdf', (req, res) => {
  res.sendFile(`${__dirname}/pdf/${req.body.planName}.pdf`)
});

app.post('/sendlink', (req, res) => {
  const {no, plans, doctorId, clientId, clientEmail} = req.body
  const newPlanObject = plans[plans.length-1]
  const transporter = nodemailer.createTransport({
    host: "smtp.qy.tom.com",
    secureConnection: true,
    port: 465,
    auth: {
      user: 'pay@voin-cn.com',
      pass: 'voin.2019',
    }
  });

  const link = "https://www.voin-cn.com/seeplan/"+no+"/"+doctorId+"/"+clientId
  const output = `
    <p>亲爱的患者:<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好，我们已经为您生成了适合您的运动康复计划。您如果想要得到它，请点击下方链接。<br>
    
    <a href=${link} target="_blank" style="text-decoration: none; ">https://www.voin-cn.com/seeplan/${no}/${doctorId}/${clientId}</a><br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;快来得到您的专属计划吧!
    </p>
    <div>有任何问题，请联系客服。</div>
    <div>客服微信号：voin2019</div>
    <div>电话：15611409403</div>
    <div>沃衍健康（中国康复医学会会员）</div>
    <div>康复动作由中国运动康复专家审阅并推荐</div>
    <img src="cid:${'logo'}"/>
  `

  let mailOptions = {
    from: 'pay@voin-cn.com',
    to: clientEmail,
    subject: '沃衍康复功能锻炼计划',
    html: output,
    attachments: [{
      filename: 'logo.png',
      path: __dirname + '/logo.png',
      cid: 'logo'
    }]
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log("not sent")
      console.log(error)
      return res.json({code: 0, msg: "后端出错了，请刷新"});
    } else {
      console.log("sent")
      console.log(info)
      return res.json({code: 1, doc: info})
    }
  })

})

app.post('/addclient', (req, res) => {
  const relation = {
    doctorId: req.body.doctorId, clientId: req.body.clientId, plans: []
  }

  // TODO: validation 要看doctorID与clientId是否符合要求

  Plans.find({doctorId: req.body.doctorId}, (err, doc) => {
    if (!err) {
      const currentClients = doc.map(plan => plan.clientId)
      if (currentClients.indexOf(req.body.clientId) >= 0) {
        return res.json({code: 2, msg: "该患者已在您列表中"})
      } else {
        Plans.create(relation, (err) => {
          if (!err) {
            return res.json({code: 1})
          } else {
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})



app.post('/deleteclient', (req, res) => {
  const { doctorId, clientId } = req.body
  Plans.remove({doctorId, clientId}, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})


app.post('/deleteplan', (req, res) => {
  const { doctorId, clientId, planId } = req.body
  Plans.findOneAndUpdate({doctorId, clientId}, {
    $pull: {
      plans: {
        _id: planId
      }
    }
  }).exec((err) => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/savetemplate', (req, res) => {
  const {doctorId, name, summary, times, sets, timesPerSets, weights, parameter1s, parameter2s,
    value1s, value2s, articles, infos, ids, totalInEach, periodDays} = req.body
  const moveList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.time = times[i]
    moveList.push(oneMove)
  }
  Doctors.findOneAndUpdate({
    _id: doctorId}, {
    $push: {
      templates: {
        name,
        summary,
        moveList,
        articles,
        totalInEach,
        periodDays
      }
    }
  }, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get("/clientbyemail", (req, res) => {
  const {clientEmail} = req.query
  Clients.findOne({email: clientEmail}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      return res.json({code: 1, doc})
    }
  })
})

app.post("/addnewclient", (req, res) => {
  const {clientEmail, clientNameB} = req.body
  Clients.create({email: clientEmail, name: clientNameB}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      return res.json({code: 1, doc})
    }
  })
})

app.post('/saveplan', (req, res) => {
  const {doctorId, clientId, name, allDay, times, sets, timesPerSets, weights, parameter1s, parameter2s,
    value1s, value2s, infos, ids, extra, articles, totalInEach, periodDays} = req.body
  const moveList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    moveList.push(oneMove)
  }
  Plans.findOne({doctorId, clientId}, (err, doc) => {
    if (!err) {
      if (doc) {
        Plans.findOneAndUpdate({
          doctorId,
          clientId
        }, {
          $push: {
            plans: {
              name,
              allDay,
              start: false,
              pdfSent: false,
              paid: false,
              moveList,
              articles,
              extra,
              totalInEach,
              periodDays,
              feedback: [],
              numbers: 1
            }
          }
        }, {new: true}, (err, doc) => {
          if (!err) {
            const newGuy = doc.plans[doc.plans.length-1]
            return res.json({code: 1, no: newGuy._id, doc})
          } else {
            console.log(err)
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      } else {
        Plans.create({doctorId, clientId, request: [], plans: [{
            name,
            allDay,
            start: false,
            pdfSent: false,
            paid: false,
            moveList,
            articles,
            extra,
            totalInEach,
            periodDays,
            feedback: [],
            numbers: 1
          }]}, (err, doc) => {
          if (!err) {
            const newGuy = doc.plans[doc.plans.length-1]
            return res.json({code: 1, no: newGuy._id, doc})
          } else {
            console.log(err)
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      }
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get("/check", (req, res) => {
  const planId = req.query.no
  const doctorId = req.query.did
  const clientId = req.query.cid

  Plans.findOne({
    doctorId,
    clientId,
    'plans._id': planId,
  }, {
    'plans.$' : 1,
  }, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
    } else {
      if (doc) {
        const plan = doc.plans[0]
        let numbers = plan.numbers
        const newGuyDate = plan.date.toISOString()
        let stringNumber = ""
        if (numbers < 10) {
          stringNumber = "0"+numbers
        } else {
          stringNumber = ""+numbers
        }
        let out_trade_no = newGuyDate.substring(2,4)+newGuyDate.substring(5,7)
          +newGuyDate.substring(8,10)+plan._id+stringNumber
        wxpay.queryOrder({ out_trade_no: out_trade_no }, function(err, order) {
          if (err) {
            return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
          }
          if (order.trade_state === 'SUCCESS') {
            Clients.findById(clientId, (err, clientDoc) => {
              if (!err) {
                Doctors.findById(doctorId, (err, doctorDoc) => {
                  if (!err) {
                    const order = plan.moveList.map(move => move.move)
                    Movements.find({ _id: { $in: order } }, (err, moveDoc) => {
                      if (!err) {
                        Movements.find({ _id: { $in: plan.articles } }, (err, articleDoc) => {
                          if (!err) {
                            moveDoc.sort((a, b) =>  {
                              return order.indexOf(a._id) - order.indexOf(b._id);
                            });
                            articleDoc.sort((a, b) =>  {
                              return plan.articles.indexOf(a._id) - order.indexOf(b._id);
                            });
                            const planDetail = moveDoc.map(move => {
                              return {"_id": move._id, name: move.name, url: move.url, info: move.info}
                            })
                            const articleDetail = articleDoc.map(move => {
                              return {moveId: move._id, moveName: move.name}
                            })

                            const info = {
                              planDetail,
                              planName: plan.name,
                              clientName: clientDoc.name,
                              allDay: plan.allDay,
                              infos: plan.moveList.map(move => move.info),
                              sets: plan.moveList.map(move => move.set),
                              timesPerSets: plan.moveList.map(move => move.timesPerSet),
                              times: plan.moveList.map(move => move.time),
                              weights: plan.moveList.map(move => move.weight),
                              betweens: plan.moveList.map(move => move.between),
                              parameter1s: plan.moveList.map(move => move.parameter1),
                              parameter2s: plan.moveList.map(move => move.parameter2),
                              value1s: plan.moveList.map(move => move.value1),
                              value2s: plan.moveList.map(move => move.value2),
                              articles: articleDetail,
                              extra: plan.extra,
                              doctorName: doctorDoc.name
                            }
                            console.log(info)
                            return res.json({code: 2, info})
                          }
                        })

                      } else {
                        return res.json({code: 0, msg: "后端出错了，请重新点击链接6"})
                      }
                    })
                  } else {
                    return res.json({code: 0, msg: "后端出错了，请重新点击链接7"})
                  }
                })
              } else {
                return res.json({code: 0, msg: "后端出错了，请重新点击链接8"})
              }
            })
          } else {
            if (numbers >= 99) {
              return res.json({code: 0, msg: "该计划已过期，请联系医生重新制定计划"})
            }
            Doctors.findById(doctorId, (err, doctorDoc) => {
              if (!err) {
                wxpay.closeOrder({ out_trade_no }, err => {
                  if (err) {
                    return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
                  }
                  let newNumber = numbers + 1
                  let newNumberString = ""
                  if (newNumber < 10) {
                    newNumberString = "0"+newNumber
                  } else {
                    newNumberString = ""+newNumber
                  }
                  let new_out_trade_no = newGuyDate.substring(2,4)+newGuyDate.substring(5,7)
                    +newGuyDate.substring(8,10)+plan._id+newNumberString
                  let total_fee = 28000
                  if (doctorId === "5b68bb0fde7068270a29d0fe") total_fee = 1
                  wxpay.createUnifiedOrder({
                    body: "VOIN 计划费用",
                    out_trade_no: new_out_trade_no,
                    total_fee: total_fee,
                    spbill_create_ip: '192.168.2.210',
                    notify_url: 'http://wxpay_notify_url',
                    trade_type: 'NATIVE',
                    product_id: '1234567890'
                  }, (err, result) => {
                    if (err) return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
                    Plans.findOneAndUpdate({
                      doctorId,
                      clientId,
                      'plans._id': planId,
                    }, {
                      $inc: {
                        'plans.$.numbers': 1
                      }
                    }, err => {
                      if (!err) {
                        QRCode.toDataURL(result.code_url, function (err, url) {
                          return res.json({code: 1, url, doctorName: doctorDoc.name, planName: plan.name})
                        })
                      } else {
                        return res.json({code: 0, msg: "后端出错了，请重新点击链接"})
                      }
                    })
                  })
                })

              } else {
                return res.json({code: 0, msg: "后端出错了，请重新点击链接7"})
              }
            })
          }
        })
      } else {
        return res.json({code: 0, msg: "此计划不存在"})
      }
    }
  })
})

app.post('/updatetemplate', (req, res) => {
  const {doctorId, templateId, name, summary, times, sets, timesPerSets, weights, parameter1s, parameter2s,
    value1s, value2s, infos, ids, articles, totalInEach, periodDays} = req.body
  const moveList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    moveList.push(oneMove)
  }
  Doctors.findOneAndUpdate({
    '_id': doctorId,
    'templates._id': templateId,
  }, {
    $set: {
      'templates.$.name': name,
      'templates.$.summary': summary,
      'templates.$.moveList': moveList,
      'templates.$.articles': articles,
      'templates.$.totalInEach': totalInEach,
      'templates.$.periodDays': periodDays
    }
  }, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/paid', (req, res) => {
  const {planId} = req.query
  wxpay.queryOrder({ out_trade_no: planId }, function(err, order){
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
    if (order.trade_state === 'SUCCESS') {
      return res.json({code: 1, paid: true})
    } else {
      return res.json({code: 1, paid: false})
    }
  });

});

app.get('/changename', (req, res) => {
  const files = fs.readdirSync('./server/con');
  let count = 0
  let success = 0
  files.forEach(file => {
    Movements.findOne({id: file.substring(0,4)}, (err, doc)=> {
      count++
      if (!err) {
        fs.renameSync("./server/con/"+file, "./server/con/"+doc._id+'.png', (err2) => {
          if (!err2) {
            success++
            console.log(count, success, doc._id)
          }
        })
      }
    })
  });
})

app.get('/move', (req, res) => {
  Doctors.findOne({username: "halfrain"}, (err, doc) => {
    if (doc) {
      const template = doc.templates[14]
      const name = template.name
      const summary = template.summary
      const articles = template.articles
      const moveList = template.moveList
      const periodDays = template.periodDays
      const totalInEach = template.totalInEach
      Templates.create({
        name, summary, articles, moveList, periodDays, totalInEach, price: 0
      }, (err, doc) => {
        if (!err) return res.json({code: name + "成功"})
      })
    }
  })
})


app.get('/changepass', (req, res) => {
  Doctors.findOneAndUpdate({username: "pistachio66"}, {$set: {password: md5Password("11111111")}},
    (err, doc) => {
      if (!err) return res.json({code: "1234"})
    })
})


app.get('/changename', (req, res) => {

  let count = 0
  let success = 0
  let failed = []

  fs.createReadStream('./server/moves1.csv')
    .pipe(csv())
    .on('data', (row) => {
      const id = row['﻿name'];
      const keys = row.keys
      const keyArray = keys.split(";").filter(key => key.length > 0)
      Movements.findOneAndUpdate({id: id}, {$set: {keywords: keyArray}}, (err, doc) =>{
        count++
        if (!err) {
          success++
          console.log(success, count)
        } else {
          console.log(id)
          failed.push(id)
        }
      })
      // console.log(keyArray)
    })
    .on('end', () => {

    });
})

app.get('/changeurl', (req, res) => {

  let count = 0
  let success = 0
  let failed = []

  fs.createReadStream('./server/moves2.csv')
    .pipe(csv())
    .on('data', (row) => {
      const id = row['﻿name'].substring(0, 4);
      const mp4 = row.mp4
      Movements.findOneAndUpdate({id: id}, {$set: {mp4: mp4}}, (err, doc) =>{
        count++
        if (!err) {
          success++
          console.log(success, count)
        } else {
          console.log(id)
          failed.push(id)
        }
      })
      // console.log(id, mp4)
    })
    .on('end', () => {

    });
})


app.post('/newsaveplan', (req, res) => {
  const {doctorId, clientId, name, allDay, times, sets, timesPerSets, weights, betweens, parameter1s, parameter2s,
    value1s, value2s, infos, ids, bars, barNames, starts, extra, articles, totalInEach, periodDays} = req.body
  const moveList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.between = betweens[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.bar = bars[i]
    oneMove.start = starts[i]
    oneMove.barName = barNames[i]
    moveList.push(oneMove)
  }
  Plans.findOne({doctorId, clientId}, (err, doc) => {
    if (!err) {
      if (doc) {
        Plans.findOneAndUpdate({
          doctorId,
          clientId
        }, {
          $push: {
            plans: {
              name,
              allDay,
              start: false,
              pdfSent: false,
              paid: false,
              empty: false,
              moveList,
              articles,
              extra,
              totalInEach,
              periodDays,
              feedback: [],
            }
          }
        }, {new: true}, (err, doc) => {
          if (!err) {
            const newGuy = doc.plans[doc.plans.length-1]
            Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
              Clients.findOne({_id: clientId}, (errCli, docCli) => {
                if (docDoc && docCli) {
                  const params = {
                    "RegionId": "cn-hangzhou",
                    "PhoneNumbers": ""+docCli.phone,
                    "SignName": "沃衍健康通知",
                    "TemplateCode": "SMS_189615644",
                    "TemplateParam": "{\"name\":\""+docDoc.name+"\"}"
                  }

                  sms.request('SendSms', params, {method: 'POST'}).then((result) => {
                    return res.json({code: 1, no: newGuy._id, doc})
                  }, (ex) => {
                    console.log("短信发送请求出错了")
                    return res.json({code: 1, no: newGuy._id, doc})
                  })
                } else {
                  return res.json({code: 1, no: newGuy._id, doc})
                }
              })
            })
          } else {
            console.log(err)
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      } else {
        Plans.create({doctorId, clientId, request: [], plans: [{
            name,
            allDay,
            start: false,
            pdfSent: false,
            paid: false,
            empty: false,
            moveList,
            articles,
            extra,
            totalInEach,
            periodDays,
            feedback: [],
            numbers: 1
          }]}, (err, doc) => {
          if (!err) {
            const newGuy = doc.plans[doc.plans.length-1]
            Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
              Clients.findOne({_id: clientId}, (errCli, docCli) => {
                if (docDoc && docCli) {
                  const params = {
                    "RegionId": "cn-hangzhou",
                    "PhoneNumbers": ""+docCli.phone,
                    "SignName": "沃衍健康通知",
                    "TemplateCode": "SMS_189615644",
                    "TemplateParam": "{\"name\":\""+docDoc.name+"\"}"
                  }

                  sms.request('SendSms', params, {method: 'POST'}).then((result) => {
                    return res.json({code: 1, no: newGuy._id, doc})
                  }, (ex) => {
                    console.log("短信发送请求出错了")
                    return res.json({code: 1, no: newGuy._id, doc})
                  })
                } else {
                  return res.json({code: 1, no: newGuy._id, doc})
                }
              })
            })
          } else {
            console.log(err)
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      }
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})


app.get("/addcases", (req, res) => {
  const names = []

  names.forEach((name, index) => {
    Cases.create({name: name}, (err, doc) => {
      if (err) console.log(name)
      else console.log(index)
    })
  })
})


// app.get('/changename', (req, res) => {
//
//   Categories.create({id: "a12m", name: "伸", contains: [
//       "1270","1271","1272","1273","1274","1275","1276","1277","1278","1279","1280","1281","1282","1287","1288","1301","1302","1370","1371","1373","2193","2219","2220","2221","2223"]})
// })
//
//
//
// <SearchInput placeholder="搜索团队模板" onChange={event => {
//   this.props.onSearchTeamTemplateChange(event.target.value)
// }} value={this.props.teamTemplateSearchValue}/>
//
// {this.props.teamMode === 1 ?
//   <Spinner /> :
//   <TemplateTable
//     focus={!this.props.focusOnMy}
//     username={this.props.username}
//     expertOrTeam="team"
//     refer={this.props.refer}
//     referee={this.props.referee}
//     doctorId={this.props.doctorId}
//     onDeleteTemplate={this.props.onDeleteTemplate}
//     onShowTemplateDetail={this.props.onShowTemplateDetail}
//     onDownloadTemplate={this.props.onDownloadTemplate}
//     templates={[...this.props.teamTemplates]}
//     page={this}
//     error1={this.props.teamError}
//   />}