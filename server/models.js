const mongoose = require('mongoose')
const { Schema } = mongoose

const doctorSchema = new Schema({
  username: {type: String, required: true},
  password: {type: String, required: true},
  name: {type: String},
  account: {type: String},
  alipayAccount: {type: String},
  bankName: {type: String},
  bankAccount: {type: String},
  phone: {type: String, required: true},
  email: {type: String},
  room: {type: String},
  refer: {type: String},
  certification: {type: String},
  occupation: {type: String},
  idnum: {type: String},
  hospital: {type: String},
  district: {type: String},
  wechat: {type: String},
  admin: {type: Number},
  code: {type: String},
  loginTimes: {type: Number},
  token: {type: String},
  childDoctors: [{
    name: {type: String, required: true},
    original: {type: Boolean, required: true}
  }],
  free30: {type: Boolean},
  qrcode: {type: Boolean},
  circle: {type: Boolean},
  templates: [{
    name: {type: String, required: true},
    summary: String,
    date: {type: Date, default: Date.now},
    category: [{type: String}],
    moveList: [{
      move: {type: String},
      info: {type: String},
      bar: {type: Boolean},
      start: {type: Boolean},
      barName: Object,
      index1: {type: Number},
      time: Object,
      timesPerSet: Object,
      weight: Object,
      set: Object,
      between: Object,
      parameter1: Object,
      parameter2: Object,
      value1: Object,
      value2: Object,
    }],
    therapyList: [{
      move: {type: String, required: true},
      therapyInfo: {type: String},
      minute: {type: String},
      timesPerDay: {type: String},
      time2: {type: String},
      period: {type: String},
      chosenParams: [{
        name: {type: String},
        value: {type: String},
      }]
    }],
    addedParamCounts: [{type: Number}],
    periodDays: [{type: String}],
    totalInEach: [{type: Number}],
    articles: [{type: String}],
    proposalContent: {type: String},
    references: [{type: String}]
  }],
  date: {type: Date, default: Date.now}
});

const clientSchema = new Schema({
  email: {type: String},
  sex: {type: String},
  age: {type: String},
  idnum: {type: String},
  history: {type: String},
  smoke: {type: String},
  description: {type: String},
  name: {type: String},
  phone: {type: String},
  date: {type: Date, default: Date.now},
  address: {type: String},
  openid: {type: String},
  unionid: {type: String},
  coupon: {type: Number},
  username: {type: String},
  wechat: {type: String},
  avatar: {type: String},
  vip: {type: Boolean},
  vipTime: {type: Number},
  app: {type: Number},
  specialPaid: {type: Boolean}
})

const movementSchema = new Schema({
  name: {type: String, required: true},
  url: {type: String},
  id: {type: String, required: true},
  keywords: [{type: String}],
  picture: {type: Boolean},
  video: {type: Boolean},
  therapy: {type: Boolean},
  mp4: {type: String},
  info: {type: String},
  screenshot: {type: String},
  params: [{
    name: { type: String},
    choices: [{type: String}],
  }]
})

const categorySchema = new Schema({
  id: {type: String, required: true},
  name: {type: String, required: true},
  contains: [{type: String}]
})

const planSchema = new Schema({
  doctorId: {type: String, required: true},
  clientId: {type: String, required: true},
  childDoctorId: {type: String},
  category: [{type: String}],
  read: {type: Boolean, default: false},
  request: [{
    date: { type: Date, required: true, default: Date.now },
    content: {type: String, required: true},
  }],
  description: {type: String},
  refer: {type: String},
  total: {type: Number},
  recognized: {type: Boolean, required: true, default: false},
  date: { type: Date, default: Date.now },
  plans: [{
    name: {type: String, required: true},
    empty: {type: Boolean},
    requestRefund: {type: Boolean},
    date: {type: Date, default: Date.now},
    allDay: {type: Number},
    period: {type: Number, required: true},
    extra: {type: String},
    start: {type: Boolean, required: true, default: false},
    startDate: {type: String},
    paid: {type: Boolean, required: true, default: false},
    completion: {type: Number},
    effect: {type: Number},
    description: {type: String},
    feedback: [{type: String}],
    savedFeedback: [{type: String}],
    paidTime: {type: Date},
    free30: {type: Boolean, default: false},
    hurt: [{type: String}],
    templateId: {type: String},
    moveList: [{
      move: {type: String, required: true},
      info: {type: String, required: true},
      time: Object,
      timesPerSet: Object,
      weight: Object,
      between: Object,
      parameter1: Object,
      parameter2: Object,
      value1: Object,
      value2: Object,
      bar: Object,
      start: Object,
      barName: Object,
      set: Object
    }],
    therapyList: [{
      move: {type: String, required: true},
      therapyInfo: {type: String},
      minute: {type: String},
      timesPerDay: {type: String},
      time2: {type: String},
      period: {type: String},
      chosenParams: [{
        name: {type: String},
        value: {type: String},
      }]
    }],
    addedParamCounts: [{type: Number}],
    percent: [],
    periodDays: [{type: String}],
    totalInEach: [{type: Number}],
    articles: [{type: String}],
  }]
})

const templateSchema = new Schema({
  name: {type: String, required: true},
  summary: String,
  date: {type: Date, default: Date.now},
  moveList: [{
    move: {type: String},
    info: {type: String},
    bar: {type: Boolean},
    start: {type: Boolean},
    barName: Object,
    index1: {type: Number},
    time: Object,
    timesPerSet: Object,
    weight: Object,
    set: Object,
    between: Object,
    parameter1: Object,
    parameter2: Object,
    value1: Object,
    value2: Object,
  }],
  therapyList: [{
    move: {type: String, required: true},
    therapyInfo: {type: String},
    minute: {type: String},
    timesPerDay: {type: String},
    time2: {type: String},
    period: {type: String},
    chosenParams: [{
      name: {type: String},
      value: {type: String},
    }]
  }],
  addedParamCounts: [{type: Number}],
  upload: {type: String},
  periodDays: [{type: String}],
  totalInEach: [{type: Number}],
  articles: [{type: String}],
  label: {type: String},
  type: {type: Number, required: true},
  price: {type: Number, required: true},
})

const recordSchema = new Schema({
  doctorId: {type: String, required: true},
  clientId: {type: String, required: true},
  description: {type: String},
  records: [{
    name: {type: String},
    hidden: {type: Boolean, default: false},
    date: {type: Date, default: Date.now}
  }]
})

const paymentSchema = new Schema({
  doctorId: {type: String, required: true},
  clientId: {type: String, required: true},
  planId: {type: String, required: true},
  price: {type: Number, required: true},
  date: { type: Date, default: Date.now },
})

const teamSchema = new Schema({
  name: {type: String, required: true},
  username: {type: String, required: true},
  password: {type: String, required: true},
  templates: [{
    name: {type: String, required: true},
    summary: String,
    date: {type: Date, default: Date.now},
    moveList: [{
      move: {type: String},
      info: {type: String},
      bar: {type: Boolean},
      start: {type: Boolean},
      barName: Object,
      index1: {type: Number},
      time: Object,
      timesPerSet: Object,
      weight: Object,
      set: Object,
      between: Object,
      parameter1: Object,
      parameter2: Object,
      value1: Object,
      value2: Object,
    }],
    therapyList: [{
      move: {type: String, required: true},
      therapyInfo: {type: String},
      minute: {type: String},
      timesPerDay: {type: String},
      time2: {type: String},
      period: {type: String},
      chosenParams: [{
        name: {type: String},
        value: {type: String},
      }]
    }],
    addedParamCounts: [{type: Number}],
    periodDays: [{type: String}],
    totalInEach: [{type: Number}],
    articles: [{type: String}]
  }],
  date: { type: Date, default: Date.now },
})

const doctorInTeamSchema = new Schema({
  doctorId: {type: String, required: true},
  teamId: {type: String, required: true},
  creator: {type: Boolean, required: true},
  date: { type: Date, default: Date.now },
})

const caseSchema = new Schema({
  name: {type: String, required: true}
})

const resourceSchema = new Schema({
  name: {type: String, required: true},
  cat: {type: String, required: true},
  label: {type: String, required: true},
})

const classSchema = new Schema({
  name: {type: String, required: true}
})

const doctorInClassSchema = new Schema({
  doctorId: {type: String, required: true},
  classId: {type: String, required: true},
  isTeacher: {type: Boolean, required: true},
})

const homeworkTitleSchema = new Schema({
  title: {type: String},
  name: {type: String},
  sex: {type: String},
  age: {type: String},
  history: {type: String},
  smoke: {type: String},
  classId: {type: String, required: true},
  description: {type: String},
  images: {type: Number},
  pictures: [{
    name: {type: String},
    date: {type: Date, default: Date.now}
  }],
  date: {type: Date, default: Date.now},
})

const homeworkSubmitSchema = new Schema({
  titleId: {type: String, required: true},
  doctorId: {type: String, required: true},
  plan: {
    name: {type: String, required: true},
    extra: {type: String},
    moveList: [{
      move: {type: String, required: true},
      info: {type: String, required: true},
      time: Object,
      timesPerSet: Object,
      weight: Object,
      between: Object,
      parameter1: Object,
      parameter2: Object,
      value1: Object,
      value2: Object,
      bar: Object,
      start: Object,
      barName: Object,
      set: Object
    }],
    therapyList: [{
      move: {type: String, required: true},
      therapyInfo: {type: String},
      minute: {type: String},
      timesPerDay: {type: String},
      time2: {type: String},
      period: {type: String},
      chosenParams: [{
        name: {type: String},
        value: {type: String},
      }]
    }],
    addedParamCounts: [{type: Number}],
    periodDays: [{type: String}],
    totalInEach: [{type: Number}],
    articles: [{type: String}],
    proposalContent: {type: String},
    references: [{type: String}]
  },
  proposal: {type: String},
  reference: [{type: String}],
  date: {type: Date, default: Date.now},
  score: {type: String},
  review: {type: String}
})

const clientCategoriesSchema = new Schema({
  doctorId: {type: String, required: true},
  name: {type: String, required: true},
  type: {type: String, required: true},
  date: {type: Date, default: Date.now},
  special: {type: Boolean}
})

const diseaseCodesSchema = new Schema({
  code: {type: String, required: true},
  name: {type: String, required: true}
})

const operationCodesSchema = new Schema({
  code: {type: String, required: true},
  name: {type: String, required: true}
})

const bindingSchema = new Schema({
  clientId1: {type: String, required: true},
  clientId2: {type: String, required: true},
  level: {type: Number, required: true},
  date: {type: Date, default: Date.now},
})

const circleSchema = new Schema({
  name: {type: String, required: true},
  xiaoeId: {type: String, required: true},
  templates: [{
    doctorId: {type: String},
    templateId: {type: String}
  }]
})

mongoose.model('doctors', doctorSchema);
mongoose.model('clients', clientSchema);
mongoose.model('movements', movementSchema)
mongoose.model('categories', categorySchema)
mongoose.model('templates', templateSchema)
mongoose.model('plans', planSchema);
mongoose.model('records', recordSchema);
mongoose.model('payments', paymentSchema);
mongoose.model('teams', teamSchema);
mongoose.model('doctorInTeams', doctorInTeamSchema);
mongoose.model('cases', caseSchema);
mongoose.model('resources', resourceSchema);
mongoose.model('classes', classSchema);
mongoose.model('doctorInClasses', doctorInClassSchema);
mongoose.model('homeworkTitles', homeworkTitleSchema);
mongoose.model('homeworkSubmits', homeworkSubmitSchema);
mongoose.model('clientCategories', clientCategoriesSchema);
mongoose.model('diseasecodes', diseaseCodesSchema);
mongoose.model('operationcodes', operationCodesSchema);
mongoose.model('bindings', bindingSchema);
mongoose.model('circles', circleSchema);

module.exports = {
  getModel: name => {
    return mongoose.model(name)
  }
}