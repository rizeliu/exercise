const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const utils = require('utility')
const nodemailer = require('nodemailer')
const cors = require('cors')
const https = require('https');
// const csv = require('csv-parser');
const fs = require('fs');
const formidable = require("formidable");
const path = require("path")
const axios = require('axios')
const randomstring = require("randomstring");
const parseString = require('xml2js').parseString;
const Core = require('@alicloud/pop-core');
const xmlparser = require('express-xml-bodyparser');
const {Base64} = require('js-base64');
const aesjs = require('aes-js');
const xmlParse = require("xml-parse");



const model = require('./models')

const Doctors = model.getModel("doctors")
const Clients = model.getModel("clients")
const Plans = model.getModel("plans")
const Movements = model.getModel("movements")
const Templates = model.getModel("templates")
const Categories = model.getModel("categories")
const Records = model.getModel("records")
const Payments = model.getModel("payments")
const Teams = model.getModel("teams")
const DoctorInTeams = model.getModel("doctorInTeams")
const Cases = model.getModel("cases")
const Resources = model.getModel("resources")
const Classes = model.getModel("classes")
const DoctorInClasses = model.getModel("doctorInClasses")
const HomeworkTitles = model.getModel("homeworkTitles")
const HomeworkSubmits = model.getModel("homeworkSubmits")
const ClientCategories = model.getModel("clientCategories")
const DiseaseCodes = model.getModel("diseasecodes")
const OperationCodes = model.getModel("operationcodes")
const Bindings = model.getModel("bindings")
const Circles = model.getModel("circles")
const _filter = {'password': 0, '__v': 0, 'templates': 0}
const _filter_team = {'password': 0, '__v': 0}


mongoose.connect('mongodb://root:1qaz2wsx@dds-2ze22d840c7202a41638-pub.mongodb.rds.aliyuncs.com:3717,dds-2ze22d840c7202a42360-pub.mongodb.rds.aliyuncs.com:3717/admin?replicaSet=mgset-16599369',
  {useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true})


// const md5Password = password => {
//   const key = 'saf9qq@#S%'
//   return utils.md5(utils.md5(key + password))
// }

const md5Password = password => {
  const key = 'i love Sty saf9qq@#S%'
  return utils.md5(utils.md5(key + password))
}

const md5Cookie = cookie => {
  const key = 'ssg^$4t$#tfgd i love Sty '
  return utils.md5(utils.md5(cookie + key))
}

// const md5Cookie = cookie => {
//   const key = 'sg^$4t$#tfgd'
//   return utils.md5(utils.md5(cookie + key))
// }

const accessKeyId = 'LTAI4FgD2pD123MBQm2tdYcn'
const accessKeySecret = 'xgdHwrNqfuqxnFGxsdsvhtZ9oqIImQ'
const appid = "wxcb27785988e2a87c"
const appid2 = "wx6309c1491183af84"
const secret = "f87b25016b12fd02bc2ac06b9ffb615a"
const secret2 = "a31fc190661de401c55806baf00b467c"

const sms = new Core({
  accessKeyId,
  accessKeySecret,
  endpoint: 'https://dysmsapi.aliyuncs.com',
  apiVersion: '2017-05-25'
});

const app = express()

app.use(express.static(path.join(__dirname, 'uploads')))
app.use('/resources', express.static(path.join(__dirname, 'articleimg')))
app.use('/steps', express.static(path.join(__dirname, 'steps')))

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By",' 3.2.1')
  res.header("Content-Type", "application/json;charset=utf-8");
  next();
});

app.use(cors())
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(xmlparser());
app.use(bodyParser.json())




app.post('/xiaoepay', (req, res) =>{
  const xml = req.body.xml
  const encrypt = "" + xml.encrypt

  const encodingKey = "appz4t5ma7u3727appz4t5ma7u3727appz4t5ma7u37="
  const originalKey = Base64.toUint8Array(encodingKey)
  const originalIv = originalKey.slice(0, 16)

  const decodedData = Base64.toUint8Array(encrypt);

  const aesCbc = new aesjs.ModeOfOperation.cbc(originalKey, originalIv);
  const decryptedBytes = aesCbc.decrypt(decodedData);

  const decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
  const idStart = decryptedText.indexOf("<user_id>")+9;
  const idEnd = decryptedText.indexOf("</user_id>")
  const orderStart = decryptedText.indexOf("<order_state>");
  const orderEnd = decryptedText.indexOf("</order_state>")
  const priceStart = decryptedText.indexOf("<price>");
  const priceEnd = decryptedText.indexOf("</price>")

  const userId = decryptedText.substring(idStart, idEnd)

  console.log(decryptedText)
  console.log(Number(decryptedText.substring(orderStart+13, orderEnd)))
  const orderState = Number(decryptedText.substring(orderStart+13, orderEnd))
  const price = Number(decryptedText.substring(priceStart+7, priceEnd))

  if (orderStart >= 0 && orderState === 1 && price > 1000) {
    axios.get('https://api.xiaoe-tech.com/token', {
      params: {
        "app_id": "appz4t5ma7u3727",
        "client_id": "xoppNXnJj3m3643",
        "secret_key": "LwcA3Qlrzcs5N2p7t4hetA0dTb8Ucl1h",
        "grant_type": "client_credential"
      }
    })
      .then(function (response) {

        const access_token = response.data.data.access_token
        axios.post('https://api.xiaoe-tech.com/xe.user.info.get/1.0.0', {
          access_token: access_token,
          user_id : userId,
          data : {
            "field_list" : [
              "wx_union_id","wx_open_id"
            ]
          }
        })
          .then(function (response) {
            const unionid = response.data.data.wx_union_id
            console.log(response.data)
            console.log(unionid)
            Clients.findOneAndUpdate({unionid: unionid, app: 2}, {$set: {vip: true, vipTime: Date.now()}},(err, doc) => {
              if (err) {
                console.log("1 error" + err)
                return res.send("success")
              } else if (doc) {
                Clients.findOne({unionid: unionid, app: 1}, (err1, doc1) => {
                  if (err1) {
                    console.log("1 error" + err1)
                    return res.send("success")
                  } else if (doc1) {
                    Plans.find({clientId: "" + doc1._id}, (err2, doc2) => {
                      if (err2) return res.send("success")
                      else {
                        if (doc2 && doc2.length > 0) {
                          let unpaidPlans = []
                          for (let i = 0; i < doc2.length; i++) {
                            unpaidPlans = unpaidPlans.concat(doc2[i].plans.filter(plan => !plan.paid).map(plan => {
                              return {
                                id: "" + plan._id,
                                date: plan.date,
                                name: plan.name,
                                doctorId: doc2[i].doctorId
                              }
                            }))
                          }
                          unpaidPlans.sort((a, b) => b.date - a.date);
                          console.log(unpaidPlans)
                          if (unpaidPlans.length > 0) {
                            const freePlanId = unpaidPlans[0].id
                            console.log("c: " + doc._id + " d: " + unpaidPlans[0].doctorId + " p: " + freePlanId)
                            Plans.findOneAndUpdate(
                              {clientId: "" + doc._id, doctorId: unpaidPlans[0].doctorId, "plans._id": freePlanId},
                              {$set: {'plans.$.paid': true}},
                              (err3, doc3) => {
                                if (err3) {
                                  console.log(err3)
                                  return res.send("success")
                                } else {
                                  console.log("成功了")
                                  console.log(doc3)
                                  return res.send("success")
                                }
                              }
                            )
                          } else {
                            Clients.findOneAndUpdate({unionid: unionid, app: 1}, {$inc: {coupon: 1}}, {new: true}, (err1, doc1) => {
                              if (err1) {
                                console.log(err1)
                                return res.send("success")
                              } else {
                                return res.send("success")
                              }
                            })
                          }
                        } else {
                          Clients.findOneAndUpdate({unionid: unionid, app: 1}, {$inc: {coupon: 1}}, {new: true}, (err1, doc1) => {
                            if (err1) {
                              console.log(err1)
                              return res.send("success")
                            } else {
                              return res.send("success")
                            }
                          })
                        }
                      }
                    })
                  } else return res.send("success")
                })
              } else {
                console.log("3 done create new")
                Clients.create({openid: "", unionid,
                  name: "", sex: "", age: "", idnum: "", history: "", smoke: "", description: "",
                  phone: "", email: "",
                  wechat: "", username: "", avatar: "", address: "", vip: true, vipTime: Date.now(), app: 2
                }, (err2, doc2) => {
                  if (err2) {
                    return res.send("success")
                  } else {
                    console.log("4 done create new done " + doc2.toString())
                    Clients.findOne({unionid: unionid, app: 1}, (err1, doc1) => {
                      if (err1) {
                        console.log("1 error" + err1)
                        return res.send("success")
                      } else if (doc1) {
                        Plans.find({clientId: "" + doc1._id}, (err2, doc2) => {
                          if (err2) return res.send("success")
                          else {
                            if (doc2 && doc2.length > 0) {
                              let unpaidPlans = []
                              for (let i = 0; i < doc2.length; i++) {
                                unpaidPlans = unpaidPlans.concat(doc2[i].plans.filter(plan => !plan.paid).map(plan => {
                                  return {
                                    id: "" + plan._id,
                                    date: plan.date,
                                    name: plan.name,
                                    doctorId: doc2[i].doctorId
                                  }
                                }))
                              }
                              unpaidPlans.sort((a, b) => b.date - a.date);
                              console.log(unpaidPlans)
                              if (unpaidPlans.length > 0) {
                                const freePlanId = unpaidPlans[0].id
                                console.log("c: " + doc._id + " d: " + unpaidPlans[0].doctorId + " p: " + freePlanId)
                                Plans.findOneAndUpdate(
                                  {clientId: "" + doc._id, doctorId: unpaidPlans[0].doctorId, "plans._id": freePlanId},
                                  {$set: {'plans.$.paid': true}},
                                  (err3, doc3) => {
                                    if (err3) {
                                      console.log(err3)
                                      return res.send("success")
                                    } else {
                                      console.log("成功了")
                                      console.log(doc3)
                                      return res.send("success")
                                    }
                                  }
                                )
                              } else {
                                Clients.findOneAndUpdate({unionid: unionid, app: 1}, {$inc: {coupon: 1}}, {new: true}, (err1, doc1) => {
                                  if (err1) {
                                    console.log(err1)
                                    return res.send("success")
                                  } else {
                                    return res.send("success")
                                  }
                                })
                              }
                            } else {
                              Clients.findOneAndUpdate({unionid: unionid, app: 1}, {$inc: {coupon: 1}}, {new: true}, (err1, doc1) => {
                                if (err1) {
                                  console.log(err1)
                                  return res.send("success")
                                } else {
                                  return res.send("success")
                                }
                              })
                            }
                          }
                        })
                      } else return res.send("success")
                    })
                  }
                })
              }
            })
          })
          .catch(function (error) {
            console.log(error);
            return res.send("success")
          });
      })
      .catch(function (error) {
        console.log(error);
        return res.send("success")
      });
  }
})

app.get('/a', (req, res) =>{

  const hashDigest = sha256(nonce + message);
  const hmacDigest = newBase64.stringify(hashDigest);


})

app.get('/changename', (req, res) => {


  // info.forEach(ea => {
  //   Movements.findOne({id: ea.id}, (err, doc) => {
  //     if (err) console.log("error: " + err + "--------------")
  //     else {
  //       if (doc == null) {
  //         console.log("未找到: " + ea.id)
  //       } else {
  //         // const newContains = ea.contains.map(cat => ""+cat)
  //         Movements.findOneAndUpdate({id: ea.id}, {info: ea.infor}, (err2, doc2) => {
  //           if (err2) console.log("error2: " + err2 + "--------------")
  //           else {
  //             console.log("成功修改" + ea.id)
  //           }
  //         })
  //       }
  //     }
  //   })
  //
  // })


})

app.get('/admin', (req, res) => {
  const { item, admin, username, doctorId, page, value } = req.query
  switch (item) {
    case "doctors":
      if (admin === "1") {
        Doctors.find({$and: [{admin: {$ne: 4 }}, {admin: {$ne: 5}}, {$or: [{username: {$regex: value}}, {name: {$regex: value}}]}]},
          {'password':0, '__v':0}, {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
          if (!err) {
            Doctors.countDocuments({$and: [{admin: {$ne: 4 }},
                {admin: {$ne: 5}},
                {$or: [{username: {$regex: value}}, {name: {$regex: value}}]}]}, (err, count) => {
              if (!err) {
                const result = response.map((doctor => {
                  return {
                    _id: doctor._id,
                    username: doctor.username,
                    idnum: doctor.idnum,
                    name: doctor.name,
                    account: doctor.account,
                    bankName: doctor.bankName,
                    bankAccount: doctor.bankAccount,
                    alipayAccount: doctor.alipayAccount,
                    phone: doctor.phone,
                    email: doctor.email,
                    wechat: doctor.wechat,
                    hospital: doctor.hospital,
                    room: doctor.room,
                    district: doctor.district,
                    refer: doctor.refer,
                    certification: doctor.certification,
                    templates: doctor.templates.length,
                    occupation: doctor.occupation,
                    date: doctor.date,
                    admin: doctor.admin,
                    free30: doctor.free30,
                    qrcode: doctor.qrcode,
                    circle: doctor.circle,
                    total: 0,
                    paid: 0
                  }
                }))
                const ids = response.map(doctor => ""+doctor._id)
                Plans.find({doctorId: {$in: ids}}, (err, doc) => {
                  if (!err) {
                    for (let i = 0; i < doc.length; i++) {
                      const index = ids.indexOf(doc[i].doctorId)
                      const planNum = doc[i].plans.length
                      const paidPlanNum = doc[i].plans.filter(plan => plan.paid).length
                      result[index].total = result[index].total + planNum
                      result[index].paid = result[index].paid + paidPlanNum
                    }
                    return res.json({response: result, count})
                  } else res.json({code: 0, msg: "后端出错了，请刷新"})
                })
              } else res.json({code: 0, msg: "后端出错了，请刷新"})
            })
          } else res.json({code: 0, msg: "后端出错了，请刷新"})
        })
        break
      } else if (admin === "2"){
        DoctorInTeams.find({doctorId}, (err1, doc1) => {
          if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
          const teams = doc1.map(re => re.teamId)
          DoctorInTeams.find({teamId: {$in: teams}}, (err2, doc2) => {
            if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
            const doctorIds = doc2.map(re => re.doctorId)
            Doctors.find({$and: [{$or: [{refer: username}, {_id: {$in: doctorIds}}]}, {$or: [{username: {$regex: value}}, {name: {$regex: value}}]}]}, {'password':0, '__v':0},
              {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
              if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
              Doctors.countDocuments({$and: [{$or: [{refer: username}, {_id: {$in: doctorIds}}]}, {$or: [{username: {$regex: value}}, {name: {$regex: value}}]}]}, (err, count) => {
                if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                const result = response.map(doctor => {
                  return {
                    _id: doctor._id,
                    username: doctor.username,
                    idnum: doctor.idnum,
                    name: doctor.name,
                    account: doctor.account,
                    bankName: doctor.bankName,
                    bankAccount: doctor.bankAccount,
                    alipayAccount: doctor.alipayAccount,
                    phone: doctor.phone,
                    email: doctor.email,
                    wechat: doctor.wechat,
                    hospital: doctor.hospital,
                    room: doctor.room,
                    district: doctor.district,
                    refer: doctor.refer,
                    certification: doctor.certification,
                    templates: doctor.templates.length,
                    occupation: doctor.occupation,
                    date: doctor.date,
                    total: 0,
                    paid: 0
                  }
                })
                const ids = response.map(doctor => ""+doctor._id)
                Plans.find({doctorId: {$in: ids}}, (err, doc) => {
                  if (err) return res.json({code: 0, msg: "后端出错了，请刷新5"})
                  for (let i = 0; i < doc.length; i++) {
                    const index = ids.indexOf(doc[i].doctorId)
                    const planNum = doc[i].plans.length
                    const paidPlanNum = doc[i].plans.filter(plan => plan.paid).length
                    result[index].total = result[index].total + planNum
                    result[index].paid = result[index].paid + paidPlanNum
                  }
                  return res.json({response: result, count})
                })
              })

            })
          })
        })
      }
      break
    case "clients":
      Clients.find({$and: [{"name" : {"$exists" : true, "$ne" : ""}}, {name: {$regex: value}}]}, {'password':0, '__v':0},
        {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
        if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
        Clients.countDocuments({$and: [{"name" : {"$exists" : true, "$ne" : ""}}, {name: {$regex: value}}]}, (err, count) => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          const result = response.map((client => {
            return {
              _id: client._id,
              openid: client.openid,
              name: client.name,
              idnum: client.idnum,
              phone: client.phone,
              sex: client.sex,
              age: client.age,
              history: client.history,
              smoke: client.smoke,
              description: client.description,
              date: client.date,
              total: 0,
              paid: 0,
              doctors: []
            }
          }))
          const ids = result.map(client => ""+client._id)
          Plans.find({clientId: {$in: ids}}, (err, doc) => {
            if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
            const doctorIds = doc.map(relation => relation.doctorId)
            Doctors.find({_id: {$in: doctorIds}}, {'password':0, '__v':0}, {sort: {date: -1}}, (err, doctors) => {
              if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
              const doctorNames = doctors.map((doctor => doctor.name))
              const newDoctorIds = doctors.map(doctor => ""+doctor._id)
              for (let i = 0; i < doc.length; i++) {
                const index = ids.indexOf(doc[i].clientId)
                const planNum = doc[i].plans.length
                const paidPlanNum = doc[i].plans.filter(plan => plan.paid).length
                const doctorIndex = newDoctorIds.indexOf(doc[i].doctorId)
                result[index].total = result[index].total + planNum
                result[index].paid = result[index].paid + paidPlanNum
                if (doctorIndex >= 0) {
                  result[index].doctors.push(doctorNames[doctorIndex])
                }
              }
              return res.json({response: result, count})
            })
          })
        })

      })

      break
    case "plans":
      if (admin === "1") {
        Plans.find({}, {'password':0, '__v':0}, {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新1"})
          Plans.countDocuments({}, (err, count) => {
            if (err) return res.json({code: 0, msg: "后端出错了，请刷新2"})
            const result = []
            const doctorIds = response.map(plan => plan.doctorId)
            const didsNoDu = doctorIds.filter((doctorId, index) => doctorIds.indexOf(doctorId) === index)
            const clientIds = response.map(plan => plan.clientId)
            const cidsNoDu = clientIds.filter((clientId, index) => clientIds.indexOf(clientId) === index)
            Doctors.find({'_id': {$in: didsNoDu}}, (err, doctorDoc) => {
              if (err) return res.json({code: 0, msg: "后端出错了，请刷新3"})
              Clients.find({'_id': {$in: cidsNoDu}}, (err, clientDoc) => {
                if (err) return res.json({code: 0, msg: "后端出错了，请刷新4", err: err, ids: cidsNoDu})
                const doctorIdsRe = doctorDoc.map(doctor => ""+doctor._id)
                const clientIdsRe = clientDoc.map(client => ""+client._id)
                response.forEach(plan => {
                  const docIn = doctorIdsRe.indexOf(plan.doctorId)
                  const cliIn = clientIdsRe.indexOf(plan.clientId)
                  plan.plans.forEach(realPlan => {
                    result.push({
                      _id: realPlan._id,
                      doctorName: docIn >=0 ? doctorDoc[docIn].name : "医生已不存在",
                      clientName: cliIn >= 0 ? clientDoc[cliIn].name : "患者已不存在",
                      doctorId: plan.doctorId,
                      clientId: plan.clientId,
                      date: realPlan.date,
                      empty: realPlan.empty ? realPlan.empty : false,
                      paidTime: realPlan.paidTime ? realPlan.paidTime : "",
                      requestRefund: realPlan.requestRefund,
                      name: realPlan.name,
                      paid: realPlan.paid
                    })
                  })
                })
                return res.json({response: result, count})
              })
            })
          })
        })
        break
      } else {
        DoctorInTeams.find({doctorId}, (err1, doc1) => {
          if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
          const teams = doc1.map(re => re.teamId)
          DoctorInTeams.find({teamId: {$in: teams}}, (err2, doc2) => {
            if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
            const doctorIds = doc2.map(re => re.doctorId)
            Doctors.find({$or: [{refer: username}, {_id: {$in: doctorIds}}]}, {'password':0, '__v':0}, {sort: {date: -1}}, (err, response) => {
              if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
              const ids = response.map(doctor => ""+doctor._id)
              const info = response.map(doctor => {
                return {
                  id: ""+doctor._id,
                  name: doctor.name
                }
              })
              Plans.find({doctorId: {$in: ids}}, {'password':0, '__v':0}, {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
                if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                Plans.countDocuments({}, (err, count) => {
                  if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                  const result = []
                  const clientIds = response.map(plan => plan.clientId)
                  const cidsNoDu = clientIds.filter((clientId, index) => clientIds.indexOf(clientId) === index)
                  Clients.find({'_id': {$in: cidsNoDu}}, (err, clientDoc) => {
                    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                    const clientIdsRe = clientDoc.map(client => ""+client._id)
                    response.forEach(plan => {
                      const docIn = ids.indexOf(plan.doctorId)
                      const cliIn = clientIdsRe.indexOf(plan.clientId)
                      plan.plans.forEach(realPlan => {
                        result.push({
                          _id: realPlan._id,
                          doctorName: docIn >= 0 ? info[docIn].name : "医生已不存在",
                          clientName: cliIn >= 0 ? clientDoc[cliIn].name : "患者已不存在",
                          date: realPlan.date,
                          name: realPlan.name,
                          paid: realPlan.paid
                        })
                      })
                    })
                    return res.json({response: result, count})
                  })
                })
              })
            })
          })
        })
        break
      }
    case "moves":
    Movements.find({$or: [{id: {$regex: value}}, {name: {$regex: value}}]}, {'password':0, '__v':0}, {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      Movements.countDocuments({$or: [{id: {$regex: value}}, {name: {$regex: value}}]}, (err, count) => {
        if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
        return res.json({response, count})
      })
    })
    break
    case "templates":
      Doctors.find({},{'password':0, '__v':0}, {sort: {date: -1}, skip: 50 * (page-1), limit: 50}, (err, response) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        Doctors.countDocuments({}, (err, count) => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          let templates = []
          response.forEach(doctor => {
            doctor.templates.forEach(template => {
              templates.push({
                templateId: template._id,
                doctorId: doctor._id,
                name: template.name,
                count: template.moveList.filter(move => !move.bar).length,
                doctorName: doctor.name
              })
            })
          })
          return res.json({response: templates, count})
        })
      })
      break
    default: return res.json({code: 1})
  }

})

app.get('/plans', (req, res) => {
  const { doctorId } = req.query
  Plans.find({doctorId}, null, {sort: {date: -1}}, (err, doc) => {
    if (!err) {
      const newDoc = doc.map(plan => {
        return {
          read: plan.read,
          recognized: plan.recognized,
          _id: plan._id,
          doctorId: plan.doctorId,
          clientId: plan.clientId,
          plans: plan.plans.map(pl => {
            return {paid: pl.paid}
          }),
          empty: plan.plans.filter(plan => plan.empty).length,
          request: plan.request,
          date: plan.date,
          category: plan.category,
          childDoctorId: plan.childDoctorId
        }
      })
      return res.json({code: 1, doc: newDoc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/plansbyclient', (req, res) => {
  const {doctorId, clientId} = req.query
  Plans.findOne({doctorId, clientId}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新1"})
    } else if (doc) {
      const newDoc = {
        read: doc.read,
        recognized: doc.recognized,
        _id: doc._id,
        doctorId: doc.doctorId,
        clientId: doc.clientId,
        childDoctor: {id: doc.childDoctorId},
        description: doc.description,
        plans: doc.plans.map(plan => {
          return {
            startDate: plan.startDate,
            periodDays: plan.periodDays,
            paid: plan.paid,
            start: plan.start,
            date: plan.date,
            name: plan.name,
            empty: plan.empty ? plan.empty : false,
            free30: plan.free30,
            _id: plan._id,
            length: plan.moveList.filter(move => !move.bar).length
          }
        }),
        request: doc.request,
        date: doc.date
      }
      if (doc.category && doc.childDoctorId) {
        ClientCategories.find({doctorId, _id: {$in: doc.category}}, (err, doc) => {
          if (err) {
            return res.json({code: 0, msg: "后端出错了，请刷新2"})
          }
          else {
            const catIds = doc.map(cat => ""+cat._id)
            const parentIds = doc.filter(cat => cat.type === "second").map(cat => cat.name.split("$")[0])
            const diseaseIds = doc.filter(cat => cat.type === "disease").map(cat => cat.name)
            const operationIds = doc.filter(cat => cat.type === "operation").map(cat => cat.name)
            catIds.push(...parentIds)
            ClientCategories.find({doctorId, _id: {$in: catIds}}, (err, doc1) => {
              if (err) return res.json({code: 0, msg: "后端出错了，请刷新3"})
              DiseaseCodes.find({_id: {$in: diseaseIds}}, (err, doc2) => {
                if (err) return res.json({code: 0, msg: "后端出错了，请刷新4"})
                OperationCodes.find({_id: {$in: operationIds}}, (err, doc3) => {
                  if (err) return res.json({code: 0, msg: "后端出错了，请刷新5"})
                  newDoc.categories = doc.map(cat => {
                    if (cat.type === "second") {
                      return {
                        type: cat.type,
                        name: cat.name.split("$")[1],
                        id: "" + cat._id,
                        parentId: cat.name.split("$")[0],
                        parentName: doc1.filter(oCat => ""+oCat._id === cat.name.split("$")[0])[0].name
                      }
                    } else if (cat.type === "defined") {
                      return {
                        type: cat.type,
                        name: cat.name,
                        id: "" + cat._id,
                      }
                    } else if (cat.type === "disease") {
                      const thisCat = doc2.filter(dCat => "" + dCat._id === cat.name)[0]
                      return {
                        type: cat.type,
                        name: thisCat.name,
                        id: "" + cat._id,
                        code: thisCat.code,
                        rootId: cat.name
                      }
                    } else {
                      const thisCat = doc3.filter(oCat => "" + oCat._id === cat.name)[0]
                      return {
                        type: cat.type,
                        name: thisCat.name,
                        id: "" + cat._id,
                        code: thisCat.code,
                        rootId: cat.name
                      }
                    }
                  })
                  Doctors.findOne({_id: ""+newDoc.doctorId}, (err, doc4) => {
                    if (err) {
                      console.log(err)
                      return res.json({code: 0, msg: "后端出错了，请刷新6"})
                    }
                    newDoc.childDoctor.name = doc4.childDoctors.filter(doctor => "" + doctor._id === newDoc.childDoctor.id)[0].name
                    return res.json({code: 1, doc: newDoc})
                  })
                })
              })
            })
          }
        })
      } else if (doc.category) {
        ClientCategories.find({doctorId, _id: {$in: doc.category}}, (err, doc) => {
          if (err) {
            return res.json({code: 0, msg: "后端出错了，请刷新7"})
          }
          else {
            const catIds = doc.map(cat => ""+cat._id)
            const parentIds = doc.filter(cat => cat.type === "second").map(cat => cat.name.split("$")[0])
            const diseaseIds = doc.filter(cat => cat.type === "disease").map(cat => cat.name)
            const operationIds = doc.filter(cat => cat.type === "operation").map(cat => cat.name)
            catIds.push(...parentIds)
            ClientCategories.find({doctorId, _id: {$in: catIds}}, (err, doc1) => {
              if (err) return res.json({code: 0, msg: "后端出错了，请刷新8"})
              DiseaseCodes.find({_id: {$in: diseaseIds}}, (err, doc2) => {
                if (err) return res.json({code: 0, msg: "后端出错了，请刷新9"})
                OperationCodes.find({_id: {$in: operationIds}}, (err, doc3) => {
                  if (err) return res.json({code: 0, msg: "后端出错了，请刷新10"})
                  newDoc.categories = doc.map(cat => {
                    if (cat.type === "second") {
                      return {
                        type: cat.type,
                        name: cat.name.split("$")[1],
                        id: "" + cat._id,
                        parentId: cat.name.split("$")[0],
                        parentName: doc1.filter(oCat => ""+oCat._id === cat.name.split("$")[0])[0].name
                      }
                    } else if (cat.type === "defined") {
                      return {
                        type: cat.type,
                        name: cat.name,
                        id: "" + cat._id,
                      }
                    } else if (cat.type === "disease") {
                      const thisCat = doc2.filter(dCat => "" + dCat._id === cat.name)[0]
                      return {
                        type: cat.type,
                        name: thisCat.name,
                        id: "" + cat._id,
                        code: thisCat.code,
                        rootId: cat.name
                      }
                    } else {
                      const thisCat = doc3.filter(oCat => "" + oCat._id === cat.name)[0]
                      return {
                        type: cat.type,
                        name: thisCat.name,
                        id: "" + cat._id,
                        code: thisCat.code,
                        rootId: cat.name
                      }
                    }
                  })
                  return res.json({code: 1, doc: newDoc})
                })
              })
            })
          }
        })
      } else if (doc.childDoctorId) {
        Doctors.findOne({_id: ""+newDoc.doctorId}, (err, doc4) => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新11"})
          newDoc.childDoctor.name = doc4.childDoctors.filter(doctor => "" + doctor._id === newDoc.childDoctor.id)[0].name
          return res.json({code: 1, doc: newDoc})
        })
      } else {
        newDoc.categories = []
        return res.json({code: 1, doc: newDoc})
      }
    } else {
      return res.json({code: 0, msg: "该医生与患者并未建立联系"})
    }
  })
})

app.get('/searchclient', (req, res) => {
  const {username} = req.query
  Clients.findOne({username: username}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else if (!doc) {
      return res.json({code: 0, msg: "无此患者"})
    } else {
      return res.json({code: 1, doc})
    }
  })
})

app.get('/clientbyid', (req, res) => {
  const {clientId} = req.query
  Clients.findById(clientId, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/clientbyid2', (req, res) => {
  const {clientId} = req.query
  Clients.findById(clientId, (err, doc) => {
    if (!err) {
      if (!doc.vip || !doc.vipTime || (doc.vip && (Date.now() - doc.vipTime) > 365 * 24 * 3600 * 1000)) {
        doc.vip = false
      }
      if (!doc.specialPaid) {
        doc.specialPaid = false
      }
      if (!doc.avatar) {
        doc.avatar = ""
      }
      return res.json({code: 1, doc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/clientsbyid', (req, res) => {
  const {clientIds} = req.body
  Clients.find({'_id': {$in: clientIds}}, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/doctorbyid', (req, res) => {
  const {doctorId} = req.query
  Doctors.findOne({'_id': doctorId}, _filter, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get("/searchmoves", (req, res) => {
  const {param0, param1, param2, param3} = req.query
    if (typeof param3 === "undefined") {
      Categories.findOne({id: param2}, (err, doc1) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        } else {
          Movements.find({"id": {$in: doc1.contains}}, null, {sort: {video: -1, name: -1}}, (err, doc2) => {
            if (!err) {
              return res.json({code: 1, doc2, title: doc1.name})
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        }
      })

    } else {
      Categories.findOne({id: param3}, (err, doc1) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        } else {
          Movements.find({"id": {$in: doc1.contains}}, null, {sort: {video: -1, name: -1}}, (err, doc2) => {
            if (!err) {
              return res.json({code: 1, doc2, title: doc1.name})
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        }
      })
    }
})

app.get("/searchtemps", (req, res) => {
  const {label} = req.query
  Templates.find({label: label, type: 2}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      const templates = doc.map(template => {
        return {
          templateName: template.name,
          count: template.moveList.filter(move => !move.bar).length,
          templateId: template._id
        }
      })
      return res.json({code: 1, doc: templates, title: label})
    }
  })
})

app.get("/searchexperttemps", (req, res) => {
  const {value} = req.query
  Templates.find({name:{$regex: value}, type: 2}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      const templates = doc.map(template => {
        return {
          templateName: template.name,
          count: template.moveList.filter(move => !move.bar).length,
          templateId: template._id
        }
      })
      let title = value === "" ? "全部" : "\""+value+"\""
      return res.json({code: 1, doc: templates, title: "在专家模板中搜索"+title})
    }
  })
})

// app.post('/register', (req, res) => {
//   const {username, password, name, id, account, bankName, bankAccount,
//     alipayAccount, phone, refer, room, hospital, certification, occupation} = req.body
//   if (username.length < 5 || password.length < 6 || !phone) return res.json({code: 0})
//   Doctors.find({username: {$in: [refer, username]}}, (err, doc) => {
//     if (!err) {
//       const two = {}
//       if (doc.length === 2) {
//         two.user = true
//         two.refer = true
//       } else if (doc.length === 1) {
//         if (doc[0].username === username) {
//           two.user = true
//         } else if (doc[0].username === refer) {
//           two.refer = true
//         }
//       }
//       if (two.user || RegExp('^voin[0-9]{4}$').test(username) ) {
//         return res.json({code: 0, msg: "该用户名已存在，请更换后输入"})
//       } else if (refer && !two.refer) {
//         return res.json({code: 0, msg: "该推荐人用户名不存在，请确认后输入"})
//       } else {
//         Doctors.find({phone: phone}, (err, doc) => {
//           if (!err) {
//             if (doc.length > 0) {
//               return res.json({code: 0, msg: "该手机号已被注册，请更换后输入"})
//             } else {
//               const time = new Date().getTime()
//               Doctors.create({username, password: md5Password(password), name, account, bankName, bankAccount, idnum: id,
//                     alipayAccount, phone, email: "", room, refer, hospital, certification, occupation, templates: [],
//                     free30: false, time: ""+time},
//                 (err, doc) => {
//                   if (!err) {
//                     doc.password = undefined
//                     return res.json({code: 1, msg: "注册成功, 跳转中", doc, token: md5Cookie(doc._id), time})
//                   } else {
//                     console.log(err)
//                     return res.json({code: 0, msg: "后端出错了，请刷新"})
//                   }
//                 })
//             }
//           } else {
//             return res.json({code: 0, msg: "后端出错了，请刷新"})
//           }
//         })
//       }
//     } else {
//       console.log(err)
//       return res.json({code: 0, msg: "后端出错了，请刷新"})
//     }
//   })
// })

app.post('/login', (req, res) => {
  const {username, password, isUsername} = req.body

  // TODO: http only cookie
  if (username === "resume") {
    Doctors.findOneAndUpdate({username}, {$inc: {loginTimes: 1}}, _filter, (err, doc) => {
      if (!err) {
        if (doc) {
          const newDoc = doc
          newDoc.templates = null
          newDoc.plans = null
          return res.json({
            code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
            doc: newDoc, token: md5Cookie(doc._id), refer: false})
        } else {
          return res.json({code: 0, msg: "用户名或密码错误"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else if (isUsername) {
    Doctors.findOne({username, password: md5Password(password)}, (err, doc) => {
      if (!err) {
        if (doc) {
          Doctors.findOne({refer: doc.username}, (err2, doc2) => {
            if (err2) return res.json({code: 0, msg: '后端出错了, 请刷新'})
            else {
              const newDoc = doc
              newDoc.templates = undefined
              newDoc.password = undefined
              newDoc.plans = undefined
              if (doc2) return res.json({
                code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
                doc: newDoc, token: md5Cookie(doc._id), refer: true})
              else return res.json({
                code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
                doc: newDoc, token: md5Cookie(doc._id), refer: false})
            }
          })
        } else {
          return res.json({code: 0, msg: "用户名或密码错误"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else {
    Doctors.findOne({phone: username, password: md5Password(password)}, (err, doc) => {
      if (!err) {
        if (doc) {
          Doctors.findOne({refer: doc.username}, (err2, doc2) => {
            if (err2) return res.json({code: 0, msg: '后端出错了, 请刷新'})
            else {
              const newDoc = doc
              newDoc.templates = undefined
              newDoc.password = undefined
              newDoc.plans = undefined
              if (doc2) return res.json({
                code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
                doc: newDoc, token: md5Cookie(doc._id), refer: true})
              else return res.json({
                code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
                doc: newDoc, token: md5Cookie(doc._id), refer: false})
            }
          })
        } else {
          return res.json({code: 0, msg: "用户名或密码错误"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }

})

app.post('/loginforget', (req, res) => {
  const {phone, verify} = req.body

  // TODO: http only cookie

  Doctors.findOneAndUpdate({phone, code: verify}, {code: ""}, _filter, (err, doc) => {
    if (!err) {
      if (doc) {
        return res.json({code: 1, username: doc.username, name: doc.name})
      } else {
        return res.json({code: 0, msg: "验证码错误"})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })

})

app.post('/resetpassword', (req, res) => {
  const {username, password} = req.body

  Doctors.findOneAndUpdate({username}, {password: md5Password(password)}, _filter, (err, doc) => {
    if (!err) {

          if (doc) {
            Doctors.findOne({refer: username}, (err2, doc2) => {
              if (err2) return res.json({code: 0, msg: '后端出错了, 请刷新'})
              else {
                const newDoc = doc
                newDoc.templates = null
                newDoc.plans = null
                if (doc2) return res.json({
                  code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
                  doc: newDoc, token: md5Cookie(doc._id), refer: true})
                else return res.json({
                  code: 1, msg: doc.name ? '欢迎回来，' + doc.name : "欢迎回来",
                  doc: newDoc, token: md5Cookie(doc._id), refer: false})
              }
            })
          } else {
            return res.json({code: 0, msg: "用户名或密码错误"})
          }

    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })

})

app.post('/newsavetemplate', (req, res) => {
  const {doctorId, name, summary, times, sets, timesPerSets, weights, betweens, parameter1s, parameter2s,
    value1s, value2s, articles, infos, ids, bars, starts, barNames, totalInEach, periodDays, proposalContent,
    references, minutes, timesPerDays, time2s, periods, therapyInfos, addedParamCounts, allChosenParams,
    therapyIds} = req.body
  const moveList = []
  const therapyList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.between = betweens[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.time = times[i]
    oneMove.bar = bars[i]
    oneMove.start = starts[i]
    oneMove.barName = barNames[i]
    moveList.push(oneMove)
  }
  for (let i = 0; i < therapyIds.length; i++) {
    const oneTherapy = {}
    oneTherapy.move = therapyIds[i]
    oneTherapy.minute = minutes[i]
    oneTherapy.timesPerDay = timesPerDays[i]
    oneTherapy.time2 = time2s[i]
    oneTherapy.period = periods[i]
    oneTherapy.therapyInfo = therapyInfos[i]
    oneTherapy.chosenParams = allChosenParams[i]
    therapyList.push(oneTherapy)
  }
  Doctors.findOneAndUpdate({
    _id: doctorId}, {
    $push: {
      templates: {
        name,
        summary,
        moveList,
        articles,
        totalInEach,
        addedParamCounts,
        therapyList,
        periodDays,
        proposalContent,
        references
      }
    }
  }, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/newsaveplan', (req, res) => {
  const {doctorId, clientId, name, allDay, times, sets, timesPerSets, weights, betweens, parameter1s, parameter2s,
    value1s, value2s, infos, ids, bars, barNames, starts, extra, articles, totalInEach, periodDays} = req.body
  const moveList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.between = betweens[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.bar = bars[i]
    oneMove.start = starts[i]
    oneMove.barName = barNames[i]
    moveList.push(oneMove)
  }
  Plans.findOne({doctorId, clientId}, (err, doc) => {
    if (!err) {
      if (doc) {
        Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
          if (docDoc) {
            const free30 = !!docDoc.free30
            console.log(free30)
            Plans.findOneAndUpdate({
              doctorId,
              clientId
            }, {
              $push: {
                plans: {
                  name,
                  allDay,
                  start: false,
                  pdfSent: false,
                  paid: false,
                  empty: false,
                  moveList,
                  articles,
                  extra,
                  totalInEach,
                  periodDays,
                  free30: free30,
                  feedback: [],
                }
              }
            }, {new: true}, (err, doc) => {
              if (!err) {
                const newGuy = doc.plans[doc.plans.length-1]
                Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
                  Clients.findOne({_id: clientId}, (errCli, docCli) => {
                    if (docDoc && docCli) {
                      const params = {
                        "RegionId": "cn-hangzhou",
                        "PhoneNumbers": ""+docCli.phone,
                        "SignName": "沃衍健康通知",
                        "TemplateCode": "SMS_189615644",
                        "TemplateParam": "{\"name\":\""+docDoc.name+"\"}"
                      }

                      sms.request('SendSms', params, {method: 'POST'}).then((result) => {
                        return res.json({code: 1, no: newGuy._id, doc})
                      }, (ex) => {
                        console.log("短信发送请求出错了")
                        return res.json({code: 1, no: newGuy._id, doc})
                      })
                    } else {
                      return res.json({code: 1, no: newGuy._id, doc})
                    }
                  })
                })
              } else {
                console.log(err)
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
            })
          }
        })
      }
      else {
        Clients.findOne({_id: clientId}, (errCli, docCli) => {
          if (docCli) {
            const coupon = docCli.coupon
            if (Number(coupon) > 0) {
              Plans.create({
                doctorId, clientId, request: [], category: [], plans: [{
                  name,
                  allDay,
                  start: false,
                  pdfSent: false,
                  paid: true,
                  empty: false,
                  moveList,
                  articles,
                  extra,
                  totalInEach,
                  periodDays,
                  feedback: [],
                  numbers: 1,
                  free30: false
                }]
              }, (err, doc) => {
                if (!err) {
                  const newGuy = doc.plans[doc.plans.length - 1]
                  Clients.findOneAndUpdate({_id: clientId}, {$inc: {coupon: -1}}, (err, doc) => {
                    if (!err) return res.json({code: 1, no: newGuy._id, doc})
                    else return res.json({code: 0, msg: "后端出错了，请刷新"})
                  })
                } else {
                  console.log(err)
                  return res.json({code: 0, msg: "后端出错了，请刷新"})
                }
              })
            } else {
              Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
                if (docDoc) {
                  const free30 = !!docDoc.free30
                  Plans.create({
                    doctorId, clientId, request: [], category: [], plans: [{
                      name,
                      allDay,
                      start: false,
                      pdfSent: false,
                      paid: false,
                      empty: false,
                      moveList,
                      articles,
                      extra,
                      totalInEach,
                      periodDays,
                      feedback: [],
                      numbers: 1,
                      free30: free30
                    }]
                  }, (err, doc) => {
                    if (!err) {
                      const newGuy = doc.plans[doc.plans.length - 1]
                      return res.json({code: 1, no: newGuy._id, doc})
                    } else {
                      console.log(err)
                      return res.json({code: 0, msg: "后端出错了，请刷新"})
                    }
                  })
                }
              })
            }
          }
        })
      }
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})


app.post('/saveplan', (req, res) => {
  const {doctorId, clientId, name, allDay, times, sets, timesPerSets, weights, betweens, parameter1s, parameter2s,
    value1s, value2s, infos, ids, bars, barNames, starts, extra, articles, totalInEach, periodDays, titleId,
    proposalContent, references, minutes, timesPerDays, time2s, periods, therapyInfos, addedParamCounts, allChosenParams,
    therapyIds
  } = req.body
  const moveList = []
  const therapyList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.between = betweens[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.bar = bars[i]
    oneMove.start = starts[i]
    oneMove.barName = barNames[i]
    moveList.push(oneMove)
  }
  for (let i = 0; i < therapyIds.length; i++) {
    const oneTherapy = {}
    oneTherapy.move = therapyIds[i]
    oneTherapy.minute = minutes[i]
    oneTherapy.timesPerDay = timesPerDays[i]
    oneTherapy.time2 = time2s[i]
    oneTherapy.period = periods[i]
    oneTherapy.therapyInfo = therapyInfos[i]
    oneTherapy.chosenParams = allChosenParams[i]
    therapyList.push(oneTherapy)
  }
  if (titleId) {
    const plan = {}
    plan.name = name
    plan.moveList = moveList
    plan.articles = articles
    plan.totalInEach = totalInEach
    plan.periodDays = periodDays
    plan.extra = extra
    plan.proposalContent = proposalContent
    plan.references = references
    plan.therapyList = therapyList
    plan.addedParamCounts = addedParamCounts
    HomeworkSubmits.findOne({doctorId, titleId}, (err, doc) => {
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      if (doc) return res.json({code: 0, msg: "您已提交该作业"})
      else {
        HomeworkSubmits.create({doctorId, titleId, plan}, (err, doc) => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else return res.json({code: 1})
        })
      }
    })
  } else {
    Plans.findOne({doctorId, clientId}, (err, doc) => {
      if (!err) {
        if (doc) {
          const plans = doc.plans
          const emptyPlans = plans.filter(plan => plan.empty).map(plan => plan._id)
          if (emptyPlans.length > 0) {
            const replaceId = emptyPlans[0]
            Plans.findOneAndUpdate({
              doctorId, clientId,
              'plans._id': replaceId,
            }, {
              $set: {
                'plans.$.name': name,
                'plans.$.allDay': allDay,
                'plans.$.moveList': moveList,
                'plans.$.therapyList': therapyList,
                'plans.$.addedParamCounts': addedParamCounts,
                'plans.$.articles': articles,
                'plans.$.totalInEach': totalInEach,
                'plans.$.periodDays': periodDays,
                'plans.$.extra': extra,
                'plans.$.feedback': [],
                'plans.$.start': false,
                'plans.$.paid': true,
                'plans.$.empty': false,
              }
            }, err => {
              if (!err) {
                return res.json({code: 1})
              } else {
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
            })
          } else {
            Clients.findOne({_id: clientId}, (errCli, docCli) => {
              if (docCli) {
                const coupon = docCli.coupon
                if (Number(coupon) > 0) {
                  Plans.findOneAndUpdate({
                    doctorId,
                    clientId
                  }, {
                    $push: {
                      plans: {
                        name,
                        allDay,
                        start: false,
                        pdfSent: false,
                        paid: true,
                        empty: false,
                        moveList,
                        articles,
                        therapyList,
                        addedParamCounts,
                        extra,
                        totalInEach,
                        periodDays,
                        feedback: [],
                        free30: false
                      }
                    }
                  }, {new: true}, (err, doc) => {
                    if (!err) {
                      const newGuy = doc.plans[doc.plans.length - 1]
                      Clients.findOneAndUpdate({_id: clientId}, {$inc: {coupon: -1}}, (err, doc) => {
                        if (!err) return res.json({code: 1, no: newGuy._id, doc})
                        else return res.json({code: 0, msg: "后端出错了，请刷新"})
                      })
                    } else {
                      console.log(err)
                      return res.json({code: 0, msg: "后端出错了，请刷新"})
                    }
                  })
                } else {
                  Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
                    if (docDoc) {
                      const free30 = !!docDoc.free30
                      Plans.findOneAndUpdate({
                        doctorId,
                        clientId
                      }, {
                        $push: {
                          plans: {
                            name,
                            allDay,
                            start: false,
                            pdfSent: false,
                            paid: false,
                            empty: false,
                            moveList,
                            articles,
                            therapyList,
                            addedParamCounts,
                            extra,
                            totalInEach,
                            periodDays,
                            feedback: [],
                            free30: free30
                          }
                        }
                      }, {new: true}, (err, doc) => {
                        if (!err) {
                          const newGuy = doc.plans[doc.plans.length - 1]
                          return res.json({code: 1, no: newGuy._id, doc})
                        } else {
                          console.log(err)
                          return res.json({code: 0, msg: "后端出错了，请刷新"})
                        }
                      })
                    }
                  })
                }
              }
            })
          }
        } else {
          return res.json({code: 0, msg: "该医生与患者未建立关系"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }
})

app.post('/newupdatetemplate', (req, res) => {
  const {doctorId, templateId, name, summary, times, sets, timesPerSets, weights, betweens, parameter1s, parameter2s,
    value1s, value2s, infos, ids, bars, starts, barNames, articles, totalInEach, periodDays, proposalContent,
    references, minutes, timesPerDays, time2s, periods, therapyInfos, addedParamCounts, allChosenParams,
    therapyIds} = req.body
  const moveList = []
  const therapyList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.between = betweens[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.bar = bars[i]
    oneMove.start = starts[i]
    oneMove.barName = barNames[i]
    moveList.push(oneMove)
  }
  for (let i = 0; i < therapyIds.length; i++) {
    const oneTherapy = {}
    oneTherapy.move = therapyIds[i]
    oneTherapy.minute = minutes[i]
    oneTherapy.timesPerDay = timesPerDays[i]
    oneTherapy.time2 = time2s[i]
    oneTherapy.period = periods[i]
    oneTherapy.therapyInfo = therapyInfos[i]
    oneTherapy.chosenParams = allChosenParams[i]
    therapyList.push(oneTherapy)
  }
  Doctors.findOneAndUpdate({
    '_id': doctorId,
    'templates._id': templateId,
  }, {
    $set: {
      'templates.$.name': name,
      'templates.$.summary': summary,
      'templates.$.moveList': moveList,
      'templates.$.articles': articles,
      'templates.$.totalInEach': totalInEach,
      'templates.$.addedParamCounts': addedParamCounts,
      'templates.$.therapyList': therapyList,
      'templates.$.periodDays': periodDays,
      'templates.$.proposalContent': proposalContent,
      'templates.$.references': references
    }
  }, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/updateplan', (req, res) => {
  const {doctorId, clientId, planId, name, summary, times, sets, timesPerSets, weights, betweens, parameter1s, parameter2s,
    value1s, value2s, infos, ids, bars, starts, barNames, articles, totalInEach, periodDays, extra, minutes, timesPerDays,
    time2s, periods, therapyInfos, addedParamCounts, allChosenParams, therapyIds} = req.body
  const moveList = []
  const therapyList = []
  const length = ids.length
  for (let i = 0; i < length; i++) {
    const oneMove = {}
    oneMove.move = ids[i]
    oneMove.info = infos[i]
    oneMove.set = sets[i]
    oneMove.timesPerSet = timesPerSets[i]
    oneMove.weight = weights[i]
    oneMove.between = betweens[i]
    oneMove.time = times[i]
    oneMove.parameter1 = parameter1s[i]
    oneMove.parameter2 = parameter2s[i]
    oneMove.value1 = value1s[i]
    oneMove.value2 = value2s[i]
    oneMove.bar = bars[i]
    oneMove.start = starts[i]
    oneMove.barName = barNames[i]
    moveList.push(oneMove)
  }
  for (let i = 0; i < therapyIds.length; i++) {
    const oneTherapy = {}
    oneTherapy.move = therapyIds[i]
    oneTherapy.minute = minutes[i]
    oneTherapy.timesPerDay = timesPerDays[i]
    oneTherapy.time2 = time2s[i]
    oneTherapy.period = periods[i]
    oneTherapy.therapyInfo = therapyInfos[i]
    oneTherapy.chosenParams = allChosenParams[i]
    therapyList.push(oneTherapy)
  }

  Plans.findOne({doctorId: doctorId, clientId: clientId, 'plans._id': planId},{
    'plans.$' : 1,
  }, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const feedback = doc1.plans[0].feedback
      const oldPeriodDays = doc1.plans[0].periodDays
      const periodTotal = []
      let current = 0
      for (let i = 0; i < oldPeriodDays.length; i++) {
        periodTotal.push(Number(oldPeriodDays[i])+current)
        current = current + Number(oldPeriodDays[i])
      }
      const startDate = doc1.plans[0].startDate
      const oldTotalInEach = doc1.plans[0].totalInEach
      const oldMoveList = doc1.plans[0].moveList
      const saved = doc1.plans[0].savedFeedback
      let percents = []
      const data = []

      if (feedback.length > 0) {
        percents = Array(oldPeriodDays.reduce((a, b) => Number(a) + Number(b), 0)).fill("");
        const date1 = new Date(
          parseInt(startDate.substring(0, 4), 10),
          parseInt(startDate.substring(4, 6), 10) - 1,
          parseInt(startDate.substring(6, 8), 10))
        feedback.forEach((feed) => {
          const dayAndNum = feed.split(" ")
          const day = dayAndNum[0]
          const num = dayAndNum[1]
          const date2 = new Date(
            parseInt(day.substring(0, 4), 10),
            parseInt(day.substring(4, 6), 10) - 1,
            parseInt(day.substring(6, 8), 10)
          )
          const diff = Math.floor((date2 - date1) / 86400000)
          percents.splice(diff, 1, num)
        })

        const timezone = 8;
        const offset_GMT = new Date().getTimezoneOffset();
        const correctDate = new Date(new Date().getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
        const diffTime = Math.abs(correctDate - date1);
        const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        const finalDay = diffDays+1

        const lastDay = saved.length > 0 ? Number(saved[saved.length-1].split(" ")[1]) : 0

        for (let i = 0; i < oldPeriodDays.length; i++) {
          const days = Number(oldPeriodDays[i])
          const movesInPeriod = oldMoveList.slice(i === 0 ? 0 : oldTotalInEach[i-1], oldTotalInEach[i]).filter(move => !move.bar)
          for (let x = 1; x <= days ; x++) {
            const day = i === 0 ? x : periodTotal[i-1]+x
            if (day <= finalDay && day >= lastDay) {
              for (let y = 0, index = -1; y < movesInPeriod.length; y++) {
                if (!betweens[y] || (x-1) % (Number(betweens[y])+1) === 0) {
                  index++
                  if (percents[day-1]) {
                    const pers = percents[day-1].split(",")
                    if (pers.length > index) {
                      data.push(""+movesInPeriod[y].move+" "+day+" "+Number(pers[index]))
                    } else {
                      data.push(""+movesInPeriod[y].move+" "+day+" 0")
                    }

                  } else {
                    data.push(""+movesInPeriod[y].move+" "+day+" 0")
                  }
                }
              }
            }
          }
        }
      }


      Plans.findOneAndUpdate({
        doctorId: doctorId,
        clientId: clientId,
        'plans._id': planId,
      }, {
        $set: {
          'plans.$.name': name,
          'plans.$.summary': summary,
          'plans.$.moveList': moveList,
          'plans.$.articles': articles,
          'plans.$.therapyList': therapyList,
          'plans.$.addedParamCounts': addedParamCounts,
          'plans.$.totalInEach': totalInEach,
          'plans.$.periodDays': periodDays,
          'plans.$.feedback': [],
          'plans.$.extra': extra,
          'plans.$.empty': false
        },
        $push: {
          'plans.$.savedFeedback': {
            $each: data
          }
        }
      }, {upsert:true}, err2 => {
        if (!err2) {
          return res.json({code: 1})
        } else {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      })
    }
  })
})

app.post('/deletetemplate', (req, res) => {
  const {myType, teamId, doctorId, templateId, admin} = req.body
  if (myType === "my") {
    Doctors.findByIdAndUpdate(doctorId, {
      $pull: {
        templates: {
          _id: templateId
        }
      }
    }).exec((err) => {
      if (!err) {
        return res.json({code: 1, myType})
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else if (myType === "expert" || admin == 4) {
    Templates.remove({'_id': templateId}, (err, doc) => {
      if (!err) {
        return res.json({code: 1, myType})
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else if (myType === "team") {
    Teams.findByIdAndUpdate(teamId, {
      $pull: {
        templates: {
          _id: templateId
        }
      }
    }).exec((err) => {
      if (!err) {
        return res.json({code: 1, myType})
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }

})

app.post('/addexpertorteam', (req, res) => {
  const {doctorId, templateId, labels, labelName, classId} = req.body
  Doctors.findOne({'_id': doctorId, 'templates._id': templateId},{
    'templates.$' : 1,
  }, (err, doc) => {
    if (doc.templates.length > 0) {
      if (classId) {
        console.log("in")
        const template = {
          name: doc.templates[0].name,
          summary: doc.templates[0].summary,
          moveList: doc.templates[0].moveList,
          periodDays: doc.templates[0].periodDays,
          totalInEach: doc.templates[0].totalInEach,
          articles: doc.templates[0].articles,
          therapyList: doc.templates[0].therapyList,
          addedParamCounts: doc.templates[0].addedParamCounts,
          label: classId,
          price: 0,
          type: 4
        }
        Templates.create(template, (err, doc) => {
          if (!err) return res.json({code: 1})
          else {
            console.log(err)
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      } else if (labelName === "expert") {
        const array = labels.map(label => {
          return {
            name: doc.templates[0].name,
            summary: doc.templates[0].summary,
            moveList: doc.templates[0].moveList,
            periodDays: doc.templates[0].periodDays,
            totalInEach: doc.templates[0].totalInEach,
            articles: doc.templates[0].articles,
            label: label,
            type: 2,
            price: 0,
          }
        })
        Templates.insertMany(
          array,
          (err, doc) => {
            if (!err) return res.json({code: 1})
            else return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        )
      } else if (labelName === "team") {
        const template = {
          name: doc.templates[0].name,
          summary: doc.templates[0].summary,
          moveList: doc.templates[0].moveList,
          periodDays: doc.templates[0].periodDays,
          therapyList: doc.templates[0].therapyList,
          addedParamCounts: doc.templates[0].addedParamCounts,
          totalInEach: doc.templates[0].totalInEach,
          articles: doc.templates[0].articles,
          price: 0,
        }
        Teams.findOneAndUpdate({_id: labels[0]}, {$push: {templates: template}}, (err, doc) => {
          if (!err) return res.json({code: 1})
          else return res.json({code: 0, msg: "后端出错了，请刷新"})
        })
      }
    } else {
      return res.json({code: 0, msg: "该模板不存在"})
    }
  })

})

app.post('/addtemplate', (req, res) => {
  const {doctorId, templateId, selfId} = req.body
  Doctors.findOne({'_id': doctorId, 'templates._id': templateId},{
    'templates.$' : 1,
  }, (err, doc) => {
    if (doc.templates.length > 0) {
      Doctors.findOneAndUpdate(
        {_id: selfId},
        {$push: {
          templates: {
            name: doc.templates[0].name,
            summary: doc.templates[0].summary,
            moveList: doc.templates[0].moveList,
            periodDays: doc.templates[0].periodDays,
            totalInEach: doc.templates[0].totalInEach,
            therapyList: doc.templates[0].therapyList,
            addedParamCounts: doc.templates[0].addedParamCounts,
            articles: doc.templates[0].articles
          }
        }},
        (err, doc) => {
          return res.json({code: 1})
        }
      )
    } else {
      return res.json({code: 1})
    }
  })
})

app.get('/doctors', (req, res) => {
  Doctors.find({}, {username: 1, name: 1}, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/sendtemplate', (req, res) => {
  const {doctorId1, templateId, doctorIds} = req.body
  Doctors.findOne({'_id': doctorId1, 'templates._id': templateId},{
    'templates.$' : 1,
  }, (err, doc) => {
    if (doc.templates.length > 0) {
      Doctors.updateMany(
        {_id: {$in: doctorIds}},
        {$push: {
          templates: {
            name: doc.templates[0].name,
            summary: doc.templates[0].summary,
            moveList: doc.templates[0].moveList,
            periodDays: doc.templates[0].periodDays,
            totalInEach: doc.templates[0].totalInEach,
            therapyList: doc.templates[0].therapyList,
            addedParamCounts: doc.templates[0].addedParamCounts,
            articles: doc.templates[0].articles
          }
        }},
        (err, doc) => {
          if (!err) return res.json({code: 1})
          else return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      )
    } else {
      return res.json({code: 0, msg: "此模版不存在"})
    }
  })
})

app.get('/templates', (req, res) => {
  const { doctorId } = req.query
  Doctors.findOne({_id: doctorId}, {'password':0, '__v':0}, {sort: {date: 1}}, (err, doc) => {
    if (!err) {
      const templates = doc.templates.map(template => {
        const coveredMoves = template.moveList.map(move => move.move)
        const articles = template.articles
        const _id = template._id
        const name = template.name
        const count = template.moveList.filter(move => !move.bar).length
        const category = template.category ? template.category : []
        const date = template.date
        return {_id, name, count, coveredMoves, articles, category, date}
      })
      return res.json({code: 1, doc: templates})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/teamtemplates', (req, res) => {
  const { doctorId, admin } = req.query
  if (admin === "4" || admin === "5") {
    const isTeacher = admin === "4"
    DoctorInClasses.findOne({doctorId, isTeacher}, (err1, doc1) => {
      if (!err1) {
        const classId = doc1.classId
        Templates.find({label: classId}, (err2, doc2) => {
          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else {
            const newDoc = doc2.map(template => {
              return {
                templateId: template._id,
                templateName: template.name,
                date: template.date,
                count: template.moveList.filter(move => !move.bar).length,
              }
            })
            return res.json({code: 1, doc: newDoc, classId})
          }
        })
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else {
    DoctorInTeams.find({doctorId}, (err1, doc1) => {
      if (!err1) {
        const teamIds = doc1.map(team => team.teamId)
        Teams.find({_id: {$in: teamIds}}, _filter_team, (err2, doc2) => {
          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else {
            const newDoc = doc2.map(team => {
              if (team._id+"" === teamIds[0]) {
                return {
                  _id: team._id,
                  name: team.name,
                  username: team.username,
                  date: team.date,
                  templates: team.templates.map(template => {
                    return {
                      templateId: template._id,
                      templateName: template.name,
                      date: template.date,
                      count: template.moveList.filter(move => !move.bar).length
                    }
                  })
                }
              }
              else {
                return {
                  _id: team._id,
                  name: team.name,
                  username: team.username,
                  date: team.date
                }
              }
            })
            newDoc.sort((a, b) =>  {
              return teamIds.indexOf(a._id+"") - teamIds.indexOf(b._id+"");
            });
            newDoc.forEach((team, index) => {
              team.creator = doc1[index].creator
            })
            return res.json({code: 1, doc: newDoc})
          }
        })
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }
})

app.get('/teamchange', (req, res) => {
  const { teamId } = req.query
  Teams.findOne({_id: teamId}, _filter_team, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const newDoc = {
        _id: doc._id,
        name: doc.name,
        username: doc.username,
        date: doc.date,
        templates: doc.templates.map(template => {
          return {
            templateId: template._id,
            templateName: template.name,
            date: template.date,
            count: template.moveList.filter(move => !move.bar).length
          }
        })
      }
      return res.json({code: 1, doc: newDoc})
    }
  })
})

app.get('/teammembers', (req, res) => {
  const { teamId } = req.query
  DoctorInTeams.find({teamId: teamId}, null, {sort: {date: 1}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      Doctors.find({_id: {$in :doc.map(relation => relation.doctorId)}}, _filter, (err1, doc1) => {
        if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const doctorIds = doc.map(doctor => doctor.doctorId)
          doc1.sort((a, b) =>  {
            return doctorIds.indexOf(""+a._id) - doctorIds.indexOf(""+b._id);
          })
          return res.json({code: 1, doc: doc1.map(doctor => {
            return {name: doctor.name, doctorId: doctor._id, username: doctor.username}
          })})
        }
      })
    }
  })
})

app.post("/deletemember", (req, res) => {
  const {teamId, doctorId} = req.body
  DoctorInTeams.remove({teamId, doctorId}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      return res.json({code: 1})
    }
  })
})

app.post("/deleteteam", (req, res) => {
  const {teamId} = req.body
  DoctorInTeams.remove({teamId}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      Teams.remove({_id: teamId}, (err1, doc1) => {
        if (err1) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        } else {
          return res.json({code: 1})
        }
      })
    }
  })
})


app.get('/info', (req, res) => {
  const {userId, token} = req.query
  if (!userId || !token) {
    return res.json({code: 2})
  }
  if (md5Cookie(userId) !== token) {
    return res.json({code: 1, msg: '后端功能更新，请刷新'})
  }
  Doctors.findOne({_id: userId}, _filter, (err, doc) => {
    if (err) {
      return res.json({code: 1, msg: '后端出错了, 请刷新'})
    }
    Doctors.findOne({refer: doc.username}, (err2, doc2) => {
      if (err2) return res.json({code: 1, msg: '后端出错了, 请刷新'})
      else {
        if (doc2) {
          doc.password = undefined
          doc.templates = undefined
          return res.json({code: 0, doc, refer: true})
        }
        else {
          DoctorInTeams.find({doctorId: doc._id}, (err3, doc3) => {
            if (doc3.length > 0) {
              doc.password = undefined
              doc.templates = undefined
              return res.json({code: 0, doc, refer: true})
            }
            else {
              doc.password = undefined
              doc.templates = undefined
              return res.json({code: 0, doc, refer: false})
            }
          })
        }
      }
    })
  })
})

app.post("/addtime", (req, res) => {
  const {userId} = req.body
  const time = new Date().getTime()
  Doctors.findOneAndUpdate({_id: userId}, {$set: {token: time}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1, token: time})
  })
})

app.get('/checktoken', (req, res) => {
  const {userId, token} = req.query
  Doctors.findById(userId, _filter,(err, doc) => {
    if (!err && doc) {
      if (!doc.token || doc.token === token) {
        return res.json({code: 1})
      } else {
        return res.json({code: 1})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/movement', (req, res) => {
  const {moveId} = req.query
  Movements.findById(moveId, {label: 0}, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/movements', (req, res) => {
  const {ids} = req.body
  Movements.find({"_id": {$in: ids}}, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/feedback', (req, res) => {
  const {subject, email, detail} = req.body
  const output = `
    <p>您收到一条用户反馈</p>
    <p>主题: ${subject}</p>
    <p>详情: ${detail}</p>
    <p>请您将回复发送给${email}</p>`

  const transporter = nodemailer.createTransport({
    host: "smtp.qy.tom.com",
    secureConnection: true,
    port: 465,
    auth: {
      user: 'pay@voin-cn.com',
      pass: 'voin.2019',
    }
  });


  let mailOptions = {
    from: 'pay@voin-cn.com',
    to: 'pay@voin-cn.com',
    subject: subject,
    html: output
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.json({code: 0, msg: "后端出错了，请刷新"});
    } else {
      return res.json({code: 1, doc: info})
    }
  });
})

app.get("/planbyid", (req, res) => {
  const {doctorId, clientId, planId} = req.query
  Plans.findOne({doctorId, clientId}, {plans: {$elemMatch: {_id: planId}}}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    if (doc1 && doc1.plans.length > 0) {
      return res.json({code: 1, moveList: doc1.plans[0].moveList, planId: doc1.plans[0]._id, therapyList: doc1.plans[0].therapyList,
        name: doc1.plans[0].name, summary: doc1.plans[0].summary, articles: doc1.plans[0].articles, addedParamCounts: doc1.plans[0].addedParamCounts,
        totalInEach: doc1.plans[0].totalInEach, periodDays: doc1.plans[0].periodDays, extra: doc1.plans[0].extra,
        allDay: doc1.plans[0].allDay, feedback: doc1.plans[0].feedback, completion: doc1.plans[0].completion,
        effect: doc1.plans[0].effect, description: doc1.plans[0].description, startDate: doc1.plans[0].startDate,
        start: doc1.plans[0].start, savedFeedback: doc1.plans[0].savedFeedback, empty: doc1.plans[0].empty
      })
    } else {
      return res.json({code: 0, msg: "无此计划"})
    }
  })
})

app.get("/planbyid2", (req, res) => {
  const doctorId = "643ffd2b79c48c7e184fb462"
  const {clientId, planId} = req.query
  const todayDate = req.query.date
  Plans.findOne({doctorId, clientId}, {plans: {$elemMatch: {_id: planId}}}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    if (doc1 && doc1.plans.length > 0) {
      const thisPlan = { moveList: doc1.plans[0].moveList,
        name: doc1.plans[0].name,
        totalInEach: doc1.plans[0].totalInEach, periodDays: doc1.plans[0].periodDays, extra: doc1.plans[0].extra,
        feedback: doc1.plans[0].feedback, hurt: doc1.plans[0].hurt,
        startDate: doc1.plans[0].startDate, start: doc1.plans[0].start
      }

      if (!thisPlan.start) {
        return res.json({code: 0, msg: "计划未开始"})
      }

      const ids = thisPlan.moveList.map(move => move.move)
      const betweens = thisPlan.moveList.map(move => move.between)
      const totalInEach = thisPlan.totalInEach
      const periodDays = thisPlan.periodDays
      const feedback = thisPlan.feedback
      const hurt = thisPlan.hurt
      const startDate = thisPlan.startDate
      const name = thisPlan.name


      Movements.find({"_id": {$in: ids}}, (err, moveResult) => {
        if (!err) {
          const planDetail = ids.map((move, index) => {
            const thisMove = moveResult.filter(aMove => ""+aMove._id === move)[0]
            return {
              _id: move,
              name: thisMove.name,
            }
          })
          const percents = Array(periodDays.reduce((a, b) => Number(a) + Number(b), 0)).fill("");
          const hurts = Array(periodDays.reduce((a, b) => Number(a) + Number(b), 0)).fill("");
          const date1 = new Date(
              parseInt(startDate.substring(0, 4), 10),
              parseInt(startDate.substring(4, 6), 10) - 1,
              parseInt(startDate.substring(6, 8), 10))
          feedback.forEach((feed) => {
            const dayAndNum = feed.split(" ")
            const day = dayAndNum[0]
            const num = dayAndNum[1]
            const date2 = new Date(
                parseInt(day.substring(0, 4), 10),
                parseInt(day.substring(4, 6), 10) - 1,
                parseInt(day.substring(6, 8), 10)
            )
            const diff = Math.floor((date2 - date1) / 86400000)
            percents.splice(diff, 1, num)
          })

          hurt.forEach((hurtInfo) => {
            const dayAndNum = hurtInfo.split(" ")
            const day = dayAndNum[0]
            const num = dayAndNum[1]
            const date2 = new Date(
                parseInt(day.substring(0, 4), 10),
                parseInt(day.substring(4, 6), 10) - 1,
                parseInt(day.substring(6, 8), 10)
            )
            const diff = Math.floor((date2 - date1) / 86400000)
            hurts.splice(diff, 1, num)
          })

          const allHurts = hurts.map((hurt, index) => {
            return [index+1, hurt === "" ? -1 : Number(hurt)]
          })


          const result = []
          for (let i = 0; i < totalInEach.length; i = i+1) {
            if (i === 0) {
              const periodPlan = planDetail.slice(0, totalInEach[i])
              result.push(periodPlan)
            } else {
              const periodPlan = planDetail.slice(totalInEach[i-1], totalInEach[i])
              result.push(periodPlan)
            }
          }

          const today = new Date(
              parseInt(todayDate.substring(0, 4), 10),
              parseInt(todayDate.substring(4, 6), 10) - 1,
              parseInt(todayDate.substring(6, 8), 10))

          const diffTime = Math.abs(today - date1);
          const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
          const ith = diffDays+1

          const allFeedback = result.map((arr, index1) => {
            const start = periodDays.slice(0, index1).reduce((a, b) => Number(a) + Number(b), 0)
            const end = periodDays.slice(0, index1+1).reduce((a, b) => Number(a) + Number(b), 0)
            const isEnd = ith - 1 >= end

            return arr.map((move, index2) => {
              const index = index1 === 0 ? index2 : totalInEach[index1-1] + index2
              const usefulPercents = [...percents.slice(
                  periodDays.slice(0, index1).reduce((a, b) => Number(a) + Number(b), 0),
                  periodDays.slice(0, index1+1).reduce((a, b) => Number(a) + Number(b), 0)
              )]

              const days = Number(periodDays[index1])
              const data = []
              for (let x = 1; x <= days; x++) {
                if (!betweens[index] || (x-1) % (Number(betweens[index])+1) === 0) {
                  if (usefulPercents[x-1]) {
                    let moveIndex = -1
                    for (let i = 0; i <= index2; i++) {
                      if (!betweens[i] || (x-1) % (Number(betweens[i])+1) === 0) {
                        moveIndex++
                      }
                    }
                    const pers = usefulPercents[x-1].split(",")
                    if (pers.length > moveIndex) {
                      data.push([x, Number(pers[moveIndex])])
                    } else {
                      data.push([x, -1])
                    }
                  } else {
                    data.push([x, -1])
                  }
                }
              }
              return {name: planDetail[index].name, data: data}
            })
          })



          return res.json({code: 1, name, feedback: allFeedback, periodDays, totalInEach, hurt: allHurts, ithDay: ith})

        } else {
          console.log(err)
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      })
    } else {
      return res.json({code: 0, msg: "无此计划"})
    }
  })
})

app.get("/homeworkbyid", (req, res) => {
  const {doctorId, titleId} = req.query
  HomeworkSubmits.findOne({doctorId, titleId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    return res.json({code: 1, moveList: doc1.plan.moveList, name: doc1.plan.name, articles: doc1.plan.articles,
      totalInEach: doc1.plan.totalInEach, periodDays: doc1.plan.periodDays, extra: doc1.plan.extra,
      therapyList: doc1.plan.therapyList, addedParamCounts: doc1.plan.addedParamCounts,
      proposalContent: doc1.plan.proposalContent, references: doc1.plan.references
    })
  })
})

app.get("/proposalbyid", (req, res) => {
  const {doctorId, titleId} = req.query
  HomeworkSubmits.findOne({doctorId, titleId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    return res.json({code: 1, name: doc1.plan.name, proposalContent: doc1.plan.proposalContent,
      references: doc1.plan.references
    })
  })
})

app.get('/templatebyid', (req, res) => {
  const {doctorId, templateId, teamId} = req.query
  if (doctorId === "expert") {
    Templates.findById(
      templateId,
      (err, doc) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        if (doc) {
          return res.json({code: 1, moveList: doc.moveList, templateId: doc._id,
            name: doc.name, summary: doc.summary, articles: doc.articles,
            totalInEach: doc.totalInEach, periodDays: doc.periodDays,
            therapyList: doc.therapyList, addedParamCounts: doc.addedParamCounts
          })
        } else {
          return res.json({code: 0, msg: "无此模版"})
        }
      }
    )
  } else if (doctorId === "team") {
    Teams.findById(
      teamId,
      {templates: {$elemMatch: {_id: templateId}}},
      (err, doc) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        if (doc && doc.templates.length > 0) {
          return res.json({code: 1, moveList: doc.templates[0].moveList, templateId: doc.templates[0]._id,
            name: doc.templates[0].name, summary: doc.templates[0].summary, articles: doc.templates[0].articles,
            totalInEach: doc.templates[0].totalInEach, periodDays: doc.templates[0].periodDays,
            therapyList: doc.templates[0].therapyList, addedParamCounts: doc.templates[0].addedParamCounts
          })
        } else {
          return res.json({code: 0, msg: "无此模版"})
        }
      }
    )  } else {
    Doctors.findById(
      doctorId,
      {templates: {$elemMatch: {_id: templateId}}},
      (err, doc) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        if (doc && doc.templates.length > 0) {
          return res.json({code: 1, moveList: doc.templates[0].moveList, templateId: doc.templates[0]._id,
            name: doc.templates[0].name, summary: doc.templates[0].summary, articles: doc.templates[0].articles,
            totalInEach: doc.templates[0].totalInEach, periodDays: doc.templates[0].periodDays,
            proposalContent: doc.templates[0].proposalContent, references: doc.templates[0].references,
            therapyList: doc.templates[0].therapyList, addedParamCounts: doc.templates[0].addedParamCounts,
          })
        } else {
          return res.json({code: 0, msg: "无此模版"})
        }
      }
    )
  }
})

app.get('/searchall', (req, res) => {
  const {first5} = req.query
  switch (first5.length) {
    case 5: {
      Movements.find({mp4: {"$exists" : true, "$ne" : ""}, $and: [
          {$or: [{"name":{$regex: first5[0]}}, {"keywords": first5[0]}]},
          {$or: [{"name":{$regex: first5[1]}}, {"keywords": first5[1]}]},
          {$or: [{"name":{$regex: first5[2]}}, {"keywords": first5[2]}]},
          {$or: [{"name":{$regex: first5[3]}}, {"keywords": first5[3]}]},
          {$or: [{"name":{$regex: first5[4]}}, {"keywords": first5[4]}]},
        ]
      }, (err, response) => {
        if (response) {
          return res.json(response)
        }
      })
    }
    break
    case 4: {
      Movements.find({mp4: {"$exists" : true, "$ne" : ""}, $and: [
          {$or: [{"name":{$regex: first5[0]}}, {"keywords": first5[0]}]},
          {$or: [{"name":{$regex: first5[1]}}, {"keywords": first5[1]}]},
          {$or: [{"name":{$regex: first5[2]}}, {"keywords": first5[2]}]},
          {$or: [{"name":{$regex: first5[3]}}, {"keywords": first5[3]}]},
        ]
      }, (err, response) => {
        if (response) {
          return res.json(response)
        }
      })
    }
    break
    case 3: {
      Movements.find({mp4: {"$exists" : true, "$ne" : ""}, $and: [
          {$or: [{"name":{$regex: first5[0]}}, {"keywords": first5[0]}]},
          {$or: [{"name":{$regex: first5[1]}}, {"keywords": first5[1]}]},
          {$or: [{"name":{$regex: first5[2]}}, {"keywords": first5[2]}]},
        ]
      }, (err, response) => {
        if (response) {
          return res.json(response)
        }
      })
    }
    break
    case 2: {
      Movements.find({mp4: {"$exists" : true, "$ne" : ""}, $and: [
          {$or: [{"name":{$regex: first5[0]}}, {"keywords": first5[0]}]},
          {$or: [{"name":{$regex: first5[1]}}, {"keywords": first5[1]}]},
        ]
      }, (err, response) => {
        if (response) {
          return res.json(response)
        }
      })
    }
    break
    case 1: {
      Movements.find({mp4: {"$exists" : true, "$ne" : ""}, $and: [
          {$or: [{"name":{$regex: first5[0]}}, {"keywords": first5[0]}]},
        ]
      }, (err, response) => {
        if (response) {
          return res.json(response)
        }
      })
    }
    break
    default: {
      return res.json({})
    }
  }

})

app.get('/searchcbynore', (req, res) => {
  const {value} = req.query
  Clients.find({$or:
      [
        {"name":{$regex: value}}, {"email": {$regex: value}}
      ]
  }, (err, response) => {
    if (response) {
      return res.json(response)
    }
  })

})

app.post('/updatepass', (req, res) => {
  const { doctorId, old, password } = req.body
  Doctors.findById(doctorId, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else if (!doc) return res.json({code: 0, msg: "无此医生记录"})
    else {
      if (doc.password === md5Password(old)) {
        Doctors.findByIdAndUpdate(doctorId, {
          $set: {
            password: md5Password(password)
          }
        }, err => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else return res.json({code: 1})
        })
      } else {
        return res.json({code: 0, msg: "原密码错误，请重新输入"})
      }
    }
  })

})

app.post('/complete', (req, res) => {
  const { doctorId, name, account, bankName, bankAccount, alipayAccount, room, phone, idnum, hospital, district, wechat,
    email, certification, occupation } = req.body
  Doctors.findOne({phone: phone}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    if (doc1 && doc1.phone && (""+doc1._id !== doctorId)) {
      return res.json({code: 0, msg: "该手机号已被注册，请重新输入"})
    } else {
      Doctors.findByIdAndUpdate(doctorId, {$set: {
          name, account, bankName, bankAccount, alipayAccount, room, phone, idnum, hospital, district, wechat,
          email, certification, occupation
        }}, {'new': true}, (err, doc) => {
        if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else if (!doc) return res.json({code: 0, msg: "无此医生记录"})
        else {
          return res.json({code: 1, doc})
        }
      })
    }
  })


})

app.post('/downloadtemplate', (req, res) => {
  const { templateId, doctorId, expertOrTeam, teamId, admin } = req.body
  if (expertOrTeam === "expert" || admin == 4) {
    Templates.findOne({_id: templateId}, (err, expertTemplate) => {
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      if (expertTemplate) {
        Doctors.findByIdAndUpdate(doctorId,
          {$push: {templates: {
                name: expertTemplate.name,
                summary: expertTemplate.summary,
                moveList: expertTemplate.moveList,
                periodDays: expertTemplate.periodDays,
                totalInEach: expertTemplate.totalInEach,
                articles: expertTemplate.articles,
                therapyList: expertTemplate.therapyList,
                addedParamCounts: expertTemplate.addedParamCounts
              }}}, (err) => {
            if (!err) {
              return res.json({code: 1})
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
      } else {
        return res.json({code: 0, msg: "此模版不存在，请刷新"})
      }
    })
  } else if (expertOrTeam === "team") {
    Teams.findById(
      teamId,
      {templates: {$elemMatch: {_id: templateId}}},
      (err, doc) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        if (doc && doc.templates.length > 0) {
          Doctors.findByIdAndUpdate(doctorId,
            {$push: {templates: {
                  name: doc.templates[0].name,
                  summary: doc.templates[0].summary,
                  moveList: doc.templates[0].moveList,
                  periodDays: doc.templates[0].periodDays,
                  totalInEach: doc.templates[0].totalInEach,
                  articles: doc.templates[0].articles,
                  therapyList: doc.templates[0].therapyList,
                  addedParamCounts: doc.templates[0].addedParamCounts
                }}}, (err) => {
              if (!err) {
                return res.json({code: 1})
              } else {
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
            })
        } else {
          return res.json({code: 0, msg: "此模版不存在，请刷新"})
        }
      }
    )
  }

})

app.get('/categories', (req, res) => {
  const doctorId = req.query.doctorId
  ClientCategories.find({doctorId}, null, {sort:{date: 1}}, (err, doc) => {
    if (err) return res.json({code: 0})
    else {
      let cats = doc.filter(cat => {
        if (cat.type === "second") {
          const parentId = cat.name.split("$")[0]
          return doc.filter(cat2 => ""+cat2._id === parentId).length > 0
        } else return true
      }).map(cat => {
        if (cat.type === "second") {
          const parentId = cat.name.split("$")[0]
          const parentName = doc.filter(cat2 => ""+cat2._id === parentId)[0].name
          const childName = cat.name.split("$")[1]
          return {
            _id: ""+cat._id,
            parentName: parentName,
            childName: childName,
            parentId: parentId,
            name: parentName + "\"父类\"" + childName,
            type: cat.type,
            date: cat.date
          }
        } else return {
          _id: ""+cat._id,
          name: cat.name,
          type: cat.type,
          date: cat.date,
          special: !!cat.special
        }
      })
      const diseaseIds = doc.filter(cat => cat.type === "disease").map(cat => cat.name)
      DiseaseCodes.find({_id: {$in: diseaseIds}}, (err, diseaseDoc) => {
        if (!err) {
          cats = cats.filter(cat => {
            if (cat.type === "disease") {
              const oneDisease = diseaseDoc.filter(det => ""+det._id === cat.name)
              return oneDisease.length > 0
            } else return true
          }).map(cat => {
            if (cat.type === "disease") {
              const oneDisease = diseaseDoc.filter(det => ""+det._id === cat.name)[0]
              return {
                _id: ""+cat._id,
                type: cat.type,
                date: cat.date,
                codeId: cat.name,
                name: oneDisease.code + " " + oneDisease.name
              }
            } else return cat
          })
          const operationIds = doc.filter(cat => cat.type === "operation").map(cat => cat.name)
          OperationCodes.find({_id: {$in: operationIds}}, (err, operationDoc) => {
            if (!err) {
              cats = cats.filter(cat => {
                if (cat.type === "operation") {
                  const oneOperation = operationDoc.filter(det => ""+det._id === cat.name)
                  return oneOperation.length > 0
                } else return true
              }).map(cat => {
                if (cat.type === "operation") {
                  const oneOperation = operationDoc.filter(det => ""+det._id === cat.name)[0]
                  return {
                    _id: ""+cat._id,
                    type: cat.type,
                    date: cat.date,
                    codeId: cat.name,
                    name: oneOperation.code + " " + oneOperation.name
                  }
                } else return cat
              })
              return res.json({code: 1, cats})
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        }
        else {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      })
    }
  })

})

app.get('/bothcode', (req, res) => {
  const {mode, value, page} = req.query
  const values = value.split(/[ ,;]+/)
  const length = values.length
  const document = mode === "7" ? DiseaseCodes : OperationCodes
  if (length >= 5) {
    document.find({$and: [
        {$or: [{code:{$regex: values[0]}}, {name: {$regex: values[0]}}]},
        {$or: [{code:{$regex: values[1]}}, {name: {$regex: values[1]}}]},
        {$or: [{code:{$regex: values[2]}}, {name: {$regex: values[2]}}]},
        {$or: [{code:{$regex: values[3]}}, {name: {$regex: values[3]}}]},
        {$or: [{code:{$regex: values[4]}}, {name: {$regex: values[4]}}]},
      ]
    }, (err, doc) => {
      if (err) {
        return res.json({code: 0, msg: err})
      } else {
        return res.json({code: 1, doc: doc.sort((a, b) => a.code.localeCompare(b.code)).slice(100*(page-1), 100*page), count: doc.length})
      }
    })
  } else if (length === 4) {
    document.find({$and: [
        {$or: [{code:{$regex: values[0]}}, {name: {$regex: values[0]}}]},
        {$or: [{code:{$regex: values[1]}}, {name: {$regex: values[1]}}]},
        {$or: [{code:{$regex: values[2]}}, {name: {$regex: values[2]}}]},
        {$or: [{code:{$regex: values[3]}}, {name: {$regex: values[3]}}]},
      ]
    }, (err, doc) => {
      if (err) {
        return res.json({code: 0, msg: err})
      } else {
        return res.json({code: 1, doc: doc.sort((a, b) => a.code.localeCompare(b.code)).slice(100*(page-1), 100*page), count: doc.length})
      }
    })
  } else if (length === 3) {
    document.find({$and: [
        {$or: [{code:{$regex: values[0]}}, {name: {$regex: values[0]}}]},
        {$or: [{code:{$regex: values[1]}}, {name: {$regex: values[1]}}]},
        {$or: [{code:{$regex: values[2]}}, {name: {$regex: values[2]}}]},
      ]
    }, (err, doc) => {
      if (err) {
        return res.json({code: 0, msg: err})
      } else {
        return res.json({code: 1, doc: doc.sort((a, b) => a.code.localeCompare(b.code)).slice(100*(page-1), 100*page), count: doc.length})
      }
    })
  } else if (length === 2) {
    document.find({$and: [
        {$or: [{code:{$regex: values[0]}}, {name: {$regex: values[0]}}]},
        {$or: [{code:{$regex: values[1]}}, {name: {$regex: values[1]}}]},
      ]
    }, (err, doc) => {
      if (err) {
        return res.json({code: 0, msg: err})
      } else {
        return res.json({code: 1, doc: doc.sort((a, b) => a.code.localeCompare(b.code)).slice(100*(page-1), 100*page), count: doc.length})
      }
    })
  } else if (length === 1) {
    document.find({$and: [
        {$or: [{code:{$regex: values[0]}}, {name: {$regex: values[0]}}]},
      ]
    }, (err, doc) => {
      if (err) {
        return res.json({code: 0, msg: err})
      } else {
        return res.json({code: 1, doc: doc.sort((a, b) => a.code.localeCompare(b.code)).slice(100*(page-1), 100*page), count: doc.length})
      }
    })
  } else {
    return res.json({code: 1, doc: [], count: 0})
  }
})

app.post('/codeids', (req, res) => {
  const {type, ids} = req.body
  if (type === "disease") {
    DiseaseCodes.find({_id: {$in: ids}}, (err, doc) => {
      if (!err) {
        return res.json({code: 1, doc})
      }
      else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else if (type === "operation") {
    OperationCodes.find({_id: {$in: ids}}, (err, doc) => {
      if (!err) {
        return res.json({code: 1, doc})
      }
      else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }

})

app.post('/addnewmove', (req, res) => {
  const {name, keywordsList, mp4, screenshot, desc, cats} = req.body
  Movements.find({}, null, {sort: {id: -1}, limit: 1}, (err, doc) => {
    if (err || doc.length < 1) {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新1"})
    } else {
      const max = doc[0].id
      const newId = Number(max)+1
      const newString = ""+newId
      Movements.create({name, id: newString, video: true, keywords: keywordsList, mp4, screenshot, info: desc}, (err1, doc1) => {
        if (err1) {
          console.log(err1)
          return res.json({code: 0, msg: "后端出错了，请刷新2"})
        } else {
          Categories.updateMany(
            {id: {$in: cats}},
            {$push: {
                contains: newString
              }},
            (err2, doc2) => {
              if (!err2) return res.json({code: 1})
              else {
                console.log(err2)
                return res.json({code: 0, msg: "后端出错了，请刷新3"})
              }
            }
          )
        }
      })
    }
  })
})


app.post('/addcategory', (req, res) => {
  const {doctorId, name, type} = req.body
  ClientCategories.create({doctorId, name, type, special: false}, (err, doc) => {
    if (!err) return res.json({code: 1, doc})
    else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/addcategories', (req, res) => {
  const {doctorId, names, type} = req.body
  const array = names.map(name => {
    return { doctorId, name, type, special: false }
  })
  ClientCategories.insertMany(array, (err, doc) => {
    if (!err) return res.json({code: 1, doc})
    else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/deletecategory', (req, res) => {
  const {doctorId, categoryId} = req.body
  ClientCategories.deleteOne({_id: categoryId}, (err, doc) => {
    if (!err) {
      Plans.updateMany(
        { doctorId },
        { $pull: { category: categoryId } },
        (err1, doc) => {
          if (!err1) {
            Doctors.update({_id: doctorId, "templates.category": categoryId},
              {$pull: {"templates.$.category": categoryId}}, { multi: true }, (err2, doc2) => {
                if (!err2) return res.json({code: 1})
                else return res.json({code: 0, msg: "后端出错了，请刷新"})
              })
          }
          else return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      )
    }
    else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/deletecategories', (req, res) => {
  const {doctorId, ids} = req.body
  ClientCategories.remove({_id: {$in: ids}}, (err, doc) => {
    if (!err) {
      Plans.update(
        { doctorId },
        { $pull: { category: {$in: ids} } },
        { multi: true },
        (err1, doc) => {
          if (!err1) return res.json({code: 1})
          else return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      )
    }
    else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/addclienttocategory', (req, res) => {
  const {doctorId, clientId, categoryIds, childDoctorId} = req.body
  Plans.findOneAndUpdate({doctorId, clientId}, {$set: {category: categoryIds, childDoctorId}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      return res.json({code: 1})
    }
  })
})

app.post('/addtemplatetocategory', (req, res) => {
  const {doctorId, templateId, categoryIds} = req.body
  Doctors.findOneAndUpdate({_id: doctorId, 'templates._id': templateId}, {$set: {'templates.$.category': categoryIds}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      return res.json({code: 1})
    }
  })
})

app.post('/clientsbyid', (req, res) => {
  const clients = req.body
  Clients.find({'_id': {$in: clients}}, (err, doc) => {
    if (!err) return res.json({code: 1, doc})
    else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/getarticle/:id', (req, res) => {
  const id = req.params.id
  Movements.findById(id, (err, doc) => {
    if (err) return res.json({code: 0})
    const data = fs.readFileSync(`${__dirname}/articles/${doc.id}-${doc.name}`);
    res.contentType("application/pdf");
    res.send(data);
  })

})

app.get('/getcourse/:id', (req, res) => {
  const id = req.params.id
  const data = fs.readFileSync(`${__dirname}/courses/${id}`);
  res.contentType("application/pdf");
  res.send(data);
})

app.get('/getresource/:id', (req, res) => {
  const id = req.params.id
  let data = fs.readFileSync(`${__dirname}/resources/${id}`);
  if (id === "足踝疾病的治疗管理.pdf") {
    data = fs.readFileSync(`${__dirname}/newResources/${id}`);
  }
  res.contentType("application/pdf");
  res.send(data);
})



app.post('/connect', (req, res) => {
  const {clientId, clientName, phone, doctorUsername, mode, childDoctorId} = req.body
  Clients.findById(clientId, (err, clientDoc) => {
    if (!err) {
      if (clientDoc) {
        Doctors.findOne({username: doctorUsername}, (err, doctorDoc) => {
          if (!err) {
            if (doctorDoc) {
              Plans.find({clientId: clientId, doctorId: doctorDoc._id}, (err, planDoc) => {
                if (!err) {
                  if (planDoc.length > 0) {
                    return res.json({code: 2})
                  } else {
                    if (mode === 2) {
                      if (childDoctorId) {
                        Plans.create({doctorId: doctorDoc._id, clientId, plans: [], category: [], childDoctorId: childDoctorId,
                          recognized: false, read: false}, (err, doc) => {
                          if (!err) {
                            if (clientName) {
                              Clients.findOneAndUpdate({_id: clientId}, {$set: {name: clientName, phone}}, (err, doc) => {
                                if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                                else {
                                  return res.json({code: 1})
                                }
                              })
                            } else {
                              return res.json({code: 1})
                            }
                          } else {
                            return res.json({code: 0, msg: "后端出错了，请刷新"})
                          }
                        })
                      } else {
                        return res.json({code: 0, msg: "请选择子医生"})
                      }
                    } else {
                      if (doctorDoc.childDoctors && doctorDoc.childDoctors.length > 0) {
                        return res.json({code: 5, childDoctors: doctorDoc.childDoctors})
                      } else {
                        Plans.create({doctorId: doctorDoc._id, clientId, plans: [], category: [],
                          recognized: false, read: false}, (err, doc) => {
                          if (!err) {
                            if (clientName) {
                              Clients.findOneAndUpdate({_id: clientId}, {$set: {name: clientName, phone}}, (err, doc) => {
                                if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                                else {
                                  return res.json({code: 1})
                                }
                              })
                            } else {
                              return res.json({code: 1})
                            }
                          } else {
                            return res.json({code: 0, msg: "后端出错了，请刷新"})
                          }
                        })
                      }
                    }
                  }
                } else {
                  return res.json({code: 0, msg: "后端出错了，请刷新"})
                }
              })
            } else {
              return res.json({code: 3})
            }
          } else {
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      } else {
        return res.json({code: 4})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/connecteddoctors', (req, res) => {
  const {clientId} = req.query
  Clients.findById(clientId, (err, doc) => {
    if (!err) {
      if (doc) {
        Plans.find({clientId}, (err, response) => {
          if (!err) {
            const dids = response.map(plan => plan.doctorId)
            Doctors.find({'_id': {$in: dids}}, (err, docs) => {
              if (err) {
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              } else {
                return res.json({code: 1, result: docs.map(doc => {
                    const chosen = response.filter(plan => plan.doctorId === ""+doc._id)
                    return {id: doc._id, name: doc.name, username: doc.username, length: chosen[0].plans.length, free30: !!doc.free30}
                  })
                })
              }
            })
          } else {
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      } else {
        return res.json({code: 2})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/isconnected', (req, res) => {
  const {openid, username} = req.query
  Clients.findOne({openid}, (err, clientDoc) => {
    if (!err) {
      if (clientDoc) {
        Doctors.findOne({username}, (err, doctorDoc) => {
          if (!err) {
            if (doctorDoc) {
              Plans.find({clientId: ""+clientDoc._id, doctorId: ""+doctorDoc._id}, (err, plan) => {
                if (!err) {
                  if (plan && plan.length > 0) {
                    return res.json({code: 1})
                  } else {
                    return res.json({code: 2})
                  }
                } else {
                  return res.json({code: 0, msg: "后端出错了，请刷新"})
                }
              })
            } else {
              return res.json({code: 4})
            }
          } else {
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
        })
      } else {
        return res.json({code: 3})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/planlist', (req, res) => {
  const { clientId, doctorId, date} = req.query
  Plans.find({clientId, doctorId}, (err, plan) => {
    if (!err) {
      if (!plan || plan.length === 0) {
        return res.json({code: 2})
      } else {
        const counts = []
        let ids = []
        const plans = plan[0].plans.map(realPlan => {
          let diff = 0
          let totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
          totalDays = totalDays === 0 ? realPlan.allDay : totalDays
          let finished = false
          if (realPlan.start && realPlan.startDate) {
            const date1 = new Date(
              parseInt(realPlan.startDate.substring(0, 4), 10),
              parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
              parseInt(realPlan.startDate.substring(6, 8), 10))
            const date2 = new Date(
              parseInt(date.substring(0, 4), 10),
              parseInt(date.substring(4, 6), 10) - 1,
              parseInt(date.substring(6, 8), 10)
            )
            diff = Math.floor((date2 - date1) / 86400000)
            if (diff >= totalDays) {
              diff = totalDays - 1
              finished = true
            }
          } else {
            diff = 0
          }
          let index = 0
          let moveDiff = diff
          while (moveDiff >= parseInt(realPlan.periodDays[index], 10)) {
            moveDiff = moveDiff - parseInt(realPlan.periodDays[index], 10)
            index = index + 1
          }
          const start = index === 0 ? 0 : realPlan.totalInEach[index - 1]
          const rawList = realPlan.moveList.slice(start, realPlan.totalInEach[index])
          const realMoveList = rawList.filter(move => !move.bar).filter(move => {
            if (move.between) return diff % (Number(move.between) + 1) === 0
            else return true
          })
          ids = ids.concat(realMoveList.map(move => move.move))
          counts.push(realMoveList.length)
          return {
            name: realPlan.name,
            date: realPlan.date,
            empty: realPlan.empty ? realPlan.empty : false,
            paidTime: realPlan.paidTime ? realPlan.paidTime : "",
            requestRefund: realPlan.requestRefund ? realPlan.requestRefund : false,
            numOfMoves: realPlan.moveList.filter(move => !move.bar).length,
            start: realPlan.start,
            paid: realPlan.paid === undefined ? false : realPlan.paid,
            free30: !!realPlan.free30,
            id: realPlan._id,
            totalDays: totalDays,
            diff: realPlan.start && realPlan.startDate ? diff + 1 : 0,
            feedback: !!realPlan.description,
            finished,
            doctorId: doctorId,
            extra: "请您在家人陪伴下完成练习，如在练习过程中有任何不适，请立即停止并向医生咨询。"+(realPlan.extra ? realPlan.extra : ""),
          }
        })
        Movements.find({ _id: {$in: ids}}, function (err, result) {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else {
            const resultIds = result.map(move => ""+move._id)
            for (let i = 0; i < counts.length; i++) {
              let iurl = []
              for (let j = 0; j < counts[i]; j++) {
                const index = counts.slice(0, i).reduce((a, b) => a + b, 0) + j
                iurl.push(resultIds.indexOf(ids[index]) < 0 ? "" : result[resultIds.indexOf(ids[index])].screenshot)
              }
              plans[i].urls = iurl
            }
            return res.json({code: 1, doc: plans.reverse()})
          }
        })
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/planlist2', (req, res) => {
  const doctorId = "643ffd2b79c48c7e184fb462"
  const fromDoctorId = "5f23a8e26266b8739f89de86"
  const { clientId, date} = req.query
  if (!clientId) {
    let ids = []
    Doctors.findOne({_id: fromDoctorId}, {'password':0, '__v':0}, {sort: {date: 1}}, (err, doc) => {
      if (!err) {
        const allTemplates = doc.templates.map(template => {
          const urlid = template.moveList[0].move
          const _id = ""+template._id
          const name = template.name
          const firstId = template.moveList[0].move
          const category = template.category ? template.category : []
          const extra = template.summary
          const added = false
          const totalDays = template.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
          return {_id, urlid, name, category, added, firstId, totalDays, extra}
        })
        const allNewTemplates = allTemplates.filter(template => !template.added && template.totalDays && template.totalDays > 0)
        const allNewTemplatesInRange = allNewTemplates.filter(template => {
          return template.category.indexOf("6449e6b9d2fa3e7ec86f9c6a") >= 0
        })
        const shuffledTemplates = allNewTemplatesInRange
            .map(value => ({ value, sort: Math.random() }))
            .sort((a, b) => a.sort - b.sort)
            .map(({ value }) => value)
        const returnedTemplates = shuffledTemplates.length > 4 ? shuffledTemplates.slice(0, 4) : shuffledTemplates
        const templateFirstIds = returnedTemplates.map(template => template.firstId)
        ids = ids.concat(templateFirstIds)
        Movements.find({ _id: {$in: ids}}, function (err, result) {
          if (err) {
            console.log(2)
            console.log(err)
            return res.json({code: 0, msg: "后端出错了，请刷新"})
          }
          else {
            const resultIds = result.map(move => ""+move._id)
            for (let i = 0; i < returnedTemplates.length; i++) {
              const index = resultIds.indexOf(returnedTemplates[i].urlid)
              returnedTemplates[i].url = result[index].screenshot
              returnedTemplates[i].urlid = undefined
              returnedTemplates[i].category = undefined
              returnedTemplates[i].added = undefined
              returnedTemplates[i].firstId = undefined
            }
            console.log(3)
            return res.json({code: 1, doc: [], recommend: returnedTemplates})
          }
        })
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else {
    Plans.findOne({clientId, doctorId}, (err, plan) => {
      if (!err) {
        console.log(plan)
        if (!plan) {
          Plans.create({doctorId: doctorId, clientId: clientId, plans: [], category: [],
            recognized: false, read: false}, (err, doc) => {
            if (!err) {
              let ids = []
              Doctors.findOne({_id: fromDoctorId}, {'password':0, '__v':0}, {sort: {date: 1}}, (err, doc) => {
                if (!err) {
                  const allTemplates = doc.templates.map(template => {
                    const urlid = template.moveList[0].move
                    const _id = ""+template._id
                    const name = template.name
                    const firstId = template.moveList[0].move
                    const category = template.category ? template.category : []
                    const extra = template.summary
                    const added = false
                    const totalDays = template.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
                    return {_id, urlid, name, category, added, firstId, totalDays, extra}
                  })
                  const allNewTemplates = allTemplates.filter(template => !template.added && template.totalDays && template.totalDays > 0)
                  const allNewTemplatesInRange = allNewTemplates.filter(template => {
                    return template.category.indexOf("6449e6b9d2fa3e7ec86f9c6a") >= 0
                  })
                  const shuffledTemplates = allNewTemplatesInRange
                    .map(value => ({ value, sort: Math.random() }))
                    .sort((a, b) => a.sort - b.sort)
                    .map(({ value }) => value)
                  const returnedTemplates = shuffledTemplates.length > 4 ? shuffledTemplates.slice(0, 4) : shuffledTemplates
                  const templateFirstIds = returnedTemplates.map(template => template.firstId)
                  ids = ids.concat(templateFirstIds)
                  Movements.find({ _id: {$in: ids}}, function (err, result) {
                    if (err) {
                      console.log(2)
                      console.log(err)
                      return res.json({code: 0, msg: "后端出错了，请刷新"})
                    }
                    else {
                      const resultIds = result.map(move => ""+move._id)
                      for (let i = 0; i < returnedTemplates.length; i++) {
                        const index = resultIds.indexOf(returnedTemplates[i].urlid)
                        returnedTemplates[i].url = result[index].screenshot
                        returnedTemplates[i].urlid = undefined
                        returnedTemplates[i].category = undefined
                        returnedTemplates[i].added = undefined
                        returnedTemplates[i].firstId = undefined
                      }
                      console.log(3)
                      return res.json({code: 1, doc: [], recommend: returnedTemplates})
                    }
                  })
                } else {
                  return res.json({code: 0, msg: "后端出错了，请刷新"})
                }
              })
            } else {
              console.log(1)
              console.log(err)
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        } else {
          const counts = []
          let ids = []
          const plans = plan.plans.map(realPlan => {
            let diff = 0
            let totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
            totalDays = totalDays === 0 ? realPlan.allDay : totalDays
            let finished = false
            if (realPlan.start && realPlan.startDate) {
              const date1 = new Date(
                  parseInt(realPlan.startDate.substring(0, 4), 10),
                  parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
                  parseInt(realPlan.startDate.substring(6, 8), 10))
              const date2 = new Date(
                  parseInt(date.substring(0, 4), 10),
                  parseInt(date.substring(4, 6), 10) - 1,
                  parseInt(date.substring(6, 8), 10)
              )
              diff = Math.floor((date2 - date1) / 86400000)
              if (diff >= totalDays) {
                diff = totalDays - 1
                finished = true
              }
            } else {
              diff = 0
            }
            let index = 0
            let moveDiff = diff
            while (moveDiff >= parseInt(realPlan.periodDays[index], 10)) {
              moveDiff = moveDiff - parseInt(realPlan.periodDays[index], 10)
              index = index + 1
            }
            const start = index === 0 ? 0 : realPlan.totalInEach[index - 1]
            const rawList = realPlan.moveList.slice(start, realPlan.totalInEach[index])
            const realMoveList = rawList.filter(move => !move.bar).filter(move => {
              if (move.between) return diff % (Number(move.between) + 1) === 0
              else return true
            })
            ids = ids.concat(realMoveList.map(move => move.move))
            counts.push(realMoveList.length)
            return {
              name: realPlan.name,
              numOfMoves: realPlan.moveList.filter(move => !move.bar).length,
              start: realPlan.start,
              id: realPlan._id,
              totalDays: totalDays,
              diff: realPlan.start && realPlan.startDate ? diff + 1 : 0,
              templateId: realPlan.templateId,
              finished: finished,
              extra: "注意：此方案为通用指导性方案，仅供参考，如需更专业的方案，请定制以符合您的实际需求。在家中进行练习时，请确保有家人陪伴。如在练习时感到不适，请立即停止并咨询医生。"+(realPlan.extra ? realPlan.extra : ""),
            }
          })

          const templateIds = plan && plan.plans ? plan.plans.map(onePlan => onePlan.templateId) : []
          Doctors.findOne({_id: fromDoctorId}, {'password':0, '__v':0}, {sort: {date: 1}}, (err, doc) => {
            if (!err) {
              const allTemplates = doc.templates.map(template => {
                const urlid = template.moveList[0].move
                const _id = ""+template._id
                const name = template.name
                const firstId = template.moveList[0].move
                const category = template.category ? template.category : []
                const extra = template.summary
                const added = templateIds.indexOf(_id) >= 0
                const totalDays = template.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
                return {_id, urlid, name, category, added, firstId, totalDays, extra}
              })
              const allNewTemplates = allTemplates.filter(template => !template.added && template.totalDays && template.totalDays > 0)
              const allNewTemplatesInRange = allNewTemplates.filter(template => {
                return template.category.indexOf("6449e6b9d2fa3e7ec86f9c6a") >= 0
              })
              const shuffledTemplates = allNewTemplatesInRange
                  .map(value => ({ value, sort: Math.random() }))
                  .sort((a, b) => a.sort - b.sort)
                  .map(({ value }) => value)
              const returnedTemplates = shuffledTemplates.length > 4 ? shuffledTemplates.slice(0, 4) : shuffledTemplates
              const templateFirstIds = returnedTemplates.map(template => template.firstId)
              ids = ids.concat(templateFirstIds)
              Movements.find({ _id: {$in: ids}}, function (err, result) {
                if (err) {
                  console.log(2)
                  console.log(err)
                  return res.json({code: 0, msg: "后端出错了，请刷新"})
                }
                else {
                  const resultIds = result.map(move => ""+move._id)
                  for (let i = 0; i < counts.length; i++) {
                    let iurl = []
                    for (let j = 0; j < counts[i]; j++) {
                      const index = counts.slice(0, i).reduce((a, b) => a + b, 0) + j
                      iurl.push(resultIds.indexOf(ids[index]) < 0 ? "" : result[resultIds.indexOf(ids[index])].screenshot)
                    }
                    plans[i].urls = iurl
                  }
                  for (let i = 0; i < returnedTemplates.length; i++) {
                    const index = resultIds.indexOf(returnedTemplates[i].urlid)
                    returnedTemplates[i].url = result[index].screenshot
                    returnedTemplates[i].urlid = undefined
                    returnedTemplates[i].category = undefined
                    returnedTemplates[i].added = undefined
                    returnedTemplates[i].firstId = undefined
                  }
                  console.log(3)
                  return res.json({code: 1, doc: plans.reverse(), recommend: returnedTemplates})
                }
              })
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        }
      } else {
        console.log(3)
        console.log(err)
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }
})


app.get('/realplan', (req, res) => {
  const {clientId, doctorId, planId, date} = req.query
  Plans.find({clientId, doctorId, 'plans._id': planId},{
    'plans.$' : 1,
  }, (err, plan) => {
    if (!err) {
      if (plan) {
        const realPlan = plan[0].plans[0]
        if (realPlan.start) {
          const date1 = new Date(
            parseInt(realPlan.startDate.substring(0, 4), 10),
            parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
            parseInt(realPlan.startDate.substring(6, 8), 10))
          const date2 = new Date(
            parseInt(date.substring(0, 4), 10),
            parseInt(date.substring(4, 6), 10) - 1,
            parseInt(date.substring(6, 8), 10)
          )
          let diff = Math.floor((date2-date1) / 86400000)
          const totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
          if (diff >= totalDays) return res.json({code: 4})
          let index = 0
          while (diff >= parseInt(realPlan.periodDays[index], 10)) {
            diff = diff - parseInt(realPlan.periodDays[index], 10)
            index = index + 1
          }
          const start = index === 0 ? 0 : realPlan.totalInEach[index-1]
          const rawList = realPlan.moveList.slice(start, realPlan.totalInEach[index])
          const realMoveList = rawList.filter(move => !move.bar).filter(move => {
            if (move.between) return diff % (Number(move.between) + 1) === 0
            else return true
          })
          const realIds = realMoveList.map(move => move.move)
          Movements.find({ _id: { $in: realIds } }, (err, moveDoc) => {
            if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              moveDoc.sort((a, b) =>  {
                return realIds.indexOf(a._id) - realIds.indexOf(b._id);
              });
              const finalMoveList = realMoveList.map((move, index) => {
                return {
                  info: realMoveList[index].info,
                  time: realMoveList[index].time,
                  timesPerSet: realMoveList[index].timesPerSet,
                  set: realMoveList[index].set,
                  parameter1: realMoveList[index].parameter1,
                  parameter2: realMoveList[index].parameter2,
                  value1: realMoveList[index].value1,
                  value2: realMoveList[index].value2,
                  name: moveDoc[index].name,
                  src: moveDoc[index].mp4
                }
              })
              return res.json({code: 1, doc: {
                  start: realPlan.start,
                  days: realPlan.periodDays[index],
                  articles: realPlan.articles,
                  moveList: finalMoveList,
                  name: realPlan.name,
                }})
            }
          })
        } else {
          return res.json({code: 2})
        }
      } else {
        return res.json({code: 3})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/realplan2', (req, res) => {
  const doctorId = "643ffd2b79c48c7e184fb462"
  const {clientId, planId, date} = req.query
  Plans.find({clientId, doctorId, 'plans._id': planId},{
    'plans.$' : 1,
  }, (err, plan) => {
    if (!err) {
      if (plan) {
        const realPlan = plan[0].plans[0]
        if (realPlan.start) {
          const date1 = new Date(
            parseInt(realPlan.startDate.substring(0, 4), 10),
            parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
            parseInt(realPlan.startDate.substring(6, 8), 10))
          const date2 = new Date(
            parseInt(date.substring(0, 4), 10),
            parseInt(date.substring(4, 6), 10) - 1,
            parseInt(date.substring(6, 8), 10)
          )
          let diff = Math.floor((date2-date1) / 86400000)
          const totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
          if (diff >= totalDays) return res.json({code: 4})
          let index = 0
          while (diff >= parseInt(realPlan.periodDays[index], 10)) {
            diff = diff - parseInt(realPlan.periodDays[index], 10)
            index = index + 1
          }
          const start = index === 0 ? 0 : realPlan.totalInEach[index-1]
          const rawList = realPlan.moveList.slice(start, realPlan.totalInEach[index])
          const realMoveList = rawList.filter(move => !move.bar).filter(move => {
            if (move.between) return diff % (Number(move.between) + 1) === 0
            else return true
          })
          const realIds = realMoveList.map(move => move.move)
          Movements.find({ _id: { $in: realIds } }, (err, moveDoc) => {
            if (err) {
              console.log(err)
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
            else {
              moveDoc.sort((a, b) =>  {
                return realIds.indexOf(a._id) - realIds.indexOf(b._id);
              });
              const finalMoveList = realMoveList.map((move, index) => {
                return {
                  info: realMoveList[index].info,
                  time: realMoveList[index].time,
                  timesPerSet: realMoveList[index].timesPerSet,
                  set: realMoveList[index].set,
                  parameter1: realMoveList[index].parameter1,
                  parameter2: realMoveList[index].parameter2,
                  value1: realMoveList[index].value1,
                  value2: realMoveList[index].value2,
                  name: moveDoc[index].name,
                  url: moveDoc[index].screenshot,
                  src: moveDoc[index].mp4
                }
              })
              return res.json({code: 1, doc: {
                  start: realPlan.start,
                  days: realPlan.periodDays[index],
                  articles: realPlan.articles,
                  moveList: finalMoveList,
                  name: realPlan.name,
                }})
            }
          })
        } else {
          return res.json({code: 2})
        }
      } else {
        return res.json({code: 3})
      }
    } else {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})


app.post('/startplan', (req, res) => {
  const { clientId, doctorId, planId, date} = req.body
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $set: {
      'plans.$.start': true,
      'plans.$.startDate': date
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/startplan2', (req, res) => {
  const doctorId = "643ffd2b79c48c7e184fb462"
  const { clientId, planId, date} = req.body
  console.log(clientId + " " + planId + " " + date)
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $set: {
      'plans.$.start': true,
      'plans.$.startDate': date
    }
  }, (err, doc) => {
    if (err) {
      console.log(doc)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
    else {
      return res.json({code: 1})
    }
  })
})

app.post('/planfeedback', (req, res) => {
  const { clientId, doctorId, planId, completion, effect, description} = req.body
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $set: {
      'plans.$.completion': completion,
      'plans.$.effect': effect,
      'plans.$.description': description
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/planfeedback2', (req, res) => {
  const doctorId = "643ffd2b79c48c7e184fb462"
  const { clientId, planId, completion} = req.body
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $set: {
      'plans.$.completion': completion
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/clientdetail', (req, res) => {
  const { clientId, name, sex, age, idnum, history, smoke, description, phone} = req.body
  Clients.findOneAndUpdate({"_id": clientId}, {
    $set: {
      name, sex, age, idnum, history, smoke, description, phone
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/clientdetail2', (req, res) => {
  const { clientId, username, name, wechat, email, avatar, address} = req.body
  const change = {}
  if (username) change.username = username
  if (name) change.name = name
  if (wechat) change.wechat = wechat
  if (email) change.email = email
  if (avatar) change.avatar = avatar
  if (address) change.address = address
  Clients.findOneAndUpdate({"_id": clientId}, {
    $set: change
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})




app.post('/editclientdetail', (req, res) => {
  const { doctorId, clientId, newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription} = req.body
  Clients.findOneAndUpdate({"_id": clientId}, {
    $set: {
      sex: newSex, age: newAge, history: newRealhistory, smoke: newSmoke, phone: newPhone
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      Plans.findOneAndUpdate({doctorId, clientId}, {$set: {description: newDescription}}, (err1, doc1) => {
        if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else return res.json({code: 1})
      })
    }
  })
})

app.get('/prepay', (req, res) => {
  const free = ["oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5EiBsMSL_bwWmzxeAArLqDM", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
  const {openid} = req.query
  const addOrderUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
  const client_ip = "39.105.79.68";
  const body = "沃衍健康康复计划费用"; // 商品简单描述
  const mch_id = "1488387462"; // 商户号，申请微信支付，腾讯给的商户号
  const nonce_str = randomstring.generate(32); // 随机字符串
  const out_trade_no = "" + new Date().getTime() + Math.floor(Math.random() * 10); //商户订单号
  const total_fee = free.indexOf(openid) >= 0 ? 1 : 29900 //支付金额，单位：分
  let sign = "";
  const notify_url = "http://123.456.789"; //异步接收微信支付结果通知的回调地址
  const trade_type = "JSAPI"; // 交易类型
  const key = "12b07361e8fa829e7cd9bedd3e0c2ff4"
  const timeStamp = Math.floor(Date.now() / 1000)
  const stringA = `appid=${appid}&body=${body}&mch_id=${mch_id}&nonce_str=${nonce_str}&notify_url=${notify_url}&openid=${openid}&out_trade_no=${out_trade_no}&spbill_create_ip=${client_ip}&total_fee=${total_fee}&trade_type=${trade_type}`;
  const stringSighTemp = stringA + "&key=Yixing0456cnliXiaomuyu038412cnli"; //32位的商户key,自定义的，这里为了隐私，我用的特殊符号给你们展示
  sign = utils.md5(stringSighTemp).toUpperCase();
  const xml = `<xml>      
    <appid>${appid}</appid>      
    <body>${body}</body>      
    <mch_id>${mch_id}</mch_id>      
    <nonce_str>${nonce_str}</nonce_str>      
    <notify_url>${notify_url}</notify_url>     
    <openid>${openid}</openid>     
    <out_trade_no>${out_trade_no}</out_trade_no>    
    <spbill_create_ip>${client_ip}</spbill_create_ip>     
    <total_fee>${total_fee}</total_fee>  
    <trade_type>${trade_type}</trade_type>    
    <sign>${sign}</sign>    
    </xml>`;
  axios({
    method: 'post',
    url: addOrderUrl,
    data: xml,
    responseType: 'text/xml',
    headers: {
      'Content-Type': 'text/xml'
    }
  }).then(response => {
    parseString(response.data, function (err, result) {
      if (err) {
        console.log(err)
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      } else {
        const beforeSign = "appId="+appid+"&nonceStr="+result.xml.nonce_str[0]+"&package="
          +"prepay_id="+result.xml.prepay_id[0]+"&signType=MD5&timeStamp="+timeStamp+"&key=Yixing0456cnliXiaomuyu038412cnli"
        return res.json({
          // response: result.xml,
          code: 1,
          nonceStr: result.xml.nonce_str[0], out_trade_no: out_trade_no,
          package: "prepay_id="+result.xml.prepay_id[0],
          paySign: utils.md5(beforeSign).toUpperCase(),
          timeStamp
        })
      }
    });

  }).catch(err => {
    return res.json({code: 0, msg: "后端出错了，请刷新"})
  })
})

app.get('/prepay2', (req, res) => {
  const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5EiBsMSL_bwWmzxeAArLqDM", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
  const {openid, mode, templateId} = req.query
  const addOrderUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
  const client_ip = "39.105.79.68";
  const body = "沃衍健康康复计划费用"; // 商品简单描述
  // const mch_id = "1613713506"; // 商户号，申请微信支付，腾讯给的商户号
  const mch_id = "1488387462"; // 商户号，申请微信支付，腾讯给的商户号
  const nonce_str = randomstring.generate(32); // 随机字符串
  const out_trade_no = "" + new Date().getTime() + Math.floor(Math.random() * 10); //商户订单号
  let total_fee = free.indexOf(openid) >= 0 ? 10 : !!mode ? 1990 : 365000 //支付金额，单位：分
  if (templateId) {
    total_fee = free.indexOf(openid) >= 0 ? 10 : 9900
  }
  let sign = "";
  const notify_url = "http://123.456.789"; //异步接收微信支付结果通知的回调地址
  const trade_type = "JSAPI"; // 交易类型
  const timeStamp = Math.floor(Date.now() / 1000)
  const stringA = `appid=${appid2}&body=${body}&mch_id=${mch_id}&nonce_str=${nonce_str}&notify_url=${notify_url}&openid=${openid}&out_trade_no=${out_trade_no}&spbill_create_ip=${client_ip}&total_fee=${total_fee}&trade_type=${trade_type}`;
  const stringSighTemp = stringA + "&key=Yixing0456cnliXiaomuyu038412cnli"; //32位的商户key,自定义的，这里为了隐私，我用的特殊符号给你们展示
  sign = utils.md5(stringSighTemp).toUpperCase();
  const xml = `<xml>      
    <appid>${appid2}</appid>      
    <body>${body}</body>      
    <mch_id>${mch_id}</mch_id>      
    <nonce_str>${nonce_str}</nonce_str>      
    <notify_url>${notify_url}</notify_url>     
    <openid>${openid}</openid>     
    <out_trade_no>${out_trade_no}</out_trade_no>    
    <spbill_create_ip>${client_ip}</spbill_create_ip>     
    <total_fee>${total_fee}</total_fee>  
    <trade_type>${trade_type}</trade_type>    
    <sign>${sign}</sign>    
    </xml>`;
  axios({
    method: 'post',
    url: addOrderUrl,
    data: xml,
    responseType: 'text/xml',
    headers: {
      'Content-Type': 'text/xml'
    }
  }).then(response => {
    parseString(response.data, function (err, result) {
      if (err) {
        console.log(err)
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      } else {
        console.log(result)
        const beforeSign = "appId="+appid2+"&nonceStr="+result.xml.nonce_str[0]+"&package="
          +"prepay_id="+result.xml.prepay_id[0]+"&signType=MD5&timeStamp="+timeStamp+"&key=Yixing0456cnliXiaomuyu038412cnli"
        return res.json({
          // response: result.xml,
          code: 1,
          nonceStr: result.xml.nonce_str[0], out_trade_no: out_trade_no,
          package: "prepay_id="+result.xml.prepay_id[0],
          paySign: utils.md5(beforeSign).toUpperCase(),
          timeStamp
        })
      }
    });

  }).catch(err => {
    console.log(err)
    return res.json({code: 0, msg: "后端出错了，请刷新"})
  })
})

app.post('/afterpay', (req, res) => {
  const {clientId, doctorId, planId} = req.body
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $set: {
      'plans.$.paid': true
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const free = ["oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
      const price = free.indexOf(clientId) >= 0 ? 0.01: 299
      Payments.create({clientId, doctorId, planId, price}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
            Clients.findOne({_id: clientId}, (errCli, docCli) => {
              if (docDoc && docCli) {
                const paramsDoc = {
                  "RegionId": "cn-hangzhou",
                  "PhoneNumbers": ""+docDoc.phone,
                  "SignName": "沃衍健康通知",
                  "TemplateCode": "SMS_189620427",
                  "TemplateParam": "{\"name\":\""+docCli.name+"\"}"
                }
                const paramsCli = {
                  "RegionId": "cn-hangzhou",
                  "PhoneNumbers": ""+docCli.phone,
                  "SignName": "沃衍健康通知",
                  "TemplateCode": "SMS_189610569",
                  "TemplateParam": "{\"day\":\""+doc.plans[0].periodDays
                    .map(day => parseInt(day, 10)).reduce((a,b) => a+b)+"\"}"
                }

                sms.request('SendSms', paramsDoc, {method: 'POST'}).then((result1) => {
                  sms.request('SendSms', paramsCli, {method: 'POST'}).then((result2) => {
                    return res.json({code: 1})
                  }, (ex) => {
                    console.log("短信发送请求出错了")
                    return res.json({code: 1})
                  })
                }, (ex) => {
                  console.log("短信发送请求出错了")
                  return res.json({code: 1})
                })
              } else {
                return res.json({code: 1})
              }
            })
          })
        }
      })
    }
  })
})

app.post('/afterpay2', (req, res) => {
  const {clientId, templateId, out_trade_no} = req.body
  console.log("afterpay2 " + clientId + " " + templateId + " " + out_trade_no)
  const doctorId = "643ffd2b79c48c7e184fb462"
  if (templateId) {
    const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs","o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
    const price = free.indexOf(clientId) >= 0 ? 0.1: 99

    let fromDoctorId = req.body.doctorId
    if (!fromDoctorId) fromDoctorId = "5f23a8e26266b8739f89de86"
    const doctorId = "643ffd2b79c48c7e184fb462"
    Doctors.findOne({
      _id: fromDoctorId,
      'templates._id': templateId,
    }, {
      'templates.$' : 1,
    }, (err, doc) => {
      if (!err) {
        if (doc) {
          const templates = doc.templates
          if (templates.length > 0 ) {
            const template = templates[0]
            Plans.findOne({clientId, doctorId}, (err1, doc1) => {
              if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
              else {
                if (doc1) {
                  Plans.findOneAndUpdate({clientId, doctorId}, {
                    $push: {
                      plans: {
                        name: template.name,
                        extra: template.summary,
                        start: false,
                        moveList: template.moveList,
                        periodDays: template.periodDays,
                        totalInEach: template.totalInEach,
                        templateId: templateId,
                        feedback: [],
                        hurt: []
                      }
                    }
                  }, {new: true}, (err, doc) => {
                    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                    else {
                      const newGuy = doc.plans[doc.plans.length - 1]
                      Payments.create({clientId, doctorId, planId: "" + newGuy._id, price}, (err2, doc2) => {
                        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
                        else {
                          return res.json({code: 1, newId: "" + newGuy._id})
                        }
                      })
                    }
                  })
                } else {
                  Plans.create({
                    doctorId, clientId, category: [], recognized: false, read: false, plans: [{
                      name: template.name,
                      extra: template.summary,
                      start: false,
                      moveList: template.moveList,
                      periodDays: template.periodDays,
                      totalInEach: template.totalInEach,
                      templateId: templateId,
                      feedback: [],
                      hurt: []
                    }]
                  }, (err, doc) => {
                    if (!err) {
                      const newGuy = doc.plans[doc.plans.length - 1]
                      Payments.create({clientId, doctorId, planId: "" + newGuy._id, price}, (err2, doc2) => {
                        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
                        else {
                          return res.json({code: 1, newId: "" + newGuy._id})
                        }
                      })
                    } else {
                      console.log(err)
                      return res.json({code: 0, msg: "后端出错了，请刷新"})
                    }
                  })
                }
              }
            })
          } else {
            return res.json({code: 0, msg: "无此计划"})
          }
        } else {
          return res.json({code: 0, msg: "无此计划"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else {
    Clients.findOneAndUpdate({_id: clientId}, {
      $set: {
        vip: true, vipTime: Date.now()
      }
    }, (err, doc) => {
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      else {
        const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs","o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
        const price = free.indexOf(clientId) >= 0 ? 0.1: 3650
        Payments.create({clientId, doctorId, planId: "noPlan", price}, (err2, doc2) => {
          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else {

            //
            //
            // const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5EiBsMSL_bwWmzxeAArLqDM", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
            //
            // const mch_id = "1488387462"; // 商户号，申请微信支付，腾讯给的商户号
            // const nonce_str = randomstring.generate(32); // 随机字符串
            // const out_order_no = "" + new Date().getTime() + Math.floor(Math.random() * 10); //商户订单号
            // let sign1 = ""
            // let sign2 = "";
            //
            //
            // const stringA = `appid=${appid2}&mch_id=${mch_id}&nonce_str=${nonce_str}&out_trade_no=${out_trade_no}`;
            // const stringSighTemp = stringA + "&key=Yixing0456cnliXiaomuyu038412cnli"; //32位的商户key,自定义的，这里为了隐私，我用的特殊符号给你们展示
            // sign1 = utils.md5(stringSighTemp).toUpperCase();
            //
            //
            // console.log("afterpay2 start")
            //
            // const xml1 = `<xml>
            //                 <appid>${appid2}</appid>
            //                 <mch_id>${mch_id}</mch_id>
            //                 <nonce_str>${nonce_str}</nonce_str>
            //                 <out_trade_no>${out_trade_no}</out_trade_no>
            //                 <sign>${sign1}</sign>
            //               </xml>`;
            //
            //
            // axios({
            //   method: 'post',
            //   url: "https://api.mch.weixin.qq.com/pay/orderquery",
            //   data: xml1,
            //   responseType: 'text/xml',
            //   headers: {
            //     'Content-Type': 'text/xml'
            //   }
            // }).then(response => {
            //   console.log("1 response " + response)
            //   parseString(response.data, function (err, result) {
            //     if (err) {
            //       console.log("2 err " + err)
            //       return res.json({code: 0, msg: "后端出错了，请刷新"})
            //     } else {
            //       console.log("3 result " + result.xml.transaction_id)
            //       const transaction_id = result.xml.transaction_id
            //       const receivers = [{"type": "PERSONAL_OPENID",
            //         "account":"oMYxa5I9OJeRzMeAx_3f-k1QqCZ4",
            //         "amount":0.01,
            //         "description": "分到个人"
            //       }]
            //
            //       const string2A = `appid=${appid2}&mch_id=${mch_id}&nonce_str=${nonce_str}&out_order_no=${out_order_no}&transaction_id=${transaction_id}&receivers=${receivers}`;
            //       const stringSighTemp2 = string2A + "&key=Yixing0456cnliXiaomuyu038412cnli"; //32位的商户key,自定义的，这里为了隐私，我用的特殊符号给你们展示
            //       sign2 = utils.md5(stringSighTemp2).toUpperCase();
            //
            //
            //
            //       const data2 = {
            //         appid: appid2,
            //         transaction_id: transaction_id,
            //         out_order_no: out_order_no,
            //         receivers: receivers,
            //         unfreeze_unsplit: true
            //       }
            //
            //       const timeStamp = Math.floor(Date.now() / 1000)
            //
            //       const beforeSign =  "POST" + "\n" + "/v3/profitsharing/orders" + "\n" + timeStamp + "\n" +nonce_str + "\n" +data + "\n"
            //
            //       axios({
            //         method: 'post',
            //         url: "https://api.mch.weixin.qq.com/v3/profitsharing/orders",
            //         data: data2,
            //         responseType: 'text/xml',
            //         headers: {
            //           'Authorization': ,
            //           'Accept': 'application/json',
            //           'Content-Type': 'text/xml',
            //           'Wechatpay-Serial': ''
            //         }
            //       }).then(response => {
            //         console.log("4 response " + response)
            //         parseString(response.data, function (err, result) {
            //           if (err) {
            //             console.log("5 err " + err)
            //             return res.json({code: 0, msg: "后端出错了，请刷新"})
            //           } else {
            //             console.log("6 result " + result)
            //             return res.json({code: 1, newId: "" + newGuy._id})
            //           }
            //         });
            //
            //       }).catch(err => {
            //         console.log("7 err " + err)
            //         return res.json({code: 0, msg: "后端出错了，请刷新"})
            //       })
            //
            //     }
            //   });
            //
            // }).catch(err => {
            //   console.log("9 err " + err)
            //   return res.json({code: 0, msg: "后端出错了，请刷新"})
            // })


            axios.get('https://api.xiaoe-tech.com/token', {
              params: {
                "app_id": "appz4t5ma7u3727",
                "client_id": "xoppNXnJj3m3643",
                "secret_key": "LwcA3Qlrzcs5N2p7t4hetA0dTb8Ucl1h",
                "grant_type": "client_credential"
              }
            })
                .then(function (response0) {
                  console.log(response0)

                  const access_token = response0.data.data.access_token
                  axios.post('https://api.xiaoe-tech.com/xe.user.info.get/1.0.0', {
                    access_token: access_token,
                    data : {
                      "wx_union_id": doc.unionid,
                      "field_list" : [
                        "user_id"
                      ]
                    }
                  })
                      .then(function (response1) {

                        const user_id = response1.data.data.user_id
                        axios.post('https://api.xiaoe-tech.com/xe.order.delivery/1.0.0', {
                          access_token: access_token,
                          user_id: user_id,
                          data: {
                            payment_type: 3,
                            resource_type: 5,
                            product_id: "p_61331979e4b0448bf6558043",
                            resource_id: "p_61331979e4b0448bf6558043",
                            user_id: user_id,
                            period: 31536000
                          }
                        })
                            .then(function (response3) {
                              console.log(response3)

                              return res.json({code: 1})
                            })
                            .catch(function (error) {
                              console.log(error)

                              return res.json({code: 1})
                            });
                      })
                      .catch(function (error) {
                        console.log(error)

                        return res.json({code: 1})
                      });
                })
                .catch(function (error) {
                  console.log(error)

                  return res.json({code: 1})
                });
          }
        })
      }
    })
  }
})

app.get('/synccircles', (req, res) => {
  axios.get('https://api.xiaoe-tech.com/token', {
    params: {
      "app_id": "appz4t5ma7u3727",
      "client_id": "xoppNXnJj3m3643",
      "secret_key": "LwcA3Qlrzcs5N2p7t4hetA0dTb8Ucl1h",
      "grant_type": "client_credential"
    }
  })
    .then(function (response) {

      const access_token = response.data.data.access_token
      axios.post('https://api.xiaoe-tech.com/xe.goods.list.get/4.0.0', {
        access_token: access_token,
        page : 1,
        page_size : 100,
        resource_type: 7
      })
        .then(function (response) {
          const circles = response.data
          console.log(response.data)
          Circles.find({}, (err, doc1) => {
            if (!err) {
              const circleInfo = circles.data.list
              const currentIds = doc1.map(circle => circle.xiaoeId)
              const newCircles = circleInfo.filter(circle => currentIds.indexOf(circle.resource_id) < 0).map(circle => {
                return {
                  name: circle.goods_name,
                  xiaoeId: circle.resource_id,
                  templates: []
                }
              })
              Circles.insertMany(newCircles, {new: true}, (err, doc2) => {
                if (!err) return res.json({code: 1, doc: [...doc1, ...doc2]})
                else return res.json({code: 0, msg: "后端出错了，请刷新"})
              })
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        })
        .catch(function (error) {
          console.log(error);
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        });
    })
    .catch(function (error) {
      console.log(error);
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    });
})


app.get('/getcircles', (req, res) => {
  Circles.find({}, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})


app.get('/isfree', (req, res) => {
  const {clientId, templateId} = req.query
  let doctorId = req.query.doctorId
  if (!doctorId) doctorId = "5f23a8e26266b8739f89de86"
  Clients.findOne({_id: clientId}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    console.log("unionid: " + doc.unionid)
    axios.get('https://api.xiaoe-tech.com/token', {
      params: {
        "app_id": "appz4t5ma7u3727",
        "client_id": "xoppNXnJj3m3643",
        "secret_key": "LwcA3Qlrzcs5N2p7t4hetA0dTb8Ucl1h",
        "grant_type": "client_credential"
      }
    })
      .then(function (response0) {

        const access_token = response0.data.data.access_token
        console.log(access_token)
        axios.post('https://api.xiaoe-tech.com/xe.user.info.get/1.0.0', {
          access_token: access_token,
          data: {
            "wx_union_id": doc.unionid,
            "field_list": [
              "user_id"
            ]
          }
        })
          .then(function (response1) {
            const user_id = response1.data.data.user_id
            console.log("user_id: " + user_id)
            axios.post('https://api.xiaoe-tech.com/xe.community.user.list/1.0.0', {
              access_token: access_token,
              user_id: user_id,
              page_index: 1,
              page_size: 100
            })
              .then(function (response3) {
                console.log(response3.data)
                if (response3.data == null || response3.data.data == null || response3.data.data.list == null
                  || response3.data.data.list.length === 0) {
                  return res.json({code: 1, result: false})
                }
                const circleList = response3.data.data.list
                console.log(circleList)
                console.log(circleList[0])
                const circleIds = circleList.map(circle => circle.community_id)
                console.log(circleIds)
                console.log(circleIds[0])
                Circles.find({xiaoeId: {$in: circleIds}}, (err2, doc2) => {
                  if (err2) {
                    console.log(err2)
                    return res.json({code: 0, msg: "后端出错了，请刷新"})
                  }
                  let allFreeTemplates = []
                  doc2.forEach(circle => {
                    allFreeTemplates = allFreeTemplates.concat(...circle.templates)
                  })
                  const templatesLeft = allFreeTemplates.filter(template => {
                    if (!template.doctorId) return template.templateId === templateId
                    return template.templateId === templateId && template.doctorId === doctorId
                  })
                  return res.json({code: 1, result: templatesLeft.length > 0})
                })
              })
              .catch(function (error) {
                console.log(error)

                return res.json({code: 0, msg: "后端出错了，请刷新"})
              })
          })
          .catch(function (error) {
            console.log(error)

            return res.json({code: 0, msg: "后端出错了，请刷新"})
          })
      })
      .catch(function (error) {
        console.log(error)

        return res.json({code: 0, msg: "后端出错了，请刷新"})
      })
  })
})




app.post('/addemplatetocircle', (req, res) => {
  const {doctorId, templateId, circleId} = req.body
  Circles.findOneAndUpdate({xiaoeId: circleId},
    {$push: {templates: {doctorId, templateId}}}, (err, doc) => {
    if (!err) {
      return res.json({code: 1, doc})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

// app.get('/afterpay2', (req, res) => {
//   const {clientId, templateId} = req.query
//   const doctorId = "643ffd2b79c48c7e184fb462"
//   if (templateId) {
//     const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs","o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
//     const price = free.indexOf(clientId) >= 0 ? 0.1: 9.9
//
//     const fromDoctorId = "5f23a8e26266b8739f89de86"
//     const doctorId = "643ffd2b79c48c7e184fb462"
//     Doctors.findOne({
//       _id: fromDoctorId,
//       'templates._id': templateId,
//     }, {
//       'templates.$' : 1,
//     }, (err, doc) => {
//       if (!err) {
//         console.log(doc)
//         if (doc) {
//           const templates = doc.templates
//           if (templates.length > 0 ) {
//             const template = templates[0]
//             Plans.findOne({clientId, doctorId}, (err1, doc1) => {
//               if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
//               else {
//                 if (doc1) {
//                   Plans.findOneAndUpdate({clientId, doctorId}, {
//                     $push: {
//                       plans: {
//                         name: template.name,
//                         extra: template.summary,
//                         start: false,
//                         moveList: template.moveList,
//                         periodDays: template.periodDays,
//                         totalInEach: template.totalInEach,
//                         templateId: templateId,
//                         feedback: [],
//                         hurt: []
//                       }
//                     }
//                   }, {new: true}, (err, doc) => {
//                     if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
//                     else {
//                       const newGuy = doc.plans[doc.plans.length - 1]
//                       Payments.create({clientId, doctorId, planId: "" + newGuy._id, price}, (err2, doc2) => {
//                         if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
//                         else {
//                           return res.json({code: 1, newId: "" + newGuy._id})
//                         }
//                       })
//                     }
//                   })
//                 } else {
//                   Plans.create({
//                     doctorId, clientId, category: [], recognized: false, read: false, plans: [{
//                       name: template.name,
//                       extra: template.summary,
//                       start: false,
//                       moveList: template.moveList,
//                       periodDays: template.periodDays,
//                       totalInEach: template.totalInEach,
//                       templateId: templateId,
//                       feedback: [],
//                       hurt: []
//                     }]
//                   }, (err, doc) => {
//                     if (!err) {
//                       const newGuy = doc.plans[doc.plans.length - 1]
//                       Payments.create({clientId, doctorId, planId: "" + newGuy._id, price}, (err2, doc2) => {
//                         if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
//                         else {
//                           return res.json({code: 1, newId: "" + newGuy._id})
//                         }
//                       })
//                     } else {
//                       console.log(err)
//                       return res.json({code: 0, msg: "后端出错了，请刷新"})
//                     }
//                   })
//                 }
//               }
//             })
//           } else {
//             return res.json({code: 0, msg: "无此计划"})
//           }
//         } else {
//           return res.json({code: 0, msg: "无此计划"})
//         }
//       } else {
//         return res.json({code: 0, msg: "后端出错了，请刷新"})
//       }
//     })
//   } else {
//     Clients.findOneAndUpdate({_id: clientId}, {
//       $set: {
//         vip: true, vipTime: Date.now()
//       }
//     }, (err, doc) => {
//       if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
//       else {
//         const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs","o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
//         const price = free.indexOf(clientId) >= 0 ? 0.1: 99
//         Payments.create({clientId, doctorId, planId: "noPlan", price}, (err2, doc2) => {
//           if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
//           else {
//             axios.get('https://api.xiaoe-tech.com/token', {
//               params: {
//                 "app_id": "appz4t5ma7u3727",
//                 "client_id": "xoppNXnJj3m3643",
//                 "secret_key": "LwcA3Qlrzcs5N2p7t4hetA0dTb8Ucl1h",
//                 "grant_type": "client_credential"
//               }
//             })
//                 .then(function (response0) {
//                   console.log(response0)
//
//                   const access_token = response0.data.data.access_token
//                   axios.post('https://api.xiaoe-tech.com/xe.user.info.get/1.0.0', {
//                     access_token: access_token,
//                     data : {
//                       "wx_union_id": doc.unionid,
//                       "field_list" : [
//                         "user_id"
//                       ]
//                     }
//                   })
//                       .then(function (response1) {
//
//                         const user_id = response1.data.data.user_id
//                         axios.post('https://api.xiaoe-tech.com/xe.order.delivery/1.0.0', {
//                           access_token: access_token,
//                           user_id: user_id,
//                           data: {
//                             payment_type: 3,
//                             resource_type: 5,
//                             product_id: "p_61331979e4b0448bf6558043",
//                             resource_id: "p_61331979e4b0448bf6558043",
//                             user_id: user_id,
//                             period: 31536000
//                           }
//                         })
//                             .then(function (response3) {
//                               console.log(response3)
//
//                               return res.json({code: 1})
//                             })
//                             .catch(function (error) {
//                               console.log(error)
//
//                               return res.json({code: 1})
//                             });
//                       })
//                       .catch(function (error) {
//                         console.log(error)
//
//                         return res.json({code: 1})
//                       });
//                 })
//                 .catch(function (error) {
//                   console.log(error)
//
//                   return res.json({code: 1})
//                 });
//           }
//         })
//       }
//     })
//   }
// })

app.post('/afterpay3', (req, res) => {
  const {clientId, templateId} = req.body
  Clients.findOneAndUpdate({_id: clientId}, {
    $set: {
      specialPaid: true
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const free = ["o1VwX448Y-errOGw2kUjJldrePhI", "o1VwX48uWOaPwhZOe_gVdU2oO7Fs","o1VwX48uWOaPwhZOe_gVdU2oO7Fs", "oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
      const price = free.indexOf(clientId) >= 0 ? 0.1: 99

      let fromDoctorId = req.body.doctorId
      if (!fromDoctorId) fromDoctorId = "5f23a8e26266b8739f89de86"
      const doctorId = "643ffd2b79c48c7e184fb462"
      Doctors.findOne({
        _id: fromDoctorId,
        'templates._id': templateId,
      }, {
        'templates.$' : 1,
      }, (err, doc) => {
        if (!err) {
          console.log(doc)
          if (doc) {
            const templates = doc.templates
            if (templates.length > 0 ) {
              const template = templates[0]
              Plans.findOne({clientId, doctorId}, (err1, doc1) => {
                if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else {
                  if (doc1) {
                    Plans.findOneAndUpdate({clientId, doctorId}, {
                      $push: {
                        plans: {
                          name: template.name,
                          extra: template.summary,
                          start: false,
                          moveList: template.moveList,
                          periodDays: template.periodDays,
                          totalInEach: template.totalInEach,
                          templateId: templateId,
                          feedback: [],
                          hurt: []
                        }
                      }
                    }, {new: true}, (err, doc) => {
                      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                      else {
                        const newGuy = doc.plans[doc.plans.length - 1]
                        Payments.create({clientId, doctorId, planId: "" + newGuy._id, price}, (err2, doc2) => {
                          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
                          else {
                            return res.json({code: 1, newId: "" + newGuy._id})
                          }
                        })
                      }
                    })
                  } else {
                    Plans.create({
                      doctorId, clientId, category: [], recognized: false, read: false, plans: [{
                        name: template.name,
                        extra: template.summary,
                        start: false,
                        moveList: template.moveList,
                        periodDays: template.periodDays,
                        totalInEach: template.totalInEach,
                        templateId: templateId,
                        feedback: [],
                        hurt: []
                      }]
                    }, (err, doc) => {
                      if (!err) {
                        const newGuy = doc.plans[doc.plans.length - 1]
                        Payments.create({clientId, doctorId, planId: "" + newGuy._id, price}, (err2, doc2) => {
                          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
                          else {
                            return res.json({code: 1, newId: "" + newGuy._id})
                          }
                        })
                      } else {
                        console.log(err)
                        return res.json({code: 0, msg: "后端出错了，请刷新"})
                      }
                    })
                  }
                }
              })
            } else {
              return res.json({code: 0, msg: "无此计划"})
            }
          } else {
            return res.json({code: 0, msg: "无此计划"})
          }
        } else {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      })
    }
  })
})


app.get('/recommendplans2', (req, res) => {
  const doctorId = "5f23a8e26266b8739f89de86"
  const otherDoctorId = "643ffd2b79c48c7e184fb462"
  const {clientId, mode} = req.query
  console.log(clientId + " " + mode)
  let date = req.query.date
  const today = new Date()
  const todayDate = ""+today.getFullYear()+(Number(today.getMonth())+1)+today.getDate()
  if (!date) date = todayDate
  ClientCategories.find({doctorId}, null, {sort:{date: 1}}, (err, doc) => {
    if (err) return res.json({code: 0})
    else {
      let cats = doc.filter(cat => {
        if (cat.type === "second") {
          const parentId = cat.name.split("$")[0]
          return doc.filter(cat2 => ""+cat2._id === parentId).length > 0
        } else if (""+cat._id === "6449e6b9d2fa3e7ec86f9c6a") {
          return false
        } else return true
      }).map(cat => {
        if (cat.type === "second") {
          const parentId = cat.name.split("$")[0]
          const parentName = doc.filter(cat2 => ""+cat2._id === parentId)[0].name
          const childName = cat.name.split("$")[1]
          return {
            _id: ""+cat._id,
            parentName: parentName,
            name: childName,
            parentId: parentId,
            type: cat.type,
            special: !!cat.special
          }
        } else return {
          _id: ""+cat._id,
          name: cat.name,
          type: cat.type,
          special: !!cat.special
        }
      })

      Plans.findOne({doctorId: otherDoctorId, clientId: clientId}, (err, doc) => {
        if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const templateIds = doc && doc.plans ? doc.plans.filter(realPlan => {
            if (!date) return true
            else {
              let diff = 0
              let totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
              totalDays = totalDays === 0 ? realPlan.allDay : totalDays
              if (realPlan.start && realPlan.startDate) {
                const date1 = new Date(
                  parseInt(realPlan.startDate.substring(0, 4), 10),
                  parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
                  parseInt(realPlan.startDate.substring(6, 8), 10))
                const date2 = new Date(
                  parseInt(date.substring(0, 4), 10),
                  parseInt(date.substring(4, 6), 10) - 1,
                  parseInt(date.substring(6, 8), 10)
                )
                diff = Math.floor((date2 - date1) / 86400000)
                return diff < totalDays;
              } else {
                return true
              }
            }
          }).map(plan => plan.templateId) : []
          Doctors.findOne({_id: doctorId}, {'password':0, '__v':0}, {sort: {date: 1}}, (err, doc) => {
            if (!err) {
              const ids = doc.templates.map(template => template.moveList[0].move)
              let templates = doc.templates.filter(template => {
                const totalDays = template.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
                return totalDays && totalDays > 0
              }).map(template => {
                const urlid = template.moveList[0].move
                const _id = ""+template._id
                const name = template.name
                const category = template.category ? template.category : []
                const added = templateIds.indexOf(_id) >= 0
                return {_id, urlid, name, category, added}
              })
              Movements.find({ _id: {$in: ids}}, function (err, result) {
                if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else {
                  const resultIds = result.map(move => ""+move._id)
                  for (let i = 0; i < templates.length; i++) {
                    const index = resultIds.indexOf(templates[i].urlid)
                    templates[i].url = result[index].screenshot
                    templates[i].urlid = undefined
                  }
                  if (!mode) {
                    cats = cats.filter(cat => !cat.special)
                    templates = templates.filter(template => {
                      for (let i = 0; i < cats.length; i++) {
                        if (template.category.indexOf(cats[i]._id) >= 0) {
                          return true
                        }
                      }
                      return false
                    })
                  } else if (mode === "special") {
                    cats = cats.filter(cat => cat._id === "64828c5ebc75e94a4a73a4b0" || cat.parentId === "64828c5ebc75e94a4a73a4b0")
                    templates = templates.filter(template => {
                      for (let i = 0; i < cats.length; i++) {
                        if (template.category.indexOf(cats[i]._id) >= 0) {
                          return true
                        }
                      }
                      return false
                    })
                  } else if (mode === "special2") {
                    cats = cats.filter(cat => cat._id === "6491397b15c546415fc801a2" || cat.parentId === "6491397b15c546415fc801a2")
                    templates = templates.filter(template => {
                      for (let i = 0; i < cats.length; i++) {
                        if (template.category.indexOf(cats[i]._id) >= 0) {
                          return true
                        }
                      }
                      return false
                    })
                  } else {
                    cats = cats.filter(cat => cat._id === mode || cat.parentId === mode)
                    templates = templates.filter(template => {
                      for (let i = 0; i < cats.length; i++) {
                        if (template.category.indexOf(cats[i]._id) >= 0) {
                          return true
                        }
                      }
                      return false
                    })
                  }

                  return res.json({code: 1, cats, doc: templates, price: !!mode ? 19.9 : 0})
                }
              })
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        }
      })
    }
  })
})

app.get('/search2', (req, res) => {
  const doctorId = "5f23a8e26266b8739f89de86"
  const otherDoctorId = "643ffd2b79c48c7e184fb462"
  const {keyWord, clientId} = req.query
  let date = req.query.date
  const today = new Date()
  const todayDate = ""+today.getFullYear()+(Number(today.getMonth())+1)+today.getDate()
  if (!date) date = todayDate
  Plans.findOne({doctorId: otherDoctorId, clientId: clientId}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const templateIds = doc && doc.plans ? doc.plans.filter(realPlan => {
        if (!date) return true
        else {
          let diff = 0
          let totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
          totalDays = totalDays === 0 ? realPlan.allDay : totalDays
          if (realPlan.start && realPlan.startDate) {
            const date1 = new Date(
              parseInt(realPlan.startDate.substring(0, 4), 10),
              parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
              parseInt(realPlan.startDate.substring(6, 8), 10))
            const date2 = new Date(
              parseInt(date.substring(0, 4), 10),
              parseInt(date.substring(4, 6), 10) - 1,
              parseInt(date.substring(6, 8), 10)
            )
            diff = Math.floor((date2 - date1) / 86400000)
            return diff < totalDays;
          } else {
            return true
          }
        }
      }).map(plan => plan.templateId) : []
      Doctors.findOne({_id: doctorId}, {'password':0, '__v':0}, {sort: {date: 1}}, (err, doc) => {
        if (!err) {
          const ids = doc.templates.map(template => template.moveList[0].move)
          const templates = doc.templates.filter(template => template.name.indexOf(keyWord) > -1).filter(template => {
            const totalDays = template.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
            return totalDays && totalDays > 0
          }).map(template => {
            const urlid = template.moveList[0].move
            const _id = ""+template._id
            const name = template.name
            const added = templateIds.indexOf(_id) >= 0
            const extra = template.summary
            const totalDays = template.periodDays.map(num => parseInt(num, 10)).reduce((a,b) => a + b, 0)
            return {_id, urlid, name, added, extra, totalDays}
          })
          Movements.find({ _id: {$in: ids}}, function (err, result) {
            if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              const resultIds = result.map(move => ""+move._id)
              for (let i = 0; i < templates.length; i++) {
                const index = resultIds.indexOf(templates[i].urlid)
                templates[i].url = result[index].screenshot
                templates[i].urlid = undefined
              }
              return res.json({code: 1, doc: templates})
            }
          })
        } else {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      })
    }
  })
})

app.get('/hotwords', (req, res) => {
  const words = "肺康复、手指钮孔畸形、桡骨茎突狭窄性腱鞘炎、贝克囊肿、髌骨软化症、正中神经疼痛、股四头肌拉伤、跟腱疼痛、桡神经疼痛、紧张性头痛、手腕扭伤或拉伤、Covid-19上肢和下肢练习、Covid-19呼吸技巧、办公桌前保健、滑雪者拇指、持续性头痛、下交叉综合征、老年人肘部强化、老年人髋关节练习、老年人椅上练习、老年人手臂强化、老年人脚踝疼痛、内收肌腱病、老年人日常、踝关节扭伤、扁平足、坐骨滑囊炎、慢性胸椎疼痛、急性胸椎疼痛、高尔夫球肘、扳机指、急性腰痛、慢性腰痛、计算机一族腰部、颈部锻炼、上班族腰痛、中背部疼痛、颈痛预防、髋关节骨性关节炎、膝关节骨性关节炎、肩关节骨性关节炎、髌股疼痛预防、椎间盘损伤引起的坐骨神经痛、产后塑形、肱二头肌长头肌腱炎、下背部疼痛、心脏、胫骨疼痛、髌骨股骨疼痛症候群、腘绳肌拉伤、OEP老年防摔倒、OTAGO力量和平衡、桡骨茎突狭窄性腱鞘炎、转子滑囊炎、压力性尿失禁、备孕期锻炼、颈痛、腰椎椎管狭窄保守、脊髓型颈椎病、颈痛+眩晕、颈部挥鞭损伤、亚急性和慢性颈痛、神经根型颈椎病上肢痛、腰突保守、儿童脑震荡、骶髂关节炎、慢性颈痛伴颈源性头痛、亚急性和慢性颈部疼痛、孕期骨盆带疼痛、耻骨联合分离疼痛、产后颈肩腕部疼痛、产后盆底肌训练、为正常分娩做准备的训练、产后全身疼痛管理、上交叉综合征、颈部疼痛、髌腱炎、肱骨外上髁炎、ITB髂胫束综合征、胫骨结节骨骺炎、梨状肌综合征、肩周炎、肘管综合症、膝骨关节炎、尺神经麻痹、腕管综合征、面神经炎、颞下颌关节疼痛、眩晕（梅尼埃病）、帕金森功能性康复、帕金森"
  const wordsArray = words.split("、")
  const shuffedWords = wordsArray
    .map(value => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value)
  const firstEight = shuffedWords.slice(0, 8)
  return res.json({code: 1, doc: firstEight})

})

app.get('/popinfo2', (req, res) => {
  const otherDoctorId = "643ffd2b79c48c7e184fb462"
  const {planId, added, clientId, date} = req.query
  let doctorId = req.query.doctorId
  if (!doctorId) doctorId = "5f23a8e26266b8739f89de86"

  if (added === "1") {
    Plans.findOne({clientId, doctorId: otherDoctorId, 'plans.templateId': planId}, {
      'plans.$' : 1,
    }, (err, doc) => {
      if (!err) {
        console.log(doc.plans)
        if (doc.plans.length > 0) {
          const plan = doc.plans[doc.plans.length-1]
          let diff = 0
          let totalDays = plan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
          totalDays = totalDays === 0 ? plan.allDay : totalDays
          let finished = false
          if (plan.start && plan.startDate) {
            const date1 = new Date(
              parseInt(plan.startDate.substring(0, 4), 10),
              parseInt(plan.startDate.substring(4, 6), 10) - 1,
              parseInt(plan.startDate.substring(6, 8), 10))
            const date2 = new Date(
              parseInt(date.substring(0, 4), 10),
              parseInt(date.substring(4, 6), 10) - 1,
              parseInt(date.substring(6, 8), 10)
            )
            diff = Math.floor((date2 - date1) / 86400000)
            if (diff >= totalDays) {
              diff = totalDays - 1
              finished = true
            }
          } else {
            diff = 0
          }
          let index = 0
          let moveDiff = diff
          while (moveDiff >= parseInt(plan.periodDays[index], 10)) {
            moveDiff = moveDiff - parseInt(plan.periodDays[index], 10)
            index = index + 1
          }
          const start = index === 0 ? 0 : plan.totalInEach[index - 1]
          const rawList = plan.moveList.slice(start, plan.totalInEach[index])
          const realMoveList = rawList.filter(move => !move.bar).filter(move => {
            if (move.between) return diff % (Number(move.between) + 1) === 0
            else return true
          })
          const ids = realMoveList.map(move => move.move)
          const returnResult = {
            name: plan.name,
            start: plan.start,
            totalDays: totalDays,
            newId: ""+plan._id,
            diff: plan.start && plan.startDate ? diff + 1 : 0,
            finished: finished,
            extra: "注意：此方案为通用指导性方案，仅供参考，如需更专业的方案，请定制以符合您的实际需求。在家中进行练习时，请确保有家人陪伴。如在练习时感到不适，请立即停止并咨询医生。"+(plan.extra ? plan.extra : ""),
          }
          Movements.find({ _id: {$in: ids}}, function (err, result) {
            if (err) {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
            else {
              const resultIds = result.map(move => ""+move._id)
              let iurl = []
              for (let j = 0; j < realMoveList.length; j++) {
                iurl.push(resultIds.indexOf(ids[j]) < 0 ? "" : result[resultIds.indexOf(ids[j])].screenshot)
              }
              returnResult.urls = iurl
              return res.json({code: 1, doc: returnResult})
            }
          })
        } else {
          return res.json({code: 0, msg: "无此计划"})
        }
      } else {
        console.log(3)
        console.log(err)
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else if (added === "2") {
    Doctors.findOne({
      _id: doctorId,
      'templates._id': planId,
    }, {
      'templates.$' : 1,
    }, (err, doc) => {
      if (!err) {
        if (doc) {
          const templates = doc.templates
          if (templates.length > 0 ) {
            const template = templates[0]
            const returnResult = {
              name: template.name,
              start: false,
              totalDays: template.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0),
              diff: 0,
              extra: "注意：此方案为通用指导性方案，仅供参考，如需更专业的方案，请定制以符合您的实际需求。在家中进行练习时，请确保有家人陪伴。如在练习时感到不适，请立即停止并咨询医生。"+(template.summary ? template.summary : ""),
            }
            const ids = template.moveList.map(move => move.move)
            Movements.find({ _id: {$in: ids}}, function (err, result) {
              if (err) {
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
              else {
                const resultIds = result.map(move => ""+move._id)
                let iurl = []
                for (let j = 0; j < template.moveList.length; j++) {
                  iurl.push(resultIds.indexOf(ids[j]) < 0 ? "" : result[resultIds.indexOf(ids[j])].screenshot)
                }
                returnResult.urls = iurl
                return res.json({code: 1, doc: returnResult})
              }
            })
          } else {
            return res.json({code: 0, msg: "无此计划"})
          }
        } else {
          return res.json({code: 0, msg: "无此计划"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else {
    Plans.find({clientId, doctorId: otherDoctorId, 'plans.templateId': planId}, {
      'plans.$' : 1,
    }, (err, doc) => {
      if (!err) {
        if (doc && doc.length > 0 && doc[0].plans.length > 0) {
          const plan = doc[0].plans[doc[0].plans.length-1]
          let diff = 0
          let totalDays = plan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
          totalDays = totalDays === 0 ? plan.allDay : totalDays
          let finished = false
          if (plan.start && plan.startDate) {
            const date1 = new Date(
              parseInt(plan.startDate.substring(0, 4), 10),
              parseInt(plan.startDate.substring(4, 6), 10) - 1,
              parseInt(plan.startDate.substring(6, 8), 10))
            const date2 = new Date(
                parseInt(date.substring(0, 4), 10),
                parseInt(date.substring(4, 6), 10) - 1,
                parseInt(date.substring(6, 8), 10)
            )
            diff = Math.floor((date2 - date1) / 86400000)
            console.log(diff)
            if (diff >= totalDays) {
              diff = totalDays - 1
              finished = true
            }
          } else {
            diff = 0
          }
          let index = 0
          let moveDiff = diff
          while (moveDiff >= parseInt(plan.periodDays[index], 10)) {
            moveDiff = moveDiff - parseInt(plan.periodDays[index], 10)
            index = index + 1
          }
          const start = index === 0 ? 0 : plan.totalInEach[index - 1]
          const rawList = plan.moveList.slice(start, plan.totalInEach[index])
          const realMoveList = rawList.filter(move => !move.bar).filter(move => {
            if (move.between) return diff % (Number(move.between) + 1) === 0
            else return true
          })
          const ids = realMoveList.map(move => move.move)
          const returnResult = {
            name: plan.name,
            start: finished ? false : plan.start,
            totalDays: totalDays,
            newId: ""+plan._id,
            diff: finished ? 0 : plan.start && plan.startDate ? diff + 1 : 0,
            finished: finished,
            extra: "注意：此方案为通用指导性方案，仅供参考，如需更专业的方案，请定制以符合您的实际需求。在家中进行练习时，请确保有家人陪伴。如在练习时感到不适，请立即停止并咨询医生。"+(plan.extra ? plan.extra : ""),
          }
          Movements.find({ _id: {$in: ids}}, function (err, result) {
            if (err) {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
            else {
              const resultIds = result.map(move => ""+move._id)
              let iurl = []
              for (let j = 0; j < realMoveList.length; j++) {
                iurl.push(resultIds.indexOf(ids[j]) < 0 ? "" : result[resultIds.indexOf(ids[j])].screenshot)
              }
              returnResult.urls = iurl
              return res.json({code: 1, doc: returnResult, added: !finished})
            }
          })
        } else {
          Doctors.findOne({
            _id: doctorId,
            'templates._id': planId,
          }, {
            'templates.$' : 1,
          }, (err, doc) => {
            if (!err) {
              if (doc) {
                const templates = doc.templates
                if (templates.length > 0 ) {
                  const template = templates[0]
                  const returnResult = {
                    name: template.name,
                    start: false,
                    totalDays: template.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0),
                    diff: 0,
                    extra: "注意：此方案为通用指导性方案，仅供参考，如需更专业的方案，请定制以符合您的实际需求。在家中进行练习时，请确保有家人陪伴。如在练习时感到不适，请立即停止并咨询医生。"+(template.summary ? template.summary : ""),
                  }
                  const ids = template.moveList.map(move => move.move)
                  Movements.find({ _id: {$in: ids}}, function (err, result) {
                    if (err) {
                      return res.json({code: 0, msg: "后端出错了，请刷新"})
                    }
                    else {
                      const resultIds = result.map(move => ""+move._id)
                      let iurl = []
                      for (let j = 0; j < template.moveList.length; j++) {
                        iurl.push(resultIds.indexOf(ids[j]) < 0 ? "" : result[resultIds.indexOf(ids[j])].screenshot)
                      }
                      returnResult.urls = iurl
                      return res.json({code: 1, doc: returnResult, added: false})
                    }
                  })
                } else {
                  return res.json({code: 0, msg: "无此计划"})
                }
              } else {
                return res.json({code: 0, msg: "无此计划"})
              }
            } else {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            }
          })
        }
      } else {
        console.log(3)
        console.log(err)
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  }
})

app.post('/addplan2', (req, res) => {
  const {clientId, planId, add} = req.body
  let fromDoctorId = req.body.doctorId
  console.log(clientId + " " + planId + " " + add + " " + fromDoctorId)
  if (!fromDoctorId) fromDoctorId = "5f23a8e26266b8739f89de86"
  const doctorId = "643ffd2b79c48c7e184fb462"
  if (add === "1" || add === 1) {
    Doctors.findOne({
      _id: fromDoctorId,
      'templates._id': planId,
    }, {
      'templates.$' : 1,
    }, (err, doc) => {
      if (!err) {
        if (doc) {
          const templates = doc.templates
          if (templates.length > 0 ) {
            const template = templates[0]
            Plans.findOne({clientId, doctorId}, (err1, doc1) => {
              if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
              else {
                const newPlans = doc1.plans.filter(realPlan => {
                  let diff = 0
                  let totalDays = realPlan.periodDays.map(num => parseInt(num, 10)).reduce((a, b) => a + b, 0)
                  if (realPlan.start && realPlan.startDate) {
                    const date1 = new Date(
                        parseInt(realPlan.startDate.substring(0, 4), 10),
                        parseInt(realPlan.startDate.substring(4, 6), 10) - 1,
                        parseInt(realPlan.startDate.substring(6, 8), 10))
                    const today = new Date()
                    const date2 = new Date(
                        today.getFullYear(),
                        today.getMonth(),
                        today.getDate()
                    )
                    diff = Math.floor((date2 - date1) / 86400000)
                    return diff < totalDays;
                  } else {
                    return true
                  }
                })
                console.log(newPlans.length)
                if (newPlans.length >= 2) {
                  return res.json({code: 2})
                } else {
                  Plans.findOneAndUpdate({clientId, doctorId}, {
                    $push: {
                      plans: {
                        name: template.name,
                        extra: template.summary,
                        start: false,
                        moveList: template.moveList,
                        periodDays: template.periodDays,
                        totalInEach: template.totalInEach,
                        templateId: planId,
                        feedback: [],
                        hurt: []
                      }
                    }
                  }, {new: true}, (err, doc) => {
                    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
                    else {
                      const newGuy = doc.plans[doc.plans.length - 1]
                      return res.json({code: 1, newId: "" + newGuy._id})
                    }
                  })
                }
              }
            })
          } else {
            return res.json({code: 0, msg: "无此计划"})
          }
        } else {
          return res.json({code: 0, msg: "无此计划"})
        }
      } else {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      }
    })
  } else {
    console.log("delete")
    Plans.findOneAndUpdate({clientId, doctorId}, {
      $pull: {
        plans: {
          templateId: planId
        }
      }
    }, (err, doc) => {
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      else return res.json({code: 1})
    })
  }

})

app.post('/newafterpay', (req, res) => {
  console.log(new Date())
  console.log(new Date().getTimezoneOffset())
  console.log(new Date(new Date().getTime() + new Date().getTimezoneOffset() * 60 * 1000 + 8 * 60 * 60 * 1000))
  const {clientId, doctorId} = req.body
  Plans.findOneAndUpdate({clientId, doctorId}, {
    $push: {
      plans: {
        name: "",
        empty: true,
        paidTime: new Date(new Date().getTime() + 8 * 60 * 60 * 1000),
        period: 1,
        extra: "",
        start: false,
        paid: true,
        moveList: [],
        periodDays: [""],
        totalInEach: [0],
        articles: []
      }
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.get('/openid', (req, res) => {
  const {js_code} = req.query
  axios.get("https://api.weixin.qq.com/sns/jscode2session?appid="+appid+"&secret="+secret+"&js_code="
    +js_code+"&grant_type=authorization_code")
    .then(response => {
      const openid = response.data.openid
      const unionid = response.data.unionid
      Clients.findOne({openid: openid}, (err1, doc1) => {
        if (err1) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        else if (doc1 && doc1.unionid) {
          return res.json({code: 1, response: response.data})
        } else if (doc1 && !doc1.unionid) {
          Clients.findOneAndUpdate({openid}, {$set: {unionid: unionid, coupon: 0}}, (err2, doc2) => {
            if (err2) {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            } else {
              console.log(doc2)
              return res.json({code: 1, response: response.data})
            }
          })
        } else if (!doc1) {
          Clients.create({openid, unionid,
            name: "", sex: "", age: "", idnum: "", history: "", smoke: "", description: "", phone: "", app: 1
          }, (err2, doc2) => {
            if (err2) {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            } else {
              return res.json({code: 1, response: response.data})
            }
          })
        }
      })
    })
    .catch(err => {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    })
})

app.post('/afteropenid', (req, res) => {
  const { openid } = req.body
  Clients.findOne({openid: openid}, (err1, doc1) => {
    if (err1) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
    else if (doc1) {
      return res.json({code: 1, clientId: doc1._id})
    } else {
      Clients.create({openid,
        name: "", sex: "", age: "", idnum: "", history: "", smoke: "", description: "", phone: "", app: 1
      }, (err2, doc2) => {
        if (err2) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        } else {
          return res.json({code: 1, clientId: doc2._id})
        }
      })
    }
  })
})


app.get('/clientid2', (req, res) => {
  const {js_code, code} = req.query

  const tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid2+"&secret="+secret2;
  const url = 'https://api.weixin.qq.com/wxa/business/getuserphonenumber';

  axios.get("https://api.weixin.qq.com/sns/jscode2session?appid="+appid2+"&secret="+secret2+"&js_code="
    +js_code+"&grant_type=authorization_code")
    .then(response => {
      const openid = response.data.openid
      const unionid = response.data.unionid
      Clients.findOne({unionid: unionid, openid: openid}, (err1, doc1) => {
        if (err1) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        else if (doc1 && doc1.phone) {
          return res.json({code: 1, clientId: ""+doc1._id, specialPaid: !!doc1.specialPaid})
        } else {
          axios.get(tokenUrl)
            .then(function (response) {
              const token = response.data.access_token
              axios({
                method: 'post',
                url: url+"?access_token="+token,
                data: {
                  // access_token: token,
                  code: code,
                }
              }).then(response1 => {
                console.log(response1.data)
                Clients.findOneAndUpdate(
                  {unionid, app: 2},
                  {$set: {openid: openid, phone: response1.data.phone_info.purePhoneNumber}},
                  (err3, doc3) => {
                    if (err3) {
                      return res.json({code: 0, msg: "后端出错了，请刷新"})
                    } else {
                      if (doc3) {
                        return res.json({code: 1, clientId: doc3._id, specialPaid: !!doc3.specialPaid})
                      } else {
                        Clients.create({openid, unionid,
                          name: "", sex: "", age: "", idnum: "", history: "", smoke: "", description: "",
                          phone: response1.data.phone_info.purePhoneNumber, email: "",
                          wechat: "", username: "", avatar: "", address: "", vip: false, app: 2
                        }, (err2, doc2) => {
                          if (err2) {
                            return res.json({code: 0, msg: "后端出错了，请刷新"})
                          } else {
                            return res.json({code: 1, clientId: doc2._id, specialPaid: false})
                          }
                        })
                      }
                    }
                  }
                )
              }).catch(err1 => {
                console.log(err1)
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              })
            })
            .catch(function (error) {
              console.log(error)
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            })
        }
      })
    })
    .catch(err => {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    })
})


app.get('/articles', (req, res) => {
  const {clientId} = req.query
  Plans.find({clientId}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      let result = []
      let artIds = []
      doc.forEach(pair => {
        pair.plans.forEach(plan => {
          if (plan.articles.length > 0) {
            result.push({articles: plan.articles, date: plan.date})
          }
        })
      })
      result.sort((a, b) => b.date - a.date)
      result.forEach(el => {
        artIds = artIds.concat(el.articles)
      })
      const rest = ["5dc8b7bcc58efb40bfa89b7d", "5dc8b7bcc58efb40bfa89b7e",
      "5dc8b7bcc58efb40bfa89b9a","5dc8b7bcc58efb40bfa89b9f","5dc8b7bcc58efb40bfa89b90",
      "5dc8b7bcc58efb40bfa89b95","5dc8b7bcc58efb40bfa89ba8","5dc8b7bcc58efb40bfa89baf",
      "5dc8b7bcc58efb40bfa89bb0","5dc8b7bcc58efb40bfa89bb1","5dc8b7bcc58efb40bfa89bb3"]
      artIds = artIds.concat(rest)
      artIds = artIds.filter((v,i) => artIds.indexOf(v) === i)
      Movements.find({ _id: { $in: artIds } }, (err, articleDoc) => {
        if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          articleDoc.sort((a, b) => {
            return artIds.indexOf(a._id+"") - artIds.indexOf(b._id+"");
          });
          return res.json({code: 1, doc: articleDoc.map(art => {
              return {
                name: art.name,
                id: art._id,
                url: "https://backend.voin-cn.com/resources/"+art._id+".png"
              }
            })
          })
        }
      })
    }

  })
})

app.post('/requestplan', (req, res) => {
  const { clientId, doctorId, content} = req.body
  Plans.findOneAndUpdate({clientId, doctorId}, {
    $push: {
      request: {
        content: content,
      },
    },
    $set: {
      read: false
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      Doctors.findOne({_id: doctorId}, (errDoc, docDoc) => {
        Clients.findOne({_id: clientId}, (errCli, docCli) => {
          if (docDoc && docCli) {
            const params = {
              "RegionId": "cn-hangzhou",
              "PhoneNumbers": ""+docDoc.phone,
              "SignName": "沃衍健康通知",
              "TemplateCode": "SMS_189520970",
              "TemplateParam": "{\"name\":\""+docCli.name+"\"}"
            }

            sms.request('SendSms', params, {method: 'POST'}).then((result) => {
              return res.json({code: 1, free30: !!docDoc.free30})
            }, (ex) => {
              console.log(ex)
              console.log("短信发送请求出错了")
              return res.json({code: 1, free30: !!docDoc.free30})
            })
          } else {
            return res.json({code: 1, free30: !!docDoc.free30})
          }
        })
      })
    }
  })
})

app.post('/todayplan', (req, res) => {
  const { clientId, doctorId, planId, date, completion } = req.body
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $push: {
      'plans.$.feedback': date + " " + completion,
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/todayplan2', (req, res) => {
  const doctorId = "643ffd2b79c48c7e184fb462"
  const { clientId, planId, date, completion, hurt } = req.body
  Plans.findOneAndUpdate({clientId, doctorId, 'plans._id': planId}, {
    $push: {
      'plans.$.feedback': date + " " + completion,
      'plans.$.hurt': date + " " + hurt
    }
  }, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/submittitle', (req, res) => {
  let {classId, title, description, name, sex, age, history, smoke, images} = req.body
  if (!images) images = []
  const pictures = images.map(image => {
    return {name: image}
  })
  HomeworkTitles.create({classId, title, description, name, sex, age, history, smoke, record: [{name: classId}], pictures}, (err, doc) => {
    if (err) {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else return res.json({code: 1, titleId: doc._id})
  })
})


app.post('/clientupload', function (req, res) {
  const form = new formidable.IncomingForm()
  let uploadDir = path.join(__dirname, "uploads");
  form.uploadDir = uploadDir; //本地文件夹目录路径

  form.parse(req, (err, fields, files) => {
    let oldPath = files.file.path;//这里的路径是图片的本地路径
    let newPath = path.join(path.dirname(oldPath), files.file.name);
    fs.rename(oldPath, newPath, err => { //fs.rename重命名图片名
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      else {
        Records.findOne({clientId: fields.clientId, doctorId: fields.doctorId}, (err, doc) => {
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          if (doc) {
            Records.findOneAndUpdate(
              {clientId: fields.clientId, doctorId: fields.doctorId},
              {$push: {records: {name: files.file.name, hidden: false}}, $set: {description: fields.description}
              }).exec((err) => {
              if (!err) {
                return res.json({code: 1})
              } else {
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
            })
          } else {
            Records.create(
              {clientId: fields.clientId,
                doctorId: fields.doctorId,
                records: [{name: files.file.name, hidden: false}],
                description: fields.description
              },
              err => {
              if (!err) {
                return res.json({code: 1})
              } else {
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
            })
          }

        })

      }
    })

  })
})

app.post('/clientupload2', function (req, res) {
  const form = new formidable.IncomingForm()
  let uploadDir = path.join(__dirname, "uploads");
  form.uploadDir = uploadDir; //本地文件夹目录路径

  form.parse(req, (err, fields, files) => {
    // if (files.myFile.size >= 1048576) {
    //   return res.json({code: 0, msg: "图片不能大于1M"})
    // }
    const after = new Date().getTime()
    let oldPath = files.myFile.path;//这里的路径是图片的本地路径
    let newPath = path.join(path.dirname(oldPath), fields.clientId + "_" + after);
    fs.rename(oldPath, newPath, err => { //fs.rename重命名图片名
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      else {
        return res.json({code: 1, url: "https://backend.voin-cn.com/"+fields.clientId + "_" + after})
      }
    })

  })
})

app.post('/bind2', function (req, res) {
  const { clientId1, js_code, level } = req.body
  axios.get("https://api.weixin.qq.com/sns/jscode2session?appid="+appid2+"&secret="+secret2+"&js_code="
    +js_code+"&grant_type=authorization_code")
    .then(response => {
      const openid = response.data.openid
      const unionid = response.data.unionid
      Clients.findOne({unionid: unionid, openid: openid}, (err1, doc1) => {
        if (err1) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
        else if (doc1) {
          if (""+doc1._id === clientId1) return res.json({code: 3})
          Bindings.findOne({clientId2: ""+doc1._id, level}, (err2, doc2) => {
            if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else if (doc2) return res.json({code: 2})
            else {
              Bindings.create({clientId1, clientId2: ""+doc1._id, level}, (err3, doc3) => {
                if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else return res.json({code: 1})
              })
            }
          })
        } else {
          Clients.create({openid, unionid,
            name: "", sex: "", age: "", idnum: "", history: "", smoke: "", description: "",
            phone: "", email: "",
            wechat: "", username: "", avatar: "", address: "", vip: false, app: 2
          }, (err4, doc4) => {
            if (err4) {
              return res.json({code: 0, msg: "后端出错了，请刷新"})
            } else {
              Bindings.create({clientId1, clientId2: ""+doc4._id, level}, (err5, doc5) => {
                if (err5) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else return res.json({code: 1, clientId2: doc4._id})
              })
            }
          })
        }
      })
    })
    .catch(err => {
      console.log(err)
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    })
})

app.post('/unbind2', function (req, res) {
  const { clientId1, clientId2 } = req.body
  Bindings.remove({clientId1, clientId2}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else {
      return res.json({code: 1})
    }
  })
})


app.get('/bindinfo2', (req, res) => {
  const {clientId} = req.query
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const clientIds = doc1.filter(bind => bind.level === 2).map(bind => bind.clientId2)
      const subIds = doc1.filter(bind => bind.level === 1).map(bind => bind.clientId2)
      Bindings.find({clientId1: {$in: subIds}, level: 2}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const subClientInfo = doc2.map(bind => {return {id: bind.clientId2, mode: 2, supId: bind.clientId1}})
          const clientInfo = clientIds.map(clientId => {return {id: clientId, mode: 1}})
          const allInfo = [...clientInfo, ...subClientInfo]
          Payments.find({_id: {$in: allInfo.map(info => info.id)}, planId: "noPlan"}, (err3, doc3) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              const subInfo = subIds.map(subId => {return {id: subId, mode: 3}})
              const allInfo2 = [...allInfo, subInfo]
              Clients.find({_id: {$in: allInfo2.map(info => info.id)}}, (err4, doc4) => {
                if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else {
                  const clients = clientIds.map(clientId => {
                    const clientInfo = doc4.filter(client => client._id+"" === clientId)[0]
                    const paymentInfo = doc3.filter(payment => payment.clientId === clientId)
                    return {
                      id: clientId,
                      name: clientInfo.name,
                      payment: paymentInfo
                    }
                  })
                  const promoters = subIds.map(subId => {
                    const subInfo = doc4.filter(client => client._id+"" === subId)[0]
                    return {
                      id: subId,
                      name: subInfo.name,
                      subClients: subClientInfo.filter(subClient => subClient.supId === subId).map(subClient => {
                        const subClientInfo = doc4.filter(client => client._id+"" === subClient.id)[0]
                        const paymentInfo = doc3.filter(payment => payment.clientId === subClient.id)
                        return {
                          id: subClient.id,
                          name: subClientInfo.name,
                          payment: paymentInfo
                        }
                      })
                    }
                  })
                  const result = {}
                  result.clients = clients
                  result.promoters = promoters
                  return res.json({code: 1, result})
                }
              })
            }
          })
        }
      })
    }
  })
})

app.get('/promotion2/overview', (req, res) => {
  const {clientId} = req.query
  const params = [{client: 20, purchaseByClients: 20000, promoter: 20, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 40, purchaseByClients: 40000, promoter: 40, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 60, purchaseByClients: 60000, promoter: 60, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 80, purchaseByClients: 80000, promoter: 80, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 100, purchaseByClients: 100000, promoter: 100, clientPercent: 0.15, promoterPercent: 0.1}
  ]
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const clientIds = doc1.filter(bind => bind.level === 2).map(bind => bind.clientId2)
      const subIds = doc1.filter(bind => bind.level === 1).map(bind => bind.clientId2)
      Bindings.find({clientId1: {$in: subIds}, level: 2}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const subClientInfo = doc2.map(bind => {return {id: bind.clientId2, mode: 2, supId: bind.clientId1}})
          const clientInfo = clientIds.map(clientId => {return {id: clientId, mode: 1}})
          const allInfo = [...clientInfo, ...subClientInfo]
          Payments.find({_id: {$in: allInfo.map(info => info.id)}, planId: "noPlan"}, (err3, doc3) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              let clientTotal = 0
              let clientOrderNumber = 0
              const clients = clientIds.map(clientId => {
                const paymentInfo = doc3.filter(payment => payment.clientId === clientId)
                paymentInfo.forEach(payment => {clientTotal += payment.price})
                clientOrderNumber += paymentInfo.length
                return {
                  id: clientId,
                  payment: paymentInfo
                }
              })
              let promoterTotal = 0
              let promoterOrderNumber = 0
              const promoters = subIds.map(subId => {
                return {
                  id: subId,
                  subClients: subClientInfo.filter(subClient => subClient.supId === subId).map(subClient => {
                    const paymentInfo = doc3.filter(payment => payment.clientId === subClient.id)
                    paymentInfo.forEach(payment => promoterTotal += payment.price)
                    promoterOrderNumber += paymentInfo.length
                    return {
                      id: subClient.id,
                      payment: paymentInfo
                    }
                  })
                }
              })

              // 判断几级，默认1级

              return res.json({code: 1, clientNumber: clients.length,
                promoterNumber: promoters.length, level: 1, amountWaiting: clientTotal*0.15 + promoterTotal*0.1,
                amountSent: 0, clientOrderNumber, promoterOrderNumber})
            }
          })
        }
      })
    }
  })
})

app.get('/promotion2/rule', (req, res) => {
  const {clientId} = req.query
  const params = [{client: 20, purchaseByClients: 20000, promoter: 20, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 40, purchaseByClients: 40000, promoter: 40, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 60, purchaseByClients: 60000, promoter: 60, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 80, purchaseByClients: 80000, promoter: 80, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 100, purchaseByClients: 100000, promoter: 100, clientPercent: 0.15, promoterPercent: 0.1}
  ]
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const clientIds = doc1.filter(bind => bind.level === 2).map(bind => bind.clientId2)
      const subIds = doc1.filter(bind => bind.level === 1).map(bind => bind.clientId2)
      Bindings.find({clientId1: {$in: subIds}, level: 2}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const subClientInfo = doc2.map(bind => {return {id: bind.clientId2, mode: 2, supId: bind.clientId1}})
          const clientInfo = clientIds.map(clientId => {return {id: clientId, mode: 1}})
          const allInfo = [...clientInfo, ...subClientInfo]
          Payments.find({_id: {$in: allInfo.map(info => info.id)}, planId: "noPlan"}, (err3, doc3) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              let clientTotal = 0
              const clients = clientIds.map(clientId => {
                const paymentInfo = doc3.filter(payment => payment.clientId === clientId)
                paymentInfo.forEach(payment => clientTotal += payment.price)
                return {
                  id: clientId,
                  payment: paymentInfo
                }
              })
              let promoterTotal = 0
              const promoters = subIds.map(subId => {
                return {
                  id: subId,
                  subClients: subClientInfo.filter(subClient => subClient.supId === subId).map(subClient => {
                    const paymentInfo = doc3.filter(payment => payment.clientId === subClient.id)
                    paymentInfo.forEach(payment => promoterTotal += payment.price)
                    return {
                      id: subClient.id,
                      payment: paymentInfo
                    }
                  })
                }
              })

              // 判断几级，默认1级

              return res.json({code: 1, clientPercent: 0.15, promoterPercent: 0.1,
                upgrade: [
                  {type: "累计绑定客户", total: 20, current: clients.length},
                  {type: "客户累计消费", total: 20000, current: clientTotal},
                  {type: "累计邀请推广员", total: 20, current: promoters.length},
                ]
              })
            }
          })
        }
      })
    }
  })
})

app.post('/promotion2/income', (req, res) => {
  const {clientId, type, status, date, pageNum, pageSize, customerId} = req.body
  const params = [{client: 20, purchaseByClients: 20000, promoter: 20, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 40, purchaseByClients: 40000, promoter: 40, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 60, purchaseByClients: 60000, promoter: 60, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 80, purchaseByClients: 80000, promoter: 80, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 100, purchaseByClients: 100000, promoter: 100, clientPercent: 0.15, promoterPercent: 0.1}
  ]
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const clientIds = doc1.filter(bind => bind.level === 2).map(bind => bind.clientId2)
      const subIds = doc1.filter(bind => bind.level === 1).map(bind => bind.clientId2)
      Bindings.find({clientId1: {$in: subIds}, level: 2}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const subClientInfo = doc2.map(bind => {return {id: bind.clientId2, mode: 2, supId: bind.clientId1}})
          const clientInfo = clientIds.map(clientId => {return {id: clientId, mode: 1}})
          const allInfo = [...clientInfo, ...subClientInfo]
          Payments.find({_id: {$in: allInfo.map(info => info.id)}, planId: "noPlan"}, (err3, doc3) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              const subInfo = subIds.map(subId => {return {id: subId, mode: 3}})
              const allInfo2 = [...allInfo, subInfo]
              Clients.find({_id: {$in: allInfo2.map(info => info.id)}}, (err4, doc4) => {
                if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else {
                  let clientOrderRecord = []
                  let clientOrderNumber = 0
                  let clientTotal = 0
                  const clients = clientIds.map(clientId => {
                    const clientInfo = doc4.filter(client => client._id+"" === clientId)[0]
                    const paymentInfo = doc3.filter(payment => payment.clientId === clientId)
                    paymentInfo.forEach(payment => {
                      const timezone = 8;
                      const offset_GMT = new Date().getTimezoneOffset();
                      const correctDate = new Date(new Date(payment.date).getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
                      const year = correctDate.getFullYear();
                      const month = ('0' + (correctDate.getMonth() + 1)).slice(-2);
                      const day = ('0' + (correctDate.getDate())).slice(-2);
                      const hour = ('0' + (correctDate.getHours())).slice(-2);
                      const minutes = ('0' + (correctDate.getMinutes())).slice(-2);
                      const seconds = ('0' + (correctDate.getSeconds())).slice(-2);
                      const copyPayment = payment
                      copyPayment.formatDate = ""+year+month+day+" "+hour+":"+minutes+":"+seconds
                      copyPayment.clientName = subClientInfo.name
                      copyPayment.percent = 0.15
                      copyPayment.status = "未结算"
                      clientOrderRecord.push(copyPayment)
                      clientTotal += payment.price
                    })
                    clientOrderNumber += paymentInfo.length
                    return {
                      id: clientId,
                      name: clientInfo.name,
                      payment: paymentInfo
                    }
                  })
                  let promoterOrderRecord = []
                  let promoterOrderNumber = 0
                  let promoterTotal = 0
                  const promoters = subIds.map(subId => {
                    const subInfo = doc4.filter(client => client._id+"" === subId)[0]
                    return {
                      id: subId,
                      name: subInfo.name,
                      subClients: subClientInfo.filter(subClient => subClient.supId === subId).map(subClient => {
                        const subClientInfo = doc4.filter(client => client._id+"" === subClient.id)[0]
                        const paymentInfo = doc3.filter(payment => payment.clientId === subClient.id)
                        paymentInfo.forEach(payment => {
                          const timezone = 8;
                          const offset_GMT = new Date().getTimezoneOffset();
                          const correctDate = new Date(new Date(payment.date).getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
                          const year = correctDate.getFullYear();
                          const month = ('0' + (correctDate.getMonth() + 1)).slice(-2);
                          const day = ('0' + (correctDate.getDate())).slice(-2);
                          const hour = ('0' + (correctDate.getHours())).slice(-2);
                          const minutes = ('0' + (correctDate.getMinutes())).slice(-2);
                          const seconds = ('0' + (correctDate.getSeconds())).slice(-2);
                          const copyPayment = payment
                          copyPayment.formatDate = ""+year+month+day+" "+hour+":"+minutes+":"+seconds
                          copyPayment.clientName = subClientInfo.name
                          copyPayment.percent = 0.1
                          copyPayment.status = "未结算"
                          promoterOrderRecord.push(copyPayment)
                          promoterTotal += payment.price
                        })
                        promoterOrderNumber += paymentInfo.length
                        return {
                          id: subClient.id,
                          name: subClientInfo.name,
                          payment: paymentInfo
                        }
                      })
                    }
                  })
                  if (type === "client") {
                    if (!!date) clientOrderRecord = clientOrderRecord.filter(record => record.formatDate.substring(0,4) === date)
                    if (!!customerId) clientOrderRecord = clientOrderRecord.filter(record => record.clientId === customerId)
                    clientOrderRecord.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
                    return res.json({code: 1, list: clientOrderRecord.slice(pageSize*(pageNum-1), pageSize*pageNum),
                      orderNumber: clientOrderNumber, amountWaiting: clientTotal * 0.15, amountSent: 0})
                  } else if (type === "promoter") {
                    if (!!date) promoterOrderRecord = promoterOrderRecord.filter(record => record.formatDate.substring(0,4) === date)
                    promoterOrderRecord.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
                    return res.json({code: 1, list: promoterOrderRecord.slice(pageSize*(pageNum-1), pageSize*pageNum),
                      orderNumber: clientOrderNumber, amountWaiting: clientTotal * 0.1, amountSent: 0})
                  } else return res.json({code: 2})
                }
              })
            }
          })
        }
      })
    }
  })
})

app.post('/promotion2/customer', (req, res) => {
  const {clientId, name, type, pageNum, pageSize, sort} = req.body
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const clientIds = doc1.filter(bind => bind.level === 2).map(bind => bind.clientId2)
      Payments.find({_id: {$in: clientIds}, planId: "noPlan"}, (err3, doc3) => {
        if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          Clients.find({_id: {$in: clientIds}}, (err4, doc4) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              let clients = clientIds.map(clientId => {
                const clientInfo = doc4.filter(client => client._id+"" === clientId)[0]
                const paymentInfo = doc3.filter(payment => payment.clientId === clientId).sort((a, b) => new Date(a.date).getTime()-new Date(b.date).getTime())
                let clientTotal = 0
                paymentInfo.forEach(payment => {
                  clientTotal += payment.price
                  const timezone = 8;
                  const offset_GMT = new Date().getTimezoneOffset();
                  const correctDate = new Date(new Date(payment.date).getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
                  const year = correctDate.getFullYear();
                  const month = ('0' + (correctDate.getMonth() + 1)).slice(-2);
                  const day = ('0' + (correctDate.getDate())).slice(-2);
                  const hour = ('0' + (correctDate.getHours())).slice(-2);
                  const minutes = ('0' + (correctDate.getMinutes())).slice(-2);
                  const seconds = ('0' + (correctDate.getSeconds())).slice(-2);
                  payment.formatDate = ""+year+month+day+" "+hour+":"+minutes+":"+seconds
                })
                return {
                  id: clientId,
                  name: clientInfo.name,
                  payment: paymentInfo,
                  lastOrderTime: paymentInfo.length > 0 ? paymentInfo[paymentInfo.length-1].formatDate : "无",
                  amountWaiting: clientTotal,
                  amountSent: 0,
                  avatar: clientInfo.avatar ? clientInfo.avatar : ""
                }
              })
              const total = clients.length
              if (!!name) {
                clients = clients.filter(client => client.name.indexOf(name) >= 0)
              }
              if (sort === 1) clients.sort((a, b) => new Date(a.payment[0].date).getTime() - new Date(b.payment[0].date).getTime())
              else if (sort === 3) clients.sort((a, b) => b.total - a.total)
              else clients.sort((a, b) => new Date(a.payment[a.payment.length-1].date).getTime() - new Date(b.payment[b.payment.length-1].date).getTime())

              clients.forEach(client => client.payment = undefined)

              return res.json({code: 1, bind: total, unbind: 0, customers: type === "1" ? clients.slice(pageSize*(pageNum-1), pageSize*pageNum) : []})
            }
          })
        }
      })
    }
  })
})

app.get('/promotion2/customer/:id', (req, res) => {
  const {clientId} = req.query
  const customerId = req.params.id
  const params = [{client: 20, purchaseByClients: 20000, promoter: 20, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 40, purchaseByClients: 40000, promoter: 40, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 60, purchaseByClients: 60000, promoter: 60, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 80, purchaseByClients: 80000, promoter: 80, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 100, purchaseByClients: 100000, promoter: 100, clientPercent: 0.15, promoterPercent: 0.1}
  ]
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const clientIds = doc1.filter(bind => bind.level === 2).map(bind => bind.clientId2)
      if (clientIds.indexOf(customerId) < 0) return res.json({code: 2})
      Payments.find({_id: customerId, planId: "noPlan"}, (err3, doc3) => {
        if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          Clients.find({_id: customerId}, (err4, doc4) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              const clientInfo = doc4[0]
              const paymentInfo = doc3.sort((a, b) => new Date(a.date).getTime()-new Date(b.date).getTime())
              let clientTotal = 0
              paymentInfo.forEach(payment => {
                clientTotal += payment.price
                const timezone = 8;
                const offset_GMT = new Date().getTimezoneOffset();
                const correctDate = new Date(new Date(payment.date).getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
                const year = correctDate.getFullYear();
                const month = ('0' + (correctDate.getMonth() + 1)).slice(-2);
                const day = ('0' + (correctDate.getDate())).slice(-2);
                const hour = ('0' + (correctDate.getHours())).slice(-2);
                const minutes = ('0' + (correctDate.getMinutes())).slice(-2);
                const seconds = ('0' + (correctDate.getSeconds())).slice(-2);
                payment.formatDate = ""+year+month+day+" "+hour+":"+minutes+":"+seconds
              })
              // return {
              //   id: clientId,
              //   name: clientInfo.name,
              //   lastOrderTime: paymentInfo[paymentInfo.length-1].formatDate,
              //   total: totalPrice
              // }
              return res.json({code: 1, name: clientInfo.name, avatar: clientInfo.avatar,
                lastOrderTime: paymentInfo.length > 0 ? paymentInfo[paymentInfo.length-1].formatDate : "无",
                bindTime: paymentInfo.length > 0 ? paymentInfo[0].formatDate : "无",
                amountWaiting: clientTotal*0.15, orderNumberWaiting: paymentInfo.length,
                amountSent: 0, orderNumberSent: 0
              })
            }
          })
        }
      })
    }
  })
})


app.post('/promotion2/promoter', (req, res) => {
  const {clientId, name, pageNum, pageSize} = req.body
  const params = [{client: 20, purchaseByClients: 20000, promoter: 20, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 40, purchaseByClients: 40000, promoter: 40, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 60, purchaseByClients: 60000, promoter: 60, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 80, purchaseByClients: 80000, promoter: 80, clientPercent: 0.15, promoterPercent: 0.1},
    {client: 100, purchaseByClients: 100000, promoter: 100, clientPercent: 0.15, promoterPercent: 0.1}
  ]
  Bindings.find({clientId1: clientId}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      const subIds = doc1.filter(bind => bind.level === 1).map(bind => bind.clientId2)
      Bindings.find({clientId1: {$in: subIds}, level: 2}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
          const subClientInfo = doc2.map(bind => {return {id: bind.clientId2, mode: 2, supId: bind.clientId1}})
          Payments.find({_id: {$in: subClientInfo.map(info => info.id)}, planId: "noPlan"}, (err3, doc3) => {
            if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
            else {
              Clients.find({_id: {$in: subClientInfo.map(info => info.id)}}, (err4, doc4) => {
                if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
                else {
                  let promoters = subClientInfo.map(subClient => {
                    const client = doc4.filter(client => client._id+"" === subClient.id)[0]
                    const paymentInfo = doc3.filter(payment => payment.clientId === subClient.id)
                    let promoterTotal = 0
                    paymentInfo.forEach(payment => {
                      promoterTotal += payment.price
                    })
                    return {
                      id: subClient.id,
                      name: client.name,
                      avatar: client.avatar,
                      promoterTotal: promoterTotal * 0.1
                    }
                  })

                  if (!!name) promoters = promoters.filter(promoter => promoter.name.indexOf(name) >= 0)

                  return res.json({code: 1, promoters: promoters.slice(pageSize*(pageNum-1), pageSize*pageNum)})
                }
              })
            }
          })
        }
      })
    }
  })
})

app.post('/doctorupload', function (req, res) {
  console.log(1)
  const form = new formidable.IncomingForm()
  let uploadDir = path.join(__dirname, "uploads");
  form.uploadDir = uploadDir; //本地文件夹目录路径

  form.parse(req, (err, fields, files) => {
    // if (files.myFile.size >= 1048576) {
    //   return res.json({code: 0, msg: "图片不能大于1M"})
    // }
    const after = new Date().getTime()
    let oldPath = files.myFile.path;//这里的路径是图片的本地路径
    let newPath = path.join(path.dirname(oldPath), fields.doctorId + "_" + after);
    fs.rename(oldPath, newPath, err => { //fs.rename重命名图片名
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      else {
        Records.findOne({clientId: fields.clientId, doctorId: fields.doctorId}, (err, doc) => {
          console.log(fields)
          if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
          if (doc) {
            Records.findOneAndUpdate(
              {clientId: fields.clientId, doctorId: fields.doctorId},
              {$push: {records: {name: fields.doctorId + "_" + after, hidden: false}}
              }).exec((err) => {
              if (!err) {
                return res.json({code: 1})
              } else {
                console.log(err)
                return res.json({code: 0, msg: "后端出错了，请刷新"})
              }
            })
          } else {
            Records.create(
              {clientId: fields.clientId,
                doctorId: fields.doctorId,
                records: [{name: fields.doctorId + "_" + after, hidden: false}],
                description: ""
              },
              err => {
                if (!err) {
                  return res.json({code: 1})
                } else {
                  console.log(err)
                  return res.json({code: 0, msg: "后端出错了，请刷新"})
                }
              })
          }

        })

      }
    })

  })
})

app.post('/homeworkupload', function (req, res) {
  console.log(1)
  const form = new formidable.IncomingForm()
  let uploadDir = path.join(__dirname, "uploads");
  form.uploadDir = uploadDir; //本地文件夹目录路径

  form.parse(req, (err, fields, files) => {
    // if (files.myFile.size >= 1048576) {
    //   return res.json({code: 0, msg: "图片不能大于1M"})
    // }
    console.log(files, fields.size)

    for (let i = 0; i < Number(fields.size); i++) {
      let oldPath = files["myFile"+i].path;//这里的路径是图片的本地路径
      let newPath = path.join(path.dirname(oldPath), fields.classId + "_" + fields.dateTime + "_" + i);
      fs.rename(oldPath, newPath, err => { //fs.rename重命名图片名
        if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
        else {
        }
      })
    }
    return res.json({code: 1})


  })
})

app.get('/records', (req, res) => {
  const {clientId, doctorId} = req.query
  Records.findOne({clientId, doctorId}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else {
      if (doc) {
        return res.json({code: 1, description: doc.description,
          records: doc.records.filter(record => !record.hidden).map(record => "https://backend.voin-cn.com/"+record.name)})
      } else {
        return res.json({code: 1, description: "",
          records: []})
      }
    }
  })
})

app.post('/recommend', (req, res) => {
  const {clientId, doctorId, roots} = req.body
  Plans.findOne({clientId, doctorId}, (err1, doc1) => {
    if (!err1) {
      const categories = doc1.category
      Doctors.findOne({_id: doctorId}, (err2, selfDoc) => {
        if (!err2) {
          const selfTemps = selfDoc.templates.filter(temp => {
            if (!temp.category || temp.category === null || temp.category.length === 0) return false
            for (let i = 0; i < categories.length; i++) {
              if (temp.category.indexOf(categories[i]) > -1) return true
            }
            return false
          })
          const codeIds = roots.filter(cat => cat.type === "disease" || cat.type === "operation").map(cat => cat.rootId)
          ClientCategories.find({doctorId: "5b68bb0fde7068270a29d0fe",
            name: {$in: codeIds}, type: {$in: ["disease", "operation"]}}, (err3, doc3) => {
            if (!err3) {
              const catIds = doc3.map(cat => ""+cat._id)
              Doctors.findOne({_id: "5b68bb0fde7068270a29d0fe"}, (err4, doc4) => {
                if (!err4) {
                  const expertTemps = doc4.templates.filter(temp => {
                    if (!temp.category || temp.category === null || temp.category.length === 0) return false
                    for (let i = 0; i < catIds.length; i++) {
                      if (temp.category.indexOf(catIds[i]) > -1) return true
                    }
                    return false
                  })
                  return res.json({code: 1, selfTemps, expertTemps })
                } else return res.json({code: 0, msg: "后端出错了，请刷新"})
              })
            } else return res.json({code: 0, msg: "后端出错了，请刷新"})
          })
        } else {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        }
      })
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/readrequest', (req, res) => {
  const { clientId, doctorId} = req.body
  Plans.findOneAndUpdate({clientId, doctorId}, {$set: { read: true }}, (err, doc) => {
    if (!err) {
      return res.json({code: 1})
    }
    else console.log(err)
  })
})


app.post('/changepass', (req, res) => {
  const { username } = req.body
  Doctors.findOneAndUpdate(
    {username: username},
    {$set: {password: md5Password("11111111")}}, (err, doc) => {
      if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
      if (doc === null) return res.json({code: 2})
      else return res.json({code: 1})
    })
})

app.get('/sendforget', (req, res) => {
  const {phone} = req.query
  const code = "" + Math.floor(Math.random() * (1000000-100000) + 100000);

  Doctors.findOneAndUpdate({phone: phone}, {code: code}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else if (doc) {

      const params = {
        "RegionId": "cn-hangzhou",
        "PhoneNumbers": ""+phone,
        "SignName": "沃衍健康",
        "TemplateCode": "SMS_189615519",
        "TemplateParam": "{\"code\":\""+code+"\"}"
      }


      sms.request('SendSms', params, {method: 'POST'}).then((result) => {
        return res.json({code: 1})
      }, (ex) => {
        console.log(ex)
        return res.json({code: 0, msg: "短信发送请求出错了，请刷新"})
      })
    } else {
      return res.json({code: 2, msg: "该手机号用户还未注册"})
    }
  })
})

app.post('/createteam', (req, res) => {
  const { doctorId, name, username, password } = req.body
  Teams.findOne({username}, (err1, doc1) => {
    if (!err1) {
      if (doc1) {
        res.json({code: 0, msg: "该团队代号已存在，请更换后输入"})
      } else {
        Teams.create({name, username, password, templates: []}, (err2, doc2) => {
          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else {
            DoctorInTeams.create({doctorId, teamId: doc2._id, creator: true}, (err3, doc3) => {
              if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
              else return res.json({code: 1, msg: "团队创建成功"})
            })
          }
        })
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/jointeam', (req, res) => {
  const { doctorId, username, password } = req.body
  Teams.findOne({username, password}, _filter_team, (err1, doc1) => {
    if (!err1) {
      if (doc1) {
        DoctorInTeams.findOne({doctorId, teamId: doc1._id}, (err2, doc2) => {
          if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
          else if (doc2) res.json({code: 0, msg: "您已在该团队中"})
          else {
            DoctorInTeams.create({doctorId, teamId: doc1._id, creator: false}, (err3, doc3) => {
              if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
              else return res.json({code: 1, msg: "加入团队"+doc1.name+"成功"})
            })
          }
        })
      } else {
        res.json({code: 0, msg: "团队代号或密码错误，请重新输入"})
      }
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.post('/cases', (req, res) => {
  const {keyWords} = req.body
  const keyWordsArray = keyWords.split(" ").filter(e => e.length > 0)
  const finalString = "(?=.*"+keyWordsArray.join(")(?=.*")+")"
  Cases.find({name: { $regex: finalString }}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else if (doc.length > 0) {
      return res.json({code: 1, doc: doc.map(c => c.name)})
    } else {
      return res.json({code: 2})
    }
  })
})

app.post('/resources', (req, res) => {
  const {cat, label, keyWords} = req.body
  const keyWordsArray = keyWords.split(" ").filter(e => e.length > 0)
  const finalString = "(?=.*"+keyWordsArray.join(")(?=.*")+")"
  const options = {
    cat: cat === "-1" ? {$ne: null} : cat,
    label: label === "-1" ? {$ne: null} : label,
    name: { $regex: finalString}
    }

  Resources.find(options, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else if (doc.length > 0) {
      return res.json({code: 1, doc: doc.map(c => c.name)})
    } else {
      return res.json({code: 2})
    }
  })
})

app.post('/allowwatching', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {admin: 2}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/disallowwatching', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {admin: 0}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/allowfree', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {free30: true}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/disallowfree', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {free30: false}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/allowqrcode', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {qrcode: true}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/disallowqrcode', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {qrcode: false}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/allowcircle', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {circle: true}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/disallowcircle', (req, res) => {
  const {doctorId} = req.body
  Doctors.findByIdAndUpdate(doctorId, {$set: {circle: false}}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    else return res.json({code: 1})
  })
})

app.post('/specialize', (req, res) => {
  const {catId} = req.body
  ClientCategories.findOneAndUpdate({
    _id: catId
  }, {
    $set: {
      special: true,
    },
  }, err => {
    if (!err) {
      return res.json({code: 1})
    } else {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    }
  })
})

app.get('/categorycode', (req, res) => {
  const tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid2+"&secret="+secret2;
  const codeUrl = 'https://api.weixin.qq.com/wxa/getwxacode?access_token='
  const {catId} = req.query

  axios.get(tokenUrl)
    .then(function (response) {
      const token = response.data.access_token
      axios({
        method: 'post',
        responseType: 'arraybuffer',
        url: codeUrl+token,
        data: {
          // access_token: token,
          path: "pages/exclusive-group/exclusive-group?mode="+catId,
          env_version: "release"
        }
      }).then(response1 => {
        const buffer = Buffer.from(response1.data, 'base64');
        return res.json({code: 1, buffer})
      }).catch(err1 => {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      })
    })
    .catch(function (error) {
      // handle error
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    })
})

app.get('/templateqr', (req, res) => {
  const tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid2+"&secret="+secret2;
  const codeUrl = 'https://api.weixin.qq.com/wxa/getwxacode?access_token='
  const {templateId, doctorId, mode} = req.query
  axios.get(tokenUrl)
      .then(function (response) {
        const token = response.data.access_token
        axios({
          method: 'post',
          responseType: 'arraybuffer',
          url: codeUrl+token,
          data: {
            // access_token: token,
            path: "pages/plan/plan?templateId="+templateId+"&doctorId="+doctorId+"&price=99",
            env_version: (mode === "2" || mode === 2) ? "develop" : "release"
          }
        }).then(response1 => {
          const buffer = Buffer.from(response1.data, 'base64');
          return res.json({code: 1, buffer})
        }).catch(err1 => {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        })
      })
      .catch(function (error) {
        // handle error
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      })
})

app.get('/promotion2/qrcode', (req, res) => {
  const tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid2+"&secret="+secret2;
  const codeUrl = 'https://api.weixin.qq.com/wxa/getwxacode?access_token='
  const {path, clientId, type} = req.query
  axios.get(tokenUrl)
      .then(function (response) {
        const token = response.data.access_token
        
        axios({
          method: 'post',
          responseType: 'arraybuffer',
          url: codeUrl+token,
          data: {
            // access_token: token,
            path: path+"?clientId="+clientId+"&type="+type,
            env_version: "release"
          }
        }).then(response1 => {
          const buffer = Buffer.from(response1.data, 'base64');
          return res.json({code: 1, buffer})
        }).catch(err1 => {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        })
      })
      .catch(function (error) {
        // handle error
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      })
})

app.get('/usernameunicode', (req, res) => {
  const {username} = req.query

  const tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secret;
  const url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit';

  axios.get(tokenUrl)
    .then(function (response) {
      const token = response.data.access_token
      axios({
        method: 'post',
        responseType: 'arraybuffer',
        url: url+"?access_token="+token,
        data: {
          // access_token: token,
          scene: username,
          is_hyaline: true
        }
      }).then(response1 => {
        const buffer = Buffer.from(response1.data, 'base64');
        return res.json({code: 1, buffer})
      }).catch(err1 => {
        return res.json({code: 0, msg: "后端出错了，请刷新"})
      })
    })
    .catch(function (error) {
      // handle error
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    })

})

app.get('/classstudent', (req, res) => {
  const {doctorId} = req.query
  DoctorInClasses.find({doctorId, isTeacher: true}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    if (doc1.length > 0) {
      const classId = doc1.map(cl => cl.classId)[0]
      DoctorInClasses.find({classId, isTeacher: false}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        const studentIds = doc2.map(cl => cl.doctorId)
        Doctors.find({_id: {$in: studentIds}}, (err3, doc3) => {
          if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
          const students = doc3.map(student => {
            return {
              name: student.name,
              username: student.username,
              studentId: student._id
            }
          })
          HomeworkTitles.find({classId}, (err4, doc4) => {
            if (err4) return res.json({code: 0, msg: "后端出错了，请刷新"})
            Classes.findOne({_id: classId}, (err5, doc5) => {
              if (err5) return res.json({code: 0, msg: "后端出错了，请刷新"})
              return res.json({code: 1, titles: doc4, students, classInfo: doc5})
            })
          })
        })
      })
    } else return res.json({code: 0, msg: "您暂无任何学生"})
  })
})

app.get('/classhomework', (req, res) => {
  const {doctorId} = req.query
  DoctorInClasses.find({doctorId, isTeacher: false}, (err1, doc1) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    if (doc1.length > 0) {
      const classId = doc1.map(cl => cl.classId)[0]
      DoctorInClasses.findOne({classId, isTeacher: true}, (err2, doc2) => {
        if (err2) return res.json({code: 0, msg: "后端出错了，请刷新"})
        const teacherId = doc2.doctorId
        Doctors.findOne({_id: teacherId}, (err3, doc3) => {
          if (err3) return res.json({code: 0, msg: "后端出错了，请刷新"})
          const teacher = {
            name: doc3.name,
            username: doc3.username,
            teacherId: doc3._id
          }
          HomeworkTitles.find({classId}, (err4, doc4) => {
            if (err4) return res.json({code: 0, msg: "后端出错了，请刷新"})
            HomeworkSubmits.find({doctorId}, (err5, doc5) => {
              const submits = doc5.map(title => {
                return {
                  titleId: title.titleId, score: title.score, review: title.review
                }
              })
              if (err5) return res.json({code: 0, msg: "后端出错了，请刷新"})
              Classes.findOne({_id: classId}, (err6, doc6) => {
                if (err6) return res.json({code: 0, msg: "后端出错了，请刷新"})
                return res.json({code: 1, titles: doc4, submits, teacher, classInfo: doc6})
              })
            })
          })
        })
      })
    } else return res.json({code: 0, msg: "您暂无任何作业"})
  })
})

app.get('/homeworkfortitle', (req, res) => {
  const {titleId} = req.query
  HomeworkSubmits.find({titleId}, (err, doc) => {
    if (err) return res.json({code: 0, msg: "后端出错了，请刷新"})
    const result = doc.map(title => {
      return {
        titleId: title.titleId,
        doctorId: title.doctorId,
        score: title.score,
        review: title.review
      }
    })
    return res.json({code: 1, doc: result})
  })
})


app.post('/submitreview', (req, res) => {
  const {studentId, titleId, score, review} = req.body
  HomeworkSubmits.findOneAndUpdate({doctorId: studentId, titleId}, {$set: {score, review}}, (err, doc) => {
    if (err) {
      return res.json({code: 0, msg: "后端出错了，请刷新"})
    } else return res.json({code: 1})
  })
})

app.post('/newdoctors', (req, res) => {
  const {doctorId, doctors} = req.body
  const newDoctors = doctors.map(doctor => {
    return {
      name: doctor,
      original: false
    }
  })
  Doctors.findById(doctorId, (err1, doc) => {
    if (err1) return res.json({code: 0, msg: "后端出错了，请刷新"})
    if (doc.childDoctors && doc.childDoctors[0]) {
      Doctors.findByIdAndUpdate(doctorId, {$push: {childDoctors: {$each: newDoctors}}}, (err, doc) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        } else return res.json({code: 1})
      })
    } else {
      newDoctors.splice(0, 0, {name: "我的医生", original: true})
      Doctors.findByIdAndUpdate(doctorId, {$set: {childDoctors: newDoctors}}, (err, doc) => {
        if (err) {
          return res.json({code: 0, msg: "后端出错了，请刷新"})
        } else return res.json({code: 1})
      })
    }
  })
})


//
const privateKey  = fs.readFileSync(`${__dirname}/https/11261939_backend.voin-cn.com.key`, 'utf8');
const certificate = fs.readFileSync(`${__dirname}/https/11261939_backend.voin-cn.com.pem`, 'utf8');


https.createServer({
  key:privateKey, cert:certificate
}, app).listen(8081, (err) => {
  if (err) {
    console.log("Node app start at https 8081 new error")
    console.log(err)
  }
  console.log("Node app start at https 8081 new")
});


// const http = require('http')
//
// http.createServer(function(req, res) {
//   res.writeHead(200, {'Content-Type': 'text/plain'})
//   res.end('來自')
// }).listen(8081)
//
// console.log('123')
//
//
//
// app.listen(process.env.PORT || 8082, () => {
//   console.log("Node app start at port 8081 " + process.env.PORT)
// })