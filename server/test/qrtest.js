const QRCode = require('qrcode')

var opts = {
  errorCorrectionLevel: 'H',
  type: 'image/jpeg',
  quality: 0.3,
  margin: 1,
  color: {
    dark:"#010599FF",
    light:"#FFBF60FF"
  }
}

QRCode.toDataURL('I love you', opts, function (err, url) {
  if (err) throw err

  const img = document.getElementById('image')
  img.src = url
})
