import classes from "../../src/components/Preview/Preview.css";
import Input from "../../src/components/UI/Input/Input";
import React from "react";

const mongoose = require('mongoose')
const model = require('../models')

mongoose.connect('mongodb://root:1qaz2wsx@dds-2ze22d840c7202a41638-pub.mongodb.rds.aliyuncs.com:3717,dds-2ze22d840c7202a42360-pub.mongodb.rds.aliyuncs.com:3717/admin?replicaSet=mgset-16599369', {useNewUrlParser: true})


const Doctors = model.getModel("doctors")
const Clients = model.getModel("clients")
const Plans = model.getModel("plans")
const Movements = model.getModel("movements")
const Templates = model.getModel("templates")
const Categories = model.getModel("categories")
const Records = model.getModel("records")

const list = Movements.aggregate([
  { $match: { } },

  { $lookup: {
      from: "phones",
      localField: "info.phone",
      foreignField: "phone",
      as: "zoneinfo"
    }
  }
]);

list.result.forEach(function(x) {
  db.names.update({_id:x._id}, {$set:{'info.timezone':'zoneinfo.timezone'}});
});


<div className={classes.Block}>
  <div className={classes.SmallRow}>
    <label>患者真实姓名</label>
  </div>
  <Input smallFont onChange={this.props.onClientNameBChange} value={this.props.clientNameB}/>
  <div className={classes.SmallRow}>
    <label>患者邮箱</label>
  </div>
  <Input smallFont onChange={this.props.onClientEmailChange} value={this.props.clientEmail}/>
</div>