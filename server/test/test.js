const csvjson = require('csvjson');
const path = require('path')
const fs = require('fs');

const data = fs.readFileSync(path.join(__dirname, 'prescription2.csv'), { encoding : 'utf8'});
/*
{
    delimiter : <String> optional default is ","
    quote     : <String|Boolean> default is null
}
*/
const options = {
  delimiter : ',', // optional
  quote     : '"' // optional
};

const result = csvjson.toSchemaObject(data, options);


fs.appendFile("input1.csv", JSON.stringify(result), function(err) {
    if (err) throw err;
    console.log('complete');
  }
);


