module.exports = information => {
  const length = information.planDetail.length
  const templateContent = information.planDetail.map((el, index) => {
    let param1 = `<span/>`
    let param2 = `<span/>`
    let param3 = `<span/>`
    let param4 = `<span/>`
    let add1 = `<span/>`
    let add2 = `<span/>`
    if (information.sets[index]) {
      param1 = `<div style="display: inline-block; width: 40%; font-size: 12px; margin: 5px 0"><span style="color: #666">组数:
            </span> ${information.sets[index]}</div>`
    }
    if (information.timesPerSets[index]) {
      param2 = `<div style="display: inline-block; width: 40%; font-size: 12px; margin: 5px 0"><span style="color: #666">每组次数:
            </span> ${information.timesPerSets[index]}</div>`
    }
    if (information.times[index]) {
      param3 = `<div style="display: inline-block; width: 40%; font-size: 12px; margin: 5px 0"><span style="color: #666">时间:
            </span> ${information.times[index]}秒</div>`
    }
    if (information.weights[index]) {
      param4 = `<div style="display: inline-block; width: 40%; font-size: 12px; margin: 5px 0"><span style="color: #666">重量:
            </span> ${information.weights[index]}千克</div>`
    }
    if (information.parameter1s[index] || information.value1s[index]) {
      add1 = `<div style="display: inline-block; width: 40%; font-size: 12px; margin: 5px 0"><span style="color: #666">${information.parameter1s[index]}:
            </span> ${information.value1s[index]}</div>`
    }
    if (information.parameter2s[index] || information.value2s[index]) {
      add2 = `<div style="display: inline-block; width: 40%; font-size: 12px; margin: 5px 0"><span style="color: #666">${information.parameter2s[index]}:
            </span> ${information.value2s[index]}</div>`
    }

    let isBreak = `<div/>`
    if (index === 2) isBreak = `<br/><div style="page-break-after:always;"/>`
    else if (index > 2 && (index-3) % 4 === 3 && index !== length - 1) isBreak = `<br/><div style="page-break-after:always;"/>`


    return (`
        <hr style="border-top: 1px solid #2B9BCF"/>
        <li style="list-style-type: none; padding: 0; margin: 0 0 15px; clear: left;">
        <h4 style=" margin: 5px 0"><a href="https://view.vzaar.com/${information.planDetail[index].url}/player" target="_blank" style="text-decoration: none; color: #1665A0">
            ${information.planDetail[index].name}</a></h4>
        <div class="flexbox" style="margin: 0 0 8px; text-align: justify">
            <a href="https://view.vzaar.com/${information.planDetail[index].url}/player" target="_blank" 
                style="text-decoration: none;">
            <img src="https://view.vzaar.com/${information.planDetail[index].url}/image" 
              style="width: 160px; height: 90px; border-radius: 3px;
              padding: 0; margin: 0;" alt="picture"/></a>
            <div style="display: inline-block; width: 320px; margin-left: 30px; vertical-align: top">
              <div style="font-size: 12px;">${information.infos[index]}</div>
              <div class="flexbox" style="text-align: justify;">
                ${param1}
                ${param2}
                ${param3}
                ${param4}
                ${add1}
                ${add2}
              </div>
            </div>
        </div>
        ${isBreak}
        </li>`)
  }).join("")




  return `
    <!doctype html>
    <html>
        <head>
        <style type="text/css">
          .flexbox:after {
            content: '';
            display: inline-block;
            width: 100%;
          }
          body {
              padding: 30px;
          }
        </style>
        <meta charset="utf-8">
        <title>VOIN处方</title>
        </head>
        <body>
        <h4 style="text-align: center">运动康复计划: ${information.planName}</h4>
        <div class="flexbox" style="margin: 0; font-size: 12px; color: #666; text-align: justify">
            <div style="display: inline-block">患者姓名: ${information.clientName}</div>
            <div style="display: inline-block">锻炼天数: ${information.allDay}</div>
        </div>
        <ul style="padding: 0; margin: 0 0 15px">
        ${templateContent}
        <hr style="border-top: 1px solid #2B9BCF"/>
        </ul>
        <div style="font-size: 12px; margin: 10px 0;">备注: ${information.extra ? information.extra : "无"}</div>
        <div style="font-size: 12px; color: #666">请您在医生的指导下进行锻炼。如在练习过程中如有任何不适，请立即停止并向医生咨询</div>
        <div style="text-align: right; font-size: 14px; padding: 10px 0;">医生：${information.doctorName}</div>
        <div style="text-align: right; font-size: 18px; padding: 40px 0;">
            沃衍康复
        </div>
        </body>
    </html>
    `;
};