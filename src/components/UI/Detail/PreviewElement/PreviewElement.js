import React, { Component } from 'react'
import Chart from 'react-apexcharts'
import docLogo from '../../../../assets/images/logo_doc.svg'


import Button from '../../Button/Button'
import Aux from "../../../../hoc/Aux/Aux";

import classes from './PreviewElement.css'

class PreviewElement extends Component {

  state = {
    addedParameterCount: 0,
    options: {
      colors: ['#398EFF'],
      yaxis: {
        seriesName: "完成度",
        min: 0,
        max: 100,
        tickAmount: 5,
        title: {
          text: "完成度",
          offsetX: 0,
          offsetY: 0,
          style: {
            color: undefined,
            fontSize: '12px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            cssClass: 'apexcharts-yaxis-title',
          },
        },
        axisBorder: {
          show: true,
          color: '#333',
          offsetX: 0,
          offsetY: 0
        },
        axisTicks: {
          show: true,
          borderType: 'solid',
          color: '#333',
          width: 4,
          offsetX: 0,
          offsetY: 0
        },
      },
      xaxis: {
        axisTicks: {
          show: true,
          borderType: 'solid',
          color: '#333',
          height: 5,
          offsetX: 0,
          offsetY: 0
        },
        axisBorder: {
          show: true,
          color: '#333',
          height: 1,
          width: '100%',
          offsetX: 0,
          offsetY: 0
        },
        min: 1,
        max: Number(this.props.periodDays[this.props.index1]),
        tickAmount: 1,
        title: {
          text: "天数",
          offsetX: 0,
          offsetY: -20,
          style: {
            color: undefined,
            fontSize: '12px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            cssClass: 'apexcharts-xaxis-title',
          },
        },
      },
      chart: {
        animations: {
          enabled: false
        },
        toolbar:{
          show: false
        }
      },
      markers: {
        size: Number(this.props.periodDays[this.props.index1]) > 20 ? 0 : 4,
        colors: undefined,
        strokeColors: '#fff',
        strokeWidth: Number(this.props.periodDays[this.props.index1]) > 20 ? 0 : 2,
        strokeOpacity: 0.9,
        strokeDashArray: 0,
        fillOpacity: 1,
        discrete: [],
        shape: "circle",
        radius: 2,
        offsetX: 0,
        offsetY: 0,
        onClick: undefined,
        onDblClick: undefined,
        hover: {
          size: undefined,
          sizeOffset: 3
        }
      },
      stroke: {
        show: true,
        curve: 'straight',
        lineCap: 'butt',
        colors: undefined,
        width: 2,
        dashArray: 0,
      },
      tooltip: {
        enabled: true,
        shared: false,
        intersect: false,
        style: {
          fontSize: '12px',
          fontFamily: undefined
        },
        onDatasetHover: {
          highlightDataSeries: false,
        },
        x: {
          show: false,
        },
        y: {
          formatter: undefined,
          title: {
            formatter: (seriesName) => seriesName,
          },
        },
        z: {
          formatter: undefined,
          title: 'Size: '
        },
        marker: {
          show: false,
        },
        // items: {
        //   display: flex,
        // },
        fixed: {
          enabled: false,
          position: 'topRight',
          offsetX: 0,
          offsetY: 0,
        },
      }
    },
  }

  componentDidMount = () => {
    if (!this.props.parameter1 && !this.props.value1 && !this.props.parameter2 && !this.props.value2) {
      this.setState({addedParameterCount: 0})
    } else if ((this.props.parameter1 || this.props.value1) && (this.props.parameter2 || this.props.value2)) {
      this.setState({addedParameterCount: 2})
    } else this.setState({addedParameterCount: 1})
  }

  addParameter = () => {
    const currentCount = this.state.addedParameterCount
    this.setState({addedParameterCount: currentCount+1})
  }

  deleteParameter = (index1, index2) => {
    const currentCount = this.state.addedParameterCount
    this.setState({addedParameterCount: currentCount-1})
    this.props.deleteParameter(index1, index2)
  }

  render() {

    let name = classes.Element
    if (this.state.addedParameterCount > 0) {
      name = classes.ElementUp
    }

    const days = Number(this.props.periodDays[this.props.index1])
    const before = this.props.index1 === 0 ? 0 :
      this.props.periodDays.slice(0, this.props.index1).reduce((a, b) => Number(a) + Number(b), 0)

    const data = []

    const lastDay = this.props.saved.length > 0 ? Number(this.props.saved[this.props.saved.length-1].split(" ")[1]) : 0


    for (let x = 1; x <= days; x++) {
      if (x < lastDay-before) {
        const multi = this.props.saved.filter(feed => Number(feed.split(" ")[1])-before === x)
        if (multi.length > 0) {
          data.push([x, Number(multi[multi.length-1].split(" ")[2])])
        }
      } else if (x === lastDay-before) {
        const multi = this.props.saved.filter(feed => Number(feed.split(" ")[1])-before === x)
        let first = null
        if (multi.length > 0) first = Number(multi[multi.length-1].split(" ")[2])
        let second = null
        if (!this.props.between || (x-1) % (Number(this.props.between)+1) === 0) {
          if (this.props.usefulPercents[x-1]) {
            let moveIndex = -1
            for (let i = 0; i <= this.props.index2; i++) {
              if (!this.props.betweens[i] || (x-1) % (Number(this.props.betweens[i])+1) === 0) {
                moveIndex++
              }
            }
            const pers = this.props.usefulPercents[x-1].split(",")
            if (pers.length > moveIndex) {
              second = Number(pers[moveIndex])
            } else {
              second = 0
            }
          }
        }
        if (second !== null ) {
          data.push([x, second])
        }
        else if (first !== null) {
          data.push([x, first])
        }
      } else {
        if (!this.props.between || (x-1) % (Number(this.props.between)+1) === 0) {
          if (this.props.usefulPercents[x-1]) {
            let moveIndex = -1
            for (let i = 0; i <= this.props.index2; i++) {
              if (!this.props.betweens[i] || (x-1) % (Number(this.props.betweens[i])+1) === 0) {
                moveIndex++
              }
            }
            const pers = this.props.usefulPercents[x-1].split(",")
            if (pers.length > moveIndex) {
              data.push([x, Number(pers[moveIndex])])
            } else {
              data.push([x, 0])
            }
          } else {
            data.push([x, 0])
          }
        }
      }
    }

    return this.props.bar ? this.props.start ?
      <div className={classes.Group}>{this.props.name}</div>
      :
      <hr className={classes.GroupEnd}/> : (
      <li className={name}>
        <div className={classes.FirstCol}>
          <div className={classes.Name}>{this.props.name}</div>
          {this.props.editing && !this.props.isEnd ?
            <div>
              <Button btnType="Grey" clicked={() =>
                this.props.deleteOne(this.props.index)}>删除</Button>
              <Button btnType="Success" disabled={this.state.addedParameterCount >= 2} clicked={this.addParameter}>添加参数</Button>
            </div>
            : null}
        </div>
        <div className={classes.Row}>
          <div className={classes.Left}>
            {this.props.screenshot ? <img src={this.props.screenshot} alt="nothing"/> : <img src={docLogo} alt="logo" className={classes.Logo}/>}
          </div>
          <div className={classes.Manage}>
            {this.props.showFeedback ?
              <Aux>
                {/*<div className={classes.ChartTitle}>完成度曲线图</div>*/}
                <Chart options={this.state.options}
                       series={[{data, name: "完成度"}]} type="line" width='100%' height='195px' />
              </Aux> : <Aux>
              <div className={classes.Numbers}>
                <span>
                  <label htmlFor={this.props.id+"between"+this.props.index}>间隔：</label>
                  {this.props.editing && !this.props.isEnd?
                    <Aux>
                      <input id={this.props.id+"between"+this.props.index} value={this.props.between}
                             onChange={event => this.props.changeSingle(0, this.props.index, event.target.value)}/>
                      <span className={classes.Unit}>天</span>
                    </Aux>
                    :
                    this.props.between ? <div className={classes.Set}>{this.props.between}天</div> : null
                  }
                </span>
                <span>
                  <label htmlFor={this.props.id+"time"+this.props.index}>时间：</label>
                  {this.props.editing && !this.props.isEnd ?
                    <Aux>
                      <input id={this.props.id+"time"+this.props.index} value={this.props.time}
                             onChange={event => this.props.changeSingle(1, this.props.index, event.target.value)}/>
                      <span className={classes.Unit}>秒</span>
                    </Aux>
                    :
                    this.props.time ? <div className={classes.Set}>{this.props.time}秒</div> : null
                  }
                </span>
                <span>
                  <label htmlFor={this.props.id+"set"+this.props.index}>组数：</label>
                  {this.props.editing && !this.props.isEnd ?
                    <input id={this.props.id+"set"+this.props.index} value={this.props.set}
                           onChange={event => this.props.changeSingle(2, this.props.index, event.target.value)}/> :
                    <div className={classes.Set}>{this.props.set}</div>
                  }
                </span>
                <span>
                  <label htmlFor={this.props.id+"timesPerSet"+this.props.index}>每组次数：</label>
                  {this.props.editing && !this.props.isEnd ?
                    <input id={this.props.id+"timesPerSet"+this.props.index} value={this.props.timesPerSet}
                           onChange={event => this.props.changeSingle(3, this.props.index, event.target.value)}/> :
                    <div className={classes.Set}>{this.props.timesPerSet}</div>
                  }
                </span>
                {this.state.addedParameterCount >= 1 ?
                  <span>
                    {this.props.editing && !this.props.isEnd ?
                      <Aux>
                        <input id={this.props.id+"parameter1"+this.props.index} value={this.props.parameter1}
                               onChange={event => this.props.changeSingle(5, this.props.index, event.target.value)}/>
                        <span>: </span>
                        <input id={this.props.id+"value1"+this.props.index} value={this.props.value1}
                               onChange={event => this.props.changeSingle(6, this.props.index, event.target.value)}/>
                        <span> </span>
                        <Button btnType="Grey" smallPadding clicked={() => this.deleteParameter(1, this.props.index)}>X</Button>
                      </Aux>
                      :
                      <Aux>
                        <label htmlFor={this.props.id+"value1"+this.props.index}>{this.props.parameter1}：</label>
                        <div className={classes.Set}>{this.props.value1}</div>
                      </Aux>}
                  </span> : null}
                {this.state.addedParameterCount >= 2 ?
                  <span>
                    {this.props.editing && !this.props.isEnd ?
                      <Aux>
                        <input id={this.props.id+"parameter2"+this.props.index} value={this.props.parameter2}
                               onChange={event => this.props.changeSingle(7, this.props.index, event.target.value)}/>
                        <span>: </span>
                        <input id={this.props.id+"value2"+this.props.index} value={this.props.value2}
                               onChange={event => this.props.changeSingle(8, this.props.index, event.target.value)}/>
                        <span> </span>
                        <Button btnType="Grey" smallPadding clicked={() => this.deleteParameter(2, this.props.index)}>X</Button>
                      </Aux>
                      :
                      <Aux>
                        <label htmlFor={this.props.id+"value2"+this.props.index}>{this.props.parameter2}：</label>
                        <div className={classes.Set}>{this.props.value2}</div>
                      </Aux>
                    }
                  </span> : null}
                  </div>
                    {this.props.editing  && !this.props.isEnd ?
                      <textarea rows="5"
                        value={this.props.info}
                        onChange={event => this.props.changeSingle(4, this.props.index, event.target.value)}/> :
                      <div className={classes.BigSet}>{this.props.info}</div>}
                      </Aux>}
          </div>
        </div>
      </li>
    )
  }
}

export default PreviewElement