import React, { Component } from 'react'

import Button from '../../Button/Button'

import classes from './ArticleElement.css'

class ArticleElement extends Component {

  render() {

    return (
      <li className={classes.Element}>
        <div className={classes.FirstCol}>
          {this.props.toClient ? <div className={classes.Name}>
              <a href={"https://backend.voin-cn.com/getarticle/"+this.props.id}
                 target="_blank" style={{textDecoration: 'none', color: '#1765A0'}}>{this.props.name}</a></div> :
            <div className={classes.Name}>{this.props.name}</div>}
          {this.props.editing ?
            <div>
              <Button btnType="Grey" clicked={() => this.props.deleteArticle(this.props.index)}>删除</Button>
            </div>
            : null}
        </div>

      </li>
    )
  }
}

export default ArticleElement