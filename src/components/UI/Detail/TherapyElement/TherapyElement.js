import React, { Component } from 'react'

import Button from '../../Button/Button'

import classes from './TherapyElement.css'


class TherapyElement extends Component {

  render() {

    let name = classes.Element
    if (this.props.addedParamCount > 0) {
      name = classes.ElementUp
    }

    const pictureName = this.props.name.replace("/", "-")

    return (
      <li className={name}>
        <div className={classes.FirstCol}>
          <div className={classes.Name}>{this.props.name}</div>
          {this.props.editing ?
            <div>
              <Button btnType="Grey" clicked={() =>
                this.props.deleteTherapy(this.props.index)}>删除</Button>
              {this.props.params.length > 0 ?
                <Button btnType="Success" disabled={this.props.addedParamCount >= this.props.params.length}
                      clicked={() => this.props.onAddParameter(this.props.index, this.props.params)}>添加参数
                </Button> : null}
            </div>
            : null}
        </div>
        <div className={classes.Row}>
          <div className={classes.Left}>
            <img src={"https://backend.voin-cn.com/"+pictureName+".png"} className={classes.Move} alt="nothing"/>
          </div>
          <div className={classes.Manage}>
            <div className={classes.TherapySet}>
              <div className={classes.Numbers}>
                <span>
                  {this.props.editing ?
                    <input id={this.props.id+"minute"+this.props.index} value={this.props.minute}
                           onChange={event => this.props.changeTherapySingle(0, this.props.index, event.target.value)}/>
                    :
                    <div className={classes.Set}>{this.props.minute}</div>
                  }
                  <label htmlFor={this.props.id+"minute"+this.props.index}>分钟/次</label>
                </span>
                <span>
                  {this.props.editing ?
                    <input id={this.props.id + "timesPerDay" + this.props.index} value={this.props.timesPerDay}
                           onChange={event => this.props.changeTherapySingle(1, this.props.index, event.target.value)}/>
                    : <div className={classes.Set}>{this.props.timesPerDay}</div>
                  }
                  <label htmlFor={this.props.id+"timesPerDay"+this.props.index}>次/日</label>
                </span>
                <span>
                  {this.props.editing ?
                    <input id={this.props.id+"time2"+this.props.index} value={this.props.time2}
                           onChange={event => this.props.changeTherapySingle(2, this.props.index, event.target.value)}/>
                    : <div className={classes.Set}>{this.props.time2}</div>
                  }
                  <label htmlFor={this.props.id+"time2"+this.props.index}>次</label>
                </span>
                <span>
                  {this.props.editing ?
                    <input id={this.props.id+"period"+this.props.index} value={this.props.period}
                           onChange={event => this.props.changeTherapySingle(3, this.props.index, event.target.value)}/>
                    : <div className={classes.Set}>{this.props.period}</div>
                  }
                  <label htmlFor={this.props.id+"period"+this.props.index}>个疗程</label>
                </span>
                {this.props.chosenParams.map((param, index) => {
                  const name = param.name
                  const indexInAll = this.props.params.map(param => param.name).indexOf(name)
                  const originalParam = this.props.params[indexInAll]
                  const chosenNames = this.props.chosenParams.map(param => param.name)
                  const restParams = this.props.params.filter(param => chosenNames.indexOf(param.name) < 0)
                  return <span key={index}>
                    {this.props.editing ? <select onChange={event => this.props.onChangeParamName(this.props.index, index, event.target.value)}
                            value={param.name}>
                      <option key={"current"} value={param.name}>{param.name}</option>
                      {restParams.map((param, index) => <option key={index} value={param.name}>{param.name}</option>)}
                    </select> : <div className={classes.Set}>{param.name}</div>}
                    <span>: </span>

                    {this.props.editing ? originalParam.choices.length === 0 ?
                      <input id={this.props.id+"value1"+this.props.index} value={param.value}
                             onChange={event => this.props.onChangeParameterValue(this.props.index, index, event.target.value)}/> :
                      <select onChange={event => this.props.onChangeParameterValue(this.props.index, index, event.target.value)}
                              value={param.value}>
                        <option key="empty" value=""/>
                        {originalParam.choices.map((choice, index) =>
                          <option key={index} value={choice}>{choice}</option>)}
                      </select> : <div className={classes.Set}>{param.value}</div>}
                    <span> </span>
                    {this.props.editing ?
                      <Button btnType="Grey" smallPadding
                              clicked={() => this.props.onDeleteTherapyParameter(this.props.index, index)}>X
                      </Button> : null}
                      </span>
                })}




              </div>
              {this.props.editing ?
                <textarea rows="5"
                          value={this.props.therapyInfo}
                          onChange={event => this.props.changeTherapySingle(4, this.props.index, event.target.value)}/> :
                <div className={classes.BigSet}>{this.props.therapyInfo}</div>}
              </div>
          </div>
        </div>

      </li>
    )
  }
}

export default TherapyElement