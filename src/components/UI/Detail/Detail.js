import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import isInteger from "is-integer"


import Title from '../../../components/Title/Title'
import Button from '../../../components/UI/Button/Button'
import Question from '../../../components/UI/Question/Question'
import Modal from '../../../components/UI/Modal/Modal'
import SingleLineSpinner from '../../../components/UI/SingleLineSpinner/SingleLineSpinner'
import Spinner from '../../../components/UI/Spinner/Spinner'
import Aux from "../../../hoc/Aux/Aux";
import PreviewElement from './PreviewElement/PreviewElement'
import ArticleElement from './ArticleElement/ArticleElement'
import TherapyElement from './TherapyElement/TherapyElement'
import classes from './Detail.css'
import * as actions from "../../../store/actions";
import Logo from "../../Logo/Logo";
import Input from "../Input/Input";


class Detail extends Component {

  state = {
    numPages: null,
    pageNumber: 1,
    tempTemplate: false
  }

  toArrArr = (totalInEach, plan) => {
    const result = []
    for (let i = 0; i < totalInEach.length; i = i+1) {
      if (i === 0) {
        const periodPlan = [...plan.slice(0, totalInEach[i])]
        result.push(periodPlan)
      } else {
        const periodPlan = [...plan.slice(totalInEach[i-1], totalInEach[i])]
        result.push(periodPlan)
      }
    }
    return result
  }

  tempTemplateSaving = () => {
    this.setState({tempTemplate: true})
    this.saving(true)
  }

  saving = template => {
    if (this.checkValid()) {
      const references = this.props.references.filter(reference => reference.length > 0)
      if (template) this.props.onSavingTemplate({
        doctorId: this.props.doctorId,
        name: this.props.planName,
        summary: this.props.introduction,
        sets: this.props.sets,
        times: this.props.times,
        timesPerSets: this.props.timesPerSets,
        weights: this.props.weights,
        betweens: this.props.betweens,
        parameter1s: this.props.parameter1s,
        parameter2s: this.props.parameter2s,
        value1s: this.props.value1s,
        value2s: this.props.value2s,
        infos: this.props.infos,
        minutes: this.props.minutes,
        timesPerDays: this.props.timesPerDays,
        time2s: this.props.time2s,
        periods: this.props.periods,
        therapyInfos: this.props.therapyInfos,
        addedParamCounts: this.props.addedParamCounts,
        allChosenParams: this.props.allChosenParams,
        therapyIds: this.props.therapyIds,
        ids: this.props.ids,
        bars: this.props.bars,
        starts: this.props.starts,
        barNames: this.props.barNames,
        totalInEach: this.props.totalInEach,
        periodDays: this.props.newPeriodDays,
        articles: this.props.articles.map(article => article.moveId),
        proposalContent: this.props.proposalContent,
        references: references
      }, false)
      else {
        const references = this.props.references.filter(reference => reference.length > 0)
        this.props.onSavingPlan({
          doctorId: this.props.doctorId,
          clientId: this.props.clientId,
          name: this.props.planName,
          allDay: this.props.allDay,
          sets: this.props.sets,
          times: this.props.times,
          timesPerSets: this.props.timesPerSets,
          weights: this.props.weights,
          betweens: this.props.betweens,
          parameter1s: this.props.parameter1s,
          parameter2s: this.props.parameter2s,
          value1s: this.props.value1s,
          value2s: this.props.value2s,
          infos: this.props.infos,
          minutes: this.props.minutes,
          timesPerDays: this.props.timesPerDays,
          time2s: this.props.time2s,
          periods: this.props.periods,
          therapyInfos: this.props.therapyInfos,
          addedParamCounts: this.props.addedParamCounts,
          allChosenParams: this.props.allChosenParams,
          therapyIds: this.props.therapyIds,
          ids: this.props.ids,
          bars: this.props.bars,
          starts: this.props.starts,
          barNames: this.props.barNames,
          extra: this.props.extra,
          totalInEach: this.props.totalInEach,
          periodDays: this.props.newPeriodDays,
          articles: this.props.articles.map(article => article.moveId),
          titleId: this.props.titleId,
          proposalContent: this.props.proposalContent,
          references: references
        }, this)
      }
    }
  }

  updating = template => {
    if (this.checkValid()) {
      const references = this.props.references.filter(reference => reference.length > 0)
      if (template) this.props.onUpdatingTemplate({
        doctorId: this.props.doctorId,
        templateId: this.props.templateId,
        name: this.props.planName,
        summary: this.props.introduction,
        sets: this.props.sets,
        times: this.props.times,
        timesPerSets: this.props.timesPerSets,
        weights: this.props.weights,
        betweens: this.props.betweens,
        parameter1s: this.props.parameter1s,
        parameter2s: this.props.parameter2s,
        value1s: this.props.value1s,
        value2s: this.props.value2s,
        infos: this.props.infos,
        minutes: this.props.minutes,
        timesPerDays: this.props.timesPerDays,
        time2s: this.props.time2s,
        periods: this.props.periods,
        therapyInfos: this.props.therapyInfos,
        addedParamCounts: this.props.addedParamCounts,
        allChosenParams: this.props.allChosenParams,
        therapyIds: this.props.therapyIds,
        ids: this.props.ids,
        bars: this.props.bars,
        starts: this.props.starts,
        barNames: this.props.barNames,
        totalInEach: this.props.totalInEach,
        periodDays: this.props.newPeriodDays,
        articles: this.props.articles.map(article => article.moveId),
        proposalContent: this.props.proposalContent,
        references: references
      }, this)
      else this.props.onUpdatingPlan({
        doctorId: this.props.doctorId,
        clientId: this.props.clientId,
        planId: this.props.planId,
        name: this.props.planName,
        allDay: this.props.allDay,
        sets: this.props.sets,
        times: this.props.times,
        timesPerSets: this.props.timesPerSets,
        weights: this.props.weights,
        betweens: this.props.betweens,
        parameter1s: this.props.parameter1s,
        parameter2s: this.props.parameter2s,
        value1s: this.props.value1s,
        value2s: this.props.value2s,
        infos: this.props.infos,
        minutes: this.props.minutes,
        timesPerDays: this.props.timesPerDays,
        time2s: this.props.time2s,
        periods: this.props.periods,
        therapyInfos: this.props.therapyInfos,
        addedParamCounts: this.props.addedParamCounts,
        allChosenParams: this.props.allChosenParams,
        therapyIds: this.props.therapyIds,
        ids: this.props.ids,
        bars: this.props.bars,
        starts: this.props.starts,
        barNames: this.props.barNames,
        extra: this.props.extra,
        totalInEach: this.props.totalInEach,
        periodDays: this.props.newPeriodDays,
        articles: this.props.articles.map(article => article.moveId)
      }, this)
    }
  }

  checkValid = () => {
    if (!this.toArrArr(this.props.totalInEach, this.props.planDetail)
      .every(arr => arr.filter(move => !move.bar).length > 0)) {
      this.props.onNoMoves()
      return false
    }
    if (!this.props.template && !this.props.newPeriodDays.every((num, index) => {
        return isInteger(Number(num)) && Number(num) > 0
      })) {
      this.props.onNotInt()
      return false
    }
    if (!this.props.betweens.every(between => between === "" || (isInteger(Number(between)) && Number(between) > 0))) {
      this.props.onNotBetween()
      return false
    }
    let total = 0
    this.props.newPeriodDays.forEach(num => {
      total = total + Number(num)
    })
    if (this.props.free30 && !this.props.template && total <= 15) {
      this.props.onNotGreaterThan15()
      return false
    }
    if (this.props.fromEditPlan) {
      const ithDay = this.getCurrentDay()
      const ithPeriod = this.getCurrentPeriod(ithDay)
      const target = ithDay - this.props.periodDays.slice(0, ithPeriod).reduce((a, b) => Number(a) + Number(b), 0)
      if (Number(this.props.newPeriodDays[ithPeriod]) < target && ithPeriod >= 0) {
        this.props.onCurrentPeriodShorter()
        return false
      }
    }

    return true
  }

  getCurrentDay = () => {
    if (!this.props.start && !this.props.showFeedback) return 0
    else {
      const timezone = 8;
      const offset_GMT = new Date().getTimezoneOffset();
      const correctDate = new Date(new Date().getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
      const startDate = new Date(parseInt(this.props.startDate.substring(0,4), 10),
        parseInt(this.props.startDate.substring(4,6), 10)-1,
        parseInt(this.props.startDate.substring(6,8), 10)
      )
      const diffTime = Math.abs(correctDate - startDate);
      const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
      return diffDays+1
    }
  }

  getCurrentPeriod = ithDay => {
    if (this.props.periodDays.length === 0 || ithDay === 0) return -1
    else {
      for (let i = 0; i < this.props.periodDays.length; i++) {
        if (ithDay <= parseInt(this.props.periodDays[i], 10)) {
          return i
        } else {
          ithDay = ithDay - parseInt(this.props.periodDays[i], 10)
        }
      }
    }
  }

  render() {

    const todayDiff = this.getCurrentDay()

    let percents = []
    if (this.props.showFeedback) {
      percents = Array(this.props.periodDays.reduce((a, b) => Number(a) + Number(b), 0)).fill("");
      const date1 = new Date(
        parseInt(this.props.startDate.substring(0, 4), 10),
        parseInt(this.props.startDate.substring(4, 6), 10) - 1,
        parseInt(this.props.startDate.substring(6, 8), 10))
      this.props.feedback.forEach((feed) => {
        const dayAndNum = feed.split(" ")
        const day = dayAndNum[0]
        const num = dayAndNum[1]
        const date2 = new Date(
          parseInt(day.substring(0, 4), 10),
          parseInt(day.substring(4, 6), 10) - 1,
          parseInt(day.substring(6, 8), 10)
        )
        const diff = Math.floor((date2 - date1) / 86400000)
        percents.splice(diff, 1, num)
      })
    }

    const info = this.props.editing && this.props.admin === 5 && this.props.titleId ? !this.props.savingSuccess ?
      <div className={classes.Caution}>请仔细检查后提交</div> : null :
      // this.props.editing && this.props.admin === 5 ? <div className={classes.Caution}>您目前处于学生模式，请选择作业后继续</div> :
      (!this.props.editing || this.props.savingSuccess) ? null :
      this.props.template || this.props.fromTemplate ?
        <div className={classes.Caution}>请您仔细核对后点击继续</div> :
        <div className={classes.Caution}>当您点击继续后，系统会发送计划到患者微信小程序，请您仔细核对后点击继续</div>


    const buttons = <div className={classes.Buttons}>
      {this.props.savingSuccess ?
        <Button clicked={() => this.props.onCloseDetailModal(
          this.props.savingSuccess, this.props.template || this.props.fromTemplate || this.state.tempTemplate, this,
          this.props.fromEditPlan, this.props.clientId, this.props.titleId
        )}
                largeFont btnType="Success">确定</Button> :
        <Button clicked={() => this.props.onCloseDetailModal(
          this.props.savingSuccess, this.props.template || this.props.fromTemplate, this
        )}
                largeFont btnType="Grey">取消</Button>}
      {(!this.props.editing || this.props.savingSuccess) ? null :
        this.props.fromEditPlan ? this.props.savedPlan.empty ?
          <Button clicked={() => this.updating(this.props.template)} largeFont btnType="Success">制定计划</Button> :
          <Button clicked={() => this.updating(this.props.template)} largeFont btnType="Success">更改原计划</Button> :
          !this.props.fromTemplate ?
            this.props.admin === 5 ?
              this.props.template ?
                <Button clicked={() => this.saving(this.props.template)} largeFont btnType="Success">继续</Button> :
              <Aux>
                <Button clicked={() => this.tempTemplateSaving()} largeFont btnType="New">保存新模板</Button>
                <Button clicked={() => this.saving(this.props.template)} largeFont btnType="Success">提交</Button>
              </Aux> :
              <Button clicked={() => this.saving(this.props.template)} largeFont btnType="Success">继续</Button> :
            <Button clicked={() => this.updating(this.props.template)} largeFont btnType="Success">更改原模板</Button>
      }
      {(!this.props.fromTemplate || this.props.savingSuccess) ? null :
        <Button clicked={() => this.saving(this.props.template)} largeFont btnType="New">创建新模板</Button>
      }
    </div>

    const errors = <Aux>
      {this.props.error !== 1 ? null : <Question>请您确保每阶段锻炼天数均为大于0的整数</Question>}
      {this.props.error !== 2 ? null : <Question>请您确保每个动作锻炼间隔天数为空或大于0的整数</Question>}
      {this.props.error !== 7 ? null : <Question>您开出的计划为15天免费计划，请确保总天数大于15天</Question>}
      {this.props.error === 4 ? <Question>请确保每个阶段都有动作</Question> : null}
      {this.props.error === 6 ? <Question>请确保现在阶段天数大于已结束天数</Question> : null}
      {this.props.error === 5 ?
        <Question>请您先<Button btnType="Success"
                             clicked={() => {this.props.history.push('/account')}}>去完善</Button>个人信息</Question> : null}
      {this.props.error === 3 ? <Question>{this.props.errorMsg}</Question> : null}
    </Aux>


    return (
      <Modal
        type="Review"
        show={this.props.showDetailModal}
        modalClosed={() => {
          if (!this.props.toClient) this.props.onCloseDetailModal(
            this.props.savingSuccess, this.props.template || this.props.fromTemplate, this,
            this.props.fromEditPlan, this.props.clientId, this.props.titleId)
        }}
      >
        {this.props.modalLoading ? <Spinner/> : <Aux>
          {this.props.showFeedback ? <Title alignCenter>计划反馈</Title> :
            this.props.template ? <Title alignCenter>模板预览</Title> : <Title alignCenter>计划预览</Title>}
          {info}
          {buttons}
          {this.props.saving ? <SingleLineSpinner/> : null}
          {errors}
          {!this.props.savingSuccess ? null : this.props.template || this.props.fromTemplate || this.state.tempTemplate ?
            <Question>模板保存成功</Question> : this.props.titleId ?
              <Question>作业提交成功</Question> : <Question>计划发送成功</Question>
          }
          {this.props.error === 3 ? null : <Aux>
            {this.props.template ?
              <div style={{color: '#333', fontSize: '14px', marginBottom: '5px', padding: '0 20px'}}>
                <div>模板名：{this.props.planName}</div>
                <div>模板介绍：{this.props.introduction}</div>
              </div> :
              <div className={classes.Combo}>
                <div style={{color: '#333', fontSize: '14px', marginBottom: '5px', width: '100%', padding: '0 20px'}}>
                  <div className={classes.Flex}>
                    <div>计划名：{this.props.planName}</div>
                    {this.props.admin === 5 || this.props.homeworkMode ? null :
                      <div>患者姓名：{this.props.clientName ? this.props.clientName : this.props.clientModeName}</div>}
                  </div>
                  {this.props.showFeedback ? <div className={classes.Flex}>
                    <div>计划状态：{this.props.status}</div>
                    {this.props.status === "已结束" ?
                      <div>该计划共{this.props.periodDays.reduce((a, b) => Number(a) + Number(b), 0)}天</div>
                      :
                      <div>该计划共{this.props.periodDays.reduce((a, b) => Number(a) + Number(b), 0)}天，今日第{todayDiff}天</div>}
                      </div> : null}
                  {this.props.showFeedback && this.props.description ? <div className={classes.Flex}>
                    <div>完成度：{this.props.completion}</div>
                    <div>效果度：{this.props.effect}</div>
                  </div> : null}
                  {this.props.showFeedback && this.props.description ? <div>具体描述：{this.props.description}</div> : null}
                </div>
              </div>
            }
          </Aux>}

          {this.props.template || this.props.fromTemplate ? null :
            this.props.editing ?
              <div style={{color: '#333', fontSize: '14px', marginBottom: '5px', padding: '0 20px'}}>
                <div>备注</div>
                <textarea className={classes.Extra}
                          value={this.props.extra ? this.props.extra : ""}
                          onChange={event => this.props.onChangeExtra(event.target.value)}/>
              </div> : <div>
                {this.props.toClient ? <div>
                    <div style={{margin: '0 0 15px', padding: '0 20px', color: '#333', fontSize: '14px'}}><span style={{marginBottom: '5px'}}>
                    备注：</span>{this.props.extra ? this.props.extra : "无"}</div>
                    <Question>请您在医生的指导下进行锻炼。如在练习过程中如有任何不适，请立即停止并向医生咨询</Question>
                  </div> :
                  <div style={{margin: '0 0 15px', padding: '0 20px', color: '#333', fontSize: '14px'}}><span style={{marginBottom: '5px'}}>
                    备注：</span>{this.props.extra ? this.props.extra : "无"}</div>}
              </div>}
              <div className={classes.Moves}>
                {this.toArrArr(this.props.totalInEach, this.props.planDetail).map((arr, index1) => {
            const start = this.props.periodDays.slice(0, index1).reduce((a, b) => Number(a) + Number(b), 0)
            const end = this.props.periodDays.slice(0, index1+1).reduce((a, b) => Number(a) + Number(b), 0)
            const isEnd = this.props.fromEditPlan && todayDiff - 1 >= end
            return (
              <div className={classes.LayerParent} key={"period"+index1}>
                {isEnd ? <div className={classes.Layer}/> : null}
                <ul className={classes.ReviewList}>
                  <div className={classes.PeriodRow}>
                    {!this.props.showFeedback && !this.props.fromEditPlan ?
                      <div className={classes.PeriodName}>第{index1+1}阶段</div>
                      : todayDiff - 1 < start ?
                        <div className={classes.PeriodName}>第{index1+1}阶段（未开始）</div>
                        : todayDiff - 1 >= end ?
                          <div className={classes.PeriodName}>第{index1+1}阶段（已结束）</div>
                          : <div className={classes.PeriodName}>第{index1+1}阶段（进行到该阶段的第{todayDiff-start}天）</div>
                    }
                    <div>
                      <label htmlFor={"day"+index1}>阶段天数：</label>
                      {this.props.editing && !isEnd?
                        <input id={"day"+index1}
                               value={this.props.newPeriodDays[index1]}
                               onChange={event => this.props.onChangePeriodDays(index1, event.target.value)}
                        /> :
                        <span>{this.props.periodDays[index1]}</span>
                      }
                    </div>
                  </div>
                  {arr.map((move, index2) => {
                    const index = index1 === 0 ? index2 : this.props.totalInEach[index1-1] + index2
                    const usefulPercents = [...percents.slice(
                      this.props.periodDays.slice(0, index1).reduce((a, b) => Number(a) + Number(b), 0),
                      this.props.periodDays.slice(0, index1+1).reduce((a, b) => Number(a) + Number(b), 0)
                    )]
                    const saved = this.props.savedFeedback.filter(feedback => {
                      const day = Number(feedback.split(" ")[1])
                      return feedback.split(" ")[0] === ""+move._id
                        && day > this.props.periodDays.slice(0, index1).reduce((a, b) => Number(a) + Number(b), 0)
                        && day <= this.props.periodDays.slice(0, index1+1).reduce((a, b) => Number(a) + Number(b), 0)
                    })
                    return (
                      <PreviewElement
                        key={"move"+index2}
                        index={index}
                        index1={index1}
                        index2={index2}
                        isEnd={isEnd}
                        id={move._id}
                        name={move.name}
                        screenshot={move.screenshot}
                        info={this.props.infos[index]}
                        time={this.props.times[index]}
                        timesPerSet={this.props.timesPerSets[index]}
                        weight={this.props.weights[index]}
                        between={this.props.betweens[index]}
                        betweens={this.props.betweens}
                        parameter1={this.props.parameter1s[index]}
                        value1={this.props.value1s[index]}
                        parameter2={this.props.parameter2s[index]}
                        value2={this.props.value2s[index]}
                        set={this.props.sets[index]}
                        bar={move.bar}
                        start={move.start}
                        changeSingle={this.props.onChangeSingle}
                        editing={this.props.editing}
                        deleteOne={this.props.onDeleteOne}
                        deleteParameter={this.props.onDeleteParameter}
                        toClient={this.props.toClient}
                        feedback={this.props.feedback}
                        saved={saved}
                        usefulPercents={usefulPercents}
                        showFeedback={this.props.showFeedback}
                        periodDays={this.props.periodDays}
                        startDate={this.props.startDate}
                      />
                    )
                  }
                    )}
                </ul>
              </div>)
          })}
              </div>


          {this.props.therapies.length > 0 && !this.props.showFeedback ?
            <div className={classes.Therapies}>
              <Title>物理治疗</Title>
              <ul className={classes.ArticleList}>
                {this.props.therapies.map((move, index) => <TherapyElement
                  key={index}
                  index={index}
                  id={move.moveId}
                  name={move.moveName}
                  editing={this.props.editing}
                  params={this.props.therapies[index].params}
                  minute={this.props.minutes[index]}
                  timesPerDay={this.props.timesPerDays[index]}
                  time2={this.props.time2s[index]}
                  period={this.props.periods[index]}
                  therapyInfo={this.props.therapyInfos[index]}
                  addedParamCount={this.props.addedParamCounts[index]}
                  chosenParams={this.props.allChosenParams[index]}
                  deleteTherapy={this.props.onDeleteTherapy}
                  onAddParameter={this.props.onAddParameter}
                  onDeleteTherapyParameter={this.props.onDeleteTherapyParameter}
                  onChangeParamName={this.props.onChangeParamName}
                  onChangeParameterValue={this.props.onChangeParameterValue}
                  changeTherapySingle={this.props.onChangeTherapySingle}
                  deleteArticle={this.props.onDeleteArticle}
                  toClient={this.props.toClient}
                />)}
              </ul></div>
            : null}


          {this.props.articles.length > 0 && !this.props.showFeedback ?
              <div className={classes.Articles}>
                <Title>文章</Title>
                <ul className={classes.ArticleList}>
                  {this.props.articles.map((move, index) => <ArticleElement
                      key={index}
                      index={index}
                      id={move.moveId}
                      name={move.moveName}
                      editing={this.props.editing}
                      deleteArticle={this.props.onDeleteArticle}
                      toClient={this.props.toClient}
                  />)}
                </ul></div>
              : null}

          {this.props.admin === 5 ? <div className={classes.NewTitle}>
                <Title>方案设计</Title>
                <div>
                  {this.props.editing ?
              <textarea value={this.props.proposalContent}
                        className={classes.Proposal} id="proposal" name="proposal" rows="15" cols="50"
                        onChange={event => this.props.onChangeProposal(event.target.value)}
              /> : <p>{this.props.proposalContentD.length > 0 ? this.props.proposalContentD : "无"}</p>}
                </div>
                <div className={classes.ReferenceTitleLine}>
                  <Title>参考文献</Title>
                  {this.props.editing ?
                  <div className={classes.ButtonDiv}>
                    <Button clicked={() => {this.props.onAddReference()}} btnType="Success">增加</Button>
                  </div> : null}
                </div>
              {this.props.editing ?
                  <div className={classes.References}>
                    {this.props.references.length > 0 ?
                        this.props.references.map((reference, index) => {
                          return <div className={classes.ReferenceLine} key={index}>
                            <Input smallPadding noVMargin smallRightMargin value={reference}
                                   onChange={(event) => this.props.onChangeReference(index, event.target.value)}
                            />
                            <Button btnType="Danger" clicked={() => {this.props.onDeleteReference(index)}}>X</Button>
                          </div>}) : <p>无</p>}
                  </div> : <div className={classes.References}>
                    {this.props.referencesD.length > 0 ?
                    this.props.referencesD.map((reference, index) => {
                      return <div className={classes.ReferenceLine} key={index}>
                        <p>{reference.length > 0 ? <a href={reference.indexOf("http") === 0 ? reference : "//"+reference} target="_blank">{reference}</a> : "无"}</p>
                      </div>}) : <p>无</p>}
                  </div>}


              </div> : null}
          {this.props.saving ? <SingleLineSpinner/> : null}
          {errors}
          {!this.props.savingSuccess ? null : this.props.template || this.props.fromTemplate || this.state.tempTemplate ?
            <Question>模板保存成功</Question> : this.props.titleId ?
              <Question>作业提交成功</Question> : <Question>计划发送成功</Question>
          }
          {info}
          {/*{!this.props.editing && !this.props.template && this.props.error !== 3 ? <Progress/> : null}*/}
          {buttons}
          {this.props.toClient ? <div className={classes.Company}>
            <div style={{fontSize: '20px'}}>沃衍健康</div>
            <div style={{height: '60px'}}><Logo/></div>
          </div> : null}
        </Aux>}
      </Modal>
    )
  }
}


const mapStateToProps = state => {
  return {
    showDetailModal: state.detail.showDetailModal,
    doctoridnum: state.authRoute.idnum,
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    free30: state.authRoute.free30,
    titleId: state.exercise.titleId,
    template: state.detail.template,
    clientId: state.exercise.clientId,
    clientName: state.detail.clientName,
    clientEmailFromDetail: state.detail.clientEmail,
    clientNameB: state.exercise.clientNameB,
    clientEmail: state.exercise.clientEmail,
    allDay: state.detail.allDay,
    planDetail: state.detail.planDetail,
    articles: state.detail.articles,
    therapies: state.detail.therapies,
    infos: state.detail.infos,
    sets: state.detail.sets,
    times: state.detail.times,
    timesPerSets: state.detail.timesPerSets,
    weights: state.detail.weights,
    betweens: state.detail.betweens,
    parameter1s: state.detail.parameter1s,
    parameter2s: state.detail.parameter2s,
    value1s: state.detail.value1s,
    value2s: state.detail.value2s,
    minutes: state.detail.minutes,
    timesPerDays: state.detail.timesPerDays,
    time2s: state.detail.time2s,
    periods: state.detail.periods,
    therapyInfos: state.detail.therapyInfos,
    addedParamCounts: state.detail.addedParamCounts,
    allChosenParams: state.detail.allChosenParams,
    therapyIds: state.detail.therapyIds,
    ids: state.detail.ids,
    bars: state.detail.bars,
    starts: state.detail.starts,
    barNames: state.detail.barNames,
    completion: state.detail.completion,
    effect: state.detail.effect,
    description: state.detail.description,
    start: state.detail.start,
    startDate: state.detail.startDate,
    status: state.detail.status,
    overallValid: state.detail.overallValid,
    saving: state.detail.saving,
    error: state.detail.error,
    errorMsg: state.detail.errorMsg,
    savingSuccess: state.detail.savingSuccess,
    planName: state.detail.planName,
    introduction: state.detail.introduction,
    extra: state.detail.extra,
    editing: state.detail.editing,
    modalLoading: state.detail.modalLoading,
    toClient: state.detail.toClient,
    feedback: state.detail.feedback,
    savedFeedback: state.detail.savedFeedback,
    showFeedback: state.detail.showFeedback,
    fromEdit: state.exercise.fromEdit,
    fromTemplate: state.exercise.fromTemplate,
    planId: state.exercise.planId,
    templateId: state.exercise.templateId,
    period: state.detail.period,
    totalInEach: state.detail.totalInEach,
    periodDays: state.detail.periodDays,
    newPeriodDays: state.detail.newPeriodDays,
    fromEditPlan: state.exercise.fromEditPlan,
    savedPlan: state.detail.savedPlan,
    homeworkMode: state.detail.homeworkMode,
    clientModeName: state.clientDetail.name,
    proposalContent: state.exercise.proposalContent,
    references: state.exercise.references,
    proposalContentD: state.detail.proposalContent,
    referencesD: state.detail.references
  }
}

const mapDispatchToPatch = dispatch => {
  return {
    onCloseDetailModal: (success, template, web, fromEditPlan, clientId, titleId) =>
      dispatch(actions.closeDetailModal(success, template, web, fromEditPlan, clientId, titleId)),
    onChangeAllDay: value => dispatch(actions.changeAllDay(value)),
    onChangeExtra: value => dispatch(actions.changeExtra(value)),
    onChangeSingle: (index1, index2, value) => dispatch(actions.changeSingle(index1, index2, value)),
    onChangeTherapySingle: (index1, index2, value) => dispatch(actions.changeTherapySingle(index1, index2, value)),
    onSavingTemplate: (info, redirect, web) => dispatch(actions.savingTemplate(info, redirect, web)),
    onSavingPlan: (info, web) => dispatch(actions.savingPlan(info, web)),
    onUpdatingPlan: (info, web) => dispatch(actions.updatingPlan(info, web)),
    onUpdatingTemplate: (info, web) => dispatch(actions.updatingTemplate(info, web)),
    onChangeToInt: (newBetweens) => dispatch(actions.changeToInt(newBetweens)),
    onNotInt: () => dispatch(actions.notInt()),
    onNotBetween: () => dispatch(actions.notBetween()),
    onNotGreaterThan15: () => dispatch(actions.notGreaterThan15()),
    onDeleteOne: index => dispatch(actions.deleteOne(index)),
    onDeleteArticle: index => dispatch(actions.deleteArticle(index)),
    onDeleteTherapy: index => dispatch(actions.deleteTherapy(index)),
    onAddParameter: (index, params) => dispatch(actions.addParameter(index, params)),
    onDeleteTherapyParameter: (index1, index2) => dispatch(actions.deleteTherapyParameter(index1, index2)),
    onChangeParamName: (index1, index2, value) => dispatch(actions.changeParameterName(index1, index2, value)),
    onChangeParameterValue: (index1, index2, value) => dispatch(actions.changeParameterValue(index1, index2, value)),
    onDeleteParameter: (index1, index2) => dispatch(actions.deleteParameter(index1, index2)),
    onNoMoves: () => dispatch(actions.noMoves()),
    onChangePeriodDays: (period, value) => dispatch(actions.changePeriodDays(period, value)),
    onNotComplete: () => dispatch(actions.notComplete()),
    onCurrentPeriodShorter: () => dispatch(actions.currentPeriodShorter()),
    onDeleteReference: (index) => dispatch(actions.deleteReference(index)),
    onChangeReference: (index, value) => dispatch(actions.changeReference(index, value)),
    onAddReference: () => dispatch(actions.addReference()),
    onChangeProposal: (value) => dispatch(actions.changeProposal(value)),
  }
}

export default connect(mapStateToProps, mapDispatchToPatch)(withRouter(Detail))