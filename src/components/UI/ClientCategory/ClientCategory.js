import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'


import Title from '../../../components/Title/Title'
import Button from '../../../components/UI/Button/Button'
import Question from '../../../components/UI/Question/Question'
import SingleLineSpinner from '../../../components/UI/SingleLineSpinner/SingleLineSpinner'
import Aux from "../../../hoc/Aux/Aux";
import classes from './ClientCategory.css'
import selfCat from '../../../assets/images/ic_自定义分类.svg'
import operationCat from '../../../assets/images/ic_手术码分类.svg'
import diseaseCat from '../../../assets/images/ic_疾病码分类.svg'
import right from '../../../assets/images/ic_展开1.svg'
import down from '../../../assets/images/ic_收起2.svg'
import up from '../../../assets/images/ic_收起.svg'
import add from '../../../assets/images/ic_新增.svg'
import deleteCatOne from '../../../assets/images/ic_删除2.svg'
import deleteCat from '../../../assets/images/ic_删除1.svg'
import SearchInput from "../Input/Input";
import Modal from "../Modal/Modal";
import Spinner from "../Spinner/Spinner";
import * as actions from "../../../store/actions";


class ClientCategory extends Component {


  chooseCategory = id => {
    const ids = [id]
    if (this.props.seconds[id]) {
      ids.push(...this.props.seconds[id].map(cat => cat._id))
    }
    this.props.onChooseCategory(id, ids)
  }

  chooseDoctorCategory = id => {
    this.props.onChooseChildDoctorCategory(id)
  }


  render() {

    const pages = Math.ceil(this.props.categoryCount / 100)

    const pageElement = this.props.categoryCount > 100 ? <div>
      {[...Array(pages).keys()].map(num =>
        this.props.currentPage === num+1 ?
          <Button smallPadding vMargin btnType="Success" key={num+1}>{num+1}</Button> :
          <Button smallPadding vMargin btnType="Light"
                  clicked={() => this.props.onSearchDiseaseCategory(this.props.clientCategoryMode,
                    this.props.currentDiseaseCodeValue, num+1)} key={num+1}>{num+1}</Button>)}
    </div> : null

    let addCategoryModal = null
    if (this.props.clientCategoryMode === 7 || this.props.clientCategoryMode === 13) {
      addCategoryModal = <Modal type="Video" modalClosed={this.props.onCloseModal}
                                show={this.props.clientCategoryMode === 7 || this.props.clientCategoryMode === 13}>
        <Title alignCenter>{this.props.clientCategoryMode === 7 ? "添加疾病码分类" : "添加手术码分类"}</Title>
        <div className={classes.SearchRow}>
          <SearchInput smallPadding placeholder={this.props.clientCategoryMode === 7 ? "搜索疾病码" : "搜索手术码"} inline noMargin grow onChange={event => {
            this.props.onDiseaseCodeSearchChange(event.target.value)}}
                       onKeyPress={event => {if (event.key === 'Enter') {
                         this.props.onSearchDiseaseCategory(this.props.clientCategoryMode, this.props.diseaseCodeValue, 1)
                       }}
                       } value={this.props.diseaseCodeValue}/>
          <Button btnType="Success"
                  clicked={() => this.props.onSearchDiseaseCategory(this.props.clientCategoryMode, this.props.diseaseCodeValue, 1)}>
            搜索</Button>
        </div>
        {this.props.addCategoryMode === 8 ? <div className={classes.Note}>
          共找到包含"{this.props.currentDiseaseCodeValue}"的{this.props.categoryCount}个分类{this.props.categoryCount > 100 ? "，当前显示"+this.props.diseaseCategories.length+"个" : ""}
        </div> : null}
        {this.props.addCategoryMode === 6 ?
          <Spinner/> : this.props.addCategoryMode === 8 ?
            <div>
              <ul className={classes.Diseases}>
                {this.props.diseaseCategories.map(cat => {
                  const found = this.props.clientCategories.map(cat => cat.codeId).indexOf(cat._id) > -1
                  const label = cat.code + " " + cat.name + (found ? "(已添加)" : "")
                  return (<li key={cat.code} className={classes.DiseaseCategory}>
                    <input type="checkbox" id={cat.code} name="disease" value={cat._id} disabled={found}
                           checked={this.props.checkedDiseaseCategories.map(item => item.id).indexOf(cat._id) >= 0}
                           onChange={(event) => this.props.onCheckDiseaseCategory(event.target.value, label)}/>
                    <label htmlFor={cat.code} className={found ? classes.Found : ""}>{label}</label>
                  </li>)
                })}
              </ul>
              {this.props.addCategoryMode === 8 ? pageElement : null}
              <div className={classes.DiseaseButtons}>
                <Button btnType="Success"
                        clicked={() => {
                          if (this.props.clientCategoryMode === 7) this.props.onRealAddCategories(this.props.checkedDiseaseCategories.map(item => item.id), this.props.doctorId, "disease")
                          else if (this.props.clientCategoryMode === 13) this.props.onRealAddCategories(this.props.checkedDiseaseCategories.map(item => item.id), this.props.doctorId, "operation")
                        }}>
                  确定
                </Button>
                <Button btnType="Danger" clicked={this.props.onCloseModal}>取消</Button>
              </div>
            </div> :
            this.props.addCategoryMode === 7 ?
              <Question>{this.props.addCategoryModeError}</Question> :
              this.props.addCategoryMode === 9 ?
                <Question>{this.props.addCategoryModeError}</Question> : null}


      </Modal>
    }

    const clientCategoriesWith = [...this.props.clientCategories]
    const childDoctorsWithType = this.props.childDoctors.map(doctor => {
      return {
        ...doctor,
        type: "child"
      }
    })
    clientCategoriesWith.push(...childDoctorsWithType)
    clientCategoriesWith.sort((a, b) => {
      return a.name < b.name ? -1 : 1
    })

    let index = -1
    for (let i = 0; i < clientCategoriesWith.length; i++) {
      if (clientCategoriesWith[i].original && clientCategoriesWith[i].name === "我的医生") {
        index = i
        break
      }
    }
    let clientCategoriesSorted = [...clientCategoriesWith]
    if (index >= 0) {
      clientCategoriesSorted = [...clientCategoriesWith.slice(0, index), ...clientCategoriesWith.slice(index+1), clientCategoriesWith[index]]
    }

    const clientCategoriesWithNumber = clientCategoriesSorted.map(cat => {
      return {
        ...cat,
        number: 0
      }
    })

    let content = []
    if (this.props.type === "client") content = this.props.clients
    else if (this.props.type === "template") content = this.props.templates
    content.forEach(client => {
      if (client.category) {
        client.category.forEach(cat => {
          clientCategoriesWithNumber.forEach(clientCat => {
            if (clientCat._id === cat) {
              clientCat.number = clientCat.number + 1
              if (clientCat.type === "second") {
                clientCategoriesWithNumber.forEach(parent => {
                  if (parent._id === clientCat.parentId) parent.number = parent.number + 1
                })
              }
            }
          })
        })
      }
      if (client.childDoctorId) {
        clientCategoriesWithNumber.forEach(clientCat => {
          if (clientCat._id === client.childDoctorId) {
            clientCat.number = clientCat.number + 1
          }
        })
      }
    })

    return (
      <Aux>
        {addCategoryModal}
        <div className={classes.First}>
          <Title>{this.props.type === "template" ? "模板分类" : "患者分类"}</Title>
          <div className={classes.Categories}>
            <div className={classes.Category}>
              <div className={classes.CategoryType} onClick={() => this.props.onCollapse(1)}>
                <div className={classes.Description}><img alt="selfCat" src={selfCat}/><div>&nbsp;自定义分类</div></div>
                {this.props.collapse1 ? <div><img alt="down" src={down}/></div> : <div><img alt="right" src={right}/></div>}
              </div>
              {this.props.collapse1 ? <div>
                <div className={classes.ButtonsAll}>
                  <Button btnType="Success" disabled={this.props.clientCategoryMode !== 0 || this.props.childCategoryMode > 0}
                          clicked={this.props.onAddCategory}>新增分类</Button>
                  {clientCategoriesWithNumber.filter(cat => cat.type === "defined").length === 0 ? null : this.props.clientCategoryMode === 5 || this.props.clientCategoryMode === 6 ?
                    <Button btnType="Grey"
                            clicked={this.props.onCancelDeleteCategory}>取消</Button> :
                    <Button btnType="Grey" disabled={this.props.clientCategoryMode !== 0 || this.props.childCategoryMode > 0}
                            clicked={this.props.onDeleteCategory}>删除分类</Button>}
                </div>
                {(this.props.childCategoryMode === 0 || this.props.childCategoryMode === 1) && this.props.clientCategoryMode > 0 && this.props.clientCategoryMode < 5 ?
                  <div className={classes.NewCategory}>
                    <SearchInput smallMargin onChange={(event) => {this.props.onAddCategoryChange(event.target.value, 1)}}
                                 value={this.props.newCategory}/>
                    {this.props.clientCategoryMode === 2 ? <Question>分类名称不能为空</Question> :
                      this.props.clientCategoryMode === 1.5 ? <Question>分类名称不能包含"$"</Question> :
                        this.props.clientCategoryMode === 3 ? <SingleLineSpinner/> :
                          this.props.clientCategoryMode === 4 ? <Question>{this.props.clientCategoryError}</Question> :
                            null}
                    {this.props.clientCategoryMode === 1 || this.props.clientCategoryMode === 2 || this.props.clientCategoryMode === 1.5 ? <div className={classes.Buttons}>
                      <Button btnType="Success"
                              clicked={() => this.props.onRealAddCategory(this.props.newCategory, this.props.doctorId, "defined", "")}>
                        确定
                      </Button>
                      <Button btnType="Grey" clicked={this.props.onCancelAddCategory}>取消</Button>
                    </div> : null}
                  </div> : null}

                <ul className={classes.List}>
                  {clientCategoriesWithNumber.filter(cat => cat.type === "defined").length > 0 ?
                    clientCategoriesWithNumber.filter(cat => cat.type === "defined").map(cat => {
                      const chosen = this.props.chosen === cat._id
                      const hasChild = !!this.props.seconds[cat._id]
                      return (<div key={cat._id} className={classes.ListItems}>
                        <li className={chosen ? classes.Chosen : ""}>
                          <div className={classes.CategoryRow}>
                            <div className={classes.Hover} onClick={() => this.chooseCategory(cat._id)}>{cat.name + " (" + cat.number + ")" + (cat.special ? "(特殊)" : "")}</div>
                            <div className={classes.Images}>
                              {this.props.clientCategoryMode === 0 && this.props.childCategoryMode === 6 && chosen ?
                                <Button btnType="Grey" smallPadding clicked={this.props.onCancelAddChildCategory}>取消</Button> : null}
                              {this.props.clientCategoryMode === 0 && this.props.childCategoryMode === 0 && chosen && this.props.firstCollapse?
                                <Aux>
                                  {this.props.doctorId === "5f23a8e26266b8739f89de86" ? !cat.special ?
                                    <Button xSmallPadding btnType="New" disabled={this.props.specialMode === 1}
                                            clicked={() => this.props.onSpecialize(cat._id)}>特殊</Button> :
                                    <Button xSmallPadding btnType="New"
                                            clicked={() => this.props.onSeeQRCode(cat._id)}>查看</Button> : null}
                                  {this.props.seconds[cat._id] ? <img alt="delete" className={classes.Image} onClick={this.props.onDeleteChildCategory} src={deleteCat}/> : null}
                                  <img alt="add" className={classes.Image} onClick={this.props.onAddChildCategory} src={add}/>
                                </Aux> : null}
                              {this.props.collapse1 && hasChild ? chosen && this.props.firstCollapse ? <img alt="up" className={classes.Image} src={up}/> : <img alt="right" className={classes.Image} src={right}/> : null}
                              {this.props.clientCategoryMode === 5 ?
                                <img alt="delete" className={classes.Image} onClick={() => this.props.onDeleteOneCategory(1, cat._id)} src={deleteCatOne}/>
                                : null}
                            </div>
                          </div>
                          <div>
                            {this.props.seeQRCode === 1 && this.props.base64Str && cat._id === this.props.seeQRCodeCatId?
                              <img className={classes.Img} src={"data:image/png;base64,"+this.props.base64Str} alt="qrcode"/>:
                              null}
                          </div>
                        </li>

                        {this.props.collapse1 && hasChild && chosen && this.props.firstCollapse ? <ul className={classes.SecondList}>
                          {clientCategoriesWithNumber.filter(sec => sec.type === "second" && sec.parentId === cat._id).map(second =>
                            <li key={second._id}
                                onClick={() => this.props.onChooseSecondCategory(second._id)}>
                              <div className={classes.SecondCategoryRow}>
                                <div>{second.childName + " (" + second.number + ")"}</div>
                                {this.props.clientCategoryMode === 0 && this.props.childCategoryMode === 6 && chosen ?
                                  <img alt="delete" onClick={(e) => {
                                    e.stopPropagation()
                                    this.props.onDeleteOneCategory(4, second._id)
                                  }} src={deleteCatOne}/>
                                  : null}
                              </div>
                              {this.props.childCategoryMode === 8 && this.props.deleteCategoryId === second._id ? <SingleLineSpinner/> :
                                this.props.childCategoryMode === 9 && this.props.deleteCategoryId === second._id ? <Question>{this.props.clientCategoryError}</Question> :
                                  this.props.childCategoryMode === 7 && this.props.deleteCategoryId === second._id ?
                                    <Question noMargin>
                                      确定删除该分类？<br/>
                                      <Button btnType="Success"
                                              clicked={() => this.props.onRealDeleteCategory(4, this.props.doctorId, second._id)}>确定</Button>
                                      <Button btnType="Grey" clicked={this.props.onDeleteSecondCategory}>取消</Button>
                                    </Question> : null}
                            </li>)}
                        </ul> : null}
                        {this.props.childCategoryMode >= 1 && this.props.childCategoryMode <= 5 && chosen ?
                          <SearchInput smallMargin onChange={(event) => {this.props.onAddCategoryChange(event.target.value, 2)}}
                                       value={this.props.newCategory}/> : null}
                        {this.props.childCategoryMode === 2 && chosen ? <Question>分类名称不能为空</Question> :
                          this.props.childCategoryMode === 3 && chosen ? <Question>分类名称不能包含"$"</Question> :
                            this.props.childCategoryMode === 4 && chosen ? <SingleLineSpinner/> :
                              this.props.childCategoryMode === 5 && chosen ? <Question>{this.props.clientCategoryError}</Question> :
                                null}
                        {this.props.childCategoryMode >= 1 && this.props.childCategoryMode <= 3 && chosen ? <div className={classes.Buttons}>
                          <Button btnType="Success"
                                  clicked={() => this.props.onRealAddCategory(this.props.newCategory, this.props.doctorId, "second", cat._id)}>
                            确定
                          </Button>
                          <Button btnType="Grey" clicked={this.props.onCancelAddChildCategory}>取消</Button>
                        </div> : null}

                        {this.props.clientCategoryMode === 7.5 && this.props.deleteCategoryId === cat._id ?
                          <SingleLineSpinner/> :
                          this.props.clientCategoryMode === 8 && this.props.deleteCategoryId === cat._id ?
                            <Question>{this.props.clientCategoryError}</Question> :
                            this.props.clientCategoryMode === 6 && this.props.deleteCategoryId === cat._id ?
                              <Question>
                                {this.props.seconds[cat._id] ? "确定删除该分类？该操作将同时删除其所有子类" : "确定删除该分类？"}<br/>
                                <Button btnType="Success"
                                        clicked={() => this.props.onRealDeleteCategory(1, this.props.doctorId, cat._id, this.props.seconds)}>确定</Button>
                                <Button btnType="Grey" clicked={this.props.onDeleteCategory}>取消</Button>
                              </Question> : null}
                      </div>)
                    }) :
                    this.props.clientCategoryMode === 0 ?
                      <Question>目前无自定义分类</Question> : null}
                </ul>
              </div> : null}
            </div>
            <div className={classes.Category}>
              <div className={classes.CategoryType} onClick={() => this.props.onCollapse(2)}>
                <div className={classes.Description}><img alt="disease" src={diseaseCat}/><div>&nbsp;疾病码分类</div></div>
                {this.props.collapse2 ? <div><img alt="down" src={down}/></div> : <div><img alt="right" src={right}/></div>}
              </div>
              {this.props.collapse2 ? <div>
                <div className={classes.ButtonsAll}>
                  <Button btnType="Success" disabled={this.props.clientCategoryMode !== 0 || this.props.childCategoryMode > 0}
                          clicked={this.props.onAddDiseaseCategory}>新增分类</Button>
                  {clientCategoriesWithNumber.filter(cat => cat.type === "disease").length === 0 ? null : this.props.clientCategoryMode === 9 ?
                    <Button btnType="Grey"
                            clicked={this.props.onCancelDeleteCategory}>取消</Button> :
                    <Button btnType="Grey" disabled={this.props.clientCategoryMode !== 0 || this.props.childCategoryMode > 0}
                            clicked={this.props.onDeleteDiseaseCategory}>删除分类</Button>}
                </div>
                <ul className={classes.List}>
                  {clientCategoriesWithNumber.filter(cat => cat.type === "disease").length > 0 ?
                    clientCategoriesWithNumber.filter(cat => cat.type === "disease").map(cat =>
                      <div key={cat._id} className={classes.ListItems}>
                        <li className={this.props.chosen === cat._id ? classes.Chosen : ""}
                            onClick={() => this.chooseCategory(cat._id)}>
                          <div className={classes.SecondCategoryRow}>
                            <div>{cat.name + " (" + cat.number + ")"}</div>
                            {this.props.clientCategoryMode === 9 ?
                              <img alt="delete" onClick={(e) => {
                                e.stopPropagation()
                                this.props.onDeleteOneCategory(2, cat._id, cat.name)
                              }} src={deleteCatOne}/>
                               : null}
                          </div>
                        </li>
                        {this.props.clientCategoryMode === 11 && this.props.deleteCategoryId === cat._id ?
                          <SingleLineSpinner/> :
                          this.props.clientCategoryMode === 12 && this.props.deleteCategoryId === cat._id ?
                            <Question>{this.props.clientCategoryError}</Question> :
                            this.props.clientCategoryMode === 10 && this.props.deleteCategoryId === cat._id ?
                              <Question>
                                确定删除该分类？<br/>
                                <Button btnType="Success"
                                        clicked={() => this.props.onRealDeleteCategory(2, this.props.doctorId, cat._id)}>确定</Button>
                                <Button btnType="Grey" clicked={this.props.onDeleteDiseaseCategory}>取消</Button>
                              </Question> : null}
                      </div>) :
                    this.props.clientCategoryMode === 0 ?
                      <Question>目前无疾病码分类</Question> : null}
                </ul>
              </div> : null}
            </div>
            <div className={classes.Category}>
              <div className={classes.CategoryType} onClick={() => this.props.onCollapse(3)}>
                <div className={classes.Description}><img alt="operation" src={operationCat}/><div>&nbsp;手术码分类</div></div>
                {this.props.collapse3 ? <div><img alt="down" src={down}/></div> : <div><img alt="right" src={right}/></div>}</div>
              {this.props.collapse3 ? <div>
                <div className={classes.ButtonsAll}>
                  <Button btnType="Success" disabled={this.props.clientCategoryMode !== 0 || this.props.childCategoryMode > 0}
                          clicked={this.props.onAddOperationCategory}>新增分类</Button>
                  {clientCategoriesWithNumber.filter(cat => cat.type === "operation").length === 0 ? null : this.props.clientCategoryMode === 14 ?
                    <Button btnType="Grey"
                            clicked={this.props.onCancelDeleteCategory}>取消</Button> :
                    <Button btnType="Grey" disabled={this.props.clientCategoryMode !== 0 || this.props.childCategoryMode > 0}
                            clicked={this.props.onDeleteOperationCategory}>删除分类</Button>}
                </div>
                <ul className={classes.List}>
                  {clientCategoriesWithNumber.filter(cat => cat.type === "operation").length > 0 ?
                    clientCategoriesWithNumber.filter(cat => cat.type === "operation").map(cat =>
                      <div key={cat._id} className={classes.ListItems}>
                        <li className={this.props.chosen === cat._id ? classes.Chosen : ""}
                            onClick={() => this.chooseCategory(cat._id)}>
                          <div className={classes.SecondCategoryRow}>
                            <div>{cat.name + " (" + cat.number + ")"}</div>
                            {this.props.clientCategoryMode === 14 ?
                              <img alt="delete" onClick={(e) => {
                                e.stopPropagation()
                                this.props.onDeleteOneCategory(3, cat._id, cat.name)
                              }} src={deleteCatOne}/>
                               : null}
                          </div>
                        </li>
                        {this.props.clientCategoryMode === 16 && this.props.deleteCategoryId === cat._id ?
                          <SingleLineSpinner/> :
                          this.props.clientCategoryMode === 17 && this.props.deleteCategoryId === cat._id ?
                            <Question>{this.props.clientCategoryError}</Question> :
                            this.props.clientCategoryMode === 15 && this.props.deleteCategoryId === cat._id ?
                              <Question>
                                确定删除该分类？<br/>
                                <Button btnType="Success"order
                                        clicked={() => this.props.onRealDeleteCategory(3, this.props.doctorId, cat._id)}>确定</Button>
                                <Button btnType="Grey" clicked={this.props.onDeleteOperationCategory}>取消</Button>
                              </Question> : null}
                      </div>) :
                    this.props.clientCategoryMode === 0 ?
                      <Question>目前无手术码分类</Question> : null}
                </ul>
              </div> : null}
            </div>
            {this.props.type === "client" ?
              <div className={classes.Category}>
                <div className={classes.CategoryType} onClick={() => this.props.onCollapse(4)}>
                  <div className={classes.Description}><img alt="operation" src={operationCat}/><div>&nbsp;医生分类</div></div>
                  {this.props.collapse4 ? <div><img alt="down" src={down}/></div> : <div><img alt="right" src={right}/></div>}</div>
                {this.props.collapse4 ? <div>
                  <ul className={classes.List}>
                    {clientCategoriesWithNumber.filter(cat => cat.type === "child").map(cat =>
                      <div key={cat._id} className={classes.ListItems}>
                        <li className={this.props.chosen === cat._id ? classes.Chosen : ""}
                            onClick={() => this.chooseDoctorCategory(cat._id)}>
                          <div className={classes.SecondCategoryRow}>
                            <div>{cat.name + " (" + cat.number + ")"}</div>
                          </div>
                        </li>
                      </div>) }
                  </ul>
                </div> : null}
              </div> : null}
          </div>


        </div>
      </Aux>

    )

  }

}


const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    childDoctors: state.authRoute.childDoctors,
    title: state.clients.title,
    clients: state.clients.allClients,
    templates: state.templates.allTemplates,
    clientCategories: state.clients.clientCategories,
    mode: state.clients.mode,
    error: state.clients.error,
    order: state.clients.order,
    clientCategoryMode: state.clients.clientCategoryMode,
    clientCategoryError: state.clients.clientCategoryError,
    newCategory: state.clients.newCategory,
    deleteCategoryId: state.clients.deleteCategoryId,
    chosen: state.clients.chosen,
    titleId: state.clients.titleId,
    diseaseCodeValue: state.clients.diseaseCodeValue,
    addCategoryMode: state.clients.addCategoryMode,
    diseaseCategories: state.clients.diseaseCategories,
    addCategoryModeError: state.clients.addCategoryModeError,
    checkedDiseaseCategories: state.clients.checkedDiseaseCategories,
    collapse1: state.clients.collapse1,
    collapse2: state.clients.collapse2,
    collapse3: state.clients.collapse3,
    collapse4: state.clients.collapse4,
    specialCatId: state.clients.specialCatId,
    specialMode: state.clients.specialMode,
    firstCollapse: state.clients.firstCollapse,
    categoryCount: state.clients.categoryCount,
    currentPage: state.clients.currentPage,
    currentDiseaseCodeValue: state.clients.currentDiseaseCodeValue,
    childCategoryMode: state.clients.childCategoryMode,
    seconds: state.clients.seconds,
    seeQRCode: state.clients.seeQRCode,
    base64Str: state.clients.base64Str,
    seeQRCodeCatId: state.clients.seeQRCodeCatId,
    seeQRCodeError: state.clients.seeQRCodeError
  }
}

const mapDispatchToPatch = dispatch => {
  return {
    onCloseModal: () => dispatch(actions.closeModal()),
    onRealDelete: (doctorId, clientId) => dispatch(actions.realClientDelete(doctorId, clientId)),
    onRealDeleteCategory: (mode, doctorId, categoryId, seconds) =>
      dispatch(actions.realDeleteCategory(mode, doctorId, categoryId, seconds)),
    onDiseaseCodeSearchChange: (value) => dispatch(actions.diseaseCodeSearchChange(value)),
    onAddCategory: () => dispatch(actions.addCategory()),
    onAddChildCategory: () => dispatch(actions.addChildCategory()),
    onDeleteChildCategory: () => dispatch(actions.deleteChildCategory()),
    onDeleteCategory: () => dispatch(actions.deleteCategory()),
    onDeleteDiseaseCategory: () => dispatch(actions.deleteDiseaseCategory()),
    onDeleteSecondCategory: () => dispatch(actions.deleteSecondCategory()),
    onDeleteOperationCategory: () => dispatch(actions.deleteOperationCategory()),
    onSpecialize: (catId) => dispatch(actions.specialize(catId)),
    onSeeQRCode: (catId) => dispatch(actions.seeQRCode(catId)),
    onCancelAddCategory: () => dispatch(actions.cancelAddCategory()),
    onCancelDeleteCategory: () => dispatch(actions.cancelDeleteCategory()),
    onAddCategoryChange: (value, mode) => dispatch(actions.addCategoryChange(value, mode)),
    onCheckDiseaseCategory: (value, label) => dispatch(actions.checkDiseaseCategory(value, label)),
    onRealAddCategory: (name, doctorId, type, first) => dispatch(actions.realAddCategory(name, doctorId, type, first)),
    onRealAddCategories: (names, doctorId, type) => dispatch(actions.realAddCategories(names, doctorId, type)),
    onChooseCategory: (id, ids) => dispatch(actions.chooseCategory(id, ids)),
    onChooseSecondCategory: (id) => dispatch(actions.chooseSecondCategory(id)),
    onDeleteOneCategory: (mode, id) => dispatch(actions.deleteOneCategory(mode, id)),
    onAddDiseaseCategory: () => dispatch(actions.addDiseaseCategory()),
    onAddOperationCategory: () => dispatch(actions.addOperationCategory()),
    onSearchDiseaseCategory: (mode, value, page) => dispatch(actions.searchDiseaseCategory(mode, value, page)),
    onCancelAddChildCategory: () => dispatch(actions.cancelAddChildCategory()),
    onChooseChildDoctorCategory: (id) => dispatch(actions.chooseChildDoctorCategory(id)),
    onCollapse: (mode) => dispatch(actions.collapse(mode)),
  }
}

export default connect(mapStateToProps, mapDispatchToPatch)(withRouter(ClientCategory))