import React from 'react'
import { connect } from 'react-redux'

import classes from './Picture.css'
import docLogo from '../../../assets/images/logo_doc.svg'
import unChosen from '../../../assets/images/ic_未选.svg'
import chosen from '../../../assets/images/ic_已选.svg'
import play from '../../../assets/images/ic_播放.svg'

import * as actions from '../../../store/actions/index';
import Aux from "../../../hoc/Aux/Aux";

const picture = props => {
  const periodPlan = props.plan.slice(
    props.currentPeriod === 0 ? 0 : props.totalInEach[props.currentPeriod-1], props.totalInEach[props.currentPeriod]
  )
  const currentMoveIdList = periodPlan.map(move => move.moveId ? move.moveId : "")
  const videoIndex = currentMoveIdList.indexOf(props.video._id)
  const currentArticleList = props.articles.map(move => move.moveId)
  const currentTherapyList = props.therapies.map(move => move.moveId)
  const added = currentMoveIdList.indexOf(props.video._id) > -1 ||
    currentArticleList.indexOf(props.video._id) > -1 || currentTherapyList.indexOf(props.video._id) > -1
  const isVideo = props.video.video
  const isTherapy = props.video.therapy

  const pictureName = props.video.name.replace("/", "-")


  return (
    <Aux>
      <div className={classes.Picture}>
        <div className={classes.ImgParent}>
          {isVideo ? <img src={props.video.screenshot} className={classes.Move} alt="nothing"/> :
            isTherapy ? <img src={"https://backend.voin-cn.com/"+pictureName+".png"} className={classes.Move} alt="nothing"/> :
            <img src={docLogo} alt="logo" className={classes.Logo}/>}
          {isVideo ?
            <div className={classes.Play}>
              <img alt="play" onClick={() => props.play(props.video.mp4, props.video.name)} className={classes.PlayImage} src={play}/>
            </div> :
            null}
        </div>
        <div onClick={added ? () => props.onDeleteMove(
            isVideo ? 1 : isTherapy ? 2 : 3, props.video._id, props.currentPeriod, videoIndex
        ) : () => props.onAddMovement(props.video.name, props.video._id, isVideo ? 1 : isTherapy ? 2 : 3, props.video.params)}
              className={classes.Check}><img alt="add" src={added ? chosen : unChosen}/></div>
        {/*<div className={classes.Row}>*/}
          {/*<div className={classes.Add}>*/}
            {/*{added ?*/}
              {/*<Button btnType="Success" fullWidth*/}
                      {/*clicked={() => props.onDeleteMove(*/}
                        {/*isVideo ? 1 : 2, props.video._id, props.currentPeriod, videoIndex*/}
                      {/*)}>已添加</Button> :*/}
              {/*<Button btnType="Light" fullWidth*/}
                      {/*clicked={() => props.onAddMovement(props.video.name, props.video._id, isVideo)}><b>添加</b></Button>}*/}
          {/*</div>*/}
          {/*{isVideo ? <Aux><div className={classes.Info}>*/}
            {/*<Button btnType="Light" fullWidth>信息</Button>*/}
            {/**/}
          {/*</div>*/}
          {/*<div className={classes.Watch}>*/}
            {/*<Button btnType="Light" clicked={() => props.play(props.video.mp4, props.video.name)}*/}
                    {/*fullWidth>视频</Button>*/}
          {/*</div></Aux> : <div className={classes.ArticleWatch}>*/}
            {/*<a href={"https://backend.voin-cn.com/getarticle/"+props.video._id} target="_blank"><Button btnType="Light" fullWidth>查看</Button></a>*/}
          {/*</div>}*/}
        {/*</div>*/}
        {isVideo ? <div className={classes.Name}>{props.video.name}</div> :
          <div className={classes.Name}>
            <a className={classes.NameRef} href={"https://backend.voin-cn.com/getarticle/"+props.video._id} target="_blank">{props.video.name}</a>
          </div>}
        <div className={classes.Tooltip}>{props.video.info}</div>

      </div>
    </Aux>

  )
}

const mapStateToProps = state => {
  return {
    plan: state.exercise.plan,
    currentPeriod: state.exercise.currentPeriod,
    totalInEach: state.exercise.totalInEach,
    currentGroupStart: state.exercise.currentGroupStart,
    currentGroupEnd: state.exercise.currentGroupEnd,
    articles: state.exercise.articles,
    therapies: state.exercise.therapies,
    covered: state.exercise.coveredMoves
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAddMovement: (name, id, type, params) => dispatch(actions.addMovement(name, id, type, params)),
    onDeleteMove: (index, moveId, period, videoIndex) => dispatch(actions.deleteMove(index, moveId, period, videoIndex))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(picture)