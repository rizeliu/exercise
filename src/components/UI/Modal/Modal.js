import React from 'react'

import classes from './Modal.css'
import Aux from '../../../hoc/Aux/Aux'
import Backdrop from '../Backdrop/Backdrop'

const modal = props => {

  let backDrop = null
  if (!props.noBackDrop) {
    backDrop = <Backdrop show={props.show} clicked={props.modalClosed}/>
  }
  let div = null
  if (props.show) {
    div = (<div className={[classes.Modal, classes[props.type]].join(' ')}>
      {props.children}
    </div>)
  } else {
    div = null
  }

  return (
    <Aux>
      {backDrop}
      {div}
    </Aux>
  )
}


export default modal