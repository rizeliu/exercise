import React from 'react'

import searchIcon from '../../../assets/images/ic_搜索_默认.svg'
import deleteNew from '../../../assets/images/ic_删除3.svg'
import classes from './Input.css'

const input = props => {

  let inputClasses = [classes.InputElement]
  let divClasses = [classes.Input]
  if (props.smallFont) inputClasses.push(classes.SmallFont)
  if (props.smallPadding) inputClasses.push(classes.SmallPadding)
  if (props.smallMargin) divClasses.push(classes.SmallMargin)
  if (props.inline) divClasses.push(classes.Inline)
  if (props.noMargin) divClasses.push(classes.NoMargin)
  if (props.noVMargin) divClasses.push(classes.NoVMargin)
  if (props.smallRightMargin) divClasses.push(classes.SmallRightMargin)
  if (props.padding) divClasses.push(classes.Padding)
  if (props.noBorder) inputClasses.push(classes.NoBorder)
  if (props.grow) divClasses.push(classes.Grow)
  if (props.grey) divClasses.push(classes.Grey)
  if (props.readOnly) inputClasses.push(classes.GreyText)
  if (props.height) inputClasses.push(classes.Height)

  return (
    <div className={divClasses.join(" ")}>
      {props.label ? <label className={classes.Label} htmlFor={props.id}>{props.label}</label> : null}
      <input
        readOnly={props.readOnly}
        id={props.id}
        className={inputClasses.join(" ")}
        value={props.value}
        onKeyPress={props.onKeyPress}
        autoFocus={props.autoFocus}
        onChange={event => props.onChange(event, props.id)}
        onBlur={props.onfocusout}
        placeholder={props.placeholder}
        type={props.password ? "password" : "text"}
      />
      {props.search ? <div className={classes.SearchIcon}><img alt="search" src={searchIcon}/></div> : null}
      {props.deletable ? <div className={classes.DeleteIcon} onClick={(event) => props.onClick(event, props.id)}><img alt="delete" src={deleteNew}/></div> : null}
    </div>
  )
}

export default input