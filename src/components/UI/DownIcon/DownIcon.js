import React from 'react'

import icon from '../../../assets/images/down-icon.png'
import classes from './DownIcon.css'

const downIcon = props => (
  <img src={icon} className={classes.DownIcon} alt="down" />
)

export default downIcon