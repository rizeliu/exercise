import React, {Component} from 'react'

import classes from './Button.css'

class Button extends Component {


  handleChildClick = e => {
    e.stopPropagation()
    if (this.props.clicked) this.props.clicked()
  }

  render() {
    let buttonClasses = [classes.Button, classes[this.props.btnType]]
    if (this.props.largeFont) buttonClasses.push(classes.LargeFont)
    if (this.props.smallPadding) buttonClasses.push(classes.SmallPadding)
    if (this.props.xSmallPadding) buttonClasses.push(classes.XSmallPadding)
    if (this.props.largePadding) buttonClasses.push(classes.LargePadding)
    if (this.props.middlePadding) buttonClasses.push(classes.MiddlePadding)
    if (this.props.fullWidth) buttonClasses.push(classes.FullWidth)
    if (this.props.vMargin) buttonClasses.push(classes.VMargin)
    if (this.props.width30) buttonClasses.push(classes.Width30)
    if (this.props.width20) buttonClasses.push(classes.Width20)

    return (
      <button className={buttonClasses.join(' ')}
              onClick={this.handleChildClick}
              disabled={this.props.disabled}
              type={this.props.type}
      >{this.props.children}</button>
    )
  }
}

export default Button