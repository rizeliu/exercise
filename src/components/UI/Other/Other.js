import React from 'react'

import classes from './Other.css'

const other = props => {
  return (
    <div className={classes.Other}>其他方式登陆：微信 QQ</div>
  )
}

export default other