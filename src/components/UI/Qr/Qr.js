import React from 'react'

import icon from '../../../assets/images/qr.png'
import classes from './Qr.css'

const Qr = props => (
  <img src={icon} className={classes.Qr} alt="qr" />
)

export default Qr