import React from 'react'

import icon from '../../../assets/images/use.png'
import classes from './UseSetting.css'

const useSetting = props => (
  <img src={icon} className={classes.UseSetting} alt="UseSetting" />
)

export default useSetting