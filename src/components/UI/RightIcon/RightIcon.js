import React from 'react'

import icon from '../../../assets/images/right-icon.png'
import classes from './RightIcon.css'

const rightIcon = props => (
  <img src={icon} className={classes.RightIcon} alt="right" />
)

export default rightIcon