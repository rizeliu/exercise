import React from 'react'

import classes from './SingleLineSpinner.css'

const spinner = () => (
  <div className={classes.Loader}>Loading...</div>
)

export default spinner