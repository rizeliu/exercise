import React from 'react'

import icon from '../../../assets/images/back-icon.png'
import classes from './BackIcon.css'

const backIcon = props => (
  <img src={icon} className={classes.BackIcon} alt="back" />
)

export default backIcon