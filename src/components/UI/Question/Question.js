import React from 'react'

import classes from './Question.css'

const question = props => {
  let questionClasses = [classes.Question]
  if (props.smallPadding) questionClasses.push(classes.SmallPadding)
  if (props.smallFont) questionClasses.push(classes.SmallFont)
  if (props.noMargin) questionClasses.push(classes.NoMargin)
  return (
    <div className={questionClasses.join(" ")}>
      {props.children}
    </div>
  )
}

export default question