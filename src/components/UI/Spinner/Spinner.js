import React from 'react'

import classes from './Spinner.css'

const spinner = props => {

  let spinnerClasses = [classes.Loader]
  if (props.overall) spinnerClasses.push(classes.Overall)
  if (props.grey) {
    spinnerClasses.push(classes.Grey)
  } else {
    spinnerClasses.push(classes.White)
  }

  return (
    <div className={spinnerClasses.join(" ")}>Loading...</div>
  )

}

export default spinner