import React from 'react'

import supportimg from '../../assets/images/support.png'
import classes from './support.css'

const support = props => (
  <div className={props.noMax ? classes.SupportNoMax : classes.Support}>
    <img src={supportimg} alt="support" />
    <div>沃衍健康（微信客服）</div>
  </div>
)

export default support