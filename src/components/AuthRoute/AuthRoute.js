import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import axios from 'axios'

import Modal from '../../components/UI/Modal/Modal'
import Question from '../../components/UI/Question/Question'
import Aux from '../../hoc/Aux/Aux'
import * as actions from '../../store/actions/index';
import Detail from "../UI/Detail/Detail";

class AuthRoute extends Component {

  getCookie = name => {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }

  componentDidMount = async () => {
    const publicList = ['/login', '/register', '/articles', '/', '/contact', '/physiotherapist']
    const specialList = ['/getnewqrcode', '/getpdf', '/seeplan']
    const pathname = this.props.location.pathname.split("/")
    const firstPathname = "/" + pathname[1]
    // TODO: 404
    if (specialList.indexOf(firstPathname) > -1){
      this.props.onAuthenticate({code: 1}, false, 0, "")
    } else {
      try {
        const user = await axios.get('https://backend.voin-cn.com/info',
          {params: {userId: this.getCookie("userId"),
              token: this.getCookie("token")}})
        if (user.data.code === 0) {
          if (!user.data.doc) {
            this.props.onAuthenticate(user.data, false, 0, "")
            this.props.onRemoveCookies()
            this.props.history.push('/')
          } else {
            this.props.onAuthenticate(user.data, true, 0, "", user.data.doc.admin, user.data.refer)
            if (publicList.indexOf(firstPathname) === 0 || publicList.indexOf(firstPathname) === 1) {
              this.props.history.push('/')
            }
          }
        } else if (user.data.code === 2) {
          this.props.onAuthenticate(user.data, false, 0, "")
          this.props.onRemoveCookies()
          if (publicList.indexOf(firstPathname) === -1) {
            this.props.history.push('/login')
          }
        } else if (user.data.code === 1) {
          this.props.onAuthenticate(user.data, false, 2, user.data.msg)
          this.props.onRemoveCookies()
        }
      } catch (error) {
        console.log(error)
        this.props.onAuthenticate({code: 2}, false, 2, "内部服务器错误，请刷新")
        this.props.onRemoveCookies()
      }
    }
  }

  render() {
    return (
      <Aux>
        {this.props.mode === 2 ? (
          <Modal show={this.props.mode === 2}>
            <Question>{this.props.error}</Question>
          </Modal>
        ) : null}
        {null}
        {this.props.showDetailModal ? (
          <Detail/>
        ) : null}
      </Aux>
    )
  }
}

const mapStateToProps = state => {
  return {
    showDetailModal: state.detail.showDetailModal,
    mode: state.authRoute.mode,
    error: state.authRoute.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuthenticate: (info, login, mode, error, admin, refer) => dispatch(actions.authenticate(info, login, mode, error, admin, refer)),
    onRemoveCookies: () => dispatch(actions.removeCookies())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AuthRoute))