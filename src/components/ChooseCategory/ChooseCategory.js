import React, {Component} from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Button from '../../components/UI/Button/Button'
import Question from '../../components/UI/Question/Question'
import Modal from '../../components/UI/Modal/Modal'
import Aux from '../../hoc/Aux/Aux'
import classes from './ChooseCategory.css'
import Spinner from "../UI/Spinner/Spinner";
import Title from "../Title/Title";
import * as actions from "../../store/actions";

class ChooseCategory extends Component {

  realAddClientToCategory = (doctorId, clientId, categoryIds, childDoctorId, type) => {
    this.props.onRealAddClientToCategory(doctorId, clientId, categoryIds, childDoctorId, type)
    // else this.props.onAddClientToEmptyCategory()
  }

  render() {
    const allDisabledIds = []
    this.props.clickParentIds.forEach(id => {
      allDisabledIds.push(...this.props.seconds[id].map(child => child._id))
      allDisabledIds.push(id)
    })

    return (
      <Modal modalClosed={this.props.onCloseModal} type="Category" show={this.props.addCategoryMode > 0 && this.props.addCategoryMode < 6}>
        <div>
          {this.props.addCategoryMode === 1 || this.props.addCategoryMode === 5 ? <Aux>
            <p style={{marginTop: "0"}} className={classes.Center}>请将"{this.props.addCategoryClientName}"这
              {this.props.type === "client" ? "位患者" : "个模板"}加入到下列分类中</p>
            <div className={classes.Columns}>
              <div className={classes.Choices}>
                <Title>自定义类</Title>
                {this.props.clientCategories.filter(cat => cat.type === "defined").map(cat => {
                  const parentChecked = this.props.clickCategoryIds.indexOf(cat._id) >= 0
                  return (<Aux key={cat._id}>
                    <input type="checkbox" id={cat._id} name="category" value={cat._id}
                           checked={parentChecked}
                           disabled={allDisabledIds.indexOf(cat._id) > -1 && !parentChecked}
                           onChange={(event) => this.props.onClickCategory(event.target.value)}/>
                    <label htmlFor={cat._id}>{cat.name}</label><br/>
                    {!!this.props.seconds[cat._id] ? this.props.seconds[cat._id].map(second => {
                      const childChecked = this.props.clickCategoryIds.indexOf(second._id) >= 0
                      return (<div key={second._id} className={classes.LeftPadding}>
                        <input type="checkbox" id={second._id} name={cat._id} value={second._id}
                               checked={this.props.clickCategoryIds.indexOf(second._id) >= 0}
                               disabled={allDisabledIds.indexOf(cat._id) > -1 && !childChecked}
                               onChange={(event) => this.props.onClickCategory(event.target.value)}/>
                        <label htmlFor={second._id}>{second.name}</label><br/>
                      </div>)
                    }) : null}
                  </Aux>)})}
              </div>
              <div className={classes.Choices}>
                <Title>疾病码类</Title>
                {this.props.clientCategories.filter(cat => cat.type === "disease").sort((a, b) => a.name.localeCompare(b.name)).map(cat => <Aux key={cat._id}>
                  <input type="checkbox" id={cat._id} name="category" value={cat._id}
                         checked={this.props.clickCategoryIds.indexOf(cat._id) >= 0}
                         onChange={(event) => this.props.onClickCategory(event.target.value)}/>
                  <label htmlFor={cat._id}>{cat.name}</label><br/>
                </Aux>)}
              </div>
              <div className={classes.Choices}>
                <Title>手术码类</Title>
                {this.props.clientCategories.filter(cat => cat.type === "operation").sort((a, b) => a.name.localeCompare(b.name)).map(cat => <Aux key={cat._id}>
                  <input type="checkbox" id={cat._id} name="category" value={cat._id}
                         checked={this.props.clickCategoryIds.indexOf(cat._id) >= 0}
                         onChange={(event) => this.props.onClickCategory(event.target.value)}/>
                  <label htmlFor={cat._id}>{cat.name}</label><br/>
                </Aux>)}
              </div>
              {this.props.type === "client" && this.props.childDoctors && this.props.childDoctors.length > 0 ? <div className={classes.Choices}>
                <Title>子医生类</Title>
                {this.props.childDoctors.slice().reverse().map(childDoctor => <Aux key={childDoctor._id}>
                  <input type="radio" id={childDoctor._id} name="child" value={childDoctor._id}
                         checked={this.props.chosenChildDoctorId === childDoctor._id}
                         onChange={(event) => this.props.onClickChildDoctor(event.target.value)}/>
                  <label htmlFor={childDoctor._id}>{childDoctor.name}</label><br/>
                </Aux>)}
              </div> : null}
            </div>
            {this.props.addCategoryMode === 5 ? <Question>请至少选择一个分类</Question> : null}
            <div className={classes.Center}>
              <Button btnType="Danger" clicked={this.props.onCloseModal}>取消</Button>
              <Button btnType="Success"
                      clicked={() => this.realAddClientToCategory(this.props.doctorId,
                          this.props.addCategoryClientId,
                          this.props.clickCategoryIds,
                          this.props.chosenChildDoctorId, this.props.type)}>继续
              </Button>
            </div>
          </Aux> : this.props.addCategoryMode === 2 ? <Spinner/> :
            this.props.addCategoryMode === 3 ? <Question>{this.props.addCategoryModeError}</Question> :
              this.props.addCategoryMode === 4 ? <div className={classes.Center}>
                <p style={{marginTop: "0"}}>"{this.props.addCategoryClientName}"
                  {this.props.type === "client" ? "患者" : "模板"}分类已更改</p>
                <Button btnType="Success" clicked={this.props.onCloseModal}>确定</Button>
              </div> : null}
        </div>
      </Modal>
    )
  }
}


const mapStateToProps = state => {
  return {
    addCategoryMode: state.clients.addCategoryMode,
    addCategoryModeError: state.clients.addCategoryModeError,
    addCategoryClientId: state.clients.addCategoryClientId,
    addCategoryClientName: state.clients.addCategoryClientName,
    clientCategories: state.clients.clientCategories,
    clickCategoryIds: state.clients.clickCategoryIds,
    clickParentIds: state.clients.clickParentIds,
    seconds: state.clients.seconds,
    childDoctors: state.authRoute.childDoctors,
    chosenChildDoctorId: state.clients.chosenChildDoctorId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCloseModal: () => dispatch(actions.closeModal()),
    onClickCategory: (value) => dispatch(actions.clickCategory(value)),
    onClickChildDoctor: (value) => dispatch(actions.clickChildDoctor(value)),
    onRealAddClientToCategory: (doctorId, clientId, categoryIds, childDoctorId, type) =>
      dispatch(actions.realAddClientToCategory(doctorId, clientId, categoryIds, childDoctorId, type)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ChooseCategory))