import React, {Component} from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Button from '../../components/UI/Button/Button'
import Question from '../../components/UI/Question/Question'
import ChooseCategory from '../../components/ChooseCategory/ChooseCategory'
import Aux from '../../hoc/Aux/Aux'
import edit from '../../assets/images/ic_编辑.svg'
import view from '../../assets/images/ic_查看.svg'


import classes from './ClientTable.css'
import * as actions from "../../store/actions";

class ClientTable extends Component {


  render() {
    let content = <Question>{this.props.error}</Question>

    let addCategory = <ChooseCategory doctorId={this.props.doctorId} type="client"/>


    if (!this.props.error) {
      if (this.props.clients.length === 0 && this.props.chosen.length > 0) {
        content = <Question>在该分类中无患者</Question>
      } else if (this.props.clients.length === 0) {
        content = <Question>目前无患者</Question>
      } else {
        content = (
          <Aux>
            {addCategory}
            <table className={classes.Table}>
              <thead>
              <tr>
                <th className={classes.Name}>姓名</th>
                {/*<th onClick={() => this.props.sortClients("refer")}*/}
                    {/*className={[classes.Clickable, classes.Name, this.props.order === "refer" ? classes.Order : ""]*/}
                      {/*.join(" ")}>推荐人</th>*/}
                <th onClick={() => this.props.sortClients("total")}
                    className={[classes.Clickable, classes.Length, this.props.order === "total" ? classes.Order : ""]
                      .join(" ")}>计划数</th>
                <th onClick={() => this.props.sortClients("paid")}
                    className={[classes.Clickable, classes.Paid, this.props.order === "paid" ? classes.Order : ""]
                      .join(" ")}>已支付</th>
                <th onClick={() => this.props.sortClients("unpaid")}
                    className={[classes.Clickable, classes.Unpaid, this.props.order === "unpaid" ? classes.Order : ""]
                      .join(" ")}>未支付</th>
                <th className={classes.Manage}>管理</th>
              </tr>
              </thead>
              <tbody>
              {this.props.clients.map((client, index) => {
                const paid = client.allPlans.filter(plan => plan).length
                const unpaid = client.allPlans.length - paid
                return (
                  <tr className={client.empty ? classes.Empty : null} key={client.clientId}>
                    <td>{client.clientName}</td>
                    <td>{client.allPlans.length}</td>
                    <td>{paid}</td>
                    <td>{unpaid}</td>
                    <td>
                      <img alt="view" className={classes.Image} src={view} title="查看" onClick={() => this.props.onClientDetail(client.clientId)}/>
                      <img alt="edit" className={classes.Image} src={edit} title="更改分类" onClick={() => this.props.onAddClientToCategory(
                        client.clientId, client.clientName, client.category, client.childDoctorId
                        )}/>
                      {client.request.length > 0 ? client.read ? <Button
                        clicked={() => this.props.onSeeRequest(index, false, client.clientId, this.props.doctorId)}
                        btnType="LightDanger">查看请求</Button> : <Button
                        clicked={() => this.props.onSeeRequest(index, true, client.clientId, this.props.doctorId)}
                        btnType="Danger">查看请求</Button> : null}
                    </td>
                  </tr>
                )
              })}
              </tbody>
            </table>
            {this.props.clients.length ? <div className={classes.Note}>共{this.props.clients.length}名患者</div> : null}
          </Aux>

        )
      }
    }

    return content
  }


}


const mapDispatchToProps = dispatch => {
  return {
    onAddClientToCategory: (id, name, category, childDoctorId) => dispatch(actions.addClientToCategory(id, name, category, childDoctorId)),
  };
};

export default connect(null, mapDispatchToProps)(withRouter(ClientTable))