import React from 'react'

import publicimg from '../../assets/images/public.png'
import classes from './public.css'

const carm = props => (
  <div className={classes.Public}>
    <img src={publicimg} alt="public" />
    <div>微信公众号：VOIN（医生版）</div>
  </div>
)

export default carm