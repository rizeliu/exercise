import React from 'react'

import carmpng from '../../assets/images/carm.png'
import classes from './CARM.css'

const carm = props => (
  <div className={classes.CARM}>
    <img src={carmpng} alt="carm" />
  </div>
)

export default carm