import React, { Component } from 'react'
import { connect } from 'react-redux'

import TempCate from './TempCate/TempCate'
import * as actions from '../../store/actions/index';
import classes from '../Categories/Categories.css'

class TempCates extends Component {

  componentDidMount = () => {
    this.props.onInit()
  }

  render() {
    let firstLi = null
    if (!this.props.root) {
      firstLi = <TempCate back key="back" name={this.props.inside} backClick={this.props.onBackClick}/>
    }


    return (
      <ul className={classes.Categories}>
        {firstLi}
        {this.props.current.map((category, index) => {
            return (
              <TempCate
                root={this.props.root}
                inside={this.props.inside}
                hasSub={!!this.props.inside && this.props.overallInfo[this.props.inside][category].length > 0}
                key={category}
                name={category}
                open={this.props.open}
                list={[...this.props.cList]}
                enter={this.props.onCategoryClick}
                drop={this.props.onDropDown}
                search={this.props.onSearchTemps}
                index={index}
              />
            )
          }
        )}
      </ul>
    )
  }
}

const mapStateToProps = state => {
  return {
    root: state.tempCates.root,
    inside: state.tempCates.inside,
    overallInfo: state.tempCates.overallInfo,
    order: state.tempCates.order,
    current: state.tempCates.current,
    open: state.tempCates.open,
    cList: state.tempCates.cList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCategoryClick: (name, index) => dispatch(actions.tempCateClick(name, index)),
    onBackClick: () => dispatch(actions.tempBackClick()),
    onDropDown: (inside, name) => dispatch(actions.tempDropDown(inside, name)),
    onSearchTemps: (a, b, c) => dispatch(actions.searchTemps(a, b, c)),
    onInit: () => dispatch(actions.tempCategoriesInit()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TempCates)