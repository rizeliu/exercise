import React from 'react'
import { connect } from 'react-redux'

import BackIcon from '../../../components/UI/BackIcon/BackIcon'
import DownIcon from '../../../components/UI/DownIcon/DownIcon'
import RightIcon from '../../../components/UI/RightIcon/RightIcon'
import classes from '../../Categories/Category/Category.css'

const tempCate = props => {

  const categoryClasses = [classes.Category]
  let icon = null
  let onClick
  if (props.root) {
    categoryClasses.push(classes.Root)
    onClick = () => props.enter(props.name, props.index)
  }
  if (props.back) {
    categoryClasses.push(classes.Back)
    icon = <BackIcon />
    onClick = () => props.backClick()
  }
  if (props.hasSub && props.open !== props.name) {
    categoryClasses.push(classes.Close)
    icon = <RightIcon />
    onClick = () => props.drop(props.inside, props.name)
  }
  if (props.hasSub && props.open === props.name) {
    categoryClasses.push(classes.Open)
    icon = <DownIcon />
    onClick = () => props.drop(props.inside, props.name)
  }
  if (!onClick) {
    onClick = () => props.search(props.inside, props.name, 0)
  }

  let ul = null
  if (!props.back && props.list.length > 0 ) {
    ul = <ul className={classes.List}>
      {props.list.map(el =>
        <li onClick={() => props.search(props.inside, props.name, el)}
            className={classes.ListItem}
            key={el}>{props.map[el] ? props.map[el] : el}</li>)}
    </ul>
  }
  return (
    <li className={classes.Cell}>
      <div onClick={onClick} className={categoryClasses.join(' ')}>
        {icon}
        {props.map[props.name] ? props.map[props.name] : props.name}
      </div>
      {props.open === props.name ? ul : null}
    </li>
  )

}

const mapStateToProps = state => {
  return {
    map: state.tempCates.map,
  };
};

export default connect(mapStateToProps)(tempCate)
