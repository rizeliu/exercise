import React from 'react'
import { connect } from 'react-redux'

import Question from '../../components/UI/Question/Question'
import Aux from '../../hoc/Aux/Aux'
import classes from './TemplateTable.css'
import Title from "../Title/Title";
import * as actions from "../../store/actions";
import ChooseCategory from "../ChooseCategory/ChooseCategory";
import edit from '../../assets/images/ic_编辑.svg'
import view from '../../assets/images/ic_查看.svg'
import assign from '../../assets/images/ic_分配模板.svg'
import change from '../../assets/images/ic_更改分类.svg'
import upload from '../../assets/images/ic_上传.svg'
import send from '../../assets/images/ic_发送.svg'
import download from '../../assets/images/ic_下载.svg'
import deleteTemp from '../../assets/images/ic_删除3.svg'

const templateTable = props => {

  let content = props.error2 ? <Question>{props.error2}</Question> : <Question>{props.error1}</Question>

  let addCategory = <ChooseCategory doctorId={props.doctorId} type="template"/>



  if (!props.error1 && !props.error2) {
    if (props.templates.length === 0) {
      content = <Aux>
        {props.title ?
          <Title noPaddingTop>{props.map[props.title] ? props.map[props.title] : props.title}</Title>
          : null}
        <div className={classes.TableContent}><Question>目前无模版</Question></div>
      </Aux>
    } else {
      content = (
        <Aux>
          {addCategory}
          {props.title ?
            <Title noPaddingTop>{props.map[props.title] ? props.map[props.title] : props.title}</Title>
            : null}
            <div className={classes.TableContent}>
              <table className={classes.Table}>
                <thead>
                <tr>
                  {props.my ? <th className={classes.MyName}>模版名</th> : null}
                  {props.my && props.focus ? <th className={classes.MyLength}>运动数</th> : null}
                  {props.my && props.focus ? <th className={classes.MyManage}>管理</th> : null}
                  {!props.my ? <th className={classes.ExpertName}>模版名</th> : null}
                  {!props.my && props.focus ? <th className={classes.ExpertLength}>运动数</th> : null}
                  {!props.my && props.focus ? <th className={classes.ExpertManage}>管理</th> : null}
                </tr>
                </thead>
                <tbody>
                {props.templates.map((template, index) => (
                  <tr key={template.templateId+index} className={classes.Tr}>
                    <td>{template.templateName}</td>
                    {props.focus ? <td>{template.count}</td> : null}
                    {props.focus ? <td className={classes.Flex}>
                      {props.qrcode ? <img alt="assign" className={classes.Image} src={view} title="二维码" onClick={() => props.onCreateQRCode(props.doctorId, template.templateId, template.templateName, 1)}/> : null}
                      {props.doctorId === "5f23a8e26266b8739f89de86" ? <img alt="assign" className={classes.Image} src={view} title="测试二维码" onClick={() => props.onCreateQRCode(props.doctorId, template.templateId, template.templateName, 2)}/> : null}

                      {props.my ?
                          <img alt="view" className={classes.Image} src={view} title="查看" onClick={() => props.onShowTemplateDetail(props.doctorId, template.templateId)}/>
                        : props.expertOrTeam === "expert" ?
                          <img alt="view" className={classes.Image} src={view} title="查看" onClick={() => props.onShowTemplateDetail("expert", template.templateId)}/>
                          :
                          <img alt="view" className={classes.Image} src={view} title="查看" onClick={() => props.onShowTemplateDetail(props.admin === 4 || props.admin === 5 ? "expert" : "team", template.templateId, props.teamId)}/>
                        }
                      {props.my
                      || (props.admin === 1 && props.expertOrTeam === "expert")
                      || (props.expertOrTeam === "team" && props.creator) ?
                        <img alt="delete" className={classes.Image} src={deleteTemp} title="删除" onClick={() => props.onDeleteTemplate(template.templateId, template.templateName,
                          props.my ? "my" : props.expertOrTeam)}/>
                        : null}
                      {props.circle ?
                        <img alt="upload" className={classes.Image} src={upload} title="上传"
                             onClick={() => props.onAddTemplateToCircle(template.templateId, template.templateName)}/> :
                        null}
                      {props.my ? <Aux>
                        {props.admin === 5 || props.admin === 4 ? null : !template.loading1 ?
                          <img alt="assign" className={classes.Image} src={assign} title="分配" onClick={() => props.onCreatePlanByTemplate(props.doctorId, template, index, props.page, "template")}/>
                          : <span className={classes.Image}>载入中...</span>}
                        {props.admin === 5 ? !template.loading1 ?
                          <img alt="upload" className={classes.Image} src={upload} title="提交作业" onClick={() => props.onCreatePlanByTemplate(props.doctorId, template, index, props.page, "template")}/>
                          : <span className={classes.Image}>载入中...</span> : null}
                        {!template.loading2 ?
                          <img alt="edit" className={classes.Image} src={edit} title="编辑" onClick={() => props.onEditTemplate(props.doctorId, template, index, props.page)}/>
                           : <span className={classes.Image}>载入中...</span>}
                        {props.admin === 1 ? <Aux>
                          <img alt="send" className={classes.Image} src={send} title="发送"
                               onClick={() => props.onSendTemplate(template.templateId, template.templateName)}/>
                          {/*<Button*/}
                            {/*clicked={() => props.onAddExpertOrTeam(template.templateId, template.templateName)}*/}
                            {/*btnType="New">上传*/}
                          {/*</Button>*/}
                        </Aux> : null}
                        {props.my && !props.teams.every(team => !team.creator) ? !template.loading3 ?
                          <img alt="upload" className={classes.Image} src={upload} title="上传"
                               onClick={() => props.onAddExpertOrTeam(template.templateId, template.templateName)}/>
                          : <span className={classes.Image}>载入中...</span> : null}
                        <img alt="change" className={classes.Image} src={change} title="更改分类" onClick={() => props.onAddTemplateToCategory(
                          template.templateId, template.templateName, template.category)}/>
                        </Aux> : <Aux>
                        {props.admin === 5 ? null : template.done ? <span className={classes.Image}>已下载</span> : template.loading4 ?
                          <span className={classes.Image}>下载中...</span> :
                          <img alt="download" className={classes.Image} src={download} title="下载"
                               onClick={() => props.onDownloadTemplate(
                                 template.templateId, props.doctorId, props.expertOrTeam, props.teamId, props.admin
                               )}/>}</Aux>}
                    </td> : null}
                  </tr>
                ))}
                </tbody>
              </table>
              {props.templates.length ? <div className={classes.Note}>共{props.templates.length}个模版</div> : null}
            </div>
        </Aux>
      )
    }
  }

  return content
}

const mapStateToProps = state => {
  return {
    map: state.tempCates.map,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddTemplateToCategory: (id, name, category) => dispatch(actions.addTemplateToCategory(id, name, category)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(templateTable)