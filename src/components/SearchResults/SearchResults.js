import React from 'react'
import { connect } from 'react-redux'

import Title from '../Title/Title'
import Aux from '../../hoc/Aux/Aux'
import Picture from '../../components/UI/Picture/Picture'
import NoImage from '../../assets/images/img_未选择分类.svg'

import classes from './SearchResults.css'


const SearchResults = props => {

  let content = (
    <Aux>
      <div style={{margin: '70px'}}/>
      <div className={classes.Center}>
        <div className={classes.Center}><img alt="NoImage" src={NoImage}/></div>
        <div className={classes.NoImageText}>未选择分类</div>
      </div>
    </Aux>
  )

  if (props.list) {
    content = (<Aux>
      <Title noPaddingTop>{props.map[props.title] ? props.map[props.title] : props.title}</Title>
      {/*{props.inSearch ? <SearchInput placeholder="分类内搜索" /> : null}*/}
      <ul className={classes.Videos}>
        {props.list.map(video => {
          if (video.video && !video.mp4) return null
          else return <li key={video._id} className={classes.Element}>
            <Picture play={props.play} showVideo={props.showVideo} video={video}/>
          </li>
        })}
      </ul>
    </Aux>)
  }

  return content
}

const mapStateToProps = state => {
  return {
    map: state.categories.map
  };
};


export default connect(mapStateToProps)(SearchResults)