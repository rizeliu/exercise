import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd"
import axios from 'axios'

import Title from '../../components/Title/Title'
import Button from '../../components/UI/Button/Button'
import Question from '../../components/UI/Question/Question'
import Input from '../../components/UI/Input/Input'
import SingleLineSpinner from '../../components/UI/SingleLineSpinner/SingleLineSpinner'
import deleteIcon from '../../assets/images/ic_计划删除.svg'
import displayIcon from '../../assets/images/ic_计划已选.svg'
import classes from './Preview.css'

import * as actions from '../../store/actions/index';

class Preview extends Component {

  state = {
    getDetailLoading: false,
    error: 0,
    ids: [],
    urls: [],
    errorMsg: ""
  }

  toArrArr = (totalInEach, plan) => {
    const result = []
    for (let i = 0; i < totalInEach.length; i = i+1) {
      if (i === 0) {
        result.push([...plan.slice(0, totalInEach[i])])
      } else {
        result.push([...plan.slice(totalInEach[i-1], totalInEach[i])])
      }
    }
    return result
  }

  proceed = (template) => {
    if (this.props.template) {
      if (this.checkTemplate()) {
        this.getMovementDetail()
      } else {
        this.props.onDisplayError(this.toArrArr(this.props.totalInEach, this.props.plan))
      }
    } else {
      if (this.checkPlan(template)) {
        this.getMovementDetail(template)
      } else {
        this.props.onDisplayError(this.toArrArr(this.props.totalInEach, this.props.plan), template)
      }
    }
  }

  getCurrentDay = () => {
    if (!this.props.start) return 0
    else {
      const timezone = 8;
      const offset_GMT = new Date().getTimezoneOffset();
      const correctDate = new Date(new Date().getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
      const startDate = new Date(parseInt(this.props.startDate.substring(0,4), 10),
        parseInt(this.props.startDate.substring(4,6), 10)-1,
        parseInt(this.props.startDate.substring(6,8), 10)
      )
      const diffTime = Math.abs(correctDate - startDate);
      const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
      return diffDays+1
    }
  }

  getCurrentPeriod = ithDay => {
    if (this.props.periodDays.length === 0 || ithDay === 0) return -1
    else {
      for (let i = 0; i < this.props.periodDays.length; i++) {
        if (ithDay <= parseInt(this.props.periodDays[i], 10)) {
          return i
        } else {
          ithDay = ithDay - parseInt(this.props.periodDays[i], 10)
        }
      }
    }
  }

  reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  onDragEndInner = (result) => {
    if (!result.destination) return
    const index = parseInt(result.source.droppableId, 10)
    const newPlan = this.props.plan.slice(index === 0 ? 0 : this.props.totalInEach[index-1], this.props.totalInEach[index])
    const [removed] = newPlan.splice(result.source.index, 1)
    newPlan.splice(result.destination.index, 0, removed)
    this.props.onChangeOrder(newPlan, index, result.source.index, result.destination.index)
  }

  onDragEndOuter = (result, ithPeriod) => {
    if (!result.destination) return
    if (result.destination.index <= ithPeriod) return
    this.props.onChangePeriodOrder(result.source.index, result.destination.index)
  }



  componentDidUpdate = (prevProps) => {
    if (prevProps.template && !this.props.template) {
      this.props.onInit()
      if (this.props.admin !== 5) {
        this.props.onFetchClients(this.props.doctorId)
      }
    }
  }

  checkPlan(template) {
    if (template) {
      return this.toArrArr(this.props.totalInEach, this.props.plan)
        .every(period => period !== undefined
          && period.filter(move => move.moveId).length > 0)
    } else {
      const moveAndName = this.toArrArr(this.props.totalInEach, this.props.plan)
          .every(period => period !== undefined
            && period.filter(move => move.moveId).length > 0)
        && this.props.planName
      const client = this.props.admin === 5 ? this.props.titleId : this.props.clientId
      return moveAndName && client
    }
  }

  checkTemplate = () => {
    return this.toArrArr(this.props.totalInEach, this.props.plan)
        .every(period => period !== undefined
          && period.filter(move => move.moveId).length > 0)
      && this.props.planName
  }

  saving = (moves, articles, totalInEach, periodDays, times,
            timesPerSets, sets, weights, betweens, infos, ids, parameter1s, parameter2s, value1s, value2s, urls, bars, starts,
            proposalContent, references, therapies, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
            addedParamCounts, allChosenParams) => {

    this.props.onSavingTemplate({
      doctorId: this.props.doctorId,
      name: this.props.planName.length > 0 ? this.props.planName : "未命名的模板",
      summary: "",
      sets,
      times,
      timesPerSets,
      weights,
      betweens,
      parameter1s,
      parameter2s,
      value1s,
      value2s,
      infos,
      minutes,
      timesPerDays,
      time2s,
      periods,
      therapyInfos,
      addedParamCounts,
      allChosenParams,
      therapyIds,
      ids,
      bars,
      starts,
      barNames: [],
      totalInEach,
      periodDays,
      articles: articles.map(article => article.moveId),
      proposalContent,
      references
    }, true, this)
  }

  getMovementDetail = async (template) => {
    this.setState({getDetailLoading: true})
    let allDay = ""
    const sets = []
    const times = []
    const timesPerSets = []
    const weights = []
    const betweens = []
    const parameter1s = []
    const parameter2s = []
    const value1s = []
    const value2s = []
    const infos = []
    const minutes = []
    const timesPerDays = []
    const time2s = []
    const periods = []
    const therapyInfos = []
    const addedParamCounts = []
    const allChosenParams = []
    const ids = []
    const therapyIds = []
    const urls = []
    const bars = []
    const starts = []
    try {
      // if (!this.props.template) {
        // if (this.props.savedTemplateId) {
        //   const template = await axios.get('/templatebyid', {
        //     params: {doctorId: this.props.doctorId, templateId: this.props.savedTemplateId}
        //   })
        //   if (template.data.code === 1) {
        //     const moveList = template.data.moveList
        //     allDay = template.data.allDay
        //     moveList.forEach(move => {
        //       sets.push(move.set)
        //       times.push(move.time)
        //       timesPerSets.push(move.timesPerSet)
        //       weights.push(move.weight)
        //       infos.push(move.info)
        //       ids.push(move.move)
        //     })
        //   } else if (template.data.code === 0) {
        //     this.setState({error: 2, errorMsg: template.data.msg, getDetailLoading: false})
        //   }
        //   const temMoves = ids.map(async id => {
        //     const movement = await axios.get('/movement', {params: {moveId: id}})
        //     if (movement.data.code === 1) {
        //       this.setState({error: 0, errorMsg: ""})
        //       return movement.data.doc
        //     } else if (movement.data.code === 0) {
        //       this.setState({error: 2, errorMsg: movement.data.msg, getDetailLoading: false})
        //     }
        //   })
        //   templateMoves = await Promise.all(temMoves)
        // }


      if (!this.props.template && this.props.admin !== 5 && !template) {
        const client = await axios.get('https://backend.voin-cn.com/clientbyid', {params: {clientId: this.props.clientId}})
        if (client.data.code === 1) {
          this.props.onSetClientName(client.data.doc.name)
          this.setState({error: 0, errorMsg: ""})
        } else if (client.data.code === 0) {
          this.props.onSetClientName("")
          this.setState({error: 2, errorMsg: client.data.msg, getDetailLoading: false})
          return
        }
      }
      if (template) {
        //保存到模板
        const actualMoveIds = this.props.plan.filter(move => move.moveId).map(move => move.moveId)
        const allMoveIds = actualMoveIds.concat(this.props.therapies.map(therapy => therapy.moveId))
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allMoveIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const moves = this.props.plan.map(move => {
            if (move.moveId) {
              const thisMove = moveResult.filter(aMove => aMove._id === move.moveId)[0]
              return {...thisMove}
            } else {
              return move
            }
          })

          this.props.plan.forEach((move, index) => {
            times.push("")
            timesPerSets.push("")
            sets.push("")
            weights.push("")
            betweens.push("")
            infos.push(move.bar ? "" : moves[index].info)
            ids.push(move.bar ? "" : moves[index]._id)
            urls.push(move.bar ? "" : moves[index].url)
            bars.push(!!move.bar)
            starts.push(!!move.start)
          })

          const therapyResult = this.props.therapies.map(therapy => {
            const thisTherapy = moveResult.filter(aMove => aMove._id === therapy.moveId)[0]
            return {...thisTherapy, _id: therapy.moveId}
          })
          this.props.therapies.forEach((therapy, index) => {
            therapy.params = therapyResult[index].params
            therapyIds.push(therapy.moveId)
            minutes.push("")
            timesPerDays.push("")
            time2s.push("")
            periods.push("")
            therapyInfos.push("")
            addedParamCounts.push(0)
            allChosenParams.push([])
          })

          const proposalContent = this.props.admin === 5 ? this.props.proposalContent : ""
          const references = this.props.references.length > 0 ? this.props.references.filter(ref => ref.length > 0) : []

          this.saving(moves, this.props.articles, [...this.props.totalInEach], Array(this.props.period).fill(""), times,
              timesPerSets, sets, weights, betweens, infos, ids, parameter1s, parameter2s, value1s, value2s, urls, bars, starts,
              proposalContent, references, this.props.therapies, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
            addedParamCounts, allChosenParams)

          // this.props.onShowDetail({
          //   planDetail: moves, template: this.props.template,
          //   allDay, sets, timesPerSets, times, weights, betweens, infos, ids, parameter1s, parameter2s, value1s, value2s,
          //   introduction: this.props.introduction, planName: this.props.planName, articles: this.props.articles,
          //   totalInEach: [...this.props.totalInEach], periodDays: Array(this.props.period).fill(""), bars, starts,
          // })
          this.setState({urls, error: 0, errorMsg: ""})
        } else {
          this.setState({error: 2, errorMsg: moveSet.data.msg})
        }
      }
      else if (this.props.introducedTemplate) {
        const actualMoveIds = this.props.plan.filter(move => move.moveId).map(move => move.moveId)
        const allMoveIds = actualMoveIds.concat(this.props.therapies.map(therapy => therapy.moveId))
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allMoveIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const moves = this.props.plan.map(move => {
            if (move.moveId) {
              const thisMove = moveResult.filter(aMove => aMove._id === move.moveId)[0]
              return {...thisMove, _id: move.moveId}
            } else {
              return move
            }
          })
          this.props.plan.forEach((move, index) => {
            if (move.move === undefined) {
              times.push("")
              timesPerSets.push("")
              sets.push("")
              weights.push("")
              betweens.push("")
              parameter1s.push("")
              parameter2s.push("")
              value1s.push("")
              value2s.push("")
              infos.push(move.bar ? "" : moves[index].info)
              ids.push(move.bar ? "" : moves[index]._id)
              urls.push(move.bar ? "" : moves[index].url)
              bars.push(!!move.bar)
              starts.push(!!move.start)
            } else {
              times.push(this.props.plan[index].time)
              timesPerSets.push(this.props.plan[index].timesPerSet)
              sets.push(this.props.plan[index].set)
              weights.push(this.props.plan[index].weight)
              betweens.push(this.props.plan[index].between ? this.props.plan[index].between : "")
              infos.push(this.props.plan[index].info)
              parameter1s.push(this.props.plan[index].parameter1)
              parameter2s.push(this.props.plan[index].parameter2)
              value1s.push(this.props.plan[index].value1)
              value2s.push(this.props.plan[index].value2)
              ids.push(move.bar ? "" : moves[index]._id)
              urls.push(moves[index].url)
              bars.push(!!move.bar)
              starts.push(!!move.start)
            }
          })

          const therapyResult = this.props.therapies.map(therapy => {
            const thisTherapy = moveResult.filter(aMove => aMove._id === therapy.moveId)[0]
            return {...thisTherapy, _id: therapy.moveId}
          })
          this.props.therapies.forEach((therapy, index) => {
            therapy.params = therapyResult[index].params
            therapyIds.push(therapy.moveId)
            minutes.push(therapy.minute)
            timesPerDays.push(therapy.timesPerDay)
            time2s.push(therapy.time2)
            periods.push(therapy.period)
            therapyInfos.push(therapy.therapyInfo)
            addedParamCounts.push(therapy.chosenParams.length)
            allChosenParams.push(therapy.chosenParams)
          })


          this.props.onShowDetail({
            planDetail: moves, template: this.props.template,
            allDay, sets, timesPerSets, times, weights, betweens, infos, ids,
            parameter1s, parameter2s, value1s, value2s, bars, starts,
            introduction: this.props.introduction, planName: this.props.planName, articles: this.props.articles,
            totalInEach: [...this.props.totalInEach], periodDays: [...this.props.periodDays],
            therapies: this.props.therapies, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
            addedParamCounts, allChosenParams
          })
          this.setState({getDetailLoading: false, urls, error: 0, errorMsg: ""})
        } else {
          this.setState({error: 2, errorMsg: moveSet.data.msg, getDetailLoading: false})
        }
      }
      else if (this.props.fromEditPlan) {
        const actualMoveIds = this.props.plan.filter(move => move.moveId).map(move => move.moveId)
        const allMoveIds = actualMoveIds.concat(this.props.therapies.map(therapy => therapy.moveId))
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allMoveIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const moves = this.props.plan.map(move => {
            if (move.moveId) {
              const thisMove = moveResult.filter(aMove => aMove._id === move.moveId)[0]
              return {...thisMove, _id: move.moveId}
            } else {
              return move
            }
          })
          this.props.plan.forEach((move, index) => {
            if (move.move === undefined) {
              times.push("")
              timesPerSets.push("")
              sets.push("")
              weights.push("")
              betweens.push("")
              parameter1s.push("")
              parameter2s.push("")
              value1s.push("")
              value2s.push("")
              infos.push(move.bar ? "" : moves[index].info)
              ids.push(move.bar ? "" : moves[index]._id)
              urls.push(move.bar ? "" : moves[index].url)
              bars.push(!!move.bar)
              starts.push(!!move.start)
            } else {
              times.push(this.props.plan[index].time)
              timesPerSets.push(this.props.plan[index].timesPerSet)
              sets.push(this.props.plan[index].set)
              weights.push(this.props.plan[index].weight)
              betweens.push(this.props.plan[index].between ? this.props.plan[index].between : "")
              infos.push(this.props.plan[index].info)
              parameter1s.push(this.props.plan[index].parameter1)
              parameter2s.push(this.props.plan[index].parameter2)
              value1s.push(this.props.plan[index].value1)
              value2s.push(this.props.plan[index].value2)
              ids.push(move.bar ? "" : moves[index]._id)
              urls.push(moves[index].url)
              bars.push(!!move.bar)
              starts.push(!!move.start)
            }
          })

          const therapyResult = this.props.therapies.map(therapy => {
            const thisTherapy = moveResult.filter(aMove => aMove._id === therapy.moveId)[0]
            return {...thisTherapy, _id: therapy.moveId}
          })
          this.props.therapies.forEach((therapy, index) => {
            therapy.params = therapyResult[index].params
            therapyIds.push(therapy.moveId)
            minutes.push(therapy.minute)
            timesPerDays.push(therapy.timesPerDay)
            time2s.push(therapy.time2)
            periods.push(therapy.period)
            therapyInfos.push(therapy.therapyInfo)
            addedParamCounts.push(therapy.chosenParams.length)
            allChosenParams.push(therapy.chosenParams)
          })


          this.props.onShowDetail({
            planDetail: moves, template: this.props.template,
            allDay, sets, timesPerSets, times, weights, betweens, infos, ids,
            parameter1s, parameter2s, value1s, value2s, bars, starts,
            introduction: this.props.introduction, planName: this.props.planName, therapies: this.props.therapies, articles: this.props.articles,
            totalInEach: [...this.props.totalInEach], periodDays: [...this.props.periodDays],
            extra: this.props.savedPlan.extra, start: this.props.start, startDate: this.props.startDate,
            fromEditPlan: this.props.fromEditPlan,
            therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos, addedParamCounts, allChosenParams
          })
          this.setState({getDetailLoading: false, urls, error: 0, errorMsg: ""})
        } else {
          this.setState({error: 2, errorMsg: moveSet.data.msg, getDetailLoading: false})
        }
      }
      else if (this.props.fromTemplate || this.props.fromTemplateToPlan) {
        const actualMoveIds = this.props.plan.filter(move => move.moveId).map(move => move.moveId)
        const allMoveIds = actualMoveIds.concat(this.props.therapies.map(therapy => therapy.moveId))
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allMoveIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const moves = this.props.plan.map(move => {
            if (move.moveId) {
              const thisMove = moveResult.filter(aMove => aMove._id === move.moveId)[0]
              return {...thisMove, _id: move.moveId}
            } else {
              return move
            }
          })
          this.props.plan.forEach((move, index) => {
            if (move.move === undefined) {
              times.push("")
              timesPerSets.push("")
              sets.push("")
              weights.push("")
              betweens.push("")
              parameter1s.push("")
              parameter2s.push("")
              value1s.push("")
              value2s.push("")
              infos.push(move.bar ? "" : moves[index].info)
              ids.push(move.bar ? "" : moves[index]._id)
              urls.push(move.bar ? "" : moves[index].url)
              bars.push(!!move.bar)
              starts.push(!!move.start)
            } else {
              times.push(this.props.plan[index].time)
              timesPerSets.push(this.props.plan[index].timesPerSet)
              sets.push(this.props.plan[index].set)
              weights.push(this.props.plan[index].weight)
              betweens.push(this.props.plan[index].between ? this.props.plan[index].between : "")
              infos.push(this.props.plan[index].info)
              parameter1s.push(this.props.plan[index].parameter1)
              parameter2s.push(this.props.plan[index].parameter2)
              value1s.push(this.props.plan[index].value1)
              value2s.push(this.props.plan[index].value2)
              ids.push(move.bar ? "" : moves[index]._id)
              urls.push(moves[index].url)
              bars.push(!!move.bar)
              starts.push(!!move.start)
            }
          })

          const therapyResult = this.props.therapies.map(therapy => {
            const thisTherapy = moveResult.filter(aMove => aMove._id === therapy.moveId)[0]
            return {...thisTherapy, _id: therapy.moveId}
          })
          this.props.therapies.forEach((therapy, index) => {
            therapy.params = therapyResult[index].params
            therapyIds.push(therapy.moveId)
            minutes.push(therapy.minute)
            timesPerDays.push(therapy.timesPerDay)
            time2s.push(therapy.time2)
            periods.push(therapy.period)
            therapyInfos.push(therapy.therapyInfo)
            addedParamCounts.push(therapy.chosenParams.length)
            allChosenParams.push(therapy.chosenParams)
          })

          this.props.onShowDetail({
            planDetail: moves, template: this.props.template,
            allDay, sets, timesPerSets, times, weights, betweens, infos, ids,
            parameter1s, parameter2s, value1s, value2s, bars, starts, therapies: this.props.therapies,
            introduction: this.props.introduction, planName: this.props.planName, articles: this.props.articles,
            totalInEach: [...this.props.totalInEach], periodDays: [...this.props.periodDays],
            therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos, addedParamCounts, allChosenParams,
          })
          this.setState({getDetailLoading: false, urls, error: 0, errorMsg: ""})
        } else {
          this.setState({error: 2, errorMsg: moveSet.data.msg, getDetailLoading: false})
        }
      }
      else {
        const actualMoveIds = this.props.plan.filter(move => move.moveId).map(move => move.moveId)
        const allMoveIds = actualMoveIds.concat(this.props.therapies.map(therapy => therapy.moveId))
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allMoveIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const moves = this.props.plan.map(move => {
            if (move.moveId) {
              const thisMove = moveResult.filter(aMove => aMove._id === move.moveId)[0]
              return {...thisMove}
            } else {
              return move
            }
          })

          this.props.plan.forEach((move, index) => {
            times.push("")
            timesPerSets.push("")
            sets.push("")
            weights.push("")
            betweens.push("")
            infos.push(move.bar ? "" : moves[index].info)
            ids.push(move.bar ? "" : moves[index]._id)
            urls.push(move.bar ? "" : moves[index].url)
            bars.push(!!move.bar)
            starts.push(!!move.start)
          })

          this.props.therapies.forEach((therapy) => {
            therapyIds.push(therapy.moveId)
            minutes.push("")
            timesPerDays.push("")
            time2s.push("")
            periods.push("")
            therapyInfos.push("")
            addedParamCounts.push(0)
            allChosenParams.push([])
          })

          this.props.onShowDetail({
            planDetail: moves, template: this.props.template,
            allDay, sets, timesPerSets, times, weights, betweens, infos, ids, parameter1s, parameter2s, value1s, value2s,
            introduction: this.props.introduction, planName: this.props.planName, therapies: this.props.therapies, articles: this.props.articles,
            totalInEach: [...this.props.totalInEach], periodDays: Array(this.props.period).fill(""), bars, starts,
            therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos, addedParamCounts, allChosenParams,
          })
          this.setState({getDetailLoading: false, urls, error: 0, errorMsg: ""})
        } else {
          this.setState({error: 2, errorMsg: moveSet.data.msg, getDetailLoading: false})
        }

      }
    } catch (error) {
      console.log(error)
      this.setState({error: 2, errorMsg: "内部服务器错误，请刷新", getDetailLoading: false})
    }
  }

  render() {

    const ithDay = this.getCurrentDay()
    const ithPeriod = this.getCurrentPeriod(ithDay)

    const noExerciseMessage = this.props.noExerciseError && !this.props.savedTemplateId ?
      <div className={classes.Block}><Question smallPadding smallFont>每阶段至少需要一个动作</Question></div> : null
    const noClientMessage = this.props.noClientError ?
      <div className={classes.Block}><Question smallPadding smallFont>请选择患者</Question></div> : null
    const noTitleMessage = this.props.noTitleError ?
      <div className={classes.Block}><Question smallPadding smallFont>请选择作业</Question></div> : null
    let noNameMessage = null
    if (this.props.noNameError) {
      noNameMessage = this.props.template ?
        <div className={classes.Block}><Question smallPadding smallFont>未为模板取名</Question></div> :
        <div className={classes.Block}><Question smallPadding smallFont>未为计划取名</Question></div>
    }
    const periods = this.toArrArr(this.props.totalInEach, this.props.plan)
    return (
      <div className={classes.Right}>
        {this.props.template || this.props.fromTemplate ? <Title noPaddingTop>模板预览</Title> : <Title noPaddingTop>计划预览</Title>}
        <div className={classes.Manage}>
          <Button clicked={() => this.props.onSearchChosenVideos(this.props.plan, this.props.therapies, this.props.articles)}
                  btnType="Grey">已选</Button>
          {this.props.fromEditPlan ? null : <Button clicked={this.props.onDeleteAll}
                                                    btnType="Grey">删除全部</Button>}
          <Button clicked={this.props.onAddPeriod}
                  btnType="Success">添加阶段</Button>
        </div>
        <DragDropContext onDragEnd={(result) => this.onDragEndOuter(result, ithPeriod)}>
          <Droppable droppableId="total">
            {(providedOuter) => (
              <ul ref={providedOuter.innerRef}
                  {...providedOuter.droppableProps}
                className={classes.Periods}
              >
                {periods.map((period, index1) => {
                  const nameEnd = this.props.fromEditPlan && !this.props.empty ?
                    index1 < ithPeriod ? "(已结束)" : index1 === ithPeriod ? "(进行中)" : "(未开始)" : ""

                  return (
                    <Draggable key={""+index1}
                               draggableId={""+index1}
                               index={index1}
                               isDragDisabled={index1 <= ithPeriod}>
                      {(providedOuter) => (<li key={""+index1}
                                          ref={providedOuter.innerRef}
                                          {...providedOuter.draggableProps}
                                          {...providedOuter.dragHandleProps}
                                          className={classes.RealPeriod}
                      >
                        <div className={index1 < ithPeriod ? classes.PassedPeriod : this.props.currentPeriod === index1 ?
                          classes.ChosenPeriod : classes.UnchosenPeriod} key={index1}>
                          {this.props.fromEditPlan && index1 < ithPeriod ? <div className={classes.Layer}/> : null}
                          <DragDropContext onDragEnd={this.onDragEndInner}>
                            <div className={index1 < ithPeriod ? classes.PassedPeriodRow : classes.PeriodRow}
                                 onClick={() => {
                                   if (ithPeriod <= index1) this.props.onChangePeriod(index1)
                                 }}>
                              <div className={classes.PeriodName}>{"第"+(index1+1)+"阶段 "+nameEnd}</div>
                              {this.props.currentPeriod === index1 ? <div className={classes.Front}>

                                  <div className={classes.Icon} onClick={() => this.props.onSearchChosenVideos(this.props.plan.slice(
                                      index1 === 0 ? 0 : this.props.totalInEach[index1-1], this.props.totalInEach[index1]
                                    ), [], [])}><img alt="display" src={displayIcon}/></div>
                                  {periods.length === 1 || index1 < ithPeriod ? null :
                                    <div onClick={() => this.props.onDeletePeriod(index1)}
                                         className={classes.Icon}><img alt="delete" src={deleteIcon}/></div>}
                                {/*<Button btnType="Success"*/}
                                {/*clicked={() => this.props.onSearchChosenVideos(this.props.plan.slice(*/}
                                {/*index1 === 0 ? 0 : this.props.totalInEach[index1-1], this.props.totalInEach[index1]*/}
                                {/*), [])}>已选</Button>*/}
                                {/*{periods.length === 1 || index1 < ithPeriod ? null : <Button btnType="Danger"*/}
                                {/*clicked={() => this.props.onDeletePeriod(index1)}>删除</Button>}*/}
                              </div> : null}
                            </div>
                            <Droppable droppableId={""+index1}>
                              {(provided) => (
                                <ul className={classes.Plan}
                                    ref={provided.innerRef}
                                    {...provided.droppableProps}>
                                  {period.map((move, index) => {
                                    if (move.bar && move.start) {
                                      return (
                                        <Draggable
                                          key={""+move.index1+" "+index+"start"}
                                          draggableId={""+move.index1+" "+index}
                                          index={index} isDragDisabled={true}>
                                          {(provided) => (<li key={""+move.index1+" "+index+"start"}
                                                              ref={provided.innerRef}
                                                              {...provided.draggableProps}
                                                              {...provided.dragHandleProps}
                                          >
                                            <div className={this.props.currentGroupStart === index1+" "+index ?
                                              classes.ChosenGroupRow : classes.UnchosenGroupRow}
                                                 onClick={() => this.props.onChangeGroup(index1, index)}
                                            >
                                              {move.editing ?
                                                <Input onChange={event => this.props.onRenamingGroup(index1, index, event)}
                                                       value={this.props.plan[index1 === 0 ? index :
                                                         this.props.totalInEach[index1-1] + index].name}
                                                       smallFont
                                                       noMargin
                                                       smallPadding
                                                       inline
                                                       onfocusout={() => this.props.onLoseFocus(index1, index)}
                                                       onKeyPress={event => {
                                                         if (event.key === 'Enter') {
                                                           this.props.onRenameGroupDone(index1, index)
                                                         }
                                                       }}
                                                       autoFocus
                                                /> :
                                                <div className={classes.GroupName}>{move.name}</div>}
                                              <div>
                                                {move.editing ?
                                                  null :
                                                  <Button btnType="New"
                                                          clicked={() => this.props.onRenameGroup(index1, index)}>修改名字</Button>}
                                                <Button btnType="Danger"
                                                        clicked={() => this.props.onDeleteGroup(index1, index)}>删除</Button>
                                              </div>
                                            </div>
                                          </li>)}
                                        </Draggable>)
                                    } else if (move.bar && !move.start) {
                                      return (
                                        <Draggable key={""+move.index1+" "+index+"start"}
                                                   draggableId={""+move.index1+" "+index+"start"}
                                                   index={index} isDragDisabled={true}>
                                          {(provided) => (<li key={""+move.index1+" "+index+"start"}
                                                              ref={provided.innerRef}
                                                              {...provided.draggableProps}
                                                              {...provided.dragHandleProps}
                                          >
                                            <hr className={this.props.currentGroupEnd === index1+" "+index ?
                                              classes.ChosenGroupEnd : classes.UnchosenGroupEnd}/>
                                          </li>)}
                                        </Draggable>
                                      )
                                    }
                                    else return (
                                        <Draggable key={move.moveId+" "+index}
                                                   draggableId={move.moveId+" "+index}
                                                   index={index}
                                                   isDragDisabled={index1 < ithPeriod}>
                                          {(provided) => (<li key={move.moveId+" "+index}
                                                              ref={provided.innerRef}
                                                              {...provided.draggableProps}
                                                              {...provided.dragHandleProps}
                                                              className={classes.RealMove}
                                          >
                                            <span>{move.moveName}</span>
                                            {index1 < ithPeriod || this.props.currentPeriod !== index1 ?
                                              null :
                                              <div className={classes.Icon}
                                                   onClick={() => this.props.onDeleteMove(1, move.moveId, index1, index)}>
                                                <img alt="delete" src={deleteIcon}/>
                                              </div>}
                                              {/*<Button btnType="Danger"*/}
                                                      {/*clicked={() => this.props.onDeleteMove(1, move.moveId, index1, index)}>*/}
                                                {/*删除*/}
                                              {/*</Button>}*/}
                                          </li>)}
                                        </Draggable>)
                                  })}
                                  {provided.placeholder}
                                </ul>
                              )}
                            </Droppable>
                          </DragDropContext>
                        </div>
                      </li>)}

                    </Draggable>
                  )
                })}
                {providedOuter.placeholder}
              </ul>
            )}
          </Droppable>



        </DragDropContext>

        {noExerciseMessage}
        {this.props.therapies.length ?
          <div className={classes.Article}>
            <Title>物理治疗</Title>
            <div className={classes.UnchosenPeriod}>
              <ul className={classes.Plan}>
                {this.props.therapies.map(move =>
                  <li key={move.moveId} className={[classes.RealMove, classes.ArticleMove].join(" ")}>
                    <span>{move.moveName}</span>
                    <div className={classes.Icon}
                         onClick={() => this.props.onDeleteMove(2, move.moveId)}>
                      <img alt="delete" src={deleteIcon}/>
                    </div>
                  </li>
                )}
              </ul>
            </div>
          </div> : null
        }

        {this.props.articles.length ?
          <div className={classes.Article}>
            <Title>文章</Title>
            <div className={classes.UnchosenPeriod}>
            <ul className={classes.Plan}>
              {this.props.articles.map(move =>
                <li key={move.moveId} className={[classes.RealMove, classes.ArticleMove].join(" ")}>
                  <span>{move.moveName}</span>
                  <div className={classes.Icon}
                       onClick={() => this.props.onDeleteMove(3, move.moveId)}>
                    <img alt="delete" src={deleteIcon}/>
                  </div>
                </li>
              )}
            </ul>
            </div>
          </div> : null
        }
        {this.props.admin === 5 || this.props.admin === 4 || this.props.template || this.props.fromTemplate ?
          null :
          <div className={classes.Block}>
            <div className={classes.SmallRow}>
              {this.props.fromEditPlan ? <label>患者 (不可更改)</label> : <label>选择患者</label>}
            </div>
            <select className={classes.Select}
                    onChange={event => this.props.onClientChange(event)}
                    value={this.props.clientId}
                    disabled={this.props.fromEditPlan}>
              <option key="empty" value=""/>
              {this.props.clients.map(client =>
                <option key={client.clientId} value={client.clientId}>{client.clientName}</option>)}
            </select>
          </div>

        }

        {this.props.admin === 5 || this.props.template || this.props.fromTemplate ? null : noClientMessage}


        {this.props.admin === 5 && !this.props.template ? <div className={classes.Block}>
          <div className={classes.SmallRow}>
            <label>选择作业</label>
          </div>
          <select className={classes.Select}
                  onChange={event => this.props.onTitleChange(event.target.value)}
                  value={this.props.titleId}>
            <option key="empty" value=""/>
            {this.props.titles.map(title =>
              <option key={title._id} value={title._id}>{title.title}</option>)}
          </select>
        </div> : null
        }

        {this.props.admin === 5 && !this.props.template && !this.props.fromTemplate ? noTitleMessage : null}

        <div className={classes.Block}>
          <div className={classes.SmallRow}>
            {this.props.template || this.props.fromTemplate ? <label>模板名</label> : <label>计划名</label>}
          </div>
          <Input border smallFont onChange={this.props.onNameChange} value={this.props.planName}/>
        </div>
        {noNameMessage}
        {this.props.template  || this.props.fromTemplate ?
          <div className={classes.Block}>
            <div className={classes.SmallRow}>
              <label>模板内容简介</label>
            </div>
            <textarea rows="10" className={classes.TextArea}
                      value={this.props.introduction}
                      onChange={event => this.props.onIntroductionChange(event)}/>
          </div> : null}
        {this.state.getDetailLoading ? <SingleLineSpinner/> : null}
        {this.state.error === 2 ? <Question>{this.state.errorMsg}</Question> : null}

        {this.props.admin === 5 && !this.props.template && !this.props.fromTemplate
        && !this.props.fromEditPlan ?
            <div className={classes.ButtonWrap}><Button btnType="Grey" largeFont fullWidth clicked={() =>
                this.props.onEditProposal(this.props.doctorId, this.props.titleId)}>设计思路</Button></div> :
            null}


        {this.props.admin === 4 || this.props.template || this.props.fromTemplate
          || this.props.fromEditPlan ? null :
          <div className={classes.ButtonWrap}><Button btnType="Grey" largeFont fullWidth clicked={() => this.proceed(true)}>保存到模板</Button></div>}

        {this.props.admin === 4 && !this.props.template ? null :
          <div className={classes.ButtonWrap}><Button btnType="Success" largeFont fullWidth clicked={() => this.proceed(false)}>继续</Button></div>}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    clients: state.clients.clients,
    clientName: state.exercise.clientName,
    clientId: state.exercise.clientId,
    clientNameB: state.exercise.clientNameB,
    clientEmail: state.exercise.clientEmail,
    planName: state.exercise.planName,
    plan: state.exercise.plan,
    totalInEach: state.exercise.totalInEach,
    currentPeriod: state.exercise.currentPeriod,
    period: state.exercise.period,
    start: state.exercise.start,
    startDate: state.exercise.startDate,
    periodDays: state.exercise.periodDays,
    currentGroupStart: state.exercise.currentGroupStart,
    currentGroupEnd: state.exercise.currentGroupEnd,
    articles: state.exercise.articles,
    therapies: state.exercise.therapies,
    savedTemplateId: state.exercise.savedTemplateId,
    savedTemplateName: state.exercise.savedTemplateName,
    introduction: state.exercise.introduction,
    noExerciseError: state.exercise.noExerciseError,
    noClientError: state.exercise.noClientError,
    noNameError: state.exercise.noNameError,
    noTitleError: state.exercise.noTitleError,
    fromEditPlan: state.exercise.fromEditPlan,
    savedPlan: state.detail.savedPlan,
    fromTemplate: state.exercise.fromTemplate,
    fromTemplateToPlan: state.exercise.fromTemplateToPlan,
    savedTemplate: state.detail.savedTemplate,
    introducedTemplate: state.exercise.introducedTemplate,
    empty: state.exercise.empty,
    titles: state.homework.titles,
    titleId: state.exercise.titleId,
    proposalContent: state.exercise.proposalContent,
    references: state.exercise.references
  }
}

const mapDispatchToPatch = dispatch => {
  return {
    onSearchChosenVideos: (plan, therapies, articles) => dispatch(actions.searchChosenVideos(plan, therapies, articles)),
    onPlayVideo: (videoUrl, videoName) => dispatch(actions.playVideo(videoUrl, videoName)),
    onNameChange: event => dispatch(actions.nameChange(event)),
    onClientChange: (event) => dispatch(actions.clientChange(event)),
    onIntroductionChange: event => dispatch(actions.introductionChange(event)),
    onDeleteMove: (index, moveId, period, periodIndex) => dispatch(actions.deleteMove(index, moveId, period, periodIndex)),
    onDeleteTemplateInPlan: () => dispatch(actions.deleteTemplateInPlan()),
    onDeletePeriod: index => dispatch(actions.deletePeriod(index)),
    onClientNameBChange: event => dispatch(actions.clientNameBChange(event)),
    onClientEmailChange: event => dispatch(actions.clientEmailChange(event)),
    onChangeOrder: (newPlan, index, source, dest) => dispatch(actions.changeOrder(newPlan, index, source, dest)),
    onDisplayError: (plan, template) => dispatch(actions.displayError(plan, template)),
    onShowDetail: info => dispatch(actions.showDetail(info)),
    onSetClientName: clientName => dispatch(actions.setClientName(clientName)),
    onAddPeriod: () => dispatch(actions.addPeriod()),
    onChangePeriod: index => dispatch(actions.changePeriod(index)),
    onDeleteAll: () => dispatch(actions.deleteAll()),
    onAddGroup: index1 => dispatch(actions.addGroup(index1)),
    onChangeGroup: (index1, index) => dispatch(actions.changeGroup(index1, index)),
    onDeleteGroup: (index1, index) => dispatch(actions.deleteGroup(index1, index)),
    onRenameGroup: (index1, index) => dispatch(actions.renameGroup(index1, index)),
    onRenameGroupDone: (index1, index) => dispatch(actions.renameGroupDone(index1, index)),
    onRenamingGroup: (index1, index, event) => dispatch(actions.renamingGroup(index1, index, event)),
    onLoseFocus: (index1, index) => dispatch(actions.loseFocus(index1, index)),
    onFetchClients: doctorId => dispatch(actions.fetchClients(doctorId)),
    onTitleChange: titleId => dispatch(actions.titleChangeStudent(titleId)),
    onChangePeriodOrder: (source, dest) => dispatch(actions.changePeriodOrder(source, dest)),
    onSavingTemplate: (info, redirect, web) => dispatch(actions.savingTemplate(info, redirect, web)),
    onEditProposal: (doctorId, titleId) => dispatch(actions.editProposal(doctorId, titleId)),
    onInit: () => dispatch(actions.previewInit())
  }
}

export default connect(mapStateToProps, mapDispatchToPatch)(withRouter(Preview))