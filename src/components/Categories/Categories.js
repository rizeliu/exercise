import React, { Component } from 'react'
import { connect } from 'react-redux'

import Category from './Category/Category'
import SearchInput from '../../components/UI/Input/Input'
import * as actions from '../../store/actions/index';
import classes from './Categories.css'

class Categories extends Component {

  componentDidMount = () => {
    this.props.onInit()
  }

  render() {
    let firstLi = null
    if (!this.props.root) {
      firstLi = <Category back key="back" name={this.props.inside} backClick={this.props.onBackClick}/>
    }

    let search = null
    if (this.props.inside === "b") {
      search = <SearchInput noBorder search smallMargin placeholder="病症内搜索" value={this.props.bcSearch}
                            onChange={event => this.props.onBcSearchChange(event.target.value)}/>
    }
    if (this.props.inside === "c") {
      search = <SearchInput noBorder search smallMargin placeholder="肌肉内搜索" value={this.props.bcSearch}
                            onChange={event => this.props.onBcSearchChange(event.target.value)}/>
    }


    return (
      <ul className={classes.Categories}>
        {firstLi}
        {search}
        {this.props.current.map((category, index) => {
          return (
            <Category
              root={this.props.root}
              inside={this.props.inside}
              hasSub={!!this.props.inside && this.props.overallInfo[this.props.inside][category].length > 0}
              key={category}
              name={category}
              open={this.props.open}
              list={[...this.props.cList]}
              enter={this.props.onCategoryClick}
              drop={this.props.onDropDown}
              search={this.props.onSearchMoves}
              click={this.props.click ? name => this.props.click(name) : null}
              index={index}
            />
          )
        })}
      </ul>
    )
  }
}

const mapStateToProps = state => {
  return {
    root: state.categories.root,
    inside: state.categories.inside,
    overallInfo: state.categories.overallInfo,
    order: state.categories.order,
    current: state.categories.current,
    open: state.categories.open,
    cList: state.categories.cList,
    bcSearch: state.categories.bcSearch,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCategoryClick: (name, index) => dispatch(actions.categoryClick(name, index)),
    onBackClick: () => dispatch(actions.backClick()),
    onDropDown: (inside, name) => dispatch(actions.dropDown(inside, name)),
    onSearchMoves: (doctorId, a, b, c) => dispatch(actions.searchMoves(doctorId, a, b, c)),
    onInit: () => dispatch(actions.categoriesInit()),
    onBcSearchChange: value => dispatch(actions.bcSearchChange(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories)