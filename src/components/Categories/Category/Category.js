import React from 'react'
import { connect } from 'react-redux'

import BackIcon from '../../../assets/images/ic_收起1.svg'
import DownIcon from '../../../assets/images/ic_展开.svg'
import RightIcon from '../../../assets/images/ic_展开1.svg'
import classes from './Category.css'

const category = props => {

  const categoryClasses = [classes.Category]
  let icon = null
  let onClick
  if (props.root) {
    categoryClasses.push(classes.Root)
    icon = <div className={classes.Icon}><img alt="RightIcon" src={RightIcon}/></div>
    onClick = () => props.enter(props.name, props.index)
  }
  if (props.back) {
    categoryClasses.push(classes.Back)
    icon = <div className={classes.Icon}><img alt="BackIcon" src={BackIcon}/></div>
    onClick = () => props.backClick()
  }
  if (props.hasSub && props.open !== props.name) {
    categoryClasses.push(classes.Close)
    icon = <div className={classes.Icon}><img alt="RightIcon" src={RightIcon}/></div>
    onClick = () => props.drop(props.inside, props.name)
  }
  if (props.hasSub && props.open === props.name) {
    categoryClasses.push(classes.Open)
    icon = <div className={classes.Icon}><img alt="DownIcon" src={DownIcon}/></div>
    onClick = () => props.drop(props.inside, props.name)
  }
  if (props.chosen === props.name) {
    categoryClasses.push(classes.Chosen)
  }
  if (!onClick) {
    categoryClasses.push(classes.Second)
    onClick = props.click ? () => props.click(props.name) : () => props.search(props.doctorId, props.inside, props.name, 0)
  }
  let ul = null
  if (!props.back && props.list.length > 0 ) {
    ul = <ul className={classes.List}>
      {props.list.map(el => {
        const smallCatClasses = [classes.ListItem]
        if (props.chosen === el) smallCatClasses.push(classes.Chosen)
        return (<li onClick={() => props.click ? props.click(el) : props.search(props.doctorId, props.inside, props.name, el)}
                    className={smallCatClasses.join(" ")}
                    key={el}>{props.map[el] ? props.map[el] : el}</li>)
      })}
    </ul>
  }
  return (
    <li className={classes.Cell}>
      <div onClick={onClick} className={categoryClasses.join(' ')}>
        {icon}
        {props.map[props.name] ? props.map[props.name] : props.name}
      </div>
      {props.open === props.name ? ul : null}
    </li>
  )

}


const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    map: state.categories.map,
    chosen: state.categories.chosen
  };
};

export default connect(mapStateToProps)(category)
