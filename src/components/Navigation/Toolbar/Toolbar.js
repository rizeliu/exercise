import React, { Component } from 'react'
import { connect } from 'react-redux';

import classes from './Toolbar.css'
import Logo from '../../Logo/Logo'
import DocLogo from '../../Logo/DocLogo'
import FirstNavigationItems from '../NavigationItems/FirstNavigationItems'
import * as actions from "../../../store/actions";
import {withRouter} from "react-router-dom";

class Toolbar extends Component {

  state = {
    expand: false
  }

  expand = () => {
    const expand = this.state.expand
    this.setState({expand: !expand})
  }

  click = link => {
    this.props.history.push(link)
    this.setState({expand: false})
  }


  render() {
    return (
      <header className={classes.Toolbar}>
        <div className={classes.First}>
          {this.props.admin === 1 || this.props.admin === 2 || this.props.admin === 0 || this.props.match.path === "/physiotherapist" ? <DocLogo/> : <Logo/>}
          <div>
            <div className={classes.Plus} onClick={this.expand}>{this.state.expand ? "x" : "+"}</div>
            <span className={[classes.Tooltip, this.state.expand ? classes.TooltipSeen : ""].join(" ")}>
            {!this.props.login ? <ul>
              <li onClick={() => {this.click("/")}}>主页</li>
              <li onClick={() => {this.click('/login')}}>登录</li>
                {this.props.match.path === "/physiotherapist" ? <li onClick={() => {this.click('/physiotherapist/register')}}>注册</li> : null}
              <li onClick={() => {this.click('/contact')}}>联系我们</li>
            </ul> : this.props.admin === 1 ? <ul>
              <li onClick={() => {this.click('/')}}>主页</li>
              <li onClick={() => {this.click('/account')}}>账号</li>
              <li onClick={() => {this.click('/admin')}}>管理员</li>
              <li onClick={() => {this.click('/exercises')}}>制定计划</li>
              <li onClick={() => {this.click('/clients')}}>患者</li>
              <li onClick={() => {this.click('/templates')}}>模板</li>
              <li onClick={() => {this.click('/cases')}}>方案</li>
              <li onClick={
                () => {
                  this.props.onLogout()
                  this.props.history.push("/")
                  this.setState({expand: false})
                }
              }>退出</li>
            </ul> : this.props.admin === 2 ? <ul>
              <li onClick={() => {this.click('/')}}>主页</li>
              <li onClick={() => {this.click('/account')}}>账号</li>
              <li onClick={() => {this.click('/admin')}}>统计</li>
              <li onClick={() => {this.click('/exercises')}}>制定计划</li>
              <li onClick={() => {this.click('/clients')}}>患者</li>
              <li onClick={() => {this.click('/templates')}}>模板</li>
              <li onClick={() => {this.click('/cases')}}>方案</li>
              <li onClick={
                () => {
                  this.props.onLogout()
                  this.props.history.push("/")
                  this.setState({expand: false})
                }
              }>退出</li>
            </ul> : this.props.admin === 4 ? <ul>
              <li onClick={() => {this.click('/')}}>主页</li>
              <li onClick={() => {this.click('/account')}}>账号</li>
              <li><a className={classes.White} href={"https://appz4t5ma7u3727.pc.xiaoe-tech.com"} target="_blank">直播课程</a></li>
              <li onClick={() => {this.click('/exercises')}}>制定计划</li>
              <li onClick={() => {this.click('/students')}}>学生</li>
              <li onClick={() => {this.click('/templates')}}>模板</li>
              <li onClick={() => {this.click('/cases')}}>方案</li>
              <li onClick={() => {this.click('/resources')}}>资源</li>
              <li onClick={
                () => {
                  this.props.onLogout()
                  this.props.history.push("/")
                  this.setState({expand: false})
                }
              }>退出</li>
            </ul> : this.props.admin === 5 ? <ul>
              <li onClick={() => {this.click('/')}}>主页</li>
              <li onClick={() => {this.click('/account')}}>账号</li>
              <li><a className={classes.White} href={"https://appz4t5ma7u3727.pc.xiaoe-tech.com"} target="_blank">直播课程</a></li>
              <li onClick={() => {this.click('/exercises')}}>制定计划</li>
              <li onClick={() => {this.click('/homework')}}>作业</li>
              <li onClick={() => {this.click('/templates')}}>模板</li>
              <li onClick={() => {this.click('/cases')}}>方案</li>
              <li onClick={() => {this.click('/resources')}}>资源</li>
              <li onClick={
                () => {
                  this.props.onLogout()
                  this.props.history.push("/")
                  this.setState({expand: false})
                }
              }>退出</li>
            </ul> : <ul>
              <li onClick={() => {this.click('/')}}>主页</li>
              <li onClick={() => {this.click('/account')}}>账号</li>
              <li onClick={() => {this.click('/exercises')}}>制定计划</li>
              <li onClick={() => {this.click('/clients')}}>患者</li>
              <li onClick={() => {this.click('/templates')}}>模板</li>
              <li onClick={() => {this.click('/cases')}}>方案</li>
              <li onClick={
                () => {
                  this.props.onLogout()
                  this.props.history.push("/")
                  this.setState({expand: false})
                }
              }>退出</li>
            </ul>}
          </span>
          </div>
          <div className={classes.LoAndRe}>
          <nav>
            <FirstNavigationItems edu={this.props.edu}/>
          </nav>
          </div>
        </div>
        {/*<div className={classes.Second}>*/}
          {/*<nav>*/}
            {/*<SecondNavigationItems/>*/}
          {/*</nav>*/}
          {/*<div className={classes.Plus} onClick={this.expand}>{this.state.expand ? "x" : "+"}</div>*/}
          {/*<span className={[classes.Tooltip, this.state.expand ? classes.TooltipSeen : ""].join(" ")}>*/}
            {/*{!this.props.login ? <ul>*/}
              {/*<li onClick={() => {this.click("/")}}>主页</li>*/}
              {/*<li onClick={() => {this.click('/login')}}>登录</li>*/}
              {/*<li onClick={() => {this.click('/register')}}>注册</li>*/}
              {/*<li onClick={() => {this.click('/contact')}}>联系我们</li>*/}
            {/*</ul> : this.props.admin === 1 ? <ul>*/}
              {/*<li onClick={() => {this.click('/')}}>主页</li>*/}
              {/*<li onClick={() => {this.click('/account')}}>账号</li>*/}
              {/*<li onClick={() => {this.click('/admin')}}>管理员</li>*/}
              {/*<li onClick={() => {this.click('/exercises')}}>制定计划</li>*/}
              {/*<li onClick={() => {this.click('/clients')}}>患者</li>*/}
              {/*<li onClick={() => {this.click('/templates')}}>模板</li>*/}
              {/*<li onClick={() => {this.click('/cases')}}>方案</li>*/}
              {/*<li onClick={*/}
                {/*() => {*/}
                  {/*this.props.onLogout()*/}
                  {/*this.props.history.push("/")*/}
                  {/*this.setState({expand: false})*/}
                {/*}*/}
              {/*}>退出</li>*/}
            {/*</ul> : this.props.admin === 2 ? <ul>*/}
              {/*<li onClick={() => {this.click('/')}}>主页</li>*/}
              {/*<li onClick={() => {this.click('/account')}}>账号</li>*/}
              {/*<li onClick={() => {this.click('/admin')}}>统计</li>*/}
              {/*<li onClick={() => {this.click('/exercises')}}>制定计划</li>*/}
              {/*<li onClick={() => {this.click('/clients')}}>患者</li>*/}
              {/*<li onClick={() => {this.click('/templates')}}>模板</li>*/}
              {/*<li onClick={() => {this.click('/cases')}}>方案</li>*/}
              {/*<li onClick={*/}
                {/*() => {*/}
                  {/*this.props.onLogout()*/}
                  {/*this.props.history.push("/")*/}
                  {/*this.setState({expand: false})*/}
                {/*}*/}
              {/*}>退出</li>*/}
            {/*</ul> : this.props.admin === 3 ? <ul>*/}
              {/*<li onClick={() => {this.click('/')}}>Home</li>*/}
              {/*<li onClick={() => {this.click('/exercises')}}>Create Exercise Plan</li>*/}
              {/*<li onClick={() => {this.click('/clients')}}>Patients</li>*/}
              {/*<li onClick={() => {this.click('/templates')}}>Templates</li>*/}
              {/*<li onClick={() => {this.click('/cases')}}>Cases</li>*/}
              {/*<li onClick={*/}
                {/*() => {*/}
                  {/*this.props.onLogout()*/}
                  {/*this.props.history.push("/")*/}
                  {/*this.setState({expand: false})*/}
                {/*}*/}
              {/*}>Logout</li>*/}
            {/*</ul> : this.props.admin === 4 ? <ul>*/}
              {/*<li onClick={() => {this.click('/')}}>主页</li>*/}
              {/*<li onClick={() => {this.click('/account')}}>账号</li>*/}
              {/*<li onClick={() => {this.click('/exercises')}}>制定计划</li>*/}
              {/*<li onClick={() => {this.click('/students')}}>学生</li>*/}
              {/*<li onClick={() => {this.click('/templates')}}>模板</li>*/}
              {/*<li onClick={() => {this.click('/cases')}}>方案</li>*/}
              {/*<li onClick={*/}
                {/*() => {*/}
                  {/*this.props.onLogout()*/}
                  {/*this.props.history.push("/")*/}
                  {/*this.setState({expand: false})*/}
                {/*}*/}
              {/*}>退出</li>*/}
            {/*</ul> : this.props.admin === 5 ? <ul>*/}
              {/*<li onClick={() => {this.click('/')}}>主页</li>*/}
              {/*<li onClick={() => {this.click('/account')}}>账号</li>*/}
              {/*<li onClick={() => {this.click('/exercises')}}>制定计划</li>*/}
              {/*<li onClick={() => {this.click('/homework')}}>作业</li>*/}
              {/*<li onClick={() => {this.click('/templates')}}>模板</li>*/}
              {/*<li onClick={() => {this.click('/cases')}}>方案</li>*/}
              {/*<li onClick={*/}
                {/*() => {*/}
                  {/*this.props.onLogout()*/}
                  {/*this.props.history.push("/")*/}
                  {/*this.setState({expand: false})*/}
                {/*}*/}
              {/*}>退出</li>*/}
            {/*</ul> : <ul>*/}
              {/*<li onClick={() => {this.click('/')}}>主页</li>*/}
              {/*<li onClick={() => {this.click('/account')}}>账号</li>*/}
              {/*<li onClick={() => {this.click('/exercises')}}>制定计划</li>*/}
              {/*<li onClick={() => {this.click('/clients')}}>患者</li>*/}
              {/*<li onClick={() => {this.click('/templates')}}>模板</li>*/}
              {/*<li onClick={() => {this.click('/cases')}}>方案</li>*/}
              {/*<li onClick={*/}
                {/*() => {*/}
                  {/*this.props.onLogout()*/}
                  {/*this.props.history.push("/")*/}
                  {/*this.setState({expand: false})*/}
                {/*}*/}
              {/*}>退出</li>*/}
            {/*</ul>}*/}
          {/*</span>*/}
        {/*</div>*/}

      </header>
    )
  }
}

const mapStateToProps = state => {
  return {
    login: state.authRoute.login,
    admin: state.authRoute.admin
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(actions.logout())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Toolbar))