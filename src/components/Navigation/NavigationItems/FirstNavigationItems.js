import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Link, withRouter} from 'react-router-dom'

import NavigationItem from './NavigationItem/NavigationItem'
import classes from './NavigationItems.css'
import * as actions from '../../../store/actions/index';

class FirstNavigationItems extends Component {

  // state = {
  //   edu: true
  // }

  // componentDidMount = async () => {
  //   if (this.props.match.params.resume === "physiotherapist") {
  //     this.setState({edu: false})
  //   } else {
  //     this.props.history.push('/')
  //   }
  // }

  render() {
    return (
      this.props.login ? <ul className={classes.NavigationItems}>
          <div className={classes.Back}><Link to="/account">欢迎回来，{this.props.name}</Link></div>
        </ul>
        : (<ul className={classes.NavigationItems}>
          <NavigationItem link={this.props.edu ? "/login" : "/physiotherapist/login"} first exact white>登录</NavigationItem>
          {this.props.edu ? null : <NavigationItem link="/physiotherapist/register" first>注册</NavigationItem>}
        </ul>)
    )
  }
}



const mapStateToProps = state => {
  return {
    name: state.authRoute.name,
    login: state.authRoute.login,
    admin: state.authRoute.admin
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(actions.logout())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FirstNavigationItems))