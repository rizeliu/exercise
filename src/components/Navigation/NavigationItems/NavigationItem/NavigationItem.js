import React from 'react'
import { NavLink } from 'react-router-dom'

import Support from '../../../support/support'
import classes from './NavigationItem.css'
import Qr from "../../../UI/Qr/Qr";

const navigationItem = props => (

  props.nonClick ?
    <li className={[classes.NavigationItemFirst, classes.Support].join(" ")}>
      <a><Qr/>{props.children}</a>
      <span className={classes.QR}>
        <Support noMax/>
      </span>
    </li> : props.external ? <li className={props.login && props.admin <= 2 ? classes.DocNavigationItem : classes.NavigationItem}>
      <a href={props.link} target="_blank">
        <div className={classes.One}>
          {props.children}
        </div>
      </a>
    </li> : props.left ?
    <li className={props.login && props.admin <= 2 ? classes.DocNavigationItem : classes.NavigationItem}>
      <NavLink to={props.link} exact={props.exact} activeClassName={classes.active}>
        <div className={classes.One}>
          {props.children}
        </div>
      </NavLink>
    </li> :
    <li className={[classes.NavigationItemFirst, props.white ? classes.White : ""].join((" "))}>
      <NavLink to={props.link} exact={props.exact} activeClassName={classes.active}>
        {props.children}
      </NavLink>
    </li>
)

export default navigationItem