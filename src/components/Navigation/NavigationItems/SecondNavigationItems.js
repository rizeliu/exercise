import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import NavigationItem from './NavigationItem/NavigationItem'
import classes from './NavigationItems.css'

const secondNavigationItems = props => (
  props.login ?
    (<ul className={classes.NavigationItems}>
      <NavigationItem link="/exercises">{props.admin === 3 ? "Create Exercise Plan" : "制定运动计划"}</NavigationItem>
      {props.admin === 4 ? <NavigationItem link="/students">学生</NavigationItem> :
        props.admin === 5 ? <NavigationItem link="/homework">作业</NavigationItem> :
        <NavigationItem link="/clients" >{props.admin === 3 ? "Patients" : "患者"}</NavigationItem>}
      <NavigationItem link="/templates">{props.admin === 3 ? "Templates" : "模板"}</NavigationItem>
      <NavigationItem link="/cases">{props.admin === 3 ? "Cases" : "方案"}</NavigationItem>
    </ul>) :
    (<ul className={classes.NavigationItems}>
      <NavigationItem link="/" exact>主页</NavigationItem>
      <NavigationItem link="/contact">联系我们</NavigationItem>
    </ul>)
)

const mapStateToProps = state => {
  return {
    login: state.authRoute.login,
    admin: state.authRoute.admin,
  }
}

export default withRouter(connect(mapStateToProps)(secondNavigationItems))