import React, {Component} from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { NavLink } from 'react-router-dom'


import classes from './Content.css'
import Aux from "../../hoc/Aux/Aux";
import contact_re from '../../assets/images/ic_联系我们_pr.svg'
import contact from '../../assets/images/ic_联系我们.svg'
import home from '../../assets/images/ic_首页.svg'
import home_re from '../../assets/images/ic_首页_pr.svg'
import exercise from '../../assets/images/ic_康复计划.svg'
import exercise_re from '../../assets/images/ic_康复计划_pr.svg'
import student from '../../assets/images/ic_学生.svg'
import student_re from '../../assets/images/ic_学生_pr.svg'
import template from '../../assets/images/ic_模板.svg'
import template_re from '../../assets/images/ic_模板_pr.svg'
import case_ic from '../../assets/images/ic_方案.svg'
import case_re from '../../assets/images/ic_方案_pr.svg'
import logout from '../../assets/images/ic_退出.svg'
import expand from '../../assets/images/ic_目录展开.svg'
import expand_re from '../../assets/images/ic_目录收起.svg'
import homework_re from '../../assets/images/ic_作业_pr.svg'
import homework from '../../assets/images/ic_作业.svg'
import live from '../../assets/images/ic_直播.svg'
import NavigationItem from "../Navigation/NavigationItems/NavigationItem/NavigationItem";
import * as actions from "../../store/actions";

class Content extends Component {

  state = {
    edu: true
  }

  componentDidMount = async () => {
    document.title = "VOIN 主页"
    // if (this.props.match.params.resume === "resume") {
    //   await this.login()
    //   this.props.history.push('/exercises')
    // }
    // if (this.props.match.params.resume === "physiotherapist") {
    //   this.setState({edu: false})
    //   console.log(1)
    // } else {
    //   this.props.history.push('/')
    //   console.log(2)
    //
    // }

    if (this.props.match.url.split("/")[1] === 'physiotherapist') {
      this.setState({edu: false})
    } else {
    }


  }

  expand = () => {
    const expand = this.state.expand
    this.setState({expand: !expand})
  }

  click = link => {
    this.props.history.push(link)
    this.setState({expand: false})
  }

  render() {

    return (
      <Aux>
        <div className={this.props.login && this.props.admin <= 2 ? classes.DocContent : classes.Content}>
          <div className={[this.props.login && this.props.admin <= 2 ? classes.DocNavigation : classes.Navigation, this.props.expand ? "" : classes.Thin].join(" ")}>
            {this.props.login && this.props.admin >= 4 ? <div className={classes.Expand} onClick={this.props.onChangeExpand}>
              <img alt="expand" src={this.props.expand ? expand : expand_re}/>
            </div> : null}
            <ul className={this.props.login && this.props.admin <= 2 ? classes.DocNavigationItems : classes.NavigationItems}>
              <div className={this.props.login && this.props.admin <= 2 ? classes.DocNavigationItemsLeft : ""}>
              <NavigationItem admin={this.props.admin} login={this.props.login} link={this.state.edu ? "/" : "/physiotherapist"} exact left>
                <img alt="phy" src={this.props.location.pathname === "/" || this.props.location.pathname === "/physiotherapist" ? home_re : home}/>{this.props.expand ? <div className={classes.Space}>首页</div> : null}
              </NavigationItem>
              {!this.props.login ?
                <NavigationItem admin={this.props.admin} login={this.props.login} link={this.state.edu ? "/contact" : "/physiotherapist/contact"} exact left>
                  <img alt="contact" src={this.props.location.pathname === "/contact" || this.props.location.pathname === "/physiotherapist/contact" ?
                    contact_re : contact}/>{this.props.expand ? <div className={classes.Space}>联系我们</div> : null}
                </NavigationItem> :
                <Aux>
                  {this.props.admin >= 4 ?
                    <NavigationItem admin={this.props.admin} login={this.props.login} link="https://appz4t5ma7u3727.pc.xiaoe-tech.com" left external>
                      <img alt="live" src={live}/>{this.props.expand ?
                      <div className={classes.Space}>直播课程</div> : null}
                    </NavigationItem> : null}
                  <NavigationItem admin={this.props.admin} login={this.props.login} link={this.props.admin === 4 ? "/exercises/template" : "/exercises"} left>
                    <img alt="exercise" src={this.props.location.pathname.indexOf( "/exercises") === 0 ? exercise_re : exercise}/>{this.props.expand ?
                    <div className={classes.Space}>{this.props.admin === 4 ? "设计模板" : "设计康复方案"}</div> : null}
                  </NavigationItem>
                  {this.props.admin === 4 ?
                    <NavigationItem admin={this.props.admin} login={this.props.login} link="/students" exact left>
                      <img alt="student" src={this.props.location.pathname === "/students" ? student_re : student}/>{this.props.expand ? <div className={classes.Space}>学生</div> : null}
                    </NavigationItem> : this.props.admin === 5 ?
                    <NavigationItem admin={this.props.admin} login={this.props.login} link="/homework" exact left>
                      <img alt="homework" src={this.props.location.pathname === "/homework" ? homework_re : homework}/>{this.props.expand ? <div className={classes.Space}>作业</div> : null}
                    </NavigationItem> : <NavigationItem admin={this.props.admin} login={this.props.login} link="/clients" exact left>
                        <img alt="client" src={this.props.location.pathname === "/clients" ? student_re : student}/>{this.props.expand ? <div className={classes.Space}>患者</div> : null}
                      </NavigationItem>}
                  <NavigationItem admin={this.props.admin} login={this.props.login} link="/templates" exact left>
                    <img alt="template" src={this.props.location.pathname === "/templates" ? template_re : template}/>{this.props.expand ? <div className={classes.Space}>模板</div> : null}
                  </NavigationItem>
                  <NavigationItem admin={this.props.admin} login={this.props.login} link="/cases" exact left>
                    <img alt="case" src={this.props.location.pathname === "/cases" ? case_re : case_ic}/>{this.props.expand ? <div className={classes.Space}>方案</div> : null}
                  </NavigationItem>
                  {this.props.admin === 4 || this.props.admin === 5 ? <NavigationItem admin={this.props.admin} login={this.props.login} link="/resources" exact left>
                    <img alt="case" src={this.props.location.pathname === "/resources" ? case_re : case_ic}/>{this.props.expand ? <div className={classes.Space}>资源</div> : null}
                  </NavigationItem> : null}
                  {this.props.admin === 1 ? <NavigationItem admin={this.props.admin} login={this.props.login} link="/admin" exact left>
                    <img alt="admin" src={this.props.location.pathname === "/admin" ? case_re : case_ic}/>{this.props.expand ? <div className={classes.Space}>管理员</div> : null}
                  </NavigationItem> : null}
                </Aux>}
              </div>
                {!this.props.login ? null :
                  <NavigationItem admin={this.props.admin} login={this.props.login} link="/logout" left><img alt="logout" src={logout}/>
                    {this.props.expand ? <div className={classes.Space}>退出</div> : null}
                  </NavigationItem>}
            </ul>
          </div>

          <div className={[this.props.login && this.props.admin <= 2 ? classes.DocRight : classes.Right, this.props.expand ? "" : classes.Wide].join(" ")}>
            <div>{this.props.children}</div>
            <footer className={classes.Footer}>
              <div>
                <NavLink to={this.state.edu ? "/contact" : "/physiotherapist/contact"}>联系我们</NavLink>
                <span> | </span>
                <NavLink to={this.state.edu ? "/cases" : "/physiotherapist/cases"}>方案</NavLink>
                <span> | </span>
                <NavLink to={this.state.edu ? "/" : "/physiotherapist"}>更新</NavLink>
                <p className={classes.Company}>北京沃衍体育科技有限公司</p>
                <p className={classes.Bei}>【京公海网安备110108001503号】<a href={"https://beian.miit.gov.cn/#/Integrated/index"} target={"_blank"}>【京ICP备17015747号-1】</a></p>
              </div>
            </footer>
          </div>

        </div>

      </Aux>

    )
  }


}

const mapStateToProps = state => {
  return {
    admin: state.authRoute.admin,
    login: state.authRoute.login,
    expand: state.authRoute.expand
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeExpand: () => dispatch(actions.changeExpand()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Content))