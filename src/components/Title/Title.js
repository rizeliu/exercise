import React from 'react'

import classes from './Title.css'

const title = props => {
  let titleClasses = [classes.Title]
  if (props.noPaddingTop) titleClasses.push(classes.NoPaddingTop)
  if (props.alignCenter) titleClasses.push(classes.Center)
  if (props.light) titleClasses.push(classes.Light)
  return (<h3 className={titleClasses.join(" ")}>{props.children}</h3>)
}

export default title