import React from 'react'

import LineChart from 'react-linechart'
import Title from '../../components/Title/Title'
import '../../../node_modules/react-linechart/dist/styles.css'
import classes from './Progress.css'

const progress = props => {
  return (
    <div className={classes.Progress}>
      <Title alignCenter>患者完成情况预览</Title>
      <LineChart
        width={500}
        height={280}
        yMin={0}
        yMax={1}
        yLabel={"完成度"}
        xLabel={"天数"}
        data={[
          {
            color: "steelblue",
            points: [{x: 1, y: 1}, {x: 2, y: 0.8}, {x: 3, y: 0.9}, {x: 4, y: 0.8}, {x: 5, y: 1}, {x: 6, y: 0.9}],
          }
        ]}
      />
    </div>
  )
}

export default progress