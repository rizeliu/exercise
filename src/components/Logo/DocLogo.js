import React from 'react'

import companyLogo from '../../assets/images/logo_doc.svg'
import classes from './Logo.css'

const logo = props => (
  <div className={classes.Logo}>
    <img src={companyLogo} alt="MyBurger" />
  </div>
)

export default logo