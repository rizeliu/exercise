import React from 'react'
import { connect } from 'react-redux'

import Aux from '../Aux/Aux'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import Spinner from '../../components/UI/Spinner/Spinner'
import classes from './Layout.css'

const layout = props => {
  return props.doneAuthentication ? (
    <Aux>
      <Toolbar edu={props.edu}/>
      <main className={classes.Content}>
        {props.children}
      </main>
    </Aux>
  ) : <Spinner overall/>
}

const mapStateToProps = state => {
  return {
    doneAuthentication: state.authRoute.done
  }
}

export default connect(mapStateToProps)(layout)