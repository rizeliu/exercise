import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import Exercises from './containers/Exercises/Exercises'
import Clients from './containers/Clients/Clients'
import AddClient from './containers/AddClient/AddClient'
import ClientDetail from './containers/ClientDetail/ClientDetail'
import Login from './containers/Login/Login'
import Register from './containers/Register/Register'
import Logout from './components/AuthRoute/Logout'
import Admin from './containers/Admin/Admin'
import Templates from './containers/Templates/Templates'
import Students from './containers/Students/Students'
import Homework from './containers/Homework/Homework'
import Contact from './containers/Contact/Contact'
import AuthRoute from './components/AuthRoute/AuthRoute'
import Account from './containers/Account/Account'
import Courses from './containers/Courses/Courses'
import Home from './containers/Home/Home'
import Aux from './hoc/Aux/Aux'
import Layout from './hoc/Layout/Layout'
import ArticlePDF from "./containers/ArticlePDF/ArticlePDF";
import Cases from "./containers/Cases/Cases";
import Resources from "./containers/Resources/Resources";

class App extends Component {

  render() {
    return (
      <Aux>
        <AuthRoute/>
        <Switch>
          <Route path="/physiotherapist/exercises/:template?" render={props => <Layout><Exercises {...props}/></Layout>}/>
          <Route path="/exercises/:template?" render={props => <Layout><Exercises {...props}/></Layout>} edu/>
          <Route path="/clients/add" render={props => <Layout><AddClient {...props}/></Layout>}/>
          <Route path="/clients/" render={props => <Layout><Clients {...props}/></Layout>}/>
          <Route path="/client/:cid/:did?" render={props => <Layout><ClientDetail {...props}/></Layout>}/>
          {/*<Route path="/physiotherapist/register" render={props => <Layout><Register {...props}/></Layout>}/>*/}
          <Route path="/physiotherapist/login" render={props => <Layout><Login {...props}/></Layout>}/>
          <Route path="/login" render={props => <Layout edu><Login {...props} edu/></Layout>}/>
          <Route path="/logout" render={props => <Layout><Logout {...props}/></Layout>}/>
          <Route path="/admin" render={props => <Layout><Admin {...props}/></Layout>}/>
          <Route path="/templates" render={props => <Layout><Templates {...props}/></Layout>}/>
          <Route path="/students" render={props => <Layout><Students {...props}/></Layout>}/>
          <Route path="/homework" render={props => <Layout><Homework {...props}/></Layout>}/>
          <Route path="/contact" render={props => <Layout edu><Contact {...props}/></Layout>}/>
          <Route path="/physiotherapist/contact" render={props => <Layout><Contact {...props}/></Layout>}/>
          <Route path="/account" render={props => <Layout><Account {...props}/></Layout>}/>
          <Route path="/physiotherapist/cases" render={props => <Layout><Cases {...props}/></Layout>}/>
          <Route path="/physiotherapist" render={props => <Layout><Home {...props}/></Layout>}/>
          <Route path="/account" render={props => <Layout><Account {...props} edu/></Layout>}/>
          <Route path="/courses" render={props => <Layout><Courses {...props}/></Layout>}/>
          <Route path="/cases" render={props => <Layout edu><Cases {...props}/></Layout>}/>
          <Route path="/resources" render={props => <Layout edu><Resources {...props}/></Layout>}/>
          {/*<Route path="/use/:action" render={props => <Layout><Action {...props}/></Layout>}/>*/}
          {/*<Route path="/use" render={props => <Layout><Use {...props}/></Layout>}/>*/}
          {/*<Route path="/articles" render={props => <Layout><Courses {...props}/></Layout>}/>*/}
          <Route path="/seeArticle" component={ArticlePDF}/>
          <Route path="/:resume?" render={props => <Layout edu><Home {...props}/></Layout>}/>
        </Switch>
      </Aux>
    );
  }
}



export default App
