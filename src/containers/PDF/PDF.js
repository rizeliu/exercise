import React, { Component } from 'react'
import axios from 'axios'

import Modal from '../../components/UI/Modal/Modal'
import Question from '../../components/UI/Question/Question'
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";


class NewQrCode extends Component {

  state = {
    message: ""
  }

  componentDidMount = async () => {
    document.title = "VOIN 获取计划"
    const no = this.props.match.params.no
    const did = this.props.match.params.did
    const cid = this.props.match.params.cid
    const response = await axios.get("https://backend.voin-cn.com/getpdf?no="+no+"&did="+did+"&cid="+cid)
    console.log(response)
    this.setState({message: response.data.msg})
  }

  render() {
    return (
      <Modal show>
        {this.state.message ? <Question>{this.state.message}</Question> : <SingleLineSpinner/>}
      </Modal>
    )
  }
}



export default NewQrCode