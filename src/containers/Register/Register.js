import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import isAlphanumeric from 'is-alphanumeric'
import axios from 'axios'
import { connect } from 'react-redux'

import Content from '../../components/Content/Content'
import Input from '../../components/UI/Input/Input'
import Button from '../../components/UI/Button/Button'
import Modal from '../../components/UI/Modal/Modal'
import Question from '../../components/UI/Question/Question'
import SingleLineSpinner from '../../components/UI/SingleLineSpinner/SingleLineSpinner'
import classes from './Register.css'
import * as actions from '../../store/actions/index';

class Register extends Component {

  state = {
    username: "",
    password: "",
    passwordAgain: "",
    name: "",
    phone: "",
    // email: "",
    id: "",
    account: "",
    alipayAccount: "",
    bankName: "",
    bankAccount: "",
    room: "",
    hospital: "",
    refer: "",
    certification: "",
    errors: [],
    mode: 0,
    error: "",
    modalTurnedOn: 0,
    occupation: "0"
  }

  componentDidMount = () => {
    document.title = "VOIN 注册"
  }

  inputChangeHandler = (event, id) => {
    const accountSwitch = id === "account"
    const value = event.target.value
    if (accountSwitch) {
      if (event.target.value === "alipay") this.setState({[id]: value, bankName: "", bankAccount: ""})
      else if (event.target.value === "bank") this.setState({[id]: value, alipayAccount: ""})
    } else this.setState({[id]: value})
  }

  checkValidate = () => {
    let error = ""
    if (!isAlphanumeric(this.state.username)) {
      error += "用户名只能包含数字和字母 "
    } else if (this.state.username.length < 5) {
      error += "用户名不得少于5位 "
    }
    if (this.state.password !== this.state.passwordAgain) {
      error += "两次密码不一致 "
    } else if (this.state.password.length < 6) {
      error += "密码长度不得少于6位 "
    }
    if (!this.state.name) {
      error += "姓名不能为空 "
    }
    if (this.state.phone.length !== 11) {
      error += "手机号应为11位 "
    }
    // if (!this.state.email) {
    //   error += "邮箱不能为空 "
    // }
    if (!this.state.account) {
      error += "请选择收款方式 "
    } else if (this.state.account === "alipay" && this.state.alipayAccount === "") {
      error += "支付宝账号不能为空 "
    } else if (this.state.account === "bank" && (this.state.bankName === "" || this.state.bankAccount === "")) {
      error += "银行名称与银行账号均不能为空 "
    }
    if (!this.state.hospital) {
      error += "所在医院不能为空 "
    }
    if (!this.state.room) {
      error += "所在科室不能为空 "
    }
    if (this.state.occupation === "0") {
      error += "职称不能为空 "
    }

    // if ((this.state.occupation === "1" || this.state.occupation === "2" || this.state.occupation === "2")
    //   && this.state.certification.length !== 21) {
    //   error += "康复医学治疗技术证管理号应为21位 "
    // }
    // if ((this.state.occupation === "3" || this.state.occupation === "4" || this.state.occupation === "5" || this.state.occupation === "6")
    //   && this.state.certification.length !== 15) {
    //   error += "执业医师资格证号应为15位 "
    // }
    return error
  }

  onTurnOffModal = () => {
    this.setState({modalTurnedOn: 0})
  }

  register = async () => {
    const error = this.checkValidate()
    const errors = error.split(" ")
    errors.pop()
    this.setState({ errors, error: "" })
    if (errors.length === 0 ) {
      this.setState({ mode: 1 })
      try {
        const response = await axios.post('https://backend.voin-cn.com/register', {
          username: this.state.username,
          password: this.state.password,
          name: this.state.name,
          id: this.state.id,
          account: this.state.account,
          bankName: this.state.bankName,
          bankAccount: this.state.bankAccount,
          alipayAccount: this.state.alipayAccount,
          room: this.state.room,
          hospital: this.state.hospital,
          phone: this.state.phone,
          // email: this.state.email,
          refer: this.state.refer,
          certification: this.state.certification,
          occupation: this.state.occupation
        })

        this.setState({mode: 0, error: response.data.msg})

        if (response.data.code === 1) {
          const now = new Date();
          const time = now.getTime();
          const expireTime = time + 30 * 24 * 60 * 60 * 1000;
          now.setTime(expireTime);
          document.cookie = "userId="+response.data.doc._id+";expires="+now.toGMTString();
          document.cookie = "token="+response.data.token+";expires="+now.toGMTString();
          this.props.onAuthenticate({...response.data, code: 0}, true, 0, "")
          setTimeout(() => {this.props.history.push('/exercises')}, 500)
        }
      } catch (error) {
        console.log(error)
        this.setState({ mode: 0, error: "内部服务器错误，请刷新"})
      }
    }
  }

  render() {

    let margin = "30px"
    if (this.state.errors.length > 0) {
      margin = "15px"
    }
    let message = null
    if (this.state.mode === 1) {
      message = <SingleLineSpinner/>
      margin = "-15px"
    } else if (this.state.error) {
      message = <Question>{this.state.error}</Question>
      margin = "15px"
    }

    let modalContent = <p/>
    switch (this.state.modalTurnedOn) {
      case 1:
        modalContent = <div>
          <h2>用户协议</h2>
          <h4>服务条款</h4>
          <p>沃衍康复网提供的服务将完全按照其发布的使用协议、服务条款和操作规则严格执行。为获得沃衍康复服务，服务使用人（以下称“会员”）应当同意本协议的全部条款并按照页面上的提示完成全部的注册程序。</p>
          <h4>目的</h4>
          <p>本协议是以规定用户使用沃衍康复网提供的服务时，沃衍康复网与会员间的权利、义务、服务条款等基本事宜为目的。</p>
          <h4>遵守法律及法律效力</h4>
          <p>本服务使用协议在向会员公告后，开始提供服务或以其他方式向会员提供服务同时产生法律效力。会员同意遵守《中华人民共和国保密法》、《计算机信息系统国际联网保密管理规定》、《中华人民共和国计算机信息系统安全保护条例》、《计算机信息网络国际联网安全保护管理办法》、《中华人民共和国计算机信息网络国际联网管理暂行规定》及其实施办法等相关法律法规的任何及所有的规定，并对会员以任何方式使用服务的任何行为及其结果承担全部责任。在任何情况下，如果本网站合理地认为会员的任何行为，包括但不限于会员的任何言论和其他行为违反或可能违反上述法律和法规的任何规定，本网站可在任何时候不经任何事先通知终止向会员提供服务。本网站可能不时的修改本协议的有关条款，一旦条款内容发生变动，本网站将会在相关的页面提示修改内容。在更改此使用服务协议时，本网站将说明更改内容的执行日期，变更理由等。且应同现行的使用服务协议一起，在更改内容发生效力前7日内及发生效力前日向会员公告。会员需仔细阅读使用服务协议更改内容，会员由于不知变更内容所带来的伤害，本网站一概不予负责。如果不同意本网站对服务条款所做的修改，用户有权停止使用网络服务。如果用户继续使用网络服务，则视为用户接受服务条款的变动。</p>
          <h4>服务内容</h4>
          <p>沃衍康复服务的具体内容由本网站根据实际情况提供，本网站保留随时变更、中断或终止部分或全部沃衍康复服务的权利。</p>
          <h4>会员的义务</h4>
          <p>用户在申请使用本网站沃衍康复服务时，必须向本网站提供准确的个人资料，如个人资料有任何变动，必须及时更新。用户注册成功后，本网站将给予每个用户一个用户帐号及相应的密码，该用户帐号和密码由用户负责保管；用户应当对以其用户帐号进行的所有活动和事件负法律责任。用户在使用本网站网络服务过程中，必须遵循以下原则：遵守中国有关的法律和法规；不得为任何非法目的而使用网络服务系统；遵守所有与网络服务有关的网络协议、规定和程序；不得利用沃衍康复服务系统传输任何危害社会，侵蚀道德风尚，宣传不法宗教组织等内容；不得利用沃衍康复服务系统进行任何可能对互联网的正常运转造成不利影响的行为；不得利用沃衍康复服务系统上载、张贴或传送任何非法、有害、胁迫、滥用、骚扰、侵害、中伤、粗俗、猥亵、诽谤、侵害他人隐私、辱骂性的、恐吓性的、庸俗淫秽的及有害或种族歧视的或道德上令人不快的包括其他任何非法的信息资料；不得利用沃衍康复服务系统进行任何不利于本网站的行为；如发现任何非法使用用户帐号或帐号出现安全漏洞的情况，应立即通告本网站。</p>
          <h4>本网站的权利及义务</h4>
          <p>本网站除特殊情况外（例如：协助公安等相关部门调查破案等），致力于努力保护会员的个人资料不被外漏，且不得在未经本人的同意下向第三者提供会员的个人资料。本网站根据提供服务的过程，经营上的变化，无需向会员得到同意即可更改，变更所提供服务的内容。本网站在提供服务过程中，应及时解决会员提出的不满事宜，如在解决过程中确有难处，可以采取公开通知方式或向会员发送电子邮件寻求解决办法。本网站在下列情况下可以不通过向会员通知，直接删除其上载的内容：有损于本网站，会员或第三者名誉的内容；利用沃衍康复服务系统上载、张贴或传送任何非法、有害、胁迫、滥用、骚扰、侵害、中伤、粗俗、猥亵、诽谤、侵害他人隐私、辱骂性的、恐吓性的、庸俗淫秽的及有害或种族歧视的或道德上令人不快的包括其他任何非法的内容；侵害本网站或第三者的版权，著作权等内容；存在与本网站提供的服务无关的内容；无故盗用他人的ID(固有用户名)，姓名上载、张贴或传送任何内容及恶意更改，伪造他人上载内容。</p>
          <h4>知识产权声明</h4>
          <p>本网站（www.voin-cn.com）所有的产品、技术、程序、页面（包括但不限于页面设计及内容）以及资料内容（包括但不限于本站所刊载的图片、视频、Flash等）均属于知识产权，仅供本用户交流、学习、研究和欣赏，未经授权，任何人不得擅自使用，否则，将依法追究法律责任。本网站用户在模板及论坛频道上传的资料内容（包括但不限于图片、视频、Flash、点评等），应保证为原创或已得到充分授权，并具有准确性、真实性、正当性、合法性，且不含任何侵犯第三人权益的内容，因抄袭、转载、侵权等行为所产生的纠纷由用户自行解决，本网站不承担任何法律责任。“沃衍康复网”及“voin-cn.com”组合为北京沃衍体育科技有限公司注册商标，任何人不得擅自使用，否则，将依法追究法律责任。</p>
          <h4>免责声明</h4>
          <p>任何人因使用本网站而可能遭致的意外及其造成的损失（包括因下载本网站可能链接的第三方网站内容而感染电脑病毒），我们对此概不负责，亦不承担任何法律责任。本网站禁止制作、复制、发布、传播等具有反动、色情、暴力、淫秽等内容的信息，一经发现，立即删除。若您因此触犯法律，我们对此不承担任何法律责任。本网站会员自行上传或通过网络收集的资源，我们仅提供一个展示、交流的平台，不对其内容的准确性、真实性、正当性、合法性负责，也不承担任何法律责任。任何单位或个人认为通过本网站网页内容可能涉嫌侵犯其著作权，应该及时向我们提出书面权利通知，并提供身份证明、权属证明及详细侵权情况证明。我们收到上述法律文件后，将会依法尽快处理。</p>
          <h4>服务变更、中断或终止</h4>
          <p>如因系统维护或升级的需要而需暂停沃衍康复服务，本网站将尽可能事先进行通告。如发生下列任何一种情形，本网站有权随时中断或终止向用户提供本协议项下的沃衍康复服务而无需通知用户：用户提供的个人资料不真实；用户违反本协议中规定的使用规则。除前款所述情形外，本网站同时保留在不事先通知用户的情况下随时中断或终止部分或全部沃衍康复服务的权利，对于所有服务的中断或终止而造成的任何损失，本网站无需对用户或任何第三方承担任何责任。</p>
          <h4>违约赔偿</h4>
          <p>用户同意保障和维护本网站及其他用户的利益，如因用户违反有关法律、法规或本协议项下的任何条款而给本网站或任何其他第三者造成损失，用户同意承担由此造成的损害赔偿责任。</p>
          <h4>修改协议</h4>
          <p>本网站将可能不时的修改本协议的有关条款，一旦条款内容发生变动，本网站将会在相关的页面提示修改内容。如果不同意本网站对服务条款所做的修改，用户有权停止使用沃衍康复服务。如果用户继续使用沃衍康复服务，则视为用户接受服务条款的变动。</p>
          <h4>法律管辖</h4>
          <p>本协议的订立、执行和解释及争议的解决均应适用中国法律。如双方就本协议内容或其执行发生任何争议，双方应尽量友好协商解决；协商不成时，任何一方均可向本网站所在地的人民法院提起诉讼。</p>
          <h4>其他规定</h4>
          <p>本协议构成双方对本协议之约定事项及其他有关事宜的完整协议，除本协议规定的之外，未赋予本协议各方其他权利。如本协议中的任何条款无论因何种原因完全或部分无效或不具有执行力，本协议的其余条款仍应有效并且有约束力。</p>
        </div>
        break;
      case 2:
        modalContent = <div>
          <h2>隐私保护</h2>
          <h4>隐私保护</h4>
          <p>沃衍康复网（www.voin-cn.com，以下称“本网站”）隐私权保护声明系本网站保护用户个人隐私的承诺。本声名适用於您与沃衍康复网站的交互行为以及您登记和使用沃衍康复的在线服务。故特此说明本网站对用户个人信息所采取的收集、使用和保护政策，请您务必仔细阅读。</p>
          <h4>使用者非个人化信息</h4>
          <p>我们将通过您的IP地址来收集非个人化的信息，例如您的浏览器性质、操作系统种类、给您提供接入服务的ISP的域名等，以优化在您计算机屏幕上显示的页面。通过收集上述信息，我们亦进行客流量统计，从而改进网站的管理和服务。</p>
          <h4>个人资料</h4>
          <p>当您在沃衍康复网站进行用户注册登记、定制业务等活动时，在您的同意及确认下，本网站将通过注册表格等形式要求您提供一些个人资料。这些个人资料包括：个人识别资料：如姓名、性别、年龄、出生日期、执业医师资格证书编号、电话、通信地址、住址、电子邮件地址等情况；个人背景： 职业、教育程度、收入状况等。请了解，我们收集您的个人数据，是为了识别我们服务的用户并为您创建一个帐户提供我们的服务。</p>
          <h4>信息安全</h4>
          <p>本网站将对您所提供的资料进行严格的管理及保护，本网站将使用相应的技术，防止您的个人资料丢失、被盗用或遭窜改。本网站得在必要时委托专业技术人员代为对该类资料进行电脑处理，以符合专业分工时代的需求。如本网站将电脑处理之通知送达予您，而您未在通知规定的时间内主动明示反对，本网站将推定您已同意。</p>
          <h4>用户权利</h4>
          <p>您对于自己的个人资料享有以下权利：随时查询及请求阅览；随时请求补充或更正；随时请求删除；请求停止电脑处理及利用。</p>
          <h4>限制利用原则</h4>
          <p>本网站惟在符合下列条件之一，方对收集之个人资料进行必要范围以外之利用：已取得您的书面同意；为免除您在生命、身体或财产方面之急迫危险；为防止他人权益之重大危害；为增进公共利益，且无害于您的重大利益。</p>
          <h4>个人资料之披露</h4>
          <p>当政府机关依照法定程序要求本网站披露个人资料时，本网站将根据执法单位之要求或为公共安全之目的提供个人资料。在此情况下之任何披露，本网站均得免责。</p>
          <h4>Cookies</h4>
          <p>Cookies是指一种技术，当使用者访问设有Cookies装置的本网站时，本网站之服务器会自动发送Cookies至阁下浏览器内，并储存到您的电脑硬盘内，可以捕获和存储有关您的计算机的信息（例如您的IP地址，浏览器类型和访问时间）。 此信息仅用于统计目的而收集，不会将您识别为个人。我们使用这些数据来帮助我们了解用户的浏览模式并塑造我们的营销。</p>
          <p>我们使用cookie记录您访问我们网站的详细信息。Cookie是存储在计算机上的字母数字标识符。Cookie不能用于运行程序或将病毒传播到您的计算机。它们主要用于方便您，例如，返回特定网页。您可以选择拒绝cookie; 但是，您可能无法完全体验所有网站功能。</p>
          <p>下列情况时本网站亦毋需承担任何责任：由于您将用户密码告知他人或与他人共享注册帐户，由此导致的任何个人资料泄露。任何由于计算机2000年问题、黑客政击、计算机病毒侵入或发作、因政府管制而造成的暂时性关闭等影响网络正常经营之不可抗力而造成的个人资料泄露、丢失、被盗用或被窜改等。由于与本网站链接的其它网站所造成之个人资料泄露及由此而导致的任何法律争议和后果。本网站之保护隐私声明的修改及更新权均属于沃衍康复网。</p>
        </div>
        break;
      case 3:
        modalContent = <div>
          <h2>法律声明</h2>
          <h4>版权声明</h4>
          <p>本网站包含之所有内容：文本、图形、LOGO、创意、及软件等之所有权归属沃衍康复，受中国及国际版权法的保护。对本网站上所有内容之复制（意指收集、组合和重新组合），本网站享有排他权并受中国及国际版权法的保护。本网站使用的所有软件的所有权归属于沃衍康复并受中国及国际版权法的保护。本网站的特有信息经网站许可方能转载，并需标明出处。</p>
          <h4>免责声明</h4>
          <p>(1) 本网站不保证提供的各项免费服务不会被修改、中断、终止、延迟，也不能保证用户信息资料的绝对完整及安全，对于用户免费服务的被修改、中断、终止、延迟及用户信息资料的丢失，本网站不承担任何责任；</p>
          <p>(2) 本网站仅对自身发布信息的真实性、合法性、准确性作担保，不担保除此之外其他用户发布信息内容的真实性、合法性、准确性，其信息内容须由访问者自行确认并承担使用该信息的风险，同时本网站不承担除自身以外其他用户发布不实信息造成的损失和责任。</p>
        </div>
        break;
      default:
      modalContent = <p/>
    }

    let account = null
    if (this.state.account === "bank") {
      account = <div className={classes.BankInput}>
        <div>
        <label htmlFor="bankName" className={classes.Label}>开户行名称</label>
        <input
          id="bankName"
          className={classes.InputElement}
          value={this.state.bankName}
          onChange={event => this.inputChangeHandler(event, "bankName")}
        />
        </div>
        <div>
        <label htmlFor="bankAccount" className={classes.Label}>银行账号</label>
        <input
          id="bankAccount"
          className={classes.InputElement}
          value={this.state.bankAccount}
          onChange={event => this.inputChangeHandler(event, "bankAccount")}
        />
        </div>
      </div>
    } else if (this.state.account === "alipay") {
      account = <div className={classes.AlipayInput}>
        <label htmlFor="alipayAccount" className={classes.Label}>支付宝账号</label>
        <input
          className={classes.InputElement}
          id="alipayAccount"
          value={this.state.alipayAccount}
          onChange={event => this.inputChangeHandler(event, "alipayAccount")}/>
      </div>
    }

    return (
      <Content>
        <Modal show={this.state.modalTurnedOn > 0} type="Video" modalClosed={this.onTurnOffModal}>
          {modalContent}
        </Modal>
        <div className={classes.Content}>
          {/*<div className={classes.Row}>*/}
            {/*<Title>注册</Title>*/}
            {/*<div style={{padding: '5px 0', lineHeight: '28px'}}>*/}
              {/*<Link to="/login"><Button btnType="Success" largeFont>立即登录</Button></Link>*/}
            {/*</div>*/}
          {/*</div>*/}
          <div className={classes.Center}>
            <div className={classes.Welcome}>欢迎注册VOIN</div>
            <div className={classes.Login}><span className={classes.Ques}> 已有账号？ </span><Link to="/physiotherapist/login">登录</Link></div>
          </div>
          <div className={classes.First}>
            <div className={classes.FirstWidth}>
              <Input id="username" label="用户名" onChange={this.inputChangeHandler} placeholder="用户名只能包含数字和字母，且不得少于5位"/>
              <Input id="password" label="密码" onChange={this.inputChangeHandler} password placeholder="密码长度不得少于6位"/>
              <Input id="passwordAgain" label="密码确认" onChange={this.inputChangeHandler} password placeholder="请再输入一次密码"/>
              <Input id="name" label="姓名" onChange={this.inputChangeHandler}/>
              <Input id="phone" label="手机号" onChange={this.inputChangeHandler}/>
              {/*<Input id="id" label="身份证号" onChange={this.inputChangeHandler} placeholder="选填"/>*/}
              {/*<Input id="email" label="邮箱" onChange={this.inputChangeHandler}/>*/}
              <div>
                <label className={classes.Label}>收款方式</label>
                <input type="radio" name="account" value="bank" onChange={event => this.inputChangeHandler(event, "account")}/>
                <span className={classes.Radio}>银行卡</span>
                <input type="radio" name="account" value="alipay" onChange={event => this.inputChangeHandler(event, "account")}/>
                <span className={classes.Radio}>支付宝</span>
              </div>
              {account}
              <Input id="hospital" label="医院" onChange={this.inputChangeHandler}/>
              <Input id="room" label="科室" onChange={this.inputChangeHandler}/>
              <label className={classes.Label} htmlFor="occupation">职称</label>
              <select className={classes.Select} id="occupation" onChange={event => this.inputChangeHandler(event, "occupation")}>
                <option value="0"/>
                <optgroup label="康复治疗师">
                  <option value="1">初级士</option>
                  <option value="2">初级师</option>
                  <option value="3">中级</option>
                </optgroup>
                <optgroup label="医师">
                  <option value="4">主任医师</option>
                  <option value="5">副主任医师</option>
                  <option value="6">主治医师</option>
                  <option value="7">住院医师</option>
                </optgroup>
              </select>
              {this.state.occupation === "0" ? null :
                this.state.occupation === "1" || this.state.occupation === "2"
                || this.state.occupation === "3" ?
                  <Input id="certification" label="康复医学治疗技术证管理号" placeholder="选填" onChange={this.inputChangeHandler}/> :
                  <Input id="certification" label="执业医师资格证号" placeholder="选填" onChange={this.inputChangeHandler}/>}
              <Input id="refer" label="推荐人用户名" onChange={this.inputChangeHandler} placeholder="选填"/>
              <ul className={classes.Errors} style={{margin: margin+" 0"}}>
                {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
              </ul>
              {message}
              <div className={classes.Note}>
                <span onClick={() => this.setState({modalTurnedOn: 1})}>用户协议</span>
                <span onClick={() => this.setState({modalTurnedOn: 2})}>隐私保护</span>
                <span onClick={() => this.setState({modalTurnedOn: 3})}>法律声明</span>
                <Link to='/contact' style={{ textDecoration: 'none' }}><span>与我们联系</span></Link>
              </div>
              <div className={classes.Consent}>注册即代表你同意以上所有协议</div>
              <Button
                disabled={this.state.mode === 1} fullWidth largeFont
                largePadding clicked={this.register} btnType="Success">
                注册
              </Button>
            </div>
          </div>
        </div>
      </Content>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuthenticate: (id, name, login, mode, error) => dispatch(actions.authenticate(id, name, login, mode, error)),
    onRemoveCookies: () => dispatch(actions.removeCookies())
  }
}

export default connect(null, mapDispatchToProps)(withRouter(Register))