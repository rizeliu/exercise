import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import classes from './Use.css'

import Content from '../../components/Content/Content'
import Title from "../../components/Title/Title";


class Use extends Component {


  componentDidMount = () => {
    document.title = "VOIN 使用向导"
  }

  render() {

    return (
      <Content padding width="70">
        <div>
          <div> <Title light>使用向导</Title></div>
          <div className={classes.Text}>用户可根据不同需求按照以下标签分类操作，正确使用沃衍健康系统。以下使用向导包括登陆与注册，创建计划，查看所有患者，查看患者详情，我的模板，专家模板，查看文献，修改个人信息等。如有其它系统问题，请联系客服。</div>

          <div> <Title light>登陆与个人系统（开发中）</Title></div>
          <div className={classes.Groups}>
            <div className={classes.Group}>
              <div className={classes.Button}>登陆与注册</div>
              <div className={classes.Explain}>基本登陆与注册操作</div>
            </div>
            <div className={classes.Group}>
              <div className={classes.Button}>修改个人信息</div>
              <div className={classes.Explain}>填写与更新个人信息，例如职位，医院等</div>
            </div>
          </div>

          <div> <Title light>计划与患者（开发中）</Title></div>
          <div className={classes.Groups}>
            <div className={classes.Group}>
              <div className={classes.Button}>查看所有患者</div>
              <div className={classes.Explain}>管理名下患者信息，更新和添加患者信息</div>
            </div>
            <div className={classes.Group}>
              <div className={classes.Button}>查看患者详情</div>
              <div className={classes.Explain}>搜索已有患者信息，查看患者详细信息及新增康复计划</div>
            </div>
            <div className={classes.Group}>
              <div className={classes.Button}>创建康复计划</div>
              <div className={classes.Explain}>搜索康复部位，新增康复计划及搜索运动计划</div>
            </div>
          </div>

          <div> <Title light>模板管理（开发中）</Title></div>
          <div className={classes.Groups}>
            <div className={classes.Group}>
              <div className={classes.Button}>我的模板</div>
              <div className={classes.Explain}>搜索，新增，编辑已有模板及分配康复模板给患者</div>
            </div>
            <div className={classes.Group}>
              <div className={classes.Button}>专家模板</div>
              <div className={classes.Explain}>搜索专家模板并添加与编辑给患者</div>
            </div>
            <div className={classes.Group}>
              <div className={classes.Button}>团队模板</div>
              <div className={classes.Explain}>搜索、查看并添加团队模板</div>
            </div>
          </div>

          <div> <Title light>在线医疗资源（开发中）</Title></div>
          <div className={classes.Groups}>
            <div className={classes.Group}>
              <div className={classes.Button}>查看文献</div>
              <div className={classes.Explain}>在线康复医疗文献学习</div>
            </div>
          </div>
        </div>
      </Content>
    )
  }
}


export default withRouter(Use)