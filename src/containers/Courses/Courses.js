import React, { Component } from 'react'
import { connect } from 'react-redux';


import Content from '../../components/Content/Content'
import Title from "../../components/Title/Title";
import classes from './Courses.css'

class Courses extends Component {

  componentDidMount = async () => {
    document.title = "VOIN 课程"
  }

  state = {
    listA: [
      "美国骨科医师学会-下背部术后功能练习手册.pdf",
      "美国骨科医师学会-全膝关节置换功能练习手册.pdf",
      "美国骨科医师学会-全髋关节置换功能练习手册.pdf",
      "美国骨科医师学会-肩关节术后功能练习手册.pdf",
      "美国骨科医师学会-肩袖与肩关节功能康复计划.pdf",
      "美国骨科医师学会-肱骨外上髁炎功能练习计划（网球肘,高尔夫球肘）.pdf",
      "美国骨科医师学会-脊柱功能康复计划（力量练习）.pdf",
      "美国骨科医师学会-腕管综合征功能练习计划.pdf",
      "美国骨科医师学会-腕管综合征功能练习计划（力量练习）.pdf",
      "美国骨科医师学会-膝关节功能康复计划.pdf",
      "美国骨科医师学会-膝关节镜功能练习手册.pdf",
      "美国骨科医师学会-足，踝功能康复计划.pdf",
      "美国骨科医师学会-髋关节功能康复计划（力量练习）.pdf"
    ],
    listB: [
      "Wii游戏技术在脑卒中康复中的应用.pdf",
      "中风幸存者的体育活动和锻炼建议（美国心脏协会美国中风协会的医疗专业人员声明）.pdf",
      "急性缺血性卒中患者早期治疗指南.pdf",
      "成人脑卒中康复指南.pdf",
      "新生儿和儿童卒中的管理美国心脏协会中风协会的科学声明.pdf",
      "脑卒中康复指南十大知识点.pdf",
      "虚拟现实在脑卒中康复中的应用.pdf",
      "运动训练可改善中风后个体的日常步态活动和步态效率.pdf",
      "通过情感和社会支持促进中风康复.pdf",
      "高强度阻力训练可改善长期卒中患者的肌肉力量、功能限制和残疾.pdf"
    ],
    listC : [
      "手部骨折后的康复策略.pdf"
    ]
  }

  render() {
    return (
      <Content padding width="60">
        <Title>{this.props.admin === 3 ? "Courses" : "课程列表"}</Title>
        <div className={classes.Category}>美国骨科医师学会功能练习计划+手册</div>
        {this.state.listA.map((course, index) =>
          <a key={index} href={"https://backend.voin-cn.com/getcourse/"+course} target="_blank"><div className={classes.Row}>
            {course}
          </div></a>)}
        <div className={classes.Category}>美国心脏协会中风协会文献</div>
        {this.state.listB.map((course, index) =>
          <a key={index} href={"https://backend.voin-cn.com/getcourse/"+course} target="_blank"><div className={classes.Row}>
            {course}
          </div></a>)}
        <div className={classes.Category}>骨科术后康复策略</div>
        {this.state.listC.map((course, index) =>
          <a key={index} href={"https://backend.voin-cn.com/getcourse/"+course} target="_blank"><div className={classes.Row}>
            {course}
          </div></a>)}
      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    admin: state.authRoute.admin
  };
};

export default connect(mapStateToProps)(Courses)