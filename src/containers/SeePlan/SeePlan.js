import React, { Component } from 'react'
import axios from 'axios'

import Modal from '../../components/UI/Modal/Modal'
import Question from '../../components/UI/Question/Question'
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";
import classes from './SeePlan.css'
import Button from "../../components/UI/Button/Button";
import * as actions from "../../store/actions";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";


class SeePlan extends Component {

  state = {
    code: 0,
    message: "",
    qr_url: "",
    planName: ""
  }

  componentDidMount = async () => {
    document.title = "VOIN 付款并查看计划"
    const no = this.props.match.params.no
    const did = this.props.match.params.did
    const cid = this.props.match.params.cid
    const response = await axios.get("https://backend.voin-cn.com/check?no="+no+"&did="+did+"&cid="+cid)
    if (response.data.code === 1) {
      this.setState({qr_url: response.data.url, code: 1,
        planName: response.data.planName})
    } else if (response.data.code === 2) {
      this.setState({code: 2})
      this.props.onShowPlanModal(response.data.info)
    } else if (response.data.code === 0) {
      this.setState({message: response.data.msg, code: 3})
    }
  }

  render() {
    return (
      <Modal show={this.state.code !== 2}>
        {this.state.code === 0 ? <SingleLineSpinner/> : null}
        {this.state.code === 3 ? <Question>{this.state.message}</Question> : null}
        {this.state.code === 1 ? <div className={classes.Center}>
          <div>请您扫描下方二维码付款。如二维码过期，刷新即可获得新二维码。</div>
          <div>计划名：{this.state.planName}，价格：280元</div>
          <div className={classes.QR}><img className="logo" alt="qr code" src={this.state.qr_url}/></div>
          <div style={{marginBottom: '15px'}}>由于该计划属于特殊商品，一经售出，概不退换。有任何问题请联系客服，我们将竭诚为您服务。</div>
          <Button btnType="Success" clicked={() => window.location.reload()}>已付款</Button>
          <Button btnType="New" clicked={() => window.location.reload()}>刷新</Button>
        </div> : null}
      </Modal>
    )
  }
}

const mapDispatchToPatch = dispatch => {
  return {
    onShowPlanModal: info => dispatch(actions.showPlanModal(info)),
  }
}

export default connect(null, mapDispatchToPatch)(withRouter(SeePlan))
