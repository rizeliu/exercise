import React, { Component } from 'react'
import axios from 'axios'

import Title from '../../components/Title/Title'
import Content from '../../components/Content/Content'
import Input from '../../components/UI/Input/Input'
import SingleLineSpinner from '../../components/UI/SingleLineSpinner/SingleLineSpinner'
import Button from '../../components/UI/Button/Button'
import Question from '../../components/UI/Question/Question'
import Public from '../../components/public/public'
import Support from '../../components/support/support'
import classes from './Contact.css'

class Contact extends Component {

  state = {
    subject: "",
    email: "",
    detail: "",
    saving: false,
    success: false,
    error: 0,
    errorMsg: ""
  }

  componentDidMount = async () => {
    document.title = "VOIN 联系我们"
  }

  changeContent = (event, id) => {
    this.setState({[id]: event.target.value, error: 0})
  }

  checkValid = () => {
    return this.state.subject && this.state.email && this.state.detail
  }

  submit = async () => {
    if (this.checkValid()) {
      this.setState({saving: true})
      try {
        const response = await axios.post("https://backend.voin-cn.com/feedback", {
          subject: this.state.subject,
          detail: this.state.detail,
          email: this.state.email
        })
        const result = response.data
        if (result.code === 1) {
          this.setState({success: true, saving: false})
        } else if (result.code === 0) {
          this.setState({error: 2, errorMsg: result.msg, saving: false})
        }
      } catch (error) {
        this.setState({error: 2, errorMsg: "内部服务器错误，请刷新", saving: false})
      }

    } else {
      this.setState({error: 1})
    }
  }

  render() {
    return (
      <Content>
        <div className={classes.Content}>
          <Title alignCenter>联系我们</Title>
          <div className={classes.First}>
            <div className={classes.Intro}>如果您遇到什么问题，或者对我们的服务有什么意见或建议，请联系我们。</div>
            <div className={classes.Intro}>我们会尽快通过邮件回复您。也可以扫描下方二维码，关注我们。</div>
            <div className={classes.Pictures}><Public/><Support/></div>
          </div>
          <div className={classes.Second}>
            <div className={classes.SecondWidth}>
              <Input label="主题" id="subject" value={this.state.subject} onChange={this.changeContent}/>
              <Input label="我的邮箱" id="email" value={this.state.email} onChange={this.changeContent}/>
              <div className={classes.Block}>
                <div>
                  <label className={classes.Label} htmlFor="detail">详情</label>
                </div>
                <textarea id="detail" rows="5" className={classes.TextArea}
                          onChange={event => this.changeContent(event, "detail")}/>
              </div>
              {this.state.saving ? <SingleLineSpinner/> : null}
              {this.state.error === 1 ? <Question>主题、邮箱与详情均不能为空</Question> : null}
              {this.state.error === 2 ? <Question>{this.state.errorMsg}</Question> : null}
              {this.state.success ? <Question>信息发送成功</Question> : null}
              <div className={classes.Submit}>
                <Button btnType="Success" fullWidth largeFont disabled={this.state.saving || this.state.success} clicked={this.submit}>提交</Button>
              </div>
            </div>
          </div>
        </div>
      </Content>
    )
  }
}

export default Contact