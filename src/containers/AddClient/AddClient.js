import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

import Title from '../../components/Title/Title'
import Content from '../../components/Content/Content'
import Button from '../../components/UI/Button/Button'
import SingleLineSpinner from '../../components/UI/SingleLineSpinner/SingleLineSpinner'
import Question from '../../components/UI/Question/Question'
import Input from '../../components/UI/Input/Input'
import * as actions from '../../store/actions/index';
import classes from './AddClient.css'

class AddClient extends Component {

  componentDidMount = () => {
    this.props.onInit()
  }

  render() {
    let question = null
    switch (this.props.mode) {
      case 1:
        question = <SingleLineSpinner />
        break
      case 2:
        question = <Question>该名患者不存在，请再试一次</Question>
        break
      case 3:
        question = <Question>您希望添加这名患者吗？ 姓名: {this.props.name}, 年龄: 24</Question>
        break
      case 4:
        question = <Question>该患者已在您列表中</Question>
        break
      case 5:
        question = <Question>新患者已添加</Question>
        break
      case 6:
        question = <Question>{this.props.error}</Question>
        break
      default: question = null
    }

    return (
      <Content padding width="40">
        <div className={classes.Title}>
          <Title>添加新患者</Title>
          <div style={{padding: '5px 0', lineHeight: '28px'}}>
            <Link to="/clients">
              <Button largeFont btnType="Success">
                回到患者
              </Button>
            </Link>
          </div>
        </div>
        <div className={classes.Content}>
          <form className={classes.Form}>
            <Input id="username" label="患者用户名" onChange={this.props.onInputChange}/>
            {question}
            <Button btnType="Success" disabled={this.props.mode > 0} largeFont
                    clicked={() => this.props.onSearchNewClient(this.props.username)}>
              寻找患者
            </Button>
            <Button btnType="Success" disabled={this.props.mode !== 3} largeFont
                    clicked={() => this.props.onAddNewClient(this.props.doctorId, this.props.clientId, this.props.doctorName, this.props.name)}>
              添加患者
            </Button>
          </form>
        </div>

      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    doctorName: state.authRoute.name,
    username: state.addClient.username,
    name: state.addClient.name,
    clientId: state.addClient.clientId,
    mode: state.addClient.mode,
    error: state.addClient.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInputChange: event => dispatch(actions.searchNewClientInputChange(event.target.value)),
    onSearchNewClient: username => dispatch(actions.searchNewClient(username)),
    onAddNewClient: (doctorId, clientId, doctorName, clientName) => dispatch(actions.addNewClient(doctorId, clientId, doctorName, clientName)),
    onInit: () => dispatch(actions.addClientInit())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddClient)