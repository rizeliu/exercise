import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'

import Content from '../../components/Content/Content'
import Title from '../../components/Title/Title'
import Button from '../../components/UI/Button/Button'
import Spinner from '../../components/UI/Spinner/Spinner'
import SearchInput from "../../components/UI/Input/Input";
import classes from './Admin.css'
import * as actions from "../../store/actions";
import Aux from "../../hoc/Aux/Aux";
import {withRouter} from "react-router-dom";
import Modal from "../../components/UI/Modal/Modal";
import Question from "../../components/UI/Question/Question";
import Input from "../../components/UI/Input/Input";
import Categories from "../../components/Categories/Categories";
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";


class Admin extends Component {

  state = {
    allList: [],
    currentList: [],
    current: "doctors",
    mode: 0,
    searchValue: "",
    order: "",
    QRUsername: "",
    base64: "",
    doctorId: "",
    templateId: "",
    doctorName: "",
    templateName: "",
    deleteTemplate: 0,
    count: 0,
    currentPage: 1,
    currentValue: "",
    addMove: 0,
    selectedCats: [],
    name: "",
    keyWords: "",
    mp4: "",
    screenshot: "",
    desc: "",
    submitMode: 0
  }

  inputChangeHandler = (event, id) => {
    const value = event.target.value
    this.setState({[id]: value})
  }

  seeQR = username => {
    const self = this
    self.setState({QRUsername: username})
    axios.get('https://backend.voin-cn.com/usernameunicode', {params: {username}})
      .then(function (response) {
        if (response.data.code === 1) {
          const buffer = new Buffer(response.data.buffer)
          const base64Str = buffer.toString('base64')
          self.setState({base64: base64Str, QRUsername: username})
        }
      })
  }

  seeSpecialQR = username => {
    const self = this
    self.setState({QRUsername: username})
    axios.get('https://backend.voin-cn.com/erweima')
      .then(function (response) {
        if (response.data.code === 1) {
          const buffer = new Buffer(response.data.buffer)
          const base64Str = buffer.toString('base64')
          self.setState({base64: base64Str, QRUsername: "特殊"})
        }
      })
  }


  closeModal = () => {
    if (this.state.deleteTemplate === 2) {
      this.change("templates")
    }
    this.setState({QRUsername: "", base64: "", doctorId: "", templateId: "", mp4: "", screenshot: "", keyWords: "",
      doctorName: "", templateName: "", deleteTemplate: 0, addMove: 0, selectedCats: [], name: "", submitMode: 0, desc: ""})
  }

  allowWatching = async (doctorId, web) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, allowing: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/allowwatching", {doctorId})
    window.location.reload(false);
  }

  allowFree30 = async (doctorId) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, allowingFree: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/allowfree", {doctorId})
    window.location.reload(false);
  }

  allowQrCode = async (doctorId) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, allowingQrCode: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/allowqrcode", {doctorId})
    window.location.reload(false);
  }

  allowCircle = async (doctorId) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, allowingCircle: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/allowcircle", {doctorId})
    window.location.reload(false);
  }

  disallowWatching = async (doctorId, web) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, disallowing: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/disallowwatching", {doctorId})
    window.location.reload(false);
  }

  disallowFree30 = async (doctorId, web) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, disallowingFree: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/disallowfree", {doctorId})
    window.location.reload(false);
  }

  disallowQrCode = async (doctorId, web) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, disallowingQrCode: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/disallowqrcode", {doctorId})
    window.location.reload(false);
  }

  disallowCircle = async (doctorId, web) => {
    const newList = this.state.currentList.map(doctor => {
      if (doctor._id === doctorId) {
        return {...doctor, disallowingCircle: true}
      } else return doctor
    })
    this.setState({currentList: newList})
    await axios.post("https://backend.voin-cn.com/disallowcircle", {doctorId})
    window.location.reload(false);
  }


  componentDidMount = async () => {
    if (this.props.admin === 1) document.title = "VOIN 管理员"
    else if (this.props.admin === 2) document.title = "VOIN 统计"
    if (!this.props.admin) return null
    this.setState({mode: 1})
    const doctors = await axios.get("https://backend.voin-cn.com/admin",
      {params: {item: "doctors", page: 1,
          admin: this.props.admin, username: this.props.selfUsername, doctorId: this.props.selfId, value: ""}})
    if (this.state.current === "doctors") {
      this.setState({
        currentList: [...doctors.data.response],
        allList: [...doctors.data.response],
        count: doctors.data.count,
        mode: 0,
        currentPage: 1
      })
    }
  }

  change = async (item, page) => {
    this.setState({current: item, currentPage: page, mode: 1, searchValue: "", order: "", currentValue: ""})
    const res = await axios.get("https://backend.voin-cn.com/admin",
      {params: {item, page, admin: this.props.admin, username: this.props.selfUsername, doctorId: this.props.selfId, value: ""}})
    if (item === "plans") {
      const plans = [...res.data.response]
      plans.sort((a,b) => a.date < b.date ? 1 : a.date < b.date ? 0 : -1 )
      if (item === this.state.current) {
        this.setState({currentList: plans,
          allList: plans, mode: 0, searchValue: "", order: "", count: res.data.count
        })
      }
    } else {
      if (item === this.state.current) {
        this.setState({
          currentList: [...res.data.response],
          allList: [...res.data.response], mode: 0, searchValue: "", order: "",
          count: res.data.count
        })
      }
    }
  }

  getPaidTime = plan => {
    return plan.empty && plan.paidTime ?
      Math.floor((new Date().getTime() + 8 * 60 * 60 * 1000 - new Date(plan.paidTime).getTime()) / 1000 / 60 / 60) : -1
  }

  sort = order => {
    // const currentList = [...this.state.currentList]
    // if (order === "unpaid") {
    //   currentList.sort((a, b) => a.total-a.paid < b.total-b.paid ? 1 : a.total-a.paid === b.total-b.paid ? 0 : -1)
    // } else if (order === "paidTime") {
    //   currentList.sort((a, b) => this.getPaidTime(b) - this.getPaidTime(a))
    // } else {
    //   currentList.sort((a, b) => a[order] < b[order] ? 1 : a[order] === b[order] ? 0 : -1)
    // }
    // this.setState({currentList: [...currentList], order: order})
  }

  getTemplate = async (doctorId, templateId, selfId, index) => {
    const newCurrentList = [
      ...this.state.currentList.slice(0, index),
      {...this.state.currentList[index], status: 1},
      ...this.state.currentList.slice(index+1)]
    await this.setState({currentList: newCurrentList})
    await axios.post("https://backend.voin-cn.com/addtemplate", {doctorId, templateId, selfId})
    const newCurrentList2 = [
      ...this.state.currentList.slice(0, index),
      {...this.state.currentList[index], status: 2},
      ...this.state.currentList.slice(index+1)]
    this.setState({currentList: newCurrentList2})
  }

  submitNewMove = async (name, keyWords, mp4, screenshot, desc, cats) => {
    const keywordsList = keyWords.split(" ").filter(keyWord => keyWord.length > 0)
    await this.setState({submitMode: 1})
    const res = await axios.post("https://backend.voin-cn.com/addnewmove", {name, keywordsList, mp4, screenshot, desc, cats})
    if (res.data.code === 1) {
      this.setState({submitMode: 2})
    } else {
      this.setState({submitMode: 3})
    }
  }

  searchChange = value => {
    this.setState({order: "", searchValue: value})
    // if (value.length === 0) {
    //   this.setState({currentList: [...this.state.allList], searchValue: ""})
    // } else {
    //   if (this.state.current === "templates") {
    //     const templates = [...this.state.allList]
    //       .filter(template => template.doctorName.includes(value) || template.name.includes(value))
    //     this.setState({currentList: templates, searchValue: value})
    //   } else if (this.state.current === "doctors") {
    //     const doctors = [...this.state.allList]
    //       .filter((doctor, index) => {
    //         return doctor.username.includes(value) || doctor.name.includes(value)
    //         || (doctor.refer && doctor.refer.includes(value))
    //       })
    //     this.setState({currentList: doctors, searchValue: value})
    //   } else if (this.state.current === "clients") {
    //     const clients = [...this.state.allList]
    //       .filter(client => client.doctors.join("").includes(value) || (client.name && client.name.includes(value)) )
    //     this.setState({currentList: clients, searchValue: value})
    //   } else if (this.state.current === "moves") {
    //     const movements = [...this.state.allList]
    //       .filter(move => move.name.includes(value) || move.id.includes(value))
    //     this.setState({currentList: movements, searchValue: value})
    //   } else if (this.state.current === "plans") {
    //     const plans = [...this.state.allList]
    //       .filter(plan => plan.doctorName.includes(value) || plan.clientName.includes(value)
    //         || plan.name.includes(value))
    //     this.setState({currentList: plans, searchValue: value})
    //   }
    // }
  }

  searchValue = async () => {
    const current = this.state.current
    if (current !== "plans" && current !== "templates") {
      this.setState({currentPage: 1, mode: 1, order: "", currentValue: this.state.searchValue})
      const res = await axios.get("https://backend.voin-cn.com/admin",
        {params: {item: current, page: 1, admin: this.props.admin, username:
            this.props.selfUsername, doctorId: this.props.selfId, value: this.state.searchValue}})
      if (this.state.current === current) {
        this.setState({
          currentList: [...res.data.response],
          allList: [...res.data.response],
          count: res.data.count,
          mode: 0,
          currentPage: 1
        })
      }
    }
  }

  seeAll = async () => {
    const current = this.state.current
    this.setState({currentPage: 1, mode: 1, order: "", currentValue: "", searchValue: ""})
    const res = await axios.get("https://backend.voin-cn.com/admin",
      {params: {item: current, page: 1, admin: this.props.admin, username:
          this.props.selfUsername, doctorId: this.props.selfId, value: ""}})
    if (this.state.current === current) {
      this.setState({
        currentList: [...res.data.response],
        allList: [...res.data.response],
        count: res.data.count,
        mode: 0,
        currentPage: 1
      })
    }
  }

  searchValueWithEnter = async event => {
    if (event.key === 'Enter') {
      await this.searchValue()
    }
  }

  getBeijingTime = date => {
    const timezone = 8;
    const offset_GMT = new Date().getTimezoneOffset();
    const correctDate = new Date(new Date(date).getTime() + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000)
    const year = correctDate.getFullYear();
    const month = ('0' + (correctDate.getMonth() + 1)).slice(-2);
    const day = ('0' + (correctDate.getDate())).slice(-2);
    const hour = ('0' + (correctDate.getHours())).slice(-2);
    const minutes = ('0' + (correctDate.getMinutes())).slice(-2);
    const seconds = ('0' + (correctDate.getSeconds())).slice(-2);
    return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
  }

  deleteTemplate = (doctorId, templateId, doctorName, templateName) => {
    this.setState({doctorId, templateId, doctorName, templateName})
  }

  realDeleteTemplate = async () => {
    this.setState({deleteTemplate: 1})
    const result = await axios.post("https://backend.voin-cn.com/deletetemplate",
      {myType: "my", teamId: "", doctorId: this.state.doctorId, templateId: this.state.templateId})
    if (result.data.code === 1) {
      this.setState({deleteTemplate: 2})
    } else {
      this.setState({deleteTemplate: 3})
    }
  }

  render() {
    if (!this.props.admin) return null
    let result = null
    if (this.state.mode === 1) {
      result = <Spinner/>
    } else if (this.state.mode === 0) {
      switch (this.state.current) {
        case "doctors":
          result = (
            <table className={classes.Table}>
              <thead>
              <tr>
                <th style={{width: '10%'}} onClick={() => this.sort("username")}
                    className={[classes.Clickable, this.state.order === "username" ? classes.Order : ""]
                      .join(" ")}>用户名</th>
                {this.props.admin === 1 ? <th style={{width: '8%'}}>查看统计权</th> : null}
                {this.props.admin === 1 ? <th style={{width: '8%'}}>15天免费</th> : null}
                {this.props.admin === 1 ? <th style={{width: '7%'}}>模版二维码</th> : null}
                {this.props.admin === 1 ? <th style={{width: '7%'}}>上传到圈子</th> : null}
                {this.props.admin === 1 ? <th style={{width: '10%'}}>二维码</th> : null}
                <th style={{width: '15%'}}>姓名</th>
                <th style={{width: '10%'}} onClick={() => this.sort("templates")}
                    className={[classes.Clickable, this.state.order === "templates" ? classes.Order : ""]
                      .join(" ")}>模板数</th>
                <th style={{width: '10%'}} onClick={() => this.sort("total")}
                    className={[classes.Clickable, this.state.order === "total" ? classes.Order : ""]
                  .join(" ")}>计划数</th>
                <th style={{width: '10%'}} onClick={() => this.sort("paid")}
                    className={[classes.Clickable, this.state.order === "paid" ? classes.Order : ""]
                      .join(" ")}>已支付</th>
                <th style={{width: '10%'}} onClick={() => this.sort("unpaid")}
                    className={[classes.Clickable, this.state.order === "unpaid" ? classes.Order : ""]
                      .join(" ")}>未支付</th>
                <th style={{width: '15%'}} onClick={() => this.sort("refer")}
                    className={[classes.Clickable, this.state.order === "refer" ? classes.Order : ""]
                      .join(" ")}>推荐人</th>
              </tr>
              </thead>
              <tbody>
              {this.state.currentList.map((doctor, index) => {
                let occupation = ""
                switch (doctor.occupation) {
                  case "1":
                    occupation = "康复治疗师 初级士"
                    break
                  case "2":
                    occupation = "康复治疗师 初级师"
                    break
                  case "3":
                    occupation = "康复治疗师 中级"
                    break
                  case "4":
                    occupation = "医师 主任医师"
                    break
                  case "5":
                    occupation = "医师 副主任医师"
                    break
                  case "6":
                    occupation = "医师 主治医师"
                    break
                  case "7":
                    occupation = "医师 住院医师"
                    break
                  default:
                    occupation = ""
                }


                return (
                  <tr key={doctor._id}>
                    <td className={classes.DoctorHover}>
                      <span>{doctor.username}</span>
                      <span className={classes.DoctorTooltip}>
                        用户名: {doctor.username} <br/>
                        姓名: {doctor.name}<br/>
                        {this.props.admin === 1 ? <Aux>id: {doctor._id}<br/></Aux> : null}
                        手机号: {doctor.phone}<br/>
                        身份证号: {doctor.idnum}<br/>
                        收款方式: {doctor.account ? doctor.account === "alipay" ? "支付宝" : "银行" : ""}<br/>
                        开户银行: {doctor.bankName}<br/>
                        银行账号: {doctor.bankAccount}<br/>
                        支付宝账号: {doctor.alipayAccount}<br/>
                        微信号: {doctor.wechat}<br/>
                        邮箱: {doctor.email}<br/>
                        医院: {doctor.hospital}<br/>
                        科室: {doctor.room}<br/>
                        病区: {doctor.district}<br/>
                        职称: {occupation}<br/>
                        职称号: {doctor.certification}<br/>
                        账号创建日期: {this.getBeijingTime(doctor.date)}<br/>
                    </span>
                    </td>
                    {this.props.admin === 1 ? doctor.admin === 3 || doctor.admin === 1 ?
                      <td>已开启</td> :!doctor.admin ? doctor.allowing ?
                      <td><Button btnType="New">开启中...</Button></td> :
                      <td><Button clicked={() => this.allowWatching(doctor._id)}
                                  btnType="New">开启</Button></td>
                      : doctor.disallowing ?
                          <td><Button btnType="Danger">取消中...</Button></td> :
                          <td><Button clicked={() => this.disallowWatching(doctor._id)}
                                    btnType="Danger">取消</Button></td> : null}
                    {this.props.admin === 1 ? doctor.free30 ? doctor.disallowingFree ?
                      <td><Button btnType="Danger">取消中...</Button></td> :
                      <td><Button clicked={() => this.disallowFree30(doctor._id)}
                                  btnType="Danger">取消</Button></td> :
                      doctor.allowingFree ?
                      <td><Button btnType="New">开启中...</Button></td> :
                      <td><Button clicked={() => this.allowFree30(doctor._id)}
                                  btnType="New">开启</Button></td>
                      :  null}
                    {this.props.admin === 1 ? !doctor.qrcode ? doctor.allowingQrCode ?
                        <td><Button btnType="New">开启中...</Button></td> :
                        <td><Button clicked={() => this.allowQrCode(doctor._id)}
                                    btnType="New">开启</Button></td>
                        : doctor.disallowingQrCode ?
                          <td><Button btnType="Danger">取消中...</Button></td> :
                          <td><Button clicked={() => this.disallowQrCode(doctor._id)}
                                      btnType="Danger">取消</Button></td> : null}
                    {this.props.admin === 1 ? !doctor.circle ? doctor.allowingCircle ?
                      <td><Button btnType="New">开启中...</Button></td> :
                      <td><Button clicked={() => this.allowCircle(doctor._id)}
                                  btnType="New">开启</Button></td>
                      : doctor.disallowingCircle ?
                        <td><Button btnType="Danger">取消中...</Button></td> :
                        <td><Button clicked={() => this.disallowCircle(doctor._id)}
                                    btnType="Danger">取消</Button></td> : null}
                    {this.props.admin === 1 ?
                      <td>
                        <Button btnType="Success" clicked={() => this.seeQR(doctor.username)}>查看</Button>
                      </td> : null}
                    <td>{doctor.name}</td>
                    <td>{doctor.templates}</td>
                    <td>{doctor.total}</td>
                    <td>{doctor.paid}</td>
                    <td>{doctor.total-doctor.paid}</td>
                    <td>{doctor.refer}</td>
                  </tr>
                )
              })}
              </tbody>
            </table>
          )
          break
        case "clients":
          // let total = 0
          // const free = ["oMYxa5I9OJeRzMeAx_3f-k1QqCZ4", "oMYxa5Ex_U2A1E8cFkljkvYaT6lI", "oMYxa5OHwf3YNyJu-4zwT6VkfpFI", "oMYxa5I040vC-3EKP9oIFV-tMAkY"]
          // this.state.currentList.forEach(client => {
          //   if (free.indexOf(client.openid) > -1) {
          //
          //   } else {
          //     total += client.paid
          //   }
          // })
          // console.log(total)
          result = (
            <table className={classes.Table}>
              <thead>
              <tr>
                <th style={{width: '10%'}}>姓名</th>
                <th style={{width: '25%'}}>id</th>
                <th style={{width: '20%'}}>绑定医生</th>
                <th style={{width: '15%'}} onClick={() => this.sort("total")}
                    className={[classes.Clickable, this.state.order === "total" ? classes.Order : ""]
                      .join(" ")}>计划数</th>
                <th style={{width: '15%'}} onClick={() => this.sort("paid")}
                    className={[classes.Clickable, this.state.order === "paid" ? classes.Order : ""]
                      .join(" ")}>已支付</th>
                <th style={{width: '15%'}} onClick={() => this.sort("unpaid")}
                    className={[classes.Clickable, this.state.order === "unpaid" ? classes.Order : ""]
                      .join(" ")}>未支付</th>
              </tr>
              </thead>
              <tbody>
              {this.state.currentList.map((client) => {
                return (<tr key={client._id}>
                  <td className={classes.ClientHover}>
                    <span>{client.name}</span>
                    <span className={classes.ClientTooltip}>
                      openid: {client.openid}<br/>
                      姓名: {client.name}<br/>
                      手机号: {client.phone}<br/>
                      身份证号: {client.idnum}<br/>
                      性别: {client.sex}<br/>
                      年龄: {client.age}<br/>
                      既往病史: {client.history}<br/>
                      吸烟史: {client.smoke}<br/>
                      具体描述: {client.description}<br/>
                      账号创建日期: {this.getBeijingTime(client.date)}<br/>
                    </span>
                  </td>
                  <td>{client._id}</td>
                  <td>{client.doctors.join("，")}</td>
                  <td>{client.total}</td>
                  <td>{client.paid}</td>
                  <td>{client.total-client.paid}</td>
                </tr>
              )})}
              </tbody>
            </table>
          )
          break
        case "plans":
          result = (
            <table className={classes.Table}>
              <thead>
              <tr>
                <th style={{width: '10%'}} onClick={() => this.sort("doctorName")}
                    className={[classes.Clickable, this.state.order === "doctorName" ? classes.Order : ""]
                      .join(" ")}>医生</th>
                <th style={{width: '10%'}} onClick={() => this.sort("clientName")}
                    className={[classes.Clickable, this.state.order === "clientName" ? classes.Order : ""]
                      .join(" ")}>患者</th>
                <th style={{width: '10%'}} onClick={() => this.sort("date")}
                    className={[classes.Clickable, this.state.order === "date" ? classes.Order : ""]
                      .join(" ")}>创建日期</th>
                {this.props.admin === 1 ?
                  <th style={{width: '10%'}} onClick={() => this.sort("paidTime")}
                      className={[classes.Clickable, this.state.order === "paidTime" ? classes.Order : ""]
                        .join(" ")}>付款时间</th> : null}
                <th style={{width: '30%'}}>计划名</th>
                <th style={{width: '5%'}} onClick={() => this.sort("paid")}
                    className={[classes.Clickable, this.state.order === "paid" ? classes.Order : ""]
                      .join(" ")}>支付</th>
                {this.props.admin === 1 ?
                  <th style={{width: '5%'}} onClick={() => this.sort("refund")}
                      className={[classes.Clickable, this.state.order === "refund" ? classes.Order : ""]
                        .join(" ")}>退款</th> : null}
                {this.props.admin === 1 ? <th style={{width: '10%'}}>查看</th> : null}
              </tr>
              </thead>
              <tbody>
              {this.state.currentList.map(plan => {
                return (
                  <tr key={plan._id}>
                    <td>{plan.doctorName}</td>
                    <td>{plan.clientName}</td>
                    <td>{this.getBeijingTime(plan.date).substring(0, 10)}</td>
                    {this.props.admin === 1 ? <td>{this.getPaidTime(plan) >= 0 ? this.getPaidTime(plan) : ""}</td> : null}
                    <td>{plan.name}</td>
                    <td>{plan.paid ? "是" : ""}</td>
                    {this.props.admin === 1 ? <td>
                      {plan.requestRefund && plan.empty ? "申请退款" : ""}
                    </td> : null}
                    {this.props.admin === 1 ? <td>
                      <Button btnType="Success"
                              clicked={() => {
                                this.props.history.push('client/'+plan.clientId+"/"+plan.doctorId)
                              }}
                      >查看</Button>
                    </td> : null}
                  </tr>
                )
              })}
              </tbody>
            </table>
          )
          break
        case "moves":
          result = (
            <div>
              <Button btnType="Success" clicked={() => {
                this.setState({addMove: 1})
              }}>添加</Button>
              <table className={classes.Table}>
                <thead>
                <tr>
                  <th style={{width: '20%'}}>ID</th>
                  <th style={{width: '10%'}} onClick={() => this.sort("id")}
                      className={[classes.Clickable, this.state.order === "id" ? classes.Order : ""]
                    .join(" ")}>4位号</th>
                  <th style={{width: '20%'}}>名称</th>
                  <th style={{width: '20%'}}>链接</th>
                  <th style={{width: '30%'}}>简介</th>
                </tr>
                </thead>
                <tbody>
                {this.state.currentList.map(move => (
                  <tr key={move._id}>
                    <td>{move._id}</td>
                    <td>{move.id}</td>
                    <td>{move.name}</td>
                    <td>{move.url}</td>
                    <td>{move.info}</td>
                  </tr>
                ))}
                </tbody>
              </table>
            </div>
          )
          break
        case "templates":
          result = (
            <table className={classes.Table}>
              <thead>
              <tr>
                <th style={{width: '20%'}} onClick={() => this.sort("doctorName")}
                    className={[classes.Clickable, this.state.order === "doctorName" ? classes.Order : ""]
                      .join(" ")}>医生</th>
                <th style={{width: '40%'}}>名称</th>
                <th style={{width: '20%'}} onClick={() => this.sort("count")}
                    className={[classes.Clickable, this.state.order === "count" ? classes.Order : ""]
                      .join(" ")}>动作数</th>
                <th style={{width: '20%'}}>管理</th>
              </tr>
              </thead>
              <tbody>
              {this.state.currentList.map((template, index) => (
                <tr key={index}>
                  <td>{template.doctorName}</td>
                  <td>{template.name}</td>
                  <td>{template.count}</td>
                  <td>
                    <Button btnType="Success"
                            clicked={() => this.props.onShowTemplateDetail(
                              template.doctorId, template.templateId
                            )}>查看</Button>
                    {template.status === 1 ?
                      <Button btnType="New">提取中...</Button> :
                      template.status === 2 ?
                        <Button btnType="New">已提取</Button> :
                        <Button btnType="New"
                                clicked={() => this.getTemplate(
                                  template.doctorId, template.templateId, this.props.selfId, index
                                )}>提取</Button>
                    }
                    <Button btnType="Danger"
                            clicked={() => this.deleteTemplate(
                              template.doctorId, template.templateId, template.doctorName, template.name
                            )}>删除</Button>
                  </td>
                </tr>
              ))}
              </tbody>
            </table>
          )
          break
        default: result = null
      }
    }

    const pages = Math.ceil(this.state.count / 50)

    const pageElement = this.state.count > 50 ? <div>
      {[...Array(pages).keys()].map(num =>
        this.state.currentPage === num+1 ?
          <Button smallPadding vMargin btnType="Success" key={num+1}>{num+1}</Button> :
          <Button smallPadding vMargin btnType="Light" clicked={() => this.change(this.state.current, num+1)} key={num+1}>{num+1}</Button>)}
    </div> : null

    const list = this.props.admin === 1 ? [
      {name: "doctors", chinese: "医生"},
      {name: "clients", chinese: "患者"},
      {name: "plans", chinese: "计划"},
      {name: "moves", chinese: "运动"},
      {name: "templates", chinese: "模板"}
    ] : this.props.admin === 2 ? [
      {name: "doctors", chinese: "医生"},
      {name: "plans", chinese: "计划"}
    ] : null


    return (
      <Content padding>
        <Modal show={this.state.QRUsername} type="Video" modalClosed={this.closeModal}>
          <div>
          <Title alignCenter>{this.state.QRUsername+"二维码"}</Title>
          <div className={classes.QRCode}>
            {!this.state.base64 ? <Spinner/> :
              <img className={classes.Img} src={"data:image/png;base64,"+this.state.base64} alt="qrcode"/>}
          </div>
          </div>
        </Modal>
        <Modal show={this.state.doctorId} modalClosed={this.closeModal}>
          <div>
            <Title alignCenter>您正在删除{this.state.doctorName}的{this.state.templateName}</Title>
            <div className={classes.Confirm}>
              {this.state.deleteTemplate === 0 ?
                <Button btnType="Success" clicked={this.realDeleteTemplate}>确定</Button> :
                this.state.deleteTemplate === 1 ?
                  <Spinner/> :
                  this.state.deleteTemplate === 2 ?
                    <Question>完成</Question> :
                    <Question>未完成，请重新操作</Question>
              }

            </div>
          </div>
        </Modal>
        <Modal show={this.state.addMove > 0} type="Video" modalClosed={this.closeModal}>
          <div className={classes.Background}>
            <Title alignCenter>新增动作</Title>
            <div>
              <Input id="name" label="名称（必填）" value={this.state.name} onChange={this.inputChangeHandler}/>
              <Input id="keyWords" label="关键字（用空格分开）"  value={this.state.keyWords} onChange={this.inputChangeHandler}/>
              <Input id="mp4" label="mp4地址（必填）"  value={this.state.mp4} onChange={this.inputChangeHandler}/>
              <Input id="screenshot" label="封面图（必填）"  value={this.state.screenshot} onChange={this.inputChangeHandler}/>
              <div className={classes.Cat}>
                <label>动作简介</label>
              </div>
              <textarea rows="10" className={classes.TextArea}
                        value={this.props.desc}
                        onChange={event => this.inputChangeHandler(event, "desc")}/>
              <div className={classes.Cat}>{this.state.selectedCats.length === 0 ? "选择分类" :
                "选择分类: "+this.state.selectedCats.map(cat => this.props.map[cat] ? this.props.map[cat] : cat).join(", ")}</div>
              <Categories click={value => {
                if (this.state.selectedCats.indexOf(value) < 0) {
                  this.setState({selectedCats: [...this.state.selectedCats, value]})
                }
              }}/>
            </div>
            {this.state.submitMode === 1 ? <SingleLineSpinner/> : this.state.submitMode === 2 ? <Question>添加成功</Question> : null}
            <Button btnType="Success" disabled={this.state.submitMode > 0} clicked={() => {
              if (this.state.name.length > 0 && this.state.mp4.length > 0 && this.state.screenshot.length > 0) {
                this.submitNewMove(this.state.name, this.state.keyWords,
                  this.state.mp4, this.state.screenshot, this.state.desc, this.state.selectedCats)
              }
            }}>添加</Button>
          </div>
        </Modal>
        {this.props.admin === 1 ? <Title>注册用户与运动统计</Title> :
          this.props.admin === 2 ? <Title>注册医生与计划统计</Title> : null}
          <div className={classes.Row}>
          <div className={classes.Buttons}>
            {list.map(item =>
              <Button
                key={item.name}
                btnType={this.state.current === item.name ? "Success" : "Light"}
                largeFont
                clicked={() => this.change(item.name, 1)}
              >
                {item.chinese}
              </Button>
            )}
          </div>
          {this.state.mode === 0 ?
            <span style={{lineHeight: "29px", color: '#606060'}}>总数：{this.state.count}</span> : null}
        </div>
        <div className={classes.Flex}>
          <SearchInput placeholder="搜索" inline noMargin grow onChange={event => {
            this.searchChange(event.target.value)
          }} value={this.state.searchValue} onKeyPress={event => this.searchValueWithEnter(event)}/>
          <div>
            <Button btnType="New" clicked={this.searchValue}>搜索</Button>
            <Button btnType="Success" clicked={this.seeAll}>全部</Button>
          </div>
        </div>
        {this.state.currentValue && this.state.mode !== 1 ? <Title>搜索{this.state.currentValue}</Title> : null}
        {result}
        {this.state.mode === 0 ?
          pageElement : null}
      </Content>
    )
  }

}

const mapStateToProps = state => {
  return {
    admin: state.authRoute.admin,
    selfUsername: state.authRoute.username,
    selfId: state.authRoute.id,
    map: state.categories.map,
    overallInfo: state.categories.overallInfo
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onShowTemplateDetail: (doctorId, templateId) => dispatch(actions.showTemplateDetail(doctorId, templateId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Admin))