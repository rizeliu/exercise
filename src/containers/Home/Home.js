import React, { Component } from 'react'
import { connect } from 'react-redux';
import Player from 'aliplayer-react';

import Content from '../../components/Content/Content'
import exercise from '../../assets/images/ic_锻炼方法.svg'
import move from '../../assets/images/ic_视频动作.svg'
import video from '../../assets/images/ic_视频库.svg'
import experience from '../../assets/images/ic_教学体验.svg'
import platform from '../../assets/images/ic_临床试验练习平台.svg'

import classes from './Home.css'
import * as actions from "../../store/actions";
import Aux from "../../hoc/Aux/Aux";

class Home extends Component {

  // state = {
  //   edu: true
  // }

  componentDidMount = async () => {
    document.title = "VOIN 主页"
    // if (this.props.match.params.resume === "resume") {
    //   await this.login()
    //   this.props.history.push('/exercises')
    // }
    if (this.props.match.url === "/physiotherapist") {
      this.props.onChangeEdu();
    }

  }

  // login = async () => {
  //   const response = await axios.post('https://backend.voin-cn.com/login', {
  //     username: "resume",
  //     isUsername: true,
  //     password: "123456"
  //   })
  //   if (response.data.code === 1) {
  //     const now = new Date();
  //     const time = now.getTime();
  //     const expireTime = time + 30 * 24 * 60 * 60 * 1000;
  //     now.setTime(expireTime);
  //     document.cookie = "userId="+response.data.doc._id+";expires="+now.toGMTString();
  //     document.cookie = "token="+response.data.token+";expires="+now.toGMTString();
  //     this.props.onAuthenticate({...response.data, code: 0}, true, 0, "", response.data.doc.admin, response.data.refer)
  //   }
  // }

  render() {
    return (
      <Content>
        <div className={classes.Flex}>
          <div className={classes.Left}>
            <div className={classes.Video}>
              <Player
                config= {{
                  // source: "https://www.voinsports.com/bb9150af7f754279a944cb722c87681b/d51e8669c4674a50873f8e8953b541ff-a2f3924d88d7435bfd71b5e140933041-sd-encrypt-stream.m3u8",
                  source: this.props.edu ? "https://www.voinsports.com/sv/30366ab3-17be767c25f/30366ab3-17be767c25f.mp4" :
                    "https://www.voinsports.com/sv/564b95a8-17b61d8ebcb/564b95a8-17b61d8ebcb.mp4",
                  width: "100%",
                  height: "100%",
                  autoplay: false,
                  isLive: false,
                  rePlay: false,
                  playsinline: true,
                  preload: true,
                  controlBarVisibility: "hover",
                  useH5Prism: true,
                  components: [
                    {
                      name: "RateComponent",
                      type: Player.components.RateComponent,
                    }
                  ]
                }}
                // onGetInstance={instance => this.setState({ instance })}
              />
              {/*<iframe title={this.props.videoName} width="100%" height="242px" src={"https://view.vzaar.com/20705172/player"} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen/>*/}
            </div>
            <p className={classes.Title}>国际领先的运动康复锻炼计划制作软件</p>
            <Aux>
              <div className={classes.Description}><div className={classes.Yellow}/>
                {this.props.edu ? "在这里，老师可以设计作业并发送到学生端，学生可以个性化制作运动康复计划，并提交到老师端。"
                  : "在这里，您可以个性化制作运动康复功能锻炼计划。"}
              </div>
              <div className={classes.Description}><div className={classes.Green}/>快速、灵活、专业是它最大的优势。</div>
              <div className={classes.Description}><div className={classes.Red}/>
                {this.props.edu ? "不断更新的视频动作库与专业的方案，将帮助您增加临床技能，提高学习效率。"
                  : "不断更新的视频动作库与专业的精品课程，将帮助您提高工作效率，创造更大价值。"}</div>
            </Aux>
          </div>
          <div className={classes.Right}>
            <p className={classes.Title2}>核心优势</p>
            <ul className={classes.Features}>
              <li>
                <div className={classes.One}>
                  <img alt="exercise" src={exercise} className={classes.Picture}/>
                  <div>
                    <div className={classes.Light}>国际最新最全的</div>
                    <div className={classes.Normal}>专业功能锻炼方法</div>
                  </div>
                </div>
              </li>
              <li>
                <div className={classes.One}>
                  <img alt="move" src={move} className={classes.Picture}/>
                  <div>
                    <div className={classes.Light}>2000+</div>
                    <div className={classes.Normal}>视频动作</div>
                  </div>
                </div>
              </li>
              <li>
                <div className={classes.One}>
                  <img alt="video" src={video} className={classes.Picture}/>
                  <div>
                    <div className={classes.Light}>不断更新升级的</div>
                    <div className={classes.Normal}>视频库</div>
                  </div>
                </div>
              </li>
              <li>
                <div className={classes.One}>
                  <img alt="experience" src={experience} className={classes.Picture}/>
                  <div>
                    <div className={classes.Light}>互动式</div>
                    <div className={classes.Normal}>教学体验</div>
                  </div>
                </div>
              </li>
              <li>
                <div className={classes.One}>
                  <img alt="platform" src={platform} className={classes.Picture}/>
                  <div>
                    <div className={classes.Light}>临床实践</div>
                    <div className={classes.Normal}>练习平台</div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        {/*<Title alignCenter>国际领先的运动康复锻炼计划制作软件</Title>*/}
        {/*<div className={classes.Features}>*/}

          {/*<div className={classes.Words}>*/}
            {/*<ul>*/}
              {/*<li>国际最新最全的专业功能锻炼方法</li>*/}
              {/*<li>2000+视频动作</li>*/}
              {/*<li>不断更新升级的视频库</li>*/}
              {/*{this.state.edu ? <li>互动式教学体验</li> : <li>最新、最前沿的精品课程</li>}*/}
              {/*{this.state.edu ? <li>临床实践练习平台</li> : <li>定期工作坊学术探讨</li>}*/}
            {/*</ul>*/}
          {/*</div>*/}
        {/*</div>*/}

        {/*{this.state.edu ?  <Question>*/}
            {/*在这里，老师可以设计作业并发送到学生端，学生可以个性化制作运动康复计划，并提交到老师端。快速、灵活、专业是它最大的优势。不断更新的视频动作库与专业的方案，将帮助您增加临床技能，提高学习效率。*/}
        {/*</Question> :*/}
        {/*<Question>*/}
          {/*在这里，您可以个性化制作运动康复功能锻炼计划。快速、灵活、专业是它最大的优势。不断更新的视频动作库与专业的精品课程，将帮助您提高工作效率，创造更大价值。*/}
        {/*</Question>}*/}
        {/*{!this.props.login ?*/}
          {/*<div className={classes.LowerSignUp}>*/}
            {/*<span className={classes.Or}>想要免费试用吗？马上</span>*/}
            {/*<Link to='/register'><Button largePadding largeFont btnType="New">注册用户</Button></Link>*/}
            {/*<span className={classes.Or}>或</span>*/}
            {/*<Link to='/login'><Button largePadding largeFont btnType="Success">登录进入</Button></Link>*/}
          {/*</div> : null}*/}
      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    login: state.authRoute.login,
    edu: state.authRoute.edu
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeEdu: () => dispatch(actions.changeEdu())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)