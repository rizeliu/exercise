import React, { Component } from 'react'
import { connect } from 'react-redux';

import Content from '../../components/Content/Content'
import Title from "../../components/Title/Title";
import classes from './Resources.css'
import SearchInput from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import axios from "axios";
import Spinner from "../../components/UI/Spinner/Spinner";
import Question from "../../components/UI/Question/Question";

class Resources extends Component {

  componentDidMount = async () => {
    document.title = "VOIN 资源"
  }

  state = {
    keyWords: "",
    mode: 1,
    error: "",
    resources: [],
    cats: ["儿科", "跟腱病", "骨盆", "呼吸训练", "呼吸障碍", "肌腱病变", "脊髓损伤", "肩", "截肢", "颈", "康复辅助技术", "老化",
      "马蹄内翻足", "脑瘫", "COVID-19新冠", "足踝", "帕金森", "身体活动", "神经学", "疼痛", "头痛", "膝", "学术技能", "腰",
      "药理学", "远程医疗", "运动", "中风"],
    labels: ["期刊文章", "书籍章节", "指南", "报告", "幻灯片", "问卷", "系统评价", "其他"],
    chosenCat: "-1",
    chosenLabel: "-1"
  }

  search = async (index, keyWords) => {
    if (index === 1) {
      await this.setState({mode: 2, chosenCat: keyWords})
    } else if (index === 2) {
      await this.setState({mode: 2, chosenLabel: keyWords})
    } else {
      await this.setState({mode: 2})
    }

    try {
      const result = await axios.post('https://backend.voin-cn.com/resources', {cat: this.state.chosenCat,
        label: this.state.chosenLabel, keyWords: this.state.keyWords})
      const res = result.data
      if (res.code === 1) {
        this.setState({mode: 1, resources: res.doc, error: ""})
      } else if (res.code === 2) {
        this.setState({mode: 3, error: ""})
      } else {
        this.setState({mode: 0, error: res.msg})
      }
    }
    catch (error) {
      this.setState({mode: 0, error: error})
    }
  }

  render() {
    return (
      <Content>
        <div className={classes.Content}>
          <Title>资源检索</Title>
          <div className={classes.Block}>
            <div className={classes.Column}>
              <select className={classes.Select}
                      onChange={event => this.search(1, event.target.value)}>
                <option key="empty" value="-1">类型</option>
                {this.state.cats.map((cat, index) =>
                    <option key={"cat"+index} value={cat}>{cat}</option>)}
              </select>
            </div>
            <div className={classes.Column}>
              <select className={classes.Select}
                      onChange={event => this.search(2, event.target.value)}>
                <option key="empty" value="-1">分类</option>
                {this.state.labels.map((label, index) =>
                    <option key={"label"+index} value={label}>{label}</option>)}
              </select>
            </div>
            <div className={classes.Search}>
              <div className={classes.SearchButton}>
                <Button btnType="Success" clicked={() => {
                  if (this.state.keyWords.trim().length > 0) this.search(3, this.state.keyWords)
                }}>搜索
                </Button></div>
              <SearchInput smallMargin search height placeholder="搜索多个关键词可用空格分开" value={this.state.keyWords}
                           onChange={event => this.setState({keyWords: event.target.value})}
                           onKeyPress={event => {
                if (event.key === 'Enter') {
                  if (this.state.keyWords.trim().length > 0) this.search(3, this.state.keyWords)
                }
              }}/>
            </div>
          </div>
          <div className={classes.Claim}>声明：所有资源全部来自对互联网公共资源的收集和整理，仅供学习之用。</div>
          <div className={classes.Resources}>
          {this.state.mode === 2 ? <Spinner grey/> : this.state.mode === 3 ? <Question>未找到相关方案</Question> :
            this.state.error.length > 0 ? <Question>{this.state.error}</Question> :
          <ul className={classes.List}>
            {this.state.resources.map((oneResource, index) =>
              <a key={index} href={"https://backend.voin-cn.com/getresource/"+oneResource} target="_blank">
                <li key={index} className={classes.Row}>
                  {oneResource}
                </li>
              </a>)}
          </ul>}
          </div>
        </div>
      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    admin: state.authRoute.admin
  };
};

export default connect(mapStateToProps)(Resources)