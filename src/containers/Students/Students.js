import React, { Component } from 'react'
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import * as actions from "../../store/actions";
import Spinner from "../../components/UI/Spinner/Spinner";
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";
import Content from "../../components/Content/Content";
import Title from "../../components/Title/Title";
import Question from "../../components/UI/Question/Question";
import Button from "../../components/UI/Button/Button";
import success from '../../assets/images/img_成功.svg'

import classes from './Students.css'
import Aux from "../../hoc/Aux/Aux";
import Modal from "../../components/UI/Modal/Modal";
import Input from "../../components/UI/Input/Input";
import axios from "axios";


class Students extends Component {

  state = {
    seeTitleDetail: false,
    newTitle: "",
    name: "",
    sex: "",
    age: "",
    history: "",
    smoke: "",
    images: [],
    addRecordError: "",
    newDescription: "",
    createNewTitle: false,
    reviewStudentId: "",
    score: "",
    review: "",
    seeReviewStudentId: "",
    selectedFiles: [],
    fileError: "",
    fileUploading: false,
    pictureName: "",
  }

  onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    const newImages = [...this.state.images]
    newImages.push(...imageList)
    this.setState({images: [...imageList]})
  };

  componentDidMount = () => {
    document.title = "VOIN 学生"
    this.props.onFetchClassesAndStudents(this.props.doctorId)
  }

  onFileChange = event => {
    const addedFiles = [...event.target.files]

    addedFiles.forEach(file => {
      file.src = URL.createObjectURL(file)
    })


    const newFiles = [...this.state.selectedFiles]
    newFiles.push(...addedFiles)


    // Update the state
    this.setState({ fileError: "", fileUploading: false, selectedFiles: newFiles});

  };

  removeImage = index => {
    const newFiles = [...this.state.selectedFiles.slice(0, index), ...this.state.selectedFiles.slice(index+1)]

    // Update the state
    this.setState({ fileError: "", fileUploading: false, selectedFiles: newFiles });
  }

  onFileUpload = async () => {

    // if (this.state.selectedFile.size >= 1048576) {
    //   this.setState({fileError: "目前无法上传大于1M图片，功能优化中", fileUploading: false})
    //   return
    // }

    this.setState({fileError: "", fileUploading: true})

    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFiles,
      this.state.selectedFiles.map(file => file.name)
    );
    formData.append("doctorId", this.props.doctorId)
    formData.append("clientId", this.props.clientId)

    const result = await axios({
      url: "https://backend.voin-cn.com/doctorupload",
      method: 'post',
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
      data: formData
    })
    if (result.data.code === 1) {
      const doctorId = this.props.match.params.did
      const clientId = this.props.match.params.cid
      this.setState({record: "", selectedFiles: [], fileError: "", fileUploading: false})
      this.props.onClientDetailInit(this.props.doctorId, clientId, !doctorId)
    } else {
      this.setState({fileError: result.data.msg, fileUploading: false})
    }
  };

  changeContent = (key, value) => {
    this.setState({[key]: value})
  }

  changeScore = value => {
    this.setState({score: value})
  }

  changeReview = value => {
    this.setState({review: value})
  }


  changeDescription = value => {
    this.setState({newDescription: value})
  }

  reviewHomework = (studentId) => {
    this.setState({reviewStudentId: studentId})
  }

  seeReview = (studentId) => {
    this.setState({seeReviewStudentId: studentId})
  }

  deleteRecord = index => {
    const records = [...this.state.images]
    records.splice(index, 1)
    this.setState({images: records})
  }


  modalClosed = () => {
    if (this.props.reviewMode === 2 && !this.props.reviewError) {
      this.props.onTitleChange(this.props.selectedTitleId)
    }
    if (this.props.newTitleMode === 2 && !this.props.newTitleError) {
      this.props.onFetchClassesAndStudents(this.props.doctorId)
    }

    this.setState({
      seeTitleDetail: false,
      newTitle: "",
      name: "",
      sex: "",
      age: "",
      history: "",
      smoke: "",
      images: [],
      newDescription: "",
      addRecordError: "",
      createNewTitle: false,
      reviewStudentId: "",
      score: "",
      review: "",
      seeReviewStudentId: "",
      selectedFiles: [],
      fileError: "",
      fileUploading: false
    })
  }



  render() {



    let homework = {}
    let studentName = ""
    let review = {}
    if (this.state.seeTitleDetail) {
      homework = this.props.titles.filter(title => this.props.selectedTitleId === title._id)[0]
    }
    if (this.state.reviewStudentId) {
      studentName = this.props.students.filter(student => student.studentId === this.state.reviewStudentId)[0].name
      homework = this.props.titles.filter(title => this.props.selectedTitleId === title._id)[0]
    }

    if (this.state.seeReviewStudentId) {
      console.log(this.props.homework)
      studentName = this.props.students.filter(student => student.studentId === this.state.seeReviewStudentId)[0].name
      homework = this.props.titles.filter(title => this.props.selectedTitleId === title._id)[0]
      review = this.props.homework.filter(ho => ho.doctorId === this.state.seeReviewStudentId)[0]
    }

    return (
      <Content padding>
        <Modal type="Review" show={this.state.seeTitleDetail} modalClosed={this.modalClosed}>
          <div className={classes.NewTitle}>
            <Title alignCenter>{"作业题目：" + homework.title}</Title>
            <table className={classes.DesTable}>
              <tbody>
              <tr>
                <td className={classes.Left}>患者姓名：</td>
                <td className={classes.Right}>{homework.name}</td>
              </tr>
              <tr>
                <td className={classes.Left}>性别：</td>
                <td className={classes.Right}>{homework.sex}</td>
              </tr>
              <tr>
                <td className={classes.Left}>年龄：</td>
                <td className={classes.Right}>{homework.age}</td>
              </tr>
              <tr>
                <td className={classes.Left}>既往病史：</td>
                <td className={classes.Right}>{homework.history}</td>
              </tr>
              <tr>
                <td className={classes.Left}>吸烟室：</td>
                <td className={classes.Right}>{homework.smoke}</td>
              </tr>
              <tr>
                <td className={classes.Left}>详细要求：</td>
                <td className={classes.Right}>{homework.description}</td>
              </tr>
              <tr>
                <td className={classes.Left}>图片：</td>
                <td className={classes.Right}>{homework && homework.pictures ? <div className={classes.Container}>
                  {homework.pictures.map((picture, index) => <div key={index}
                                                         className={classes.RecordDiv}>
                      <img alt="click" onClick={() => this.setState({pictureName: picture.name})}
                           className={classes.Record} src={"https://backend.voin-cn.com/"+picture.name} />
                    </div>)}
                  </div> : null}
                  {this.state.pictureName.length > 0 ? <div>
                    <TransformWrapper>
                      <TransformComponent>
                        <img alt="record" className={classes.BigRecord} src={"https://backend.voin-cn.com/"+this.state.pictureName} />
                      </TransformComponent>
                    </TransformWrapper>
                  </div> : null}
                </td>

              </tr>
              </tbody>

            </table>

          </div>
        </Modal>
        <Modal type={this.props.newTitleMode === 2 && !this.props.newTitleError ? "" : "Review"}
               show={this.state.createNewTitle} modalClosed={this.modalClosed}>
          <div className={classes.NewTitle}>
            <div className={classes.NewTitleInner}>
              <Title alignCenter>新增作业</Title>
              {this.props.newTitleMode === 2 && !this.props.newTitleError ?
                <div className={classes.Success}>
                  <img alt="success" src={success}/>
                  <div>新增作业成功</div>
                </div> : <div>
                <Input label="题目" value={this.state.newTitle} onChange={(event) => this.changeContent("newTitle", event.target.value)}/>
                <div className={classes.OneLine}>
                <Input label="患者姓名" value={this.state.name} padding onChange={(event) => this.changeContent("name", event.target.value)}/>
                <Input label="性别" value={this.state.sex} padding onChange={(event) => this.changeContent("sex", event.target.value)}/>
                <Input label="年龄" value={this.state.age} padding onChange={(event) => this.changeContent("age", event.target.value)}/>
                </div>
                <Input label="既往病史" value={this.state.history} onChange={(event) => this.changeContent("history", event.target.value)}/>
                <Input label="吸烟史" value={this.state.smoke} onChange={(event) => this.changeContent("smoke", event.target.value)}/>
                <div className={classes.SmallRow}>
                  <label>详细要求</label>
                </div>
                <textarea rows="5" className={classes.TextArea}
                      value={this.state.newDescription}
                      onChange={event => this.changeDescription(event.target.value)}
                />
                    <div>

                      <label htmlFor="files" className={classes.Label}>上传图片
                        <input type="file" id="files" accept="image/png, image/jpg, image/jpeg"
                               multiple={true} onClick={(event) => {event.target.value = ''}}
                               className={classes.Hidden} onInput={this.onFileChange}/>
                      </label>


                      <div className={classes.Container}>
                        {this.state.selectedFiles.map((record, index) => <div key={index}
                                                                       className={classes.RecordDiv}>
                          <img alt="" className={classes.RecordEditing} src={record.src}/>
                          <div className="image-item__btn-wrapper">
                            <Button btnType="Danger" clicked={() => this.removeImage(index)}>删除</Button>
                          </div>
                        </div>)}
                      </div>

                    </div>

                    {/*<ImageUploading*/}
                    {/*    multiple*/}
                    {/*    value={this.props.images}*/}
                    {/*    onChange={this.onChange}*/}
                    {/*    maxNumber={69}*/}
                    {/*    dataURLKey="data_url"*/}
                    {/*    acceptType={["jpg", "bmp"]}*/}
                    {/*>*/}
                    {/*  {({*/}
                    {/*      imageList,*/}
                    {/*      onImageUpload,*/}
                    {/*      onImageRemoveAll,*/}
                    {/*      onImageUpdate,*/}
                    {/*      onImageRemove,*/}
                    {/*      isDragging,*/}
                    {/*      dragProps*/}
                    {/*    }) => (*/}
                    {/*      // write your building UI*/}
                    {/*      <div className="upload__image-wrapper">*/}
                    {/*        <Button*/}
                    {/*            btnType="Success"*/}
                    {/*            clicked={onImageUpload}*/}
                    {/*            {...dragProps}*/}
                    {/*        >*/}
                    {/*          上传图片*/}
                    {/*        </Button>*/}
                    {/*        <div className={classes.Container}>*/}
                    {/*          {this.state.images.map((record, index) => <div key={index}*/}
                    {/*                                                 className={classes.RecordDiv}>*/}
                    {/*            <img alt="" className={classes.Record} src={record.data_url}/>*/}
                    {/*            <div className="image-item__btn-wrapper">*/}
                    {/*              <Button btnType="Danger" clicked={() => onImageRemove(index)}>删除</Button>*/}
                    {/*            </div>*/}
                    {/*          </div>)}*/}
                    {/*        </div>*/}
                    {/*        /!*{this.state.images.map((image, index) => (*!/*/}
                    {/*        /!*    <div key={index} className="image-item">*!/*/}
                    {/*        /!*      <img src={image.data_url} alt="" width="100" />*!/*/}
                    {/*        /!*      <div className="image-item__btn-wrapper">*!/*/}
                    {/*        /!*        <Button btnType="Danger" clicked={() => onImageRemove(index)}>删除</Button>*!/*/}
                    {/*        /!*      </div>*!/*/}
                    {/*        /!*    </div>*!/*/}
                    {/*        /!*))}*!/*/}
                    {/*      </div>*/}
                    {/*  )}*/}
                    {/*</ImageUploading>*/}

                <div className={classes.Button}>
                  {this.props.newTitleMode === 0 ? <Button middlePadding width20 btnType="Success"
                          clicked={() => {
                            this.setState({fileError: "", fileUploading: true})
                            this.props.onSubmitNewTitle(
                              this.props.classInfo._id, this.state.newTitle, this.state.name, this.state.sex,
                              this.state.age, this.state.history, this.state.smoke, this.state.newDescription,
                              this.props.doctorId, this.state.selectedFiles, this.state.selectedFiles.map(file => file.name)
                            )
                            // this.onFileUpload()
                          }}>确定</Button> :
                    this.props.newTitleMode === 1 ? <SingleLineSpinner/> :
                      this.props.newTitleMode === 2 && !this.props.newTitleError ?
                        null :
                        <Question>新增作业失败，请重新添加</Question>
                  }
              </div>
            </div>}
            </div>
          </div>
        </Modal>
        <Modal type="Video" show={this.state.reviewStudentId} modalClosed={this.modalClosed}>
          <div className={classes.NewTitle}>
            <Title alignCenter>打分并点评</Title>


            <table className={classes.DesTable}>
              <tbody>
              <tr>
                <td className={classes.Left}>学生：</td>
                <td className={classes.Right}>{studentName}</td>
              </tr>
              <tr>
                <td className={classes.Left}>作业题目：</td>
                <td className={classes.Right}>{homework.title}</td>
              </tr>
              <tr>
                <td className={classes.Left}>分数：</td>
                <td className={classes.Right}><Input value={this.state.score} onChange={(event) => this.changeScore(event.target.value)}/></td>
              </tr>
              <tr>
                <td className={classes.Left}>点评：</td>
                <td className={classes.Right}><textarea rows="5" className={classes.TextArea}
                                                        value={this.props.review}
                                                        onChange={event => this.changeReview(event.target.value)}/></td>
              </tr>
              </tbody>

            </table>


            <div className={classes.Button}>
              {this.props.reviewMode === 0 ?
                <Button btnType="Success"
                        clicked={() => this.props.onSubmitReview(
                          this.state.reviewStudentId, this.props.selectedTitleId, this.state.score, this.state.review
                        )}>确定</Button> :
                this.props.reviewMode === 1 ? <SingleLineSpinner/> :
                  this.props.reviewMode === 2 && !this.props.reviewError ?
                    <Question>保存成功</Question> :
                    <Question>保存失败，请重新打分</Question>
              }
            </div>
          </div>
        </Modal>
        <Modal show={this.state.seeReviewStudentId} modalClosed={this.modalClosed}>
          <div className={classes.NewTitle}>
            <Title alignCenter>查看评分</Title>
            <table className={classes.DesTable}>
              <tbody>
              <tr>
                <td className={classes.Left}>学生：</td>
                <td className={classes.Right}>{studentName}</td>
              </tr>
              <tr>
                <td className={classes.Left}>作业题目：</td>
                <td className={classes.Right}>{homework.title}</td>
              </tr>
              <tr>
                <td className={classes.Left}>分数：</td>
                <td className={classes.Right}>{review.score}</td>
              </tr>
              <tr>
                <td className={classes.Left}>点评：</td>
                <td className={classes.Right}>{review.review}</td>
              </tr>
              </tbody>

            </table>
          </div>
        </Modal>
        <Modal type="Video" show={this.props.proposalState > 0} modalClosed={this.props.onCloseProposal}>
          {this.props.proposalState === 1 ? <Spinner/> :
              this.props.proposalState === 2 ? <Title>{this.props.proposalErr}</Title> :
                  this.props.proposalState === 3 ? <Aux>
                    <div className={classes.NewTitle}>
                      <Title>方案设计</Title>
                      <div>
                        <textarea readOnly={true} value={this.props.proposalContent && this.props.proposalContent.length > 0 ? this.props.proposalContent : "无"}
                                  className={classes.Proposal} id="proposal" name="proposal" rows="15" cols="50"
                        />
                      </div>
                      <div className={classes.ReferenceTitleLine}>
                        <Title>参考文献</Title>
                      </div>
                      <div className={classes.References}>
                        {this.props.references && this.props.references.length > 0 ?
                            this.props.references.map((reference, index) => {
                              return <div className={classes.ReferenceLine} key={index}>
                                <div><a href={reference.indexOf("http") === 0 ? reference : "//"+reference} target="_blank">{reference}</a></div>
                              </div>
                            }) : <div>无</div>}
                    </div>

                  </div></Aux> : null}

        </Modal>
        <div className={classes.Content}>
        <Title>学生信息</Title>
        {this.props.mode === 1 ? <Spinner grey/> : this.props.fetchError ?
          <Question>{this.props.fetchError}</Question> : <div>
            <div className={classes.Class}>
              <div>班级名称: {this.props.classInfo.name}</div>
              <Button btnType="Success" clicked={() => this.setState({createNewTitle: true})}>新增题目</Button>
            </div>
            <div className={classes.Title}>
              <div>
                <div className={classes.TitleSpan}>选择作业：</div>
                <select className={classes.Select}
                        onChange={event => this.props.onTitleChange(event.target.value)}
                        value={this.props.selectedTitleId}>
                  <option key="empty" value=""/>
                  {this.props.titles.map(title =>
                    <option key={title._id} value={title._id}>{title.title}</option>)}
                </select>
              </div>
              {this.props.selectedTitleId && this.props.titleMode === 0 ?
                <Button btnType="Grey" clicked={() => this.setState({seeTitleDetail: true, pictureName: ""})}>查看题目详情</Button> : null}
            </div>

            {this.props.titleMode === 1 ? <Spinner grey/> : <div className={classes.TableDiv}><table className={classes.Table}>
              <thead>
              <tr>
                <th>学生姓名</th>
                <th>用户名</th>
                {this.props.selectedTitleId ? <Aux>
                  <th>作业</th>
                  <th>设计思路</th>
                  <th>评价</th>
                </Aux> : null}
              </tr>
              </thead>
              <tbody>

              {this.props.students.map(student => {
                const thisHomework = this.props.homework.filter(ho => ho.doctorId === student.studentId)
                return (
                  <tr key={student.studentId}>
                    <td>{student.name}</td>
                    <td>{student.username}</td>
                    {this.props.selectedTitleId ? <Aux>
                      <td>{thisHomework.length > 0 ?
                        <div className={classes.LilButton} onClick={() => this.props.onSeeHomeworkDetail(student.studentId, this.props.selectedTitleId)}>查看</div> :
                        "未提交"}</td>
                      <td>{thisHomework.length > 0 ?
                          <div className={classes.LilButton} onClick={() => this.props.onSeeProposal(student.studentId, this.props.selectedTitleId)}>查看</div> :
                          "未提交"}</td>
                      <td>{thisHomework.length > 0 ? thisHomework[0].score ?
                        <div className={classes.LilButton} onClick={() => this.seeReview(student.studentId)}>查看评价</div> :
                        <div className={classes.LilButton} onClick={() => this.reviewHomework(student.studentId)}>评分</div> : "无"}</td>
                    </Aux> : null}
                  </tr>
                )
              })}
              </tbody>
            </table></div>}
          </div>}
        </div>
      </Content>
    )

  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    mode: state.student.mode,
    students: state.student.students,
    classInfo: state.student.classInfo,
    titles: state.student.titles,
    selectedTitleId: state.student.selectedTitleId,
    titleMode: state.student.titleMode,
    fetchError: state.student.fetchError,
    titleError: state.student.titleError,
    homework: state.student.homework,
    newTitleMode: state.student.newTitleMode,
    newTitleError: state.student.newTitleError,
    reviewMode: state.student.reviewMode,
    reviewError: state.student.reviewError,
    newTitleId: state.student.newTitleId,
    name: state.student.name,
    proposalState: state.student.proposalState,
    proposalContent: state.student.proposalContent,
    references: state.student.references,
    proposalErr: state.student.proposalErr
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchClassesAndStudents: doctorId => dispatch(actions.fetchClassesAndStudents(doctorId)),
    onSubmitNewTitle: (classId, title, name, sex, age, history, smoke, description, doctorId, selectedFiles, names) =>
      dispatch(actions.submitNewTitle(classId, title, name, sex, age, history, smoke, description, doctorId, selectedFiles, names)),
    onSubmitReview: (studentId, titleId, score, review) => dispatch(actions.submitReview(studentId, titleId, score, review)),
    onTitleChange: titleId => dispatch(actions.titleChange(titleId)),
    onSeeHomeworkDetail: (doctorId, titleId) => dispatch(actions.seeHomeworkDetail(doctorId, titleId)),
    onSeeProposal: (doctorId, titleId) => dispatch(actions.seeProposal(doctorId, titleId)),
    onCloseProposal: () => dispatch(actions.closeStudentProposal()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Students))