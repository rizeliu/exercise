import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import classes from './Action.css'
import Content from "../../components/Content/Content";
import Title from "../../components/Title/Title";
import Aux from "../../hoc/Aux/Aux";

class Action extends Component {

  state = {
    content: {
      articles: {
        name: "查看课程",
        summary: "在线查看医疗文献与视频课程",
        steps: [{
          name: "登陆系统或注册账号",
          picture: "https://backend.voin-cn.com/steps/articles_1.png"
        }, {
          name: "选择右上角“课程”选项",
          picture: "https://backend.voin-cn.com/steps/articles_2.png"
        }, {
          name: "在课程列表内选择想查看的医疗文献或视频课程",
          picture: "https://backend.voin-cn.com/steps/articles_3.png"
        }]
      }
    }
  }

  componentDidMount = () => {
    document.title = "VOIN 使用向导"
  }

  render() {

    const action = this.props.match.params.action

    return (
      <Content padding width="70">
        <div className={classes.Content}>
          <div className={classes.First}>
            <div className={classes.Category}>登陆与个人系统</div>
            <div className={classes.Action}>注册与登陆</div> <br/>
            <div className={classes.Action}>修改个人信息</div> <br/>
            <div className={classes.Category}>计划与患者</div>
            <div className={classes.Action}>查看所有患者</div><br/>
            <div className={classes.Action}>查看患者详情</div><br/>
            <div className={classes.Action}>创建康复计划</div><br/>
            <div className={classes.Category}>模板管理</div>
            <div className={classes.Action}>我的模板</div><br/>
            <div className={classes.Action}>专家模板</div><br/>
            <div className={classes.Action}>团队模板</div><br/>
            <div className={classes.Category}>在线医疗资源</div>
            <div className={[classes.Action, action==="articles" ? classes.Chosen : ""].join(" ")}>查看课程</div>
          </div>
          <div className={classes.Second}>
            <Title light>{this.state.content[action].name}</Title>
            <div>{this.state.content[action].summary}</div>
            {this.state.content[action].steps.map((step, index) => <Aux key={index}>
              <div>{(index+1)+". "+step.name}</div>
              <img className={classes.Image} src={step.picture} alt="none"/>
            </Aux>)}
          </div>
        </div>

      </Content>
    )
  }
}


export default withRouter(Action)