import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Title from '../../components/Title/Title'
import Content from '../../components/Content/Content'
import ClientTable from '../../components/ClientTable/ClientTable'
import Button from '../../components/UI/Button/Button'
import Spinner from '../../components/UI/Spinner/Spinner'
import Modal from '../../components/UI/Modal/Modal'
import Aux from '../../hoc/Aux/Aux'
import Question from '../../components/UI/Question/Question'
import SearchInput from "../../components/UI/Input/Input";
import ClientCategory from "../../components/UI/ClientCategory/ClientCategory";
import * as actions from '../../store/actions/index';
import classes from './Clients.css'

class Clients extends Component {


  componentDidUpdate = async () => {
    if (this.props.mode === 5) {
      await this.props.onFetchClients(this.props.doctorId)
    }
  }

  componentDidMount = async () => {
    document.title = "VOIN 患者"
    await this.props.onFetchClients(this.props.doctorId)
  }



  createPlanForClient = (clientName, clientEmail) => {
    this.props.history.push('/exercises')
    this.props.onCreatePlanForClient(clientName, clientEmail)
  }

  clientDetail = clientId => {
    this.props.history.push('/client/'+clientId)
  }


  render() {
    // let deleteQuestion = (
    //   <Modal modalClosed={this.props.onCloseModal} type="Warning" show={this.props.mode === 2}>
    //     <div className={classes.Center}>
    //     <p style={{marginTop: "0"}}>您确定要删除"{this.props.deleteClientName}"这位患者吗？注意：这位患者的所有计划也都将随之删除。</p>
    //     <Button btnType="Danger" clicked={this.props.onCloseModal}>取消</Button>
    //     <Button btnType="Success" clicked={() => this.props.onRealDelete(this.props.doctorId, this.props.deleteClientId)}>继续</Button>
    //     </div>
    //   </Modal>
    // )
    // if (this.props.mode === 6) {
    //   deleteQuestion = <Modal modalClosed={this.props.onDeleteFinished} show={this.props.mode === 6}>
    //     <Question>{this.props.deleteError}</Question>
    //   </Modal>
    // }
    // if (this.props.mode === 3) {
    //   deleteQuestion = <Modal modalClosed={this.props.onCloseModal} show={this.props.mode === 3}>
    //     <Spinner/>
    //   </Modal>
    // }
    // if (this.props.mode === 4) {
    //   deleteQuestion = <Modal modalClosed={this.props.onDeleteFinished} show={this.props.mode === 4}>
    //     <Question>该患者及其所有计划均已删除</Question>
    //     <div className={classes.Center}><Button btnType="Success" clicked={this.props.onDeleteFinished}>完成</Button></div>
    //   </Modal>
    // }
    let request = []
    if (this.props.requestIndex >= 0) {

      request = <Modal type="Video" modalClosed={this.props.onCloseModal} show={this.props.requestIndex >= 0}>
        <Title alignCenter>患者请求</Title>
        {this.props.clients[this.props.requestIndex].request.reverse().map((word, index) =>{
          const date = new Date(word.date)
          const year = ""+date.getFullYear()
          const month = date.getMonth()+1 >= 10 ? ""+(date.getMonth()+1): "0"+ (date.getMonth()+1)
          const day = date.getDate() >= 10 ? ""+date.getDate(): "0"+ date.getDate()
          const hour = date.getHours() >= 10 ? ""+date.getHours(): "0"+ date.getHours()
          const minute = date.getMinutes() >= 10 ? ""+date.getMinutes(): "0"+ date.getMinutes()
          const second = date.getSeconds() >= 10 ? ""+date.getSeconds(): "0"+ date.getSeconds()
          return (
            <div key={index} className={classes.Request}>
              <div className={classes.Time}>{year+"/"+month+"/"+day+" "+hour+":"+minute+":"+second}</div>
              <div className={classes.Content}>{word.content}</div>
            </div>
          )
        })}
      </Modal>
    }

    const allCats = [...this.props.clientCategories]
    allCats.push(...this.props.childDoctors)

    return (
      <Aux>
        {request}
        <Content padding>
          <div className={classes.Package}>
          {this.props.mode === 1 ? <Spinner grey/> : <Aux>
              <div className={classes.Title}>
                <Title>{this.props.admin === 3 ? "My Patients" : "我的患者"}</Title>
                <div style={{padding: '15px 0'}}>
                  <Button btnType="Success" largeFont clicked={this.props.onShowAllClients}>所有患者</Button>
                </div>
              </div>
              {this.props.clients.filter(client => client.empty > 0).length > 0 && this.props.mode !== 1 ?
                <Question>您有患者已支付的计划未制定</Question> : null}
              <SearchInput placeholder="搜索患者" onChange={event => {
                this.props.onClientSearchChange(event.target.value)
              }} value={this.props.searchValue}/>
              <div className={classes.FullContent}>
                <ClientCategory type="client"/>

                <div className={classes.Second}>
                  {this.props.chosen ? <Title noPaddingTop>"{allCats.filter(cat => cat._id === this.props.titleId)[0].name}"分类中的患者</Title> : null}
                  <Title noPaddingTop>{this.props.title ? this.props.title : null}</Title>
                  <ClientTable
                    onClientDetail={this.clientDetail}
                    onCreatePlanForClient={this.createPlanForClient}
                    onDeleteClient={this.props.onDeleteClient}
                    onSeeRequest={this.props.onSeeRequest}
                    clients={[...this.props.clients]}
                    doctorId={this.props.doctorId}
                    error={this.props.error}
                    sortClients={this.props.onSortClients}
                    order={this.props.order}
                    chosen={this.props.chosen}
                    seconds={this.props.seconds}
                  />
                </div>
              </div>
            </Aux>}
          </div>
        </Content>
      </Aux>
    )
  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    childDoctors: state.authRoute.childDoctors,
    title: state.clients.title,
    clients: state.clients.clients,
    allClients: state.clients.allClients,
    clientCategories: state.clients.clientCategories,
    requestIndex: state.clients.requestIndex,
    mode: state.clients.mode,
    deleteClientId: state.clients.deleteClientId,
    deleteClientName: state.clients.deleteClientName,
    error: state.clients.error,
    deleteError: state.clients.deleteError,
    searchValue: state.clients.searchValue,
    order: state.clients.order,
    chosen: state.clients.chosen,
    titleId: state.clients.titleId,
    seconds: state.clients.seconds,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchClients: doctorId => dispatch(actions.fetchClients(doctorId)),
    onShowAllClients: () => dispatch(actions.showAllClients()),
    onDeleteClient: (clientId, name) => dispatch(actions.deleteClient(clientId, name)),
    onCloseModal: () => dispatch(actions.closeModal()),
    onRealDelete: (doctorId, clientId) => dispatch(actions.realClientDelete(doctorId, clientId)),
    onDeleteFinished: () => dispatch(actions.deleteClientFinished()),
    onClientSearchChange: (value) => dispatch(actions.clientSearchChange(value)),
    onCreatePlanForClient: (clientId) => dispatch(actions.createPlanForClient(clientId)),
    onSeeRequest: (index, first, clientId, doctorId) => dispatch(actions.seeRequest(index, first, clientId, doctorId)),
    onSortClients: (category) => dispatch(actions.sortClients(category)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Clients))