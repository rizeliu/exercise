import React, { Component } from 'react'
import { connect } from 'react-redux';
import Player from 'aliplayer-react';

import Title from '../../components/Title/Title'
import Content from '../../components/Content/Content'
import Categories from '../../components/Categories/Categories'
import SearchInput from '../../components/UI/Input/Input'
import SearchResults from '../../components/SearchResults/SearchResults'
import Modal from '../../components/UI/Modal/Modal'
import Spinner from '../../components/UI/Spinner/Spinner'
import Aux from '../../hoc/Aux/Aux'
import Question from '../../components/UI/Question/Question'
import Preview from '../../components/Preview/Preview'
import Button from "../../components/UI/Button/Button";
import * as actions from '../../store/actions/index'

import classes from './Exercises.css'
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";
import Input from "../../components/UI/Input/Input";
import axios from "axios";

class Exercises extends Component {

  state = {
    searchValue: "",
  }

  componentDidMount = async () => {
    document.title = "VOIN 制定计划或模板"
    this.props.onInit()
    var self = this
    if (!this.props.match.params.template) this.props.onFetchClients(this.props.doctorId)
    if (this.props.admin === 3) this.props.onSearchMoves(this.props.doctorId, "a", "a01", "a01a")
    if (this.props.admin === 5) this.props.onFetchClassesAndHomework(this.props.doctorId)
    if (this.props.username !== "halfrain" && !this.props.initDone) {
      const user = await axios.post('https://backend.voin-cn.com/addtime', {userId: "" + self.props.doctorId})
      if (user.data.code !== 0) {
        // const interval = setInterval(async function () {
        //   const tokenResponse = await axios.get('https://backend.voin-cn.com/checktoken',
        //     {
        //       params: {
        //         userId: "" + self.props.doctorId,
        //         token: user.data.token
        //       }
        //     })
        //   if (tokenResponse.data.code === 2) {
        //     clearInterval(interval)
        //     self.props.onAuthenticate({code: 2}, false, 2, "该用户已在其他地方登录。如需继续使用，请刷新后重新登录。")
        //   }
        // }, 10000)
      }
    }
    if (!this.props.initDone) this.props.onInitDone()
  }

  componentWillUnmount = () => {
    this.props.onUnmount()
  }

  searchAll = () => {
    const full = this.state.searchValue.split(/[ ,;]+/)
    const filter = full.filter(word => word.length > 0)
    const first5 = filter.splice(0, 5)
    if (first5.length > 0 && first5.length <= 5) this.props.onAllSearchChange(first5)
  }

  playVideo = (videoUrl, videoName) => {
    if (this.props.name) this.props.onPlayVideo(videoUrl, videoName)
    else this.props.onCompleteMessage()
  }

  render() {

    let searchResults = null
    if (this.props.mode === 0) {
      searchResults = <SearchResults
        play={this.playVideo}
        showVideo={this.props.showVideo}
        list={this.props.videoList ? [...this.props.videoList] : null}
        title={this.props.title}
        inSearch={this.props.inSearch}
      />
    } else if (this.props.mode === 1) {
      searchResults = <Spinner/>
    } else if (this.props.mode === 2) {
      searchResults = (
        <Aux>
          <div style={{margin: '15px'}}/>
          <Question>{this.props.error}</Question>
        </Aux>
      )
    }


    return (
      <Content edu={this.props.edu}>
        <div className="prism-player" id="J_prismPlayer"/>
        {this.props.showCompleteMessage ?
          <Modal show={this.props.showCompleteMessage} type="Video" modalClosed={this.props.onCloseMessage}>
            <Question>您的个人基本信息不完善，请您去完善信息</Question>
            <div style={{'textAlign': 'center'}}>
              <Button btnType="Danger" clicked={this.props.onCloseMessage}>取消</Button>
              <Button btnType="Success" clicked={() => {this.props.history.push('/account')}}>去完善</Button>
            </div>
          </Modal> : null}
          <Modal show={this.props.showVideo} type="Video" modalClosed={this.props.onCloseVideo}>

            <h3 style={{paddingBottom: '10px', textAlign: 'center', color: '#1E669E', margin: '0'}}>{this.props.videoName}</h3>
            {/*<iframe title={this.props.name} width="100%" height="337.5" src={"https://view.vzaar.com/"+this.props.videoUrl+"/player"} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen/>*/}

            <div className={classes.Player}>
            <Player
              config= {{
                source: this.props.videoUrl,
                width: "100%",
                height: "100%",
                autoplay: true,
                isLive: false,
                rePlay: false,
                playsinline: true,
                preload: true,
                controlBarVisibility: "hover",
                useH5Prism: true,
                components: [
              {
                name: "RateComponent",
                type: Player.components.RateComponent,
              }
                ]
              }}
              // onGetInstance={instance => this.setState({ instance })}
            />
            </div>
          </Modal>
        <Modal type="Video" show={this.props.introducing} modalClosed={this.props.onCloseTemplates}>
          <Title alignCenter>引入模板</Title>
          {this.props.introducingLoading ? <Spinner/> : <div>
            <table className={classes.Table}>
              <thead>
              <tr>
                <th style={{width: "20%"}}>选择</th>
                <th style={{width: "60%"}}>名称</th>
                <th style={{width: "20%"}}>动作数</th>
              </tr>
              </thead>
              <tbody>
              {this.props.templates.map((template, index) => (
                <tr key={index}>
                  <td className={classes.Center}>
                    <input type="radio" name="template" onClick={() => this.props.onChangeTemplateId(template._id)}/>
                  </td>
                  <td>{template.name}</td>
                  <td className={classes.Center}>{template.count}</td>
                </tr>
              ))}
              </tbody>
            </table>
            {this.props.introducingOneLoading ? <SingleLineSpinner /> : <div className={classes.Buttons}>
              <Button btnType="Success"
                      clicked={() => this.props.onIntroduceOneTemplate(this.props.doctorId, this.props.introduceTemplateId)}>确定</Button>
            </div>}
          </div>
          }
        </Modal>
        <Modal type="Video" show={this.props.proposalState === 1} modalClosed={this.props.onCloseProposal}>
          <div className={classes.NewTitle}>
            <Title>方案设计</Title>
            <div>
              <textarea value={this.props.proposalContent}
                        className={classes.Proposal} id="proposal" name="proposal" rows="15" cols="50"
                        onChange={event => this.props.onChangeProposal(event.target.value)}
              />
            </div>
            <div className={classes.ReferenceTitleLine}>
              <Title>参考文献</Title>
              <div className={classes.ButtonDiv}>
              <Button clicked={() => {this.props.onAddReference()}} btnType="Success">增加</Button>
              </div>
            </div>
            <div className={classes.References}>
              {this.props.references.map((reference, index) => {
                return <div className={classes.ReferenceLine} key={index}>
                  <Input smallPadding noVMargin smallRightMargin value={reference}
                         onChange={(event) => this.props.onChangeReference(index, event.target.value)}
                  />
                  <Button btnType="Danger" clicked={() => {this.props.onDeleteReference(index)}}>X</Button>
                </div>
              })}
            </div>

          </div>
        </Modal>
        <div className={classes.Package}>

          {this.props.fromEditPlan ? <div className={classes.Note}>您正在更改计划，我们会为您保存原计划运动信息</div> : null}
          {this.props.fromTemplate ? <div className={classes.Note}>您正在更改模版，我们会为您保存原模版运动信息</div> : null}
          {this.props.admin === 5 && this.props.titleId ?
            <div className={classes.Note}>您正在完成作业</div> : null}
          <div className={classes.Title}>
            {this.props.match.params.template === "template" ?
              <Title>{this.props.admin === 3 ? "Create a New Template" : "创建新的模板"}</Title> :
              <Title>{this.props.admin === 3 ? "Create a New Plan" : "创建新的计划"}</Title>}
            {/*<div className={classes.SearchButton}><Button btnType="Success" clicked={this.searchAll}>搜索</Button></div>*/}

            {this.props.admin === 4 ? null : this.props.match.params.template === "template" ?
              <div className={classes.Switch}>
                <Button btnType="Success" largeFont clicked={() => {this.props.onSwitchTo("plan", this)}}>切换至计划</Button>
              </div> :
              <div className={classes.Switch}>
                {!this.props.introducedTemplate ?
                  <Button btnType="Grey" largeFont clicked={() => {this.props.onIntroduceTemplate(this.props.doctorId)}}>引入模板</Button> : null}
                <Button btnType="Success" largeFont clicked={() => {this.props.onSwitchTo("template", this)}}>切换至模板</Button>
              </div>}
          </div>
          <div className={classes.Search}>
          <SearchInput noBorder search placeholder="所有运动搜索" value={this.state.searchValue} onChange={event => {
            this.setState({searchValue: event.target.value})
          }} onKeyPress={event => {
            if (event.key === 'Enter') {
              this.searchAll()
            }
          }}/>
          </div>
          <div className={classes.Content}>
            <div className={classes.First}><Categories/></div>
            <div className={classes.Middle}>

              {searchResults}
            </div>
            <Preview template={this.props.match.params.template === "template"}/>
        </div>
        </div>
      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    name: state.authRoute.name,
    username: state.authRoute.username,
    token: state.authRoute.token,
    initDone: state.exercise.initDone,
    idnum: state.authRoute.idnum,
    admin: state.authRoute.admin,
    showVideo: state.exercise.showVideo,
    showCompleteMessage: state.exercise.showCompleteMessage,
    videoList: state.categories.videoList,
    videoUrl: state.exercise.videoUrl,
    mode: state.categories.mode,
    error: state.categories.error,
    videoName: state.exercise.videoName,
    title: state.categories.title,
    inSearch: state.categories.inSearch,
    fromEditPlan: state.exercise.fromEditPlan,
    fromTemplate: state.exercise.fromTemplate,
    empty: state.exercise.empty,
    templates: state.exercise.templates,
    introducing: state.exercise.introducing,
    introducingLoading: state.exercise.introducingLoading,
    introduceTemplateId: state.exercise.introduceTemplateId,
    introducingOneLoading: state.exercise.introducingOneLoading,
    introducedTemplate: state.exercise.introducedTemplate,
    titleId: state.exercise.titleId,
    proposalState: state.exercise.proposalState,
    proposalContent: state.exercise.proposalContent,
    references: state.exercise.references
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSwitchTo: (mode, web) => dispatch(actions.switchTo(mode, web)),
    onIntroduceTemplate: (doctorId) => dispatch(actions.introduceTemplate(doctorId)),
    onCloseTemplates: () => dispatch(actions.closeTemplates()),
    onCloseProposal: () => dispatch(actions.closeProposal()),
    onIntroduceOneTemplate: (doctorId, templateId) => dispatch(actions.introduceOneTemplate(doctorId, templateId)),
    onChangeTemplateId: (templateId) => dispatch(actions.changeTemplateId(templateId)),
    onDeleteReference: (index) => dispatch(actions.deleteReference(index)),
    onChangeReference: (index, value) => dispatch(actions.changeReference(index, value)),
    onAddReference: () => dispatch(actions.addReference()),
    onChangeProposal: (value) => dispatch(actions.changeProposal(value)),
    onPlayVideo: (videoUrl, videoName) => dispatch(actions.playVideo(videoUrl, videoName)),
    onCompleteMessage: () => dispatch(actions.completeMessage()),
    onCloseVideo: () => dispatch(actions.closeVideo()),
    onCloseMessage: () => dispatch(actions.closeMessage()),
    onAllSearchChange: first5 => dispatch(actions.allSearchChange(first5)),
    onInit: () => dispatch(actions.exerciseInit()),
    onInitDone: () => dispatch(actions.exerciseInitDone()),
    onFetchClients: doctorId => dispatch(actions.fetchClients(doctorId)),
    onUnmount : () => dispatch(actions.exerciseUnmount()),
    onSearchMoves: (doctorId, a, b, c) => dispatch(actions.searchMoves(doctorId, a, b, c)),
    onFetchClassesAndHomework: doctorId => dispatch(actions.fetchClassesAndHomework(doctorId)),
    onAuthenticate: (info, login, mode, error, admin, refer) => dispatch(actions.authenticate(info, login, mode, error, admin, refer)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Exercises)