import React, { Component } from 'react'
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import * as actions from "../../store/actions";
import Spinner from "../../components/UI/Spinner/Spinner";
import Content from "../../components/Content/Content";
import Title from "../../components/Title/Title";
import Question from "../../components/UI/Question/Question";
import Button from "../../components/UI/Button/Button";

import classes from './Homework.css'
import Modal from "../../components/UI/Modal/Modal";

class Homework extends Component {

  state = {
    seeTitleId: "",
    seeReviewTitleId: "",
    picture: "",
    pictureName: ""
  }

  componentDidMount = () => {
    document.title = "VOIN 作业"
    this.props.onFetchClassesAndHomework(this.props.doctorId)
  }

  modalClosed = () => {
    this.setState({
      seeTitleId: "",
      seeReviewTitleId: ""
    })
    this.props.onModalClosed()

  }

  completeHomework = titleId => {
    this.props.history.push("/exercises")
    this.props.onCompleteHomework(titleId)
  }

  seeReview = titleId => {
    this.setState({seeReviewTitleId: titleId})
  }

  // showRecord = picture => {
  //   this.setState({picture: picture.name})
  // }

  render() {

    let homework = {}
    let review = {}
    if (this.state.seeTitleId) {
      homework = this.props.titles.filter(title => this.state.seeTitleId === title._id)[0]
    }
    if (this.state.seeReviewTitleId) {
      homework = this.props.titles.filter(title => this.state.seeReviewTitleId === title._id)[0]
      review = this.props.submits.filter(title => title.titleId === this.state.seeReviewTitleId)[0]
    }
    let modal = null
    if (this.state.picture) {
      modal = <Modal show={this.state.picture} type="Record" modalClosed={() => this.showRecord("")}>
        <img alt="" src={"https://backend.voin-cn.com/"+this.state.picture} style={{maxWidth: '100%'}}/>
      </Modal>
    }

    return (
      <Content padding>
        <Modal type="Review" show={this.state.seeTitleId} modalClosed={this.modalClosed}>
          <div className={classes.NewTitle}>
            <Title alignCenter>{"作业题目：" + homework.title}</Title>
            <table className={classes.DesTable}>
              <tbody>
              <tr>
                <td className={classes.Left}>患者姓名：</td>
                <td className={classes.Right}>{homework.name}</td>
              </tr>
              <tr>
                <td className={classes.Left}>性别：</td>
                <td className={classes.Right}>{homework.sex}</td>
              </tr>
              <tr>
                <td className={classes.Left}>年龄：</td>
                <td className={classes.Right}>{homework.age}</td>
              </tr>
              <tr>
                <td className={classes.Left}>既往病史：</td>
                <td className={classes.Right}>{homework.history}</td>
              </tr>
              <tr>
                <td className={classes.Left}>吸烟室：</td>
                <td className={classes.Right}>{homework.smoke}</td>
              </tr>
              <tr>
                <td className={classes.Left}>详细要求：</td>
                <td className={classes.Right}>{homework.description}</td>
              </tr>
              <tr>
                <td className={classes.Left}>图片：</td>
                <td className={classes.Right}>{homework && homework.pictures ? <div className={classes.Container}>
                  {homework.pictures.map((picture, index) => <div key={index}
                                                                  className={classes.RecordDiv}>
                      <img alt="" onClick={() => this.setState({pictureName: picture.name})}
                           className={classes.Record} src={"https://backend.voin-cn.com/"+picture.name}/>
                    </div>)}
                  </div> : null}
                  {this.state.pictureName.length > 0 ? <div>
                    <TransformWrapper wrapperStyle={"width: 100%"}>
                      <TransformComponent>
                        <img alt="" className={classes.BigRecord} src={"https://backend.voin-cn.com/"+this.state.pictureName} />
                      </TransformComponent>
                    </TransformWrapper>
                  </div> : null}
                </td>

              </tr>
              </tbody>

            </table>
          </div>
        </Modal>
        <Modal show={this.state.seeReviewTitleId} modalClosed={this.modalClosed}>
          <div className={classes.NewTitle}>
            <Title alignCenter>查看评分</Title>
            <div className={classes.Line}>
              <div>作业题目：{homework.title}</div>
            </div>
            <table className={classes.DesTable}>
              <tbody>
              <tr>
                <td className={classes.Left}>分数：</td>
                <td className={classes.Right}>{review.score}</td>
              </tr>
              <tr>
                <td className={classes.Left}>点评：</td>
                <td className={classes.Right}>{review.review}</td>
              </tr>
              </tbody>
            </table>
          </div>
        </Modal>
        {modal}

        <div className={classes.Package}>
        <Title>作业信息</Title>
        {this.props.mode === 1 ? <Spinner grey/> : this.props.fetchError ?
          <Question>{this.props.fetchError}</Question> : <div>
            <div className={classes.Class}>
              <div>班级名称: {this.props.classInfo.name}</div>
              <div>老师: {this.props.teacher.name}</div>
            </div>
            {this.props.titles.length > 0 ?
              <div className={classes.TableDiv}>
              <table className={classes.Table}>
                <thead>
                <tr>
                  <th>作业题目</th>
                  <th>详细要求</th>
                  <th>操作</th>
                  <th>评价</th>
                </tr>
                </thead>
                <tbody>

                {this.props.titles.map(title => {
                  const submitOne = this.props.submits.filter(submit => submit.titleId === title._id)
                  const isSubmitted = submitOne.length > 0
                  const isReviewed = isSubmitted && submitOne[0].score
                  return (
                    <tr key={title._id}>
                      <td>{title.title}</td>
                      <td><div className={classes.Clickable} onClick={() => this.setState({seeTitleId: title._id, pictureName: ""})}>查看</div></td>
                      <td>{isSubmitted ? <div className={classes.Clickable} onClick={() =>
                          this.props.onSeeHomeworkDetail(this.props.doctorId, submitOne[0].titleId)}>查看已提交</div>
                        : <Button btnType="Success" clicked={() => this.completeHomework(title._id)}>去完成</Button>}</td>
                      <td>{isReviewed ?
                        <div className={classes.Clickable} onClick={() => this.seeReview(title._id)}>查看</div>
                         : <div>未评价</div>}</td>
                    </tr>
                  )
                })}

                </tbody>
              </table></div> : <div className={classes.NoHomework}><Question>目前没有作业</Question></div>}
          </div>}
        </div>
      </Content>
    )

  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    mode: state.homework.mode,
    teacher: state.homework.teacher,
    titles: state.homework.titles,
    classInfo: state.homework.classInfo,
    submits: state.homework.submits,
    fetchError: state.homework.fetchError,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchClassesAndHomework: doctorId => dispatch(actions.fetchClassesAndHomework(doctorId)),
    onCompleteHomework: titleId => dispatch(actions.completeHomework(titleId)),
    onSeeHomeworkDetail: (doctorId, titleId) => dispatch(actions.seeHomeworkDetail(doctorId, titleId)),
    onModalClosed: () => dispatch(actions.modalClosed()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Homework))