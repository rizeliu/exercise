import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import isAlphanumeric from 'is-alphanumeric'

import Title from '../../components/Title/Title'
import Content from '../../components/Content/Content'
import TemplateTable from '../../components/TemplateTable/TemplateTable'
import Button from '../../components/UI/Button/Button'
import Spinner from '../../components/UI/Spinner/Spinner'
import Modal from '../../components/UI/Modal/Modal'
import Aux from '../../hoc/Aux/Aux'
import Question from '../../components/UI/Question/Question'
import * as actions from '../../store/actions/index';
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";
import SearchInput from "../../components/UI/Input/Input";
import Input from "../../components/UI/Input/Input";
import ClientCategory from "../../components/UI/ClientCategory/ClientCategory";
import axios from "axios";
import classes from './Templates.css'

class Templates extends Component {

  state = {
    createTeam: false,
    joinTeam: false,
    name: "",
    username: "",
    password: "",
    errors: [],
    errorOrSpin: "",
    confirm: false
  }

  componentDidUpdate = () => {
    if (this.props.mode === 5) {
      this.props.onFetchTemplates(this.props.doctorId)
      // this.props.onFetchTeamTemplate(this.props.refer, this.props.referee, this.props.username)
      this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
    }
    if (this.props.clientMode === 5) {
      console.log("here")
      this.props.onFetchTemplates(this.props.doctorId)
      // this.props.onFetchTeamTemplate(this.props.refer, this.props.referee, this.props.username)
      this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
    }
    if (this.props.mode === 27) {
      this.props.onFetchTemplates(this.props.doctorId)
      // this.props.onFetchTeamTemplate(this.props.refer, this.props.referee, this.props.username)
      this.props.onFetchTeamsAndFirst(this.props.doctorId)
      this.changeState(1)
    }
    if (this.props.mode === 8) {
      this.props.onFetchDoctors()
    }
  }

  componentDidMount = () => {
    document.title = "VOIN 模板"
    this.props.onFetchTemplates(this.props.doctorId)
    // if (this.props.name) this.props.onFetchTeamTemplate(this.props.refer, this.props.referee, this.props.username)
    this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
    if (this.props.circle) {
      this.props.onGetCircle()
    }
  }


  // createPlanByTemplate = (templateId, templateName, coveredMoves) => {
  //   this.props.history.push('/exercises')
  //   this.props.onCreatePlanByTemplate(templateId, templateName, coveredMoves)
  // }

  changeState = number => {
    this.setState({errors: [], error: "", mode: 0})
    this.props.onChangeContent(number)
  }

  closeModal = () => {
    this.setState({
      createTeam: false, name: "", username: "", password: "", errors: [], errorOrSpin: "", confirm: false,
      joinTeam: false
    })
  }

  inputChangeHandler = (event, id) => {
    const value = event.target.value
    this.setState({[id]: value})
  }

  createTeam = async () => {
    this.setState({ errors: [] })
    let error = ""
    if (this.state.name.length === 0) error += "团队名称不能为空 "
    if (this.state.username.length === 0) error += "团队代号不能为空 "
    if (!isAlphanumeric(this.state.username)) error += "团队代号只能为字母或数字 "
    if (this.state.password.length < 6) error += "密码长度不得少于6位 "
    if (error.length === 0) {
      this.setState({errorOrSpin: "spin"})
      const response = await axios.post('https://backend.voin-cn.com/createteam', {
        doctorId: this.props.doctorId,
        name: this.state.name,
        username: this.state.username,
        password: this.state.password
      })
      this.setState({errorOrSpin: response.data.msg})
      if (response.data.code === 1) {
        this.setState({confirm: true})
      }
    } else {
      const errors = error.split(" ")
      errors.pop()
      this.setState({ errors })
    }
  }

  joinTeam = async () => {
    if (this.state.username.length > 0 && this.state.password.length > 0) {
      this.setState({errorOrSpin: "spin"})
      const response = await axios.post('https://backend.voin-cn.com/jointeam', {
        doctorId: this.props.doctorId,
        username: this.state.username,
        password: this.state.password
      })
      this.setState({errorOrSpin: response.data.msg})
      if (response.data.code === 1) {
        this.setState({confirm: true})
      }
    }
  }

  render() {
    let deleteQuestion = (
      <Modal modalClosed={this.props.onCloseModal} type="Warning" show={this.props.mode === 2}>
        <div className={classes.Center}>
          <p style={{marginTop: "0"}}>您确定要删除"{this.props.deleteTemplateName}"这个模板吗？注意：这个操作无法还原。</p>
          <Button btnType="Grey" clicked={this.props.onCloseModal}>取消</Button>
          <Button btnType="Success" clicked={() => this.props.onRealDelete(
            this.props.deleteMy, this.props.teamId, this.props.doctorId, this.props.deleteTemplateId, this.props.admin)}>继续</Button>
        </div>
      </Modal>
    )
    let templateQR = (
      <Modal modalClosed={this.props.onCloseModal} type="Warning" show={this.props.mode >= 28 && this.props.mode <= 30}>
        <div className={classes.Center}>
          <p style={{marginTop: "0"}}>{this.props.createQRTemplateName + "二维码"}</p>
          {this.props.mode === 28 ? <Spinner></Spinner> :
              this.props.mode === 29 ?
                  <img className={classes.Img} src={"data:image/png;base64,"+this.props.base64Str} alt="qrcode"/> :
                  <Question>{this.props.createQRError}</Question>}
        </div>
      </Modal>
    )
    if (this.props.mode === 6) {
      deleteQuestion = <Modal modalClosed={this.props.onDeleteFinished} show={this.props.mode === 6}>
        <Question>{this.props.deleteError}</Question>
      </Modal>
    }
    if (this.props.mode === 3) {
      deleteQuestion = <Modal modalClosed={this.props.onCloseModal} show={this.props.mode === 3}>
        <Spinner/>
      </Modal>
    }
    if (this.props.mode === 4) {
      deleteQuestion = <Modal modalClosed={this.props.onDeleteFinished} show={this.props.mode === 4}>
        <Question>该模板已删除</Question>
        <div className={classes.Center}><Button btnType="Success" clicked={this.props.onDeleteFinished}>完成</Button></div>
      </Modal>
    }
    if (this.props.mode === 7) {
      deleteQuestion = <Modal modalClosed={this.props.onDeleteFinished} show={this.props.mode === 7}>
        <Question>该模板已删除</Question>
        <div className={classes.Center}><Button btnType="Success" clicked={this.props.onDeleteFinished}>完成</Button></div>
      </Modal>
    }
    let send = null
    if (this.props.mode >= 8 && this.props.mode <= 12) {
      send = <Modal type="Review"
                    modalClosed={this.props.onCloseModal}
                    show={this.props.mode >= 8 && this.props.mode <= 12}>
        <div className={classes.SendPackage}>
        <Title alignCenter>您将要发送模板"{this.props.sendTemplateName}"，请选择医生</Title>
        <SearchInput placeholder="搜索医生" onChange={event => {
          this.props.onSearchDoctors(event.target.value)
        }} value={this.props.doctorSearchValue}/>
        {this.props.mode === 10 ? <Spinner/> : this.props.sendError ?
          <Question>{this.props.sendError}</Question> :
          <div className={classes.Doctors}>
          {this.props.doctors.map(doctor => {
            return (
              <div key={doctor.username} className={classes.Doctor}>
                <input type="checkbox" id={doctor.username} name="doctors" value={doctor._id}
                       checked={this.props.sendingDoctorIds.indexOf(doctor._id) >= 0}
                       onChange={event => this.props.onChangeDoctor(event.target.value)}/>
                <label htmlFor={doctor.username}> {doctor.name}（{doctor.username}）</label>
              </div>
            )
          })}
        </div>}
        <div className={classes.Center}>
          {this.props.mode === 11 ? <SingleLineSpinner/> : null}
          {this.props.mode === 12 && this.props.realSendError ?
            <Question>{this.props.realSendError}</Question> : null
          }
          {this.props.mode === 12 && !this.props.realSendError ?
            <Question>保存成功</Question> : null
          }
          {this.props.mode === 12 ? <Button
            clicked={this.props.onCloseModal}
            btnType="Success">确定</Button> : null}
          {this.props.mode === 9 ? <Button btnType="Success"
                  clicked={() => {
                    if (this.props.sendingDoctorIds.length > 0 && this.props.mode === 9) {
                      this.props.onRealSendTemplate(
                        this.props.doctorId, this.props.sendTemplateId, this.props.sendingDoctorIds
                      )
                    }
                  }}
          >确定</Button> : null}
        </div>
        </div>
      </Modal>
    }
    if (this.props.mode >= 13 && this.props.mode <= 15) {
      send = <Modal type={this.props.admin === 4 ? "" : "Video"}
                    modalClosed={() => {
                      if (this.props.mode === 15) {
                        this.changeState(2)
                        this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
                        if (this.props.labels.length > 0) this.props.onTeamChange(this.props.labels[0])
                      }
                      this.props.onCloseModal()
                    }}
                    show={this.props.mode >= 13 && this.props.mode <= 15}>
        {this.props.admin === 4 ? <div className={classes.AddTemplate}>您将要上传模板"{this.props.addTemplateName}"到班级模板</div> :
          <div className={classes.AddTemplate}>您将要上传模板"{this.props.addTemplateName}"，请选择</div>}
        <div className={classes.Groups}>
          {/*{this.props.admin === 1 ? <div className={classes.ExpertGroup}>*/}
            {/*<Title>到专家模板</Title>*/}
            {/*<div className={classes.Labels}>*/}
              {/*{this.props.initial.map(i => <div key={i}>*/}
                {/*<div className={classes.I}>{this.props.map[i]}</div>*/}
                {/*<div>{Object.keys(this.props.overallInfo[i]).map(j => {*/}
                    {/*return <div key={j}> {this.props.overallInfo[i][j].length > 0 ?*/}
                      {/*<div>*/}
                        {/*<div className={classes.NonJ}>{this.props.map[j]}</div>*/}
                        {/*<div className={classes.KParent}>*/}
                          {/*{this.props.overallInfo[i][j].map(k => <div key={k} className={classes.K}>*/}
                            {/*<input type="checkbox" id={k} name="expert" value={k}*/}
                                   {/*checked={this.props.labels.indexOf(k) > -1}*/}
                                   {/*onChange={event => this.props.onChangeLabel(event.target.name, event.target.value)}/>*/}
                            {/*<label htmlFor={k}>{this.props.map[k]}</label></div>)}*/}
                        {/*</div>*/}
                      {/*</div> :*/}
                      {/*<div className={classes.RadioJ}>*/}
                        {/*<input type="checkbox" id={j} name="expert" value={j}*/}
                               {/*checked={this.props.labels.indexOf(j) > -1}*/}
                               {/*onChange={event => this.props.onChangeLabel(event.target.name, event.target.value)}/>*/}
                        {/*<label htmlFor={j}>{this.props.map[j]}</label>*/}
                      {/*</div>*/}
                    {/*}</div>*/}
                  {/*}*/}
                {/*)}</div>*/}
              {/*</div>)}*/}
            {/*</div>*/}
          {/*</div> : null}*/}
          {this.props.teams.filter(team => team.creator).length > 0 ? <div className={classes.TeamGroup}>
            {this.props.admin === 4 ? null : <Title>到团队模板</Title>}
            {this.props.admin === 4 ? null : <div className={classes.Labels}>
              {this.props.teams.filter(team => team.creator).map(team => <div className={classes.RadioTeam} key={team.teamId}>
                <input type="radio" id={team.teamId} name="team" value={team.teamId}
                       checked={this.props.labels.indexOf(team.teamId) > -1}
                       onChange={event => this.props.onChangeLabel(event.target.name, event.target.value)}/>
                <label htmlFor={team.teamId}>{team.name+"（代号："+team.username+"）"}</label>
              </div>)}
            </div>}
          </div> : null}
        </div>


        <div className={classes.Center}>
          {this.props.mode === 14 ? <SingleLineSpinner/> : null}
          {this.props.mode === 15 && this.props.realAddExpertError ?
            <Question>{this.props.realAddExpertError}</Question> : null
          }
          {this.props.mode === 15 && !this.props.realAddExpertError ?
            <Question>上传成功</Question> : null
          }
          {this.props.mode === 15 ? <Button
            clicked={() => {
              this.props.onCloseModal()
              this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
            }}
            btnType="Success">确定</Button> : null}
          {this.props.mode === 13 ? <Button btnType="Success"
                                           clicked={() => {
                                             if (this.props.admin === 4) {
                                               this.props.onRealAddClass(
                                                 this.props.doctorId, this.props.addTemplateId, this.props.teamId
                                               )
                                             } else if (this.props.labels.length > 0 && this.props.mode === 13) {
                                               this.props.onRealAddExpertOrTeam(
                                                 this.props.doctorId, this.props.labelName, this.props.labels, this.props.addTemplateId
                                               )
                                             }
                                           }}
          >确定</Button> : null}
        </div>
      </Modal>
    } else if (this.props.mode >= 16 && this.props.mode <= 18) {
      send = <Modal show={this.props.mode >= 16 && this.props.mode <= 18}
                    modalClosed={this.props.onCloseModal}>
        <div className={classes.ModalDiv}>
          {this.props.deleteMemberError ? <Question>{this.props.deleteMemberError}</Question> :
            this.props.deleteMemberSuccess ? <Question>踢出成员成功</Question> :
            this.props.mode === 16 ? <Spinner/> : this.props.mode === 17 ?
          <Aux>
            <Title alignCenter>{this.props.teams.filter(team => team.teamId === this.props.teamId)[0].name}团队成员</Title>
            <ul className={classes.Members}>
              {this.props.teamMembers.map((member, index) =>
                <li key={index}>
                  <div className={classes.MemberRow}>
                    <span>{member.name+" ("+member.username+")"}</span>
                    {this.props.username === member.username ? null :
                      <Button btnType="Grey"
                              clicked={() => this.props.onDeleteTeamMember(member.doctorId)}>
                        踢出</Button>}
                  </div>
                </li>)}
            </ul>
            {this.props.deleteDoctorId === "spin" ? <SingleLineSpinner/> : this.props.deleteDoctorId ?
              <Question>您确定要踢出{this.props.teamMembers.filter(member => member.doctorId === this.props.deleteDoctorId)[0].name}吗？
                <Button btnType="Success"
                        clicked={() => this.props.onRealDeleteTeamMember(this.props.teamId, this.props.deleteDoctorId)}
                >确定</Button></Question> : null}
          </Aux> : <Question>{this.props.teamMembersError}</Question>}
        </div>
      </Modal>
    } else if (this.props.mode >= 19 && this.props.mode <= 22) {
      send = <Modal show={this.props.mode >= 19 && this.props.mode <= 22}
                    modalClosed={() => {
                      if (this.props.mode >= 20 && this.props.mode <= 22) {
                        this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
                      }
                      this.props.onCloseModal()
                    }}>
        {this.props.mode === 21 ? <Question>您已离开此团队</Question> :
          this.props.mode === 22 ? <Question>{this.props.leaveTeamError}</Question> :
          <div className={classes.LeaveModal}>
          <Question>您确定要离开"{this.props.teams.filter(team => team.teamId === this.props.teamId)[0].name}"团队吗？</Question>
          {this.props.mode === 20 ? <SingleLineSpinner/> :
            <Button btnType="Success"
                    clicked={() => this.props.onRealLeaveTeam(this.props.teamId, this.props.doctorId)}>确定</Button>}
        </div>}
      </Modal>
    } else if (this.props.mode >= 23 && this.props.mode <= 26) {
      send = <Modal show={this.props.mode >= 23 && this.props.mode <= 26}
                    modalClosed={() => {
                      if (this.props.mode >= 24 && this.props.mode <= 26) {
                        this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
                      }
                      this.props.onCloseModal()
                    }}>
        {this.props.mode === 25 ? <Question>您已删除此团队</Question> :
          this.props.mode === 26 ? <Question>{this.props.deleteTeamError}</Question> :
            <div className={classes.LeaveModal}>
              <Question>您确定要删除"{this.props.teams.filter(team => team.teamId === this.props.teamId)[0].name}"团队吗？该团队所有模板都将被删除，所有团队成员都会离开。</Question>
              {this.props.mode === 24 ? <SingleLineSpinner/> :
                <Button btnType="Success"
                        clicked={() => this.props.onRealDeleteTeam(this.props.teamId)}>确定</Button>}
            </div>}
      </Modal>
    } else if (this.props.mode >= 31 ) {
      send = <Modal type="Video" show={this.props.mode >= 31}
                    modalClosed={() => {
                      if (this.props.mode >= 32) {
                        this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
                        if (this.props.circle) {
                          this.props.onGetCircle()
                        }
                      }
                      this.props.onCloseModal()
                    }}>
        <Title>将{this.props.addCircleTemplateName}模板加入圈子</Title>

        {this.props.circles.map(circle => {
          const templatesLeft = circle.templates.filter(template => {
            if (!template.doctorId) return this.props.doctorId === "5f23a8e26266b8739f89de86"
              && template.templateId === this.props.addCircleTemplateId
            else return template.doctorId === this.props.doctorId
              && template.templateId === this.props.addCircleTemplateId
          })
          return <div className={classes.RadioTeam} key={circle.xiaoeId}>
            <input type="radio" id={circle.xiaoeId} name={circle.name} value={circle.xiaoeId}
                   checked={this.props.chosenCircleId === circle.xiaoeId || templatesLeft.length > 0}
                   disabled={templatesLeft.length > 0}
                   onChange={event => this.props.onChangeCircle(event.target.name, event.target.value)}/>
            <label htmlFor={circle.xiaoeId}>{circle.name}</label>
          </div>})}
        {this.props.mode === 32 ? <SingleLineSpinner/> : null}
        {this.props.mode === 33 ? <Question>添加成功</Question> : null}
        <Button btnType="Success"
                disabled={this.props.mode >= 32}
                clicked={() => this.props.onRealAddTemplateToCircle(this.props.doctorId, this.props.addCircleTemplateId, this.props.chosenCircleId)}>确定</Button>
      </Modal>
    }

    let createTeam = null
    if (this.state.createTeam) {
      createTeam = <Modal show={this.state.createTeam} modalClosed={() => {
        if (this.state.confirm) {
          this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
        }
        this.closeModal()
      }}>
        <div className={classes.Modal}>
          <Title alignCenter>创建新团队</Title>
          <Input id="name" label="团队名称" placeholder="团队名称不能为空"
                 onChange={this.inputChangeHandler} value={this.state.name}/>
          <Input id="username" label="团队代号" placeholder="团队代号只能为字母或数字"
                 onChange={this.inputChangeHandler} value={this.state.username}/>
          <Input id="password" label="密码" placeholder="密码长度不得少于6位"
                 onChange={this.inputChangeHandler} value={this.state.password} password/>
          <ul className={classes.Errors}>
            {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
          </ul>
          {this.state.errorOrSpin ? this.state.errorOrSpin === "spin" ? <SingleLineSpinner/> : <Question>{this.state.errorOrSpin}</Question> : null}
          <div className={classes.Buttons}>
            {this.state.confirm ? <Button btnType="Success" clicked={() => {
                this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
                this.closeModal()
              }}>确定</Button> :
              <Button btnType="Success" clicked={this.createTeam}>创建</Button>}
          </div>
        </div>
      </Modal>
    }

    let joinTeam = null
    if (this.state.joinTeam) {
      createTeam = <Modal show={this.state.joinTeam} modalClosed={() => {
        if (this.state.confirm) {
          this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
        }
        this.closeModal()
      }}>
        <div className={classes.Modal}>
          <Title alignCenter>加入团队</Title>
          <Input id="username" label="团队代号"
                 onChange={this.inputChangeHandler} value={this.state.username}/>
          <Input id="password" label="密码"
                 onChange={this.inputChangeHandler} value={this.state.password} password/>
          {this.state.errorOrSpin ? this.state.errorOrSpin === "spin" ? <SingleLineSpinner/> : <Question>{this.state.errorOrSpin}</Question> : null}
          <div className={classes.Buttons}>
            {this.state.confirm ? <Button btnType="Success" clicked={() => {
                this.props.onFetchTeamsAndFirst(this.props.doctorId, this.props.admin)
                this.closeModal()
              }}>确定</Button> :
              <Button btnType="Success" clicked={this.joinTeam}>加入</Button>}
          </div>
        </div>
      </Modal>
    }

    let chosenTeam
    if (this.props.teams.length > 0 && this.props.teamId) {
      chosenTeam = this.props.teams.filter(team => team.teamId === this.props.teamId)[0]
    }

    return (
      <Aux>
        {send}
        {deleteQuestion}
        {templateQR}
        {createTeam}
        {joinTeam}
        <Content padding width={this.props.admin === 5 ? "60" : ""}>
          <div className={classes.Package}>
            <div className={classes.Types}>
              <div className={[classes.Type, this.props.content === 1 ? classes.Chosen : ""].join(" ")} onClick={() => this.changeState(1)}>我的模板</div>
              <div className={[classes.Type, this.props.content === 2 ? classes.Chosen : ""].join(" ")} onClick={() => this.changeState(2)}>
                {this.props.admin <= 2 ? "团队模板" : "班级模板"}
              </div>
              {this.props.username === "voin100" ? <div className={[classes.Type, this.props.content === 3 ? classes.Chosen : ""].join(" ")} onClick={() => this.changeState(3)}>
                圈子
              </div> : null}
            </div>
          {this.props.content === 1 ? this.props.mode === 1 ? <Spinner grey/> : <div>
            <div className={classes.Title}>
              <Title>我的模板</Title>
              <div style={{padding: '10px 0'}}>
                <Button btnType="Grey" largeFont clicked={this.props.onShowAllTemplates}>所有模版</Button>
                <Button btnType="Success" largeFont clicked={() => this.props.onCreateNewTemplate(this)}>添加新模板</Button>
              </div>
            </div>
            <SearchInput placeholder="搜索模板" onChange={event => {
              this.props.onSearchTemplateChange(event.target.value)
            }} value={this.props.templateSearchValue}/>
            <div className={classes.Pack}>
              <ClientCategory type="template"/>
              <div className={[classes.MyTemplate, this.props.focusOnMy ? classes.Focus : classes.NotFocus].join(" ")}>
                {this.props.templateTitle ? <Title>{this.props.templateTitle}</Title> : null}
                {this.props.chosen ? <Title>"{this.props.clientCategories.filter(cat => cat._id === this.props.titleId)[0].name}"分类中的模板</Title> : null}
                {!this.props.templateTitle && !this.props.chosen ? <div style={{marginTop: "45px"}}/> : null}
                <TemplateTable
                  my
                  expertOrTeam=""
                  referee={this.props.referee}
                  focus={true}
                  admin={this.props.admin}
                  qrcode={this.props.qrcode}
                  circle={this.props.circle}
                  doctorId={this.props.doctorId}
                  username={this.props.username}
                  onDeleteTemplate={this.props.onDeleteTemplate}
                  onCreateQRCode={this.props.onCreateQRCode}
                  onCreatePlanByTemplate={this.props.onCreatePlanByTemplate}
                  onShowTemplateDetail={this.props.onShowTemplateDetail}
                  onAddExpert={this.props.onAddExpert}
                  onAddTeam={this.props.onAddTeam}
                  onAddExpertOrTeam={this.props.onAddExpertOrTeam}
                  onAddTemplateToCircle={this.props.onAddTemplateToCircle}
                  onSendTemplate={this.props.onSendTemplate}
                  templates={[...this.props.templates]}
                  teams={[...this.props.teams]}
                  onEditTemplate={this.props.onEditTemplate}
                  page={this}
                  error1={this.props.error}/>
              </div>
            </div>
          </div> : this.props.content === 2 ?
            <Aux>
              <div className={classes.Title}>
                {this.props.admin >= 4 ? <Title>班级模板</Title> : <Title>团队模板</Title>}
                {this.props.admin >= 4 ? null :<div className={classes.CreateTeamButton}>
                  <Button btnType="Success" largeFont clicked={() => {this.setState({createTeam: true})}}>创建团队</Button>
                  <Button btnType="Grey" largeFont clicked={() => {this.setState({joinTeam: true})}}>加入团队</Button>
                </div>}
              </div>
              <div>
                {this.props.teamMode === 1 ? <Spinner grey/> :
                  this.props.teams.length > 0 ?
                    <div className={classes.Flex}>
                      {this.props.admin >= 4 ? null :<div className={classes.First}>
                        <Title>选择团队</Title>
                        <div className={classes.TeamSelect}>
                          <div className={classes.CreatorButtons}>
                            {this.props.teams.filter(team => team.teamId === this.props.teamId)[0].creator ?
                              <Aux>
                                <Button btnType="Success" clicked={() => this.props.onSeeTeamMembers(this.props.teamId)}>查看成员</Button>
                                <Button btnType="Grey" clicked={this.props.onDeleteTeam}>删除团队</Button>
                              </Aux>
                              : <Button btnType="Grey" clicked={this.props.onLeaveTeam}>退出团队</Button>}
                          </div>
                          <div>
                            <ul className={classes.Teams}>
                              {this.props.teams.map(team =>
                                <li key={team.teamId} className={[classes.Team, this.props.teamId === team.teamId ? classes.ChosenTeam : ""].join(" ")}
                                    onClick={() => this.props.onTeamChange(team.teamId)}>
                                  {team.name}{" ("+team.username+")"}{team.creator ? " (创建者)" : null}
                                </li>)}
                            </ul>
                          </div>
                          {/*<select id="team" className={classes.Select}*/}
                                  {/*value={this.props.teamId}*/}
                                  {/*onChange={event => this.props.onTeamChange(event.target.value)}>*/}
                            {/*{this.props.teams.map(team => <option key={team.teamId} value={team.teamId}>*/}
                              {/*{team.name}{" ("+team.username+")"}{team.creator ? " (创建者)" : null}*/}
                            {/*</option>)}*/}
                          {/*</select>*/}
                        </div>
                      </div>}
                      <div className={this.props.admin >= 4 ? classes.ClassTemplate : classes.TeamTemplate}>
                        {this.props.admin >= 4 ? null :
                          <Title>{chosenTeam ? chosenTeam.name + "（" + chosenTeam.username + "）中的模板" : "未选中团队"}</Title>}
                        {this.props.teamTemplates.length > 0 ?
                          <TemplateTable
                            focus={true}
                            creator={this.props.teams.filter(team => team.teamId === this.props.teamId)[0].creator}
                            expertOrTeam="team"
                            teamId={this.props.teamId}
                            admin={this.props.admin}
                            qrcode={this.props.qrcode}
                            circle={this.props.circle}
                            doctorId={this.props.doctorId}
                            onDeleteTemplate={this.props.onDeleteTemplate}
                            onShowTemplateDetail={this.props.onShowTemplateDetail}
                            onAddTemplateToCircle={this.props.onAddTemplateToCircle}
                            onDownloadTemplate={this.props.onDownloadTemplate}
                            templates={[...this.props.teamTemplates]}
                            page={this}
                            error1={this.props.expertError}
                            error2={this.props.tempCateError}/>
                          :
                          <div className={classes.NoTeam}><Question>该团队目前没有模板</Question></div>}
                        </div>
                    </div> : <div className={classes.NoTeam}><Question>您还没有创建或加入团队</Question></div>}
              </div>

            </Aux> : <Aux>
              <div className={classes.Title}>
                <Title>圈子</Title>
                <div className={classes.CreateTeamButton}>
                  <Button btnType="Success" largeFont clicked={() => {this.props.onSyncCircle()}}>同步圈子</Button>
                </div>
              </div>
              <div>
                {this.props.circles.length > 0 ?
                  this.props.circles.map((circle, index) => <div key={circle.xiaoeId}>{circle.name}</div>) :
                  <Question>暂无圈子</Question>}
              </div>

            </Aux>}
          </div>
        </Content>

      </Aux>
    )
  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    username: state.authRoute.username,
    name: state.authRoute.name,
    refer: state.authRoute.refer,
    referee: state.authRoute.referee,
    qrcode: state.authRoute.qrcode,
    circle: state.authRoute.circle,
    mode: state.templates.mode,
    clientMode: state.clients.mode,
    expertMode: state.templates.expertMode,
    teamMode: state.templates.teamMode,
    expertError: state.templates.expertError,
    teamError: state.templates.teamError,
    templates: state.templates.templates,
    expertTemplates: state.tempCates.templateList,
    teamTemplates: state.templates.teamTemplates,
    teams: state.templates.teams,
    teamId: state.templates.teamId,
    teamMembers: state.templates.teamMembers,
    teamMembersError: state.templates.teamMembersError,
    leaveTeamError: state.templates.leaveTeamError,
    deleteTeamError: state.templates.deleteTeamError,
    deleteDoctorId: state.templates.deleteDoctorId,
    deleteMemberError: state.templates.deleteMemberError,
    deleteMemberSuccess: state.templates.deleteMemberSuccess,
    // expertTemplates: state.templates.expertTemplates,
    tempCateMode: state.tempCates.mode,
    tempCateError: state.tempCates.error,
    title: state.tempCates.title,
    map: state.tempCates.map,
    overallInfo: state.tempCates.overallInfo,
    initial: state.tempCates.initial,
    doctors: state.templates.doctors,
    sendingDoctorIds: state.templates.sendingDoctorIds,
    labels: state.templates.labels,
    labelName: state.templates.labelName,
    sendError: state.templates.sendError,
    realSendError: state.templates.realSendError,
    realAddExpertError: state.templates.realAddExpertError,
    error: state.templates.error,
    deleteTemplateId: state.templates.deleteTemplateId,
    deleteTemplateName: state.templates.deleteTemplateName,
    createQRTemplateId: state.templates.createQRTemplateId,
    createQRTemplateName: state.templates.createQRTemplateName,
    base64Str: state.templates.base64Str,
    createQRError: state.templates.createQRError,
    sendTemplateId: state.templates.sendTemplateId,
    sendTemplateName: state.templates.sendTemplateName,
    addTemplateId: state.templates.addTemplateId,
    addTemplateName: state.templates.addTemplateName,
    deleteError: state.templates.deleteError,
    deleteMy: state.templates.deleteMy,
    templateSearchValue: state.templates.templateSearchValue,
    expertTemplateSearchValue: state.templates.expertTemplateSearchValue,
    teamTemplateSearchValue: state.templates.teamTemplateSearchValue,
    focusOnMy: state.templates.focusOnMy,
    doctorSearchValue: state.templates.doctorSearchValue,
    templateTitle: state.templates.templateTitle,
    chosen: state.clients.chosen,
    titleId: state.clients.titleId,
    clientCategories: state.clients.clientCategories,
    content: state.templates.content,
    circles: state.templates.circles,
    addCircleTemplateId: state.templates.addCircleTemplateId,
    addCircleTemplateName: state.templates.addCircleTemplateName,
    addCircleTemplateError: state.templates.addCircleTemplateError,
    chosenCircleName: state.templates.chosenCircleName,
    chosenCircleId: state.templates.chosenCircleId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchTemplates: doctorId => dispatch(actions.fetchTemplates(doctorId)),
    // onFetchExpertTemplate: () => dispatch(actions.fetchExpertTemplates()),
    onFetchTeamTemplate: (refer, referee, username) => dispatch(actions.fetchTeamTemplate(refer, referee, username)),
    onFetchTeamsAndFirst: (doctorId, admin) => dispatch(actions.fetchTeamsAndFirst(doctorId, admin)),
    onDownloadTemplate: (templateId, doctorId, expertOrTeam, teamId, admin) =>
      dispatch(actions.downloadTemplate(templateId, doctorId, expertOrTeam, teamId, admin)),
    onCreateNewTemplate: page => dispatch(actions.createNewTemplate(page)),
    onDeleteTemplate: (templateId, name, my) => dispatch(actions.deleteTemplate(templateId, name, my)),
    onCreateQRCode: (doctorId, templateId, templateName, mode) => dispatch(actions.createQRCode(doctorId, templateId, templateName, mode)),
    onCloseModal: () => dispatch(actions.closeModal()),
    onRealDelete: (myType, teamId, doctorId, templateId, admin) =>
      dispatch(actions.realTemplateDelete(myType, teamId, doctorId, templateId, admin)),
    onDeleteFinished: () => dispatch(actions.deleteTemplateFinished()),
    onSendTemplate: (templateId, name) => dispatch(actions.sendTemplate(templateId, name)),
    onAddExpert: (templateId, templateName) => dispatch(actions.addExpert(templateId, templateName)),
    onAddTeam: (doctorId, addTemplateId, username, index) => dispatch(actions.addTeam(doctorId, addTemplateId, username, index)),
    onAddExpertOrTeam: (templateId, templateName) => dispatch(actions.addExpertOrTeam(templateId, templateName)),
    onDeleteTeamMember: (doctorId) => dispatch(actions.deleteTeamMember(doctorId)),
    onRealDeleteTeamMember: (teamId, doctorId) => dispatch(actions.realDeleteTeamMember(teamId, doctorId)),
    onLeaveTeam: () => dispatch(actions.leaveTeam()),
    onDeleteTeam: () => dispatch(actions.deleteTeam()),
    onRealLeaveTeam: (teamId, doctorId) => dispatch(actions.realLeaveTeam(teamId, doctorId)),
    onRealDeleteTeam: (teamId) => dispatch(actions.realDeleteTeam(teamId)),
    onTeamChange: (value) => dispatch(actions.teamChange(value)),
    onSeeTeamMembers: (teamId) => dispatch(actions.seeTeamMembers(teamId)),
    // onRealAddExpert: (doctorId, labels, addTemplateId) => dispatch(actions.realAddExpert(doctorId, labels, addTemplateId)),
    onRealAddExpertOrTeam: (doctorId, labelName, labels, addTemplateId) => dispatch(actions.realAddExpertOrTeam(doctorId, labelName, labels, addTemplateId)),
    onRealAddClass: (doctorId, addTemplateId, classId) => dispatch(actions.realAddClass(doctorId, addTemplateId, classId)),
    onFetchDoctors: (doctorId, templateId) => dispatch(actions.fetchAllDoctors(doctorId, templateId)),
    onChangeDoctor: (doctorId) => dispatch(actions.changeDoctor(doctorId)),
    onChangeLabel: (name, value) => dispatch(actions.changeLabel(name, value)),
    onRealSendTemplate: (doctorId1, templateId, doctorIds) =>
      dispatch(actions.realSendTemplate(doctorId1, templateId, doctorIds)),
    onCreatePlanByTemplate: (doctorId, template, index, page, type) =>
      dispatch(actions.createPlanByTemplate(doctorId, template, index, page, type)),
    onAddTemplateToCircle: (templateId, templateName) => dispatch(actions.addTemplateToCircle(templateId, templateName)),
    onChangeCircle: (name, id) => dispatch(actions.changeCircle(name, id)),
    onShowTemplateDetail: (doctorId, templateId, teamId) => dispatch(actions.showTemplateDetail(doctorId, templateId, teamId)),
    onShowExpertTemplateDetail: (templates, templateId) => dispatch(actions.showExpertTemplateDetail(templates, templateId)),
    onEditTemplate: (doctorId, template, index, page) => dispatch(actions.editTemplate(doctorId, template, index, page)),
    onSearchTemplateChange: (value) => dispatch(actions.searchTemplateChange(value)),
    onSearchExpertTemplateChange: (value) => dispatch(actions.searchExpertTemplateChange(value)),
    onSearchTeamTemplateChange: (value) => dispatch(actions.searchTeamTemplateChange(value)),
    // onSwitchTemplateFocusToMy: (value) => dispatch(actions.switchTemplateFocusToMy(value)),
    onSearchAllTemplates: value => dispatch(actions.searchAllTemplates(value)),
    onSearchDoctors: value => dispatch(actions.searchDoctors(value)),
    onShowAllTemplates: () => dispatch(actions.showAllTemplates()),
    onChangeContent: (content) => dispatch(actions.changeContent(content)),
    onSyncCircle: () => dispatch(actions.syncCircle()),
    onGetCircle: () => dispatch(actions.getCircle()),
    onRealAddTemplateToCircle: (doctorId, templateId, circleId) => dispatch(actions.realAddTemplateToCircle(doctorId, templateId, circleId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Templates))