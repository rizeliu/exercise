import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import axios from 'axios'
import { connect } from 'react-redux'

import Content from '../../components/Content/Content'
import Title from '../../components/Title/Title'
import Input from '../../components/UI/Input/Input'
import Button from '../../components/UI/Button/Button'
import Question from '../../components/UI/Question/Question'
import SingleLineSpinner from '../../components/UI/SingleLineSpinner/SingleLineSpinner'
import classes from './Login.css'
import * as actions from '../../store/actions/index';
import Modal from "../../components/UI/Modal/Modal";
import Aux from "../../hoc/Aux/Aux";

class Login extends Component {

  state = {
    username: "",
    isUsername: false,
    password: "",
    mode: 0,
    error: "",
    errors: [],
    phone: "",
    verify: "",
    forget: false,
    forgetError: "",
    stop: false,
    change: false,
    forgetUsername: "",
    forgetName: "",
    forgetPassword: "",
    forgetPasswordConfirm: "",
    countDown: 60
  }

  componentDidMount = () => {
    document.title = "VOIN 登录"
  }

  inputChangeHandler = (event, id) => {
    const value = event.target.value
    this.setState({[id]: value})
  }

  switch = (type) => {
    if (type === "number") {
      this.setState({isUsername: false, username: ""})
    } else {
      this.setState({isUsername: true, username: ""})
    }
  }

  forget = () => {
    this.setState({forget: true, phone: "", verify: ""})
  }

  close = () => {
    this.setState({forget: false, phone: "", verify: ""})
  }

  sendMessage = () => {
    if (this.state.phone.length !== 11) {
      this.setState({forgetError: "手机号长度应为11位"})
    } else {
      this.setState({forgetError: "spin"})
      const self = this
      axios.get("https://backend.voin-cn.com/sendforget", {params: {phone: this.state.phone}})
        .then(function (response) {
          if (response.data.code === 2) {
            self.setState({forgetError: response.data.msg})
          } else if (response.data.code === 0) {
            self.setState({forgetError: response.data.msg})
          } else if (response.data.code === 1) {
            self.setState({forgetError: "短信发送成功"})
          }
        })
          .catch(function (error) {
            self.setState({forgetError: "内部服务器错误，请刷新"})
          })

      this.setState({forgetError: "", stop: true})
      const process = setInterval(() => {
        const countDown = this.state.countDown-1
        this.setState({countDown: countDown})
      }, 1000);

      setTimeout(() => {
        clearInterval(process)
        this.setState({stop: false, countDown: 60})
      }, 1000*60)
    }
  }

  loginForget = async () => {
    this.setState({forgetError: "spin"})
    if (this.state.verify.length === 6) {
      const response = await axios.post('https://backend.voin-cn.com/loginforget', {
        phone: this.state.phone,
        verify: this.state.verify
      })
      if (response.data.code === 1) {
        this.setState({change: true, forgetUsername: response.data.username, forgetName: response.data.name,
          forgetError: ""})
      } else {
        this.setState({forgetError: response.data.msg})
      }
    } else {
      this.setState({forgetError: "请输入6位验证码"})
    }
  }

  resetPassword = async () => {
    this.setState({ errors: [] })
    let error = ""
    if (this.state.forgetPassword.length < 6) error += "密码长度不得少于6位 "
    if (this.state.forgetPassword !== this.state.forgetPasswordConfirm) error += "两次密码不一致 "
    if (error.length === 0) {
      this.setState({forgetError: "spin"})
      const response = await axios.post('https://backend.voin-cn.com/resetpassword', {
        username: this.state.forgetUsername,
        password: this.state.forgetPassword
      })
      this.setState({forgetError: response.data.msg})
      if (response.data.code === 1) {
        const now = new Date();
        const time = now.getTime();
        const expireTime = time + 30 * 24 * 60 * 60 * 1000;
        now.setTime(expireTime);
        document.cookie = "userId="+response.data.doc._id+";expires="+now.toGMTString();
        document.cookie = "token="+response.data.token+";expires="+now.toGMTString();
        this.props.onAuthenticate({...response.data, code: 0}, true, 0, "", response.data.doc.admin, response.data.refer)
        if (response.data.doc.admin === 4) {
          setTimeout(() => {this.props.history.push('/exercises/template')}, 500)
        } else {
          setTimeout(() => {this.props.history.push('/exercises')}, 500)
        }
      }
    } else {
      const errors = error.split(" ")
      errors.pop()
      this.setState({ errors })
    }
  }

  login = async () => {
    this.setState({mode: 1})
    try {
      const response = await axios.post('https://backend.voin-cn.com/login', {
        username: this.state.username,
        isUsername: this.state.isUsername,
        password: this.state.password
      })
      this.setState({mode: 0, error: response.data.msg})
      if (response.data.code === 1) {
        const now = new Date();
        const time = now.getTime();
        const expireTime = time + 30 * 24 * 60 * 60 * 1000;
        now.setTime(expireTime);
        document.cookie = "userId="+response.data.doc._id+";expires="+now.toGMTString();
        document.cookie = "token="+response.data.token+";expires="+now.toGMTString();
        this.props.onAuthenticate({...response.data, code: 0}, true, 0, "", response.data.doc.admin, response.data.refer)
        if (response.data.doc.admin === 4) {
          setTimeout(() => {this.props.history.push('/exercises/template')}, 500)
        } else {
          setTimeout(() => {this.props.history.push('/exercises')}, 500)
        }
      }
    } catch (error) {
      console.log(error)
      this.setState({ mode: 0, error: "内部服务器错误，请刷新"})
    }
  }

  render() {

    let margin = "20px"
    let message = null
    if (this.state.mode === 1) {
      message = <SingleLineSpinner/>
      margin = "-15px"
    } else if (this.state.error) {
      message = <Question>{this.state.error}</Question>
      margin = "15px"
    }

    let modal = null
    if (this.state.forget) {
      modal = <Modal show={this.state.forget} modalClosed={this.close}>
        <div className={classes.Modal}>
          {this.state.change ?
            <Aux>
              <Title alignCenter light>验证码登录并重置密码</Title>
              <div className={classes.Info}>验证码正确，您的姓名为{this.state.forgetName ? "空" : this.state.forgetName}，您的用户名为{this.state.forgetUsername}，请重新设定您的密码</div>
              <Input id="forgetPassword" label="密码"
                     onChange={this.inputChangeHandler}
                     value={this.state.forgetPassword}
                     placeholder="密码长度不得少于6位"
                     password/>
              <Input id="forgetPasswordConfirm" label="密码确认"
                     onChange={this.inputChangeHandler}
                     value={this.state.forgetPasswordConfirm}
                     placeholder="请再输一次密码"
                     password/>
              <ul className={classes.Errors} style={{margin: margin+" 0"}}>
                {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
              </ul>
              {this.state.forgetError ? this.state.forgetError === "spin" ? <SingleLineSpinner/> : <Question>{this.state.forgetError}</Question> : null}
              <div className={classes.Buttons}>
                <Button btnType="Success" middlePadding fullWidth clicked={this.resetPassword}>确定</Button>
              </div>
            </Aux> :
            <Aux>
              <Title alignCenter light>验证码登录并重置密码</Title>
              <Input id="phone" label="手机号" onChange={this.inputChangeHandler} value={this.state.phone}/>
              <div className={classes.Input}>
                <label className={classes.Label} htmlFor="verify">验证码</label>
                <div className={classes.Code}>
                  <input
                    id="verify"
                    className={classes.InputElement}
                    value={this.state.verify}
                    onChange={event => this.inputChangeHandler(event, "verify")}
                  />
                  {this.state.stop ? <Button btnType="Grey" width30 disabled>已发送({this.state.countDown})</Button>
                    : <Button btnType="Grey" width30 clicked={this.sendMessage}>发送验证码</Button>}
                </div>
              </div>

              {this.state.forgetError ? this.state.forgetError === "spin" ? <SingleLineSpinner/> : <Question>{this.state.forgetError}</Question> : null}
              <div className={classes.Buttons}>
                <Button btnType="Success" middlePadding fullWidth clicked={this.loginForget}>登录</Button>
              </div>
            </Aux>}
        </div>
      </Modal>
    }

    return (
      <Content>
        {modal}
        <div className={classes.Content}>
          <div className={classes.Types}>
            <div className={[classes.Type, this.state.isUsername ? "" : classes.Chosen].join(" ")} onClick={() => this.switch("number")}>手机号登录</div>
            <div className={[classes.Type, this.state.isUsername ? classes.Chosen : ""].join(" ")} onClick={() => this.switch("username")}>用户名登录</div>
          </div>
          <div className={classes.First}>
            {/*<div className={classes.Row}>*/}
              {/*<div className={classes.Switch}>*/}
                {/*{this.state.isUsername ?*/}
                  {/*<Button btnType="New" clicked={this.switch}>改为手机号登录</Button> :*/}
                  {/*<Button btnType="New" clicked={this.switch}>改为用户名登录</Button>*/}
                {/*}*/}
              {/*</div>*/}
            {/*</div>*/}
            <div className={classes.FirstWidth}>
              <Input id="username" label={this.state.isUsername ? "用户名" : "手机号"}
                     onChange={this.inputChangeHandler}
                     value={this.state.username}
              />
              <Input id="password" label="密码"
                     onChange={this.inputChangeHandler}
                     value={this.state.password}
                     password/>
              <div style={{margin: margin+" 0"}}/>
              {message}
              <div className={classes.ForgetParent}>
              <div className={classes.Forget} onClick={this.forget}>忘记密码？</div>
              </div>
              <Button
                disabled={!this.state.username || !this.state.password} clicked={this.login}
                fullWidth largeFont largePadding btnType="Success">
                登录
              </Button>
            </div>
          </div>
        </div>
      </Content>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuthenticate: (info, login, mode, error, admin, refer) =>
      dispatch(actions.authenticate(info, login, mode, error, admin, refer)),
    onRemoveCookies: () => dispatch(actions.removeCookies())
  }
}

export default connect(null, mapDispatchToProps)(withRouter(Login))