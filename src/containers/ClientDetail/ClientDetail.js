import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import axios from 'axios';

import Title from '../../components/Title/Title'
import Content from '../../components/Content/Content'
import Button from '../../components/UI/Button/Button'
import Spinner from '../../components/UI/Spinner/Spinner'
import Modal from '../../components/UI/Modal/Modal'
import Question from '../../components/UI/Question/Question'
import Aux from '../../hoc/Aux/Aux'
import ChooseCategory from "../../components/ChooseCategory/ChooseCategory";
import * as actions from '../../store/actions/index';
import classes from "../ClientDetail/ClientDetail.css";
import Input from "../../components/UI/Input/Input";
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";

class ClientDetail extends Component {

  state = {
    record: "",
    selectedFile: null,
    fileError: "",
    fileUploading: false
  }


  // On file select (from the pop up)
  onFileChange = event => {

    // Update the state
    this.setState({ fileError: "", fileUploading: false, selectedFile: event.target.files[0] });

  };

  onFileUpload = async () => {

    // if (this.state.selectedFile.size >= 1048576) {
    //   this.setState({fileError: "目前无法上传大于1M图片，功能优化中", fileUploading: false})
    //   return
    // }

    this.setState({fileError: "", fileUploading: true})

    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );
    formData.append("doctorId", this.props.doctorId)
    formData.append("clientId", this.props.clientId)
    console.log(this.state.selectedFile)

    const result = await axios({
      url: "https://backend.voin-cn.com/doctorupload",
      method: 'post',
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
      data: formData,
      headers: { "Content-Type": "multipart/form-data" },
    })
    if (result.data.code === 1) {
      const doctorId = this.props.match.params.did
      const clientId = this.props.match.params.cid
      this.setState({record: "", selectedFile: null, fileError: "", fileUploading: false})
      this.props.onClientDetailInit(this.props.doctorId, clientId, !doctorId)
    } else {
      this.setState({fileError: result.data.msg, fileUploading: false})
    }
  };

  componentDidMount = () => {
    const clientId = this.props.match.params.cid
    const doctorId = this.props.match.params.did
    if (doctorId) {
      document.title = "VOIN 医生患者信息"
      if (this.props.admin === 1) {
        this.props.onClientDetailInit(doctorId, clientId, false)
      } else {
        this.props.history.push('/clients')
      }
    } else {
      document.title = "VOIN 患者信息"
      this.props.onClientDetailInit(this.props.doctorId, clientId, true)
    }
  }

  componentDidUpdate = () => {
    const doctorId = this.props.match.params.did
    const clientId = this.props.match.params.cid
    if (this.props.mode === 5 || this.props.clientMode === 5) {
      this.setState({record: "", selectedFile: null, fileError: "", fileUploading: false})
      this.props.onClientDetailInit(this.props.doctorId, clientId, !doctorId)
    }
  }

  createPlanForClient = (clientId) => {
    this.props.history.push('/exercises')
    this.props.onCreatePlanForClient(clientId)
  }

  showRecord = record => {
    this.setState({record})
  }


  render() {
    if (this.props.mode === 1) return <Spinner overall/>
    if (this.props.error) return (<Modal show={this.props.error}>
      <Question>{this.props.error}</Question>
      <div style={{'textAlign': 'center'}}><Link to='/clients'><Button btnType="Success">回到患者</Button></Link></div>
    </Modal>)
    let deleteQuestion = (
      <Modal modalClosed={this.props.onCloseModal} type="Warning" show={this.props.mode === 2}>
        <div className={classes.Center}>
          <p style={{marginTop: "0"}}>您确定要删除"{this.props.deletePlanName}"这个计划吗？</p>
          <Button btnType="Danger" clicked={this.props.onCloseModal}>取消</Button>
          <Button btnType="Success" clicked={() => this.props.onRealPlanDelete(this.props.doctorId,
            this.props.clientId, this.props.deletePlanId)}>继续</Button>
        </div>
      </Modal>
    )
    if (this.props.mode === 6) {
      deleteQuestion = <Modal modalClosed={this.props.onDeletePlanFinished} show={this.props.mode === 6}>
        <Question>{this.props.deleteError}</Question>
      </Modal>
    }
    if (this.props.mode === 3) {
      deleteQuestion = <Modal modalClosed={this.props.onCloseModal} show={this.props.mode === 3}>
        <Spinner/>
      </Modal>
    }
    if (this.props.mode === 4) {
      deleteQuestion = <Modal modalClosed={this.props.onDeletePlanFinished} show={this.props.mode === 4}>
        <Question>该计划已删除</Question>
        <div className={classes.Center}><Button btnType="Success" clicked={this.props.onDeletePlanFinished}>完成</Button></div>
      </Modal>
    }
    let modal = null
    if (this.state.record) {
      modal = <Modal show={this.state.record} type="Record" modalClosed={() => this.showRecord("")}>
        <img alt="" src={this.state.record} style={{maxWidth: '100%'}}/>
      </Modal>
    }

    let addCategory = <ChooseCategory doctorId={this.props.doctorId} type="client"/>

    const defined = this.props.categories.filter(cat => cat.type === "defined" || cat.type === "second")
    const disease = this.props.categories.filter(cat => cat.type === "disease")
    const operation = this.props.categories.filter(cat => cat.type === "operation")

    return (
      <Aux>
        {addCategory}
        {modal}
        {deleteQuestion}
        <Content padding>
          <div className={classes.Package}>
          <div className={classes.Title}>
            {this.props.self ? <Title>{this.props.admin === 3 ? "Patient Detail" : "患者详情"}</Title> : <Title>医生患者详情</Title>}
            {this.props.self ? <div style={{padding: '5px 0', lineHeight: '28px'}}>
              <Button clicked={() => this.createPlanForClient(this.props.clientId)} btnType="New" largeFont>新增计划</Button>
            </div> : <div style={{padding: '5px 0', lineHeight: '28px'}}>
              <Button clicked={() => {
                this.props.history.push('/admin')
              }} btnType="New" largeFont>回到管理员</Button>
            </div>}
          </div>
          <div className={classes.Content}>
            <div className={classes.First}>
              {!this.props.self ? <div className={classes.Subtitle}>医生基本资料</div>  : null}
              {!this.props.self ? <table className={classes.Table1}>
                <tbody>
                <tr key="name">
                  <td className={classes.Left}>姓名</td>
                  <td className={classes.Right}>{this.props.doctorName}</td>
                </tr>
                <tr key="username">
                  <td className={classes.Left}>用户名</td>
                  <td className={classes.Right}>{this.props.doctorUsername}</td>
                </tr>
                </tbody>
              </table> : null}
              <div className={classes.Subtitle}>
                <div>患者基本资料</div>
                {this.props.self ?
                  <div>
                    {this.props.edit ? <Aux>
                        <Button btnType="New" clicked={this.props.onCancelEditClientDetail}>取消</Button>
                        {this.props.saving ? <Button btnType="Success">保存中...</Button> : <Button btnType="Success"
                                clicked={() => this.props.onSaveClientDetail(
                                  this.props.doctorId,
                                  this.props.clientId,
                                  this.props.newAge,
                                  this.props.newSex,
                                  this.props.newRealhistory,
                                  this.props.newSmoke,
                                  this.props.newPhone,
                                  this.props.newDescription,
                                )}>保存</Button>}
                    </Aux> :
                      <Button btnType="New" clicked={this.props.onEditClientDetail}>编辑</Button>}
                  </div> : null}
              </div>
              <table className={classes.Table1}>
                <tbody>
                <tr key="name">
                  <td className={classes.Left}>姓名</td>
                  <td className={classes.Right}>{this.props.name}</td>
                </tr>
                <tr key="phone">
                  <td className={classes.Left}>手机号</td>
                  {this.props.edit ? <td><Input
                    onChange={this.props.onChangeClientDetail}
                    noMargin id="newPhone"
                    value={this.props.newPhone}/></td> : <td className={classes.Right}>{this.props.phone}</td>}
                </tr>
                <tr key="sex">
                  <td className={classes.Left}>性别</td>
                  {this.props.edit ? <td><Input
                    onChange={this.props.onChangeClientDetail}
                    noMargin id="newSex"
                    value={this.props.newSex}/></td> : <td className={classes.Right}>{this.props.sex}</td>}
                </tr>
                <tr key="age">
                  <td className={classes.Left}>年龄</td>
                  {this.props.edit ? <td><Input
                    onChange={this.props.onChangeClientDetail}
                    noMargin id="newAge"
                    value={this.props.newAge}/></td> : <td className={classes.Right}>{this.props.age}</td>}
                </tr>
                <tr key="history">
                  <td className={classes.Left}>既往病史</td>
                  {this.props.edit ? <td><Input
                    onChange={this.props.onChangeClientDetail}
                    noMargin id="newRealhistory"
                    value={this.props.newRealhistory}/></td> : <td className={classes.Right}>{this.props.realhistory}</td>}
                </tr>
                <tr key="smoke">
                  <td className={classes.Left}>吸烟史</td>
                  {this.props.edit ? <td><Input
                    onChange={this.props.onChangeClientDetail}
                    noMargin id="newSmoke"
                    value={this.props.newSmoke}/></td> : <td className={classes.Right}>{this.props.smoke}</td>}
                </tr>
                </tbody>
              </table>
              <div className={classes.Description}>具体描述</div>
              <div className={classes.DescriptionRight}>{this.props.description ? this.props.description : "无"}</div>
              <div className={classes.Description}>医生描述</div>
              {this.props.edit ?
                <textarea value={this.props.newDescription}
                          onChange={event => this.props.onChangeClientDetail(event, "newDescription")}/> :
                <div className={classes.DescriptionRight}>{this.props.doctorDesc}</div>}
              <div className={classes.Subtitle}>
                <div>患者分类</div>
                {this.props.self ?
                  <div>
                    <Button disabled={this.props.edit} clicked={() => this.props.onAddClientToCategory(
                      this.props.doctorId, this.props.clientId, this.props.name, this.props.categories.map(cat => cat.id)
                    )} btnType="New">更改</Button>
                </div> : null}
              </div>
              <div className={classes.AllCates}>
                {this.props.categories.length === 0 && !this.props.childDoctor.id ? <div className={classes.Description}>暂无分类</div> : null}
                {defined.length > 0 ? <div>
                  <div className={classes.Description}>自定义分类</div>
                  <ul className={classes.Categories}>
                    {defined.map(cat => {
                      if (cat.type === "defined") return (<li className={classes.CategoryItem} key={cat.id}>
                        <span className={classes.Span}>{cat.name}</span></li>)
                      else return (<li className={classes.CategoryItem} key={cat.id}>
                        <span className={classes.Span}>{cat.parentName}</span> > <span className={classes.Span}>{cat.name}</span></li>)
                    })}
                  </ul>
                </div> : null}
                {disease.length > 0 ? <div>
                  <div className={classes.Description}>疾病码分类</div>
                  <ul className={classes.Categories}>
                    {disease.map(cat => <li className={classes.CategoryItem} key={cat.id}><span className={classes.Span}>{cat.code + " " +cat.name}</span></li>)}
                  </ul>
                </div> : null}
                {operation.length > 0 ? <div>
                  <div className={classes.Description}>手术码分类</div>
                  <ul className={classes.Categories}>
                    {operation.map(cat => <li className={classes.CategoryItem} key={cat.id}><span className={classes.Span}>{cat.code + " " +cat.name}</span></li>)}
                  </ul>
                </div> : null}
                {this.props.childDoctor.id ? <div>
                  <div className={classes.Description}>医生分类</div>
                  <ul className={classes.Categories}>
                    <li className={classes.CategoryItem} key={this.props.childDoctor.id}><span className={classes.Span}>{this.props.childDoctor.name}</span></li>
                  </ul>
                </div> : null}
              </div>
              <div className={classes.Subtitle}>患者病历</div>
              <div className={classes.Description}>病历描述</div>
              <div className={classes.DescriptionRightWithout}>{this.props.recordDes}</div>
              <div className={classes.Description}>病历图片</div>
              <div className={classes.SmallSubtitle}>
                <label htmlFor="files" className={classes.Label}>上传图片
                  <input type="file" id="files" accept="image/png, image/jpg, image/jpeg"
                         className={classes.Hidden} onChange={this.onFileChange}/>
                </label>
                <div>
                  <Button btnType="New" disabled={!this.state.selectedFile || this.state.fileUploading} clicked={this.onFileUpload}>
                    确定
                  </Button>
                  {/*<Button btnType="Danger" clicked={this.onFileUpload}>*/}
                    {/*删除*/}
                  {/*</Button>*/}
                </div>

              </div>
              {this.state.fileUploading ? <SingleLineSpinner/> : null}
              {this.state.selectedFile ? <div className={classes.Description}>选中图片：{this.state.selectedFile.name}</div> : null}
              {this.state.fileError ? <Question>{this.state.fileError}</Question> : null}
              <div className={classes.Container}>
                {this.props.records.map(record => <div key={record}
                                                       className={classes.RecordDiv}>
                  <img alt="" className={classes.Record} src={record} onClick={() => this.showRecord(record)}/>
                </div>)}
              </div>
            </div>
            <div className={classes.Middle}>
              <div className={classes.Subtitle}>患者计划</div>
              {this.props.plans.length ? (<table className={classes.Table2}>
                <thead>
                <tr>
                  <th className={classes.Name}>计划名</th>
                  <th className={classes.Length}>运动数</th>
                  <th className={classes.Start}>状态</th>
                  <th className={classes.Date}>创建日期</th>
                  <th className={classes.Manage}>管理</th>
                </tr>
                </thead>
                <tbody>
                {this.props.plans.map((plan, index) => {
                  let end = false
                  if (plan.startDate) {
                    const date1 = new Date(
                      parseInt(plan.startDate.substring(0, 4), 10),
                      parseInt(plan.startDate.substring(4, 6), 10) - 1,
                      parseInt(plan.startDate.substring(6, 8), 10))
                    const today = new Date()
                    const todayDiff = Math.floor((today - date1) / 86400000)+1
                    end = todayDiff >= plan.periodDays.reduce((a, b) => Number(a) + Number(b), 0)
                  }
                  const status = plan.paid ? plan.start ? end ? "已结束" : "进行中" : "未开始" : "未付款"
                  const date = new Date(plan.date)
                  const dateString = date.getFullYear()+"-"+
                    (date.getMonth()+1 >= 10 ? date.getMonth()+1 : "0"+ (date.getMonth()+1))+"-"
                    +(date.getDate() >= 10 ? date.getDate() : "0"+ date.getDate())
                  return (
                    <tr key={index}>
                      {plan.empty ? <td><Question noMargin>患者已支付，请尽快制定计划</Question></td> : <td>{plan.name}</td>}
                      <td>{plan.length}</td>
                      <td>{status}</td>
                      <td>{dateString}</td>
                      <td>
                        {!plan.empty ? <Button clicked={() => this.props.onSeePlanDetail(
                          this.props.match.params.did ? this.props.match.params.did : this.props.doctorId,
                          this.props.clientId, plan._id
                        )} btnType="Success">查看</Button> : null}
                        {!this.props.self || end ? null : !plan.loading ?
                            <Button clicked={() =>
                              this.props.onEditPlan(
                                this.props.doctorId,
                                this.props.clientId,
                                plan._id, index, this
                              )} btnType="New">{plan.empty ? "制定计划" : "编辑"}</Button>
                        : <Button btnType="New">载入中...</Button>}
                        {plan.start ? <Button clicked={() => this.props.onSeeFeedback(
                          this.props.match.params.did ? this.props.match.params.did : this.props.doctorId,
                          this.props.clientId, plan._id, status)}
                                              btnType="Danger">查看反馈</Button> : null}
                      </td>
                    </tr>
                  )
                })}
                </tbody>
              </table>) : <Question>未为该患者制定计划</Question>}
              {this.props.plans.length ? <div className={classes.Note}>共{this.props.plans.length}个计划</div> : null}
              {this.props.self ? <Aux>
                <div className={classes.Subtitle}>推荐模板</div>
                <table className={classes.Table2}>
                  <thead>
                  <tr>
                    <th className={classes.Name2}>模板名</th>
                    <th className={classes.Length2}>运动数</th>
                    <th className={classes.Manage2}>管理</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.props.selfTemps.map((temp, index) => {
                    return (
                      <tr key={index}>
                        <td>{temp.name}</td>
                        <td>{temp.moveList.filter(move => !move.bar).length}</td>
                        <td>
                          <Button btnType="Success"
                                  clicked={() => this.props.onShowTemplateDetail(this.props.doctorId, ""+temp._id)}
                          >查看</Button>
                          {!temp.loading1 ? <Button
                            clicked={() => this.props.onCreatePlanByTemplate(this.props.doctorId, temp, index, this, "detail", "self", this.props.clientId)}
                            btnType="New">分配模版
                          </Button> : <Button btnType="New">载入中...</Button>}
                        </td>
                      </tr>
                    )
                  })}
                  {this.props.expertTemps.map((temp, index) => {
                    return (
                      <tr key={index}>
                        <td>{temp.name}</td>
                        <td>{temp.moveList.filter(move => !move.bar).length}</td>
                        <td>
                          <Button btnType="Success"
                                  clicked={() => this.props.onShowTemplateDetail("5b68bb0fde7068270a29d0fe", ""+temp._id)}
                          >查看</Button>
                          {!temp.loading1 ? <Button
                            clicked={() => this.props.onCreatePlanByTemplate(this.props.doctorId, temp, index, this, "detail", "expert", this.props.clientId)}
                            btnType="New">分配模版
                          </Button> : <Button btnType="New">载入中...</Button>}
                        </td>
                      </tr>
                    )
                  })}
                  </tbody>
                </table>
              </Aux> : null}
            </div>
          </div>
          </div>
        </Content>
      </Aux>
    )

  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    admin: state.authRoute.admin,
    mode: state.clientDetail.mode,
    error: state.clientDetail.error,
    clientMode: state.clients.mode,
    clientId: state.clientDetail.clientId,
    name: state.clientDetail.name,
    phone: state.clientDetail.phone,
    age: state.clientDetail.age,
    sex: state.clientDetail.sex,
    realhistory: state.clientDetail.history,
    smoke: state.clientDetail.smoke,
    description: state.clientDetail.description,
    newAge: state.clientDetail.newAge,
    newSex: state.clientDetail.newSex,
    newRealhistory: state.clientDetail.newRealhistory,
    newSmoke: state.clientDetail.newSmoke,
    newPhone: state.clientDetail.newPhone,
    newDescription: state.clientDetail.newDescription,
    doctorDesc: state.clientDetail.doctorDesc,
    recordDes: state.clientDetail.recordDes,
    records: state.clientDetail.records,
    date: state.clientDetail.date,
    plans: state.clientDetail.plans,
    deletePlanId: state.clientDetail.deletePlanId,
    deletePlanName: state.clientDetail.deletePlanName,
    deleteError: state.clientDetail.deleteError,
    self: state.clientDetail.self,
    doctorName: state.clientDetail.doctorName,
    doctorUsername: state.clientDetail.doctorUsername,
    edit: state.clientDetail.edit,
    saving: state.clientDetail.saving,
    categories: state.clientDetail.categories,
    childDoctor: state.clientDetail.childDoctor,
    selfTemps: state.clientDetail.selfTemps,
    expertTemps: state.clientDetail.expertTemps,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onClientDetailInit: (doctorId, clientId, self) => dispatch(actions.clientDetailInit(doctorId, clientId, self)),
    onSeePlanDetail: (doctorId, clientId, planId) => dispatch(actions.seePlanDetail(doctorId, clientId, planId)),
    onSeeFeedback: (doctorId, clientId, planId, status) => dispatch(actions.seeFeedback(doctorId, clientId, planId, status)),
    onDeletePlan: (planId, planName) => dispatch(actions.deletePlan(planId, planName)),
    onCloseModal: () => dispatch(actions.closeModal()),
    onRealPlanDelete: (doctorId, clientId, planId) => dispatch(actions.realPlanDelete(doctorId, clientId, planId)),
    onDeletePlanFinished: () => dispatch(actions.deletePlanFinished()),
    onCreatePlanForClient: (clientId) => dispatch(actions.createPlanForClient(clientId)),
    onEditPlan: (doctorId, clientId, planId, index, page) =>
      dispatch(actions.editPlan(doctorId, clientId, planId, index, page)),
    onEditClientDetail: () => dispatch(actions.editClientDetail()),
    onCancelEditClientDetail: () => dispatch(actions.cancelEditClientDetail()),
    onChangeClientDetail: (event, id) => dispatch(actions.changeClientDetail(event.target.value, id)),
    onSaveClientDetail: (doctorId, clientId, newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription) =>
      dispatch(actions.saveClientDetail(doctorId, clientId, newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription)),
    onAddClientToCategory: (doctorId, clientId, name, category) =>
      dispatch(actions.addClientToCategoryDetail(doctorId, clientId, name, category)),
    onShowTemplateDetail: (doctorId, templateId, teamId) => dispatch(actions.showTemplateDetail(doctorId, templateId, teamId)),
    onCreatePlanByTemplate: (doctorId, template, index, page, type, from, clientId) =>
      dispatch(actions.createPlanByTemplate(doctorId, template, index, page, type, from, clientId)),

  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientDetail))