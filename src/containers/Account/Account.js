import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import axios from "axios";

import Content from '../../components/Content/Content'
import Title from '../../components/Title/Title'


import classes from './Account.css'
import Input from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import SingleLineSpinner from "../../components/UI/SingleLineSpinner/SingleLineSpinner";
import Question from "../../components/UI/Question/Question";
import Aux from "../../hoc/Aux/Aux";
import * as actions from "../../store/actions";

import Spinner from "../../components/UI/Spinner/Spinner";

class Account extends Component {

  state = {
    edit: false,
    mode: 0,
    content: 1,
    errors: [],
    error: "",
    msg: "",
    old: "",
    password: "",
    passwordAgain: "",
    name: "",
    idnum: "",
    account: "",
    hospital: "",
    district: "",
    bankName: "",
    bankAccount: "",
    wechat: "",
    alipayAccount: "",
    phone: "",
    email: "",
    room: "",
    occupation: "0",
    certification: "",
    username: "",
    base64: "",
    newDoctors: [""]
  }

  checkValidateC = () => {
    let error = ""

    if (!this.state.name) {
      error += "姓名不能为空 "
    }
    // if (!this.state.idnum) {
    //   error += "身份证号不能为空 "
    // }
    if (!this.state.account) {
      error += "请选择收款方式 "
    } else if (this.state.account === "alipay" && this.state.alipayAccount === "") {
      error += "支付宝账号不能为空 "
    } else if (this.state.account === "bank" && (this.state.bankName === "" || this.state.bankAccount === "")) {
      error += "银行名称于银行账号均不能为空 "
    }
    if (!this.state.phone) {
      error += "手机号不能为空 "
    }
    if (!this.state.wechat) {
      error += "微信号不能为空 "
    }
    if (!this.state.email) {
      error += "邮箱不能为空 "
    }
    if (!this.state.hospital) {
      error += "医院不能为空 "
    }
    if (!this.state.room) {
      error += "所在科室不能为空 "
    }
    if (this.state.occupation === "0") {
      error += "职称不能为空 "
    }
    // if (this.state.certification === "") {
    //   if (this.state.occupation === "1" || this.state.occupation === "2" || this.state.occupation === "3") {
    //     error += "康复医学治疗技术证管理号不能为空 "
    //   } else {
    //     error += "执业医师资格证号不能为空 "
    //   }
    // }
    return error
  }

  saveInfo = async () => {
    const error = this.checkValidateC()
    const errors = error.split(" ")
    errors.pop()
    this.setState({ errors, error: "" })
    if (errors.length === 0 ) {
      this.setState({ mode: 1 })
      try {
        const response = await axios.post('https://backend.voin-cn.com/complete', {
          doctorId: this.props.doctorId,
          name: this.state.name,
          account: this.state.account,
          bankName: this.state.bankName,
          bankAccount: this.state.bankAccount,
          alipayAccount: this.state.alipayAccount,
          room: this.state.room,
          phone: this.state.phone,
          email: this.state.email,
          certification: this.state.certification,
          occupation: this.state.occupation,
          idnum: this.state.idnum,
          hospital: this.state.hospital,
          district: this.state.district,
          wechat: this.state.wechat
        })
        if (response.data.code === 0) {
          this.setState({mode: 2, msg: response.data.msg})
        } else if (response.data.code === 1) {
          this.setState({mode: 3, msg: "信息保存成功", edit: false})
          this.props.onAuthenticate({...response.data, code:0}, true, 0, "", this.props.admin)
        }
      } catch (error) {
        this.setState({ mode: 2, msg: "内部服务器错误，请刷新"})
      }
    }
  }

  editInfo = () => {
    this.setState({
      edit: true,
      errors: [],
      error: "",
      msg: "",
      mode: 0,
      name: this.props.name,
      account: this.props.account,
      bankName: this.props.bankName,
      bankAccount: this.props.bankAccount,
      alipayAccount: this.props.alipayAccount,
      phone: this.props.phone,
      email: this.props.email,
      idnum: this.props.idnum,
      hospital: this.props.hospital,
      district: this.props.district,
      wechat: this.props.wechat,
      room: this.props.room,
      occupation: this.props.occupation,
      certification: this.props.certification,
    })
  }

  cancelEdit = () => {
    this.setState({
      edit: false,
      errors: [],
      error: "",
      msg: "",
      mode: 0,
      name: "",
      account: "",
      bankName: "",
      bankAccount: "",
      alipayAccount: "",
      idnum: "",
      hospital: "",
      wechat: "",
      district: "",
      phone: "",
      email: "",
      room: "",
      occupation: "0",
      certification: "",
    })
  }

  inputChangeHandler = (event, id) => {
    if (id.substring(0, 3) === "new") {
      const number = parseInt(id.substring(3), 10)
      const value = event.target.value
      if (number < this.state.newDoctors.length) {
        this.setState({newDoctors: [...this.state.newDoctors.slice(0, number), value, ...this.state.newDoctors.slice(number+1)]})
      }
    } else {
      const accountSwitch = id === "account"
      const occupationSwith = id === "occupation"
      const value = event.target.value
      if (accountSwitch) {
        if (event.target.value === "alipay") this.setState({[id]: value, bankName: "", bankAccount: ""})
        else if (event.target.value === "bank") this.setState({[id]: value, alipayAccount: ""})
      } else if (occupationSwith) {
        this.setState({[id]: value, certification: ""})
      } else {
        this.setState({[id]: value})
      }
    }
  }

  deleteNewDoctor = (event, id) => {
    const number = parseInt(id.substring(3), 10)
    if (number < this.state.newDoctors.length) {
      this.setState({newDoctors: [...this.state.newDoctors.slice(0, number), ...this.state.newDoctors.slice(number+1)]})
    }
  }

  componentDidMount = () => {
    document.title = "VOIN 账号信息"
    if (this.props.admin !== 4 && this.props.admin !== 5) {
      const self = this
      axios.get('https://backend.voin-cn.com/usernameunicode', {params: {username: this.props.username}})
        .then(function (response) {
          if (response.data.code === 1) {
            const buffer = new Buffer(response.data.buffer)
            const base64Str = buffer.toString('base64')
            self.setState({base64: base64Str})
          }
        })
    }
  }

  updatePassword = async () => {
    const error = this.checkValidateU()
    const errors = error.split(" ")
    errors.pop()
    this.setState({ errors, error: "" })
    if (errors.length === 0 ) {
      this.setState({ mode: 1 })
      try {
        const response = await axios.post('https://backend.voin-cn.com/updatepass', {
          doctorId: this.props.doctorId,
          old: this.state.old,
          password: this.state.password,
        })
        if (response.data.code === 0) {
          this.setState({mode: 2, msg: response.data.msg})
        } else if (response.data.code === 1) {
          this.setState({mode: 3, msg: "密码修改成功"})
        }
      } catch (error) {
        this.setState({ mode: 2, msg: "内部服务器错误，请刷新"})
      }
    }
  }

  changePassword = async () => {
    this.setState({ error: "", mode: 1 })
    try {
      const response = await axios.post('https://backend.voin-cn.com/changepass', {
        username: this.state.username,
      })
      if (response.data.code === 0) {
        this.setState({mode: 2, msg: response.data.msg})
      } else if (response.data.code === 1) {
        this.setState({mode: 3, msg: "密码修改成功"})
      } else if (response.data.code === 2) {
        this.setState({mode: 3, msg: "该用户名不存在"})
      }
    } catch (error) {
      this.setState({ mode: 2, msg: "内部服务器错误，请刷新"})
    }
  }

  checkValidateU = () => {
    let error = ""
    if (this.state.old.length === 0) {
      error += "原密码不得为空 "
    }
    if (this.state.password !== this.state.passwordAgain) {
      error += "两次新密码不一致 "
    } else if (this.state.password.length < 8) {
      error += "新密码长度不得少于8位 "
    }
    return error
  }

  changeState = number => {
    this.setState({content: number, errors: [], error: "", mode: 0})
  }

  addADoctor = () => {
    const newDoctors = [...this.state.newDoctors, ""]
    this.setState({newDoctors: newDoctors})
  }

  saveNewDoctors = async () => {
    const error = this.checkValid()
    const errors = error.split(" ")
    errors.pop()
    this.setState({ errors, error: "" })
    if (errors.length === 0 ) {
      this.setState({ mode: 1 })
      try {
        const response = await axios.post('https://backend.voin-cn.com/newdoctors', {
          doctorId: this.props.doctorId,
          doctors: this.state.newDoctors,
        })
        if (response.data.code === 0) {
          this.setState({mode: 2, msg: response.data.msg})
        } else if (response.data.code === 1) {
          this.setState({mode: 3, msg: "新增医生成功"})
          setTimeout(() => {
            window.location.reload()
          }, 1000)
        }
      } catch (error) {
        this.setState({ mode: 2, msg: "内部服务器错误，请刷新"})
      }
    }
  }

  checkValid = () => {
    let error = ""

    for (let i = 0; i < this.state.newDoctors.length; i++) {
      if (this.state.newDoctors[i].indexOf(' ') >= 0 || this.state.newDoctors[i].length === 0) {
        error += "医生姓名中不得为空，且不得含有空格，请重新填写后保存 "
        break
      }
    }
    if (error) return error

    const names = this.props.childDoctors.map(child => child.name)
    if (names.filter(name => name === "我的医生").length === 0) names.push("我的医生")
    names.push(...this.state.newDoctors)
    names.sort((a, b) => a < b ? -1 : a > b ? 1 : 0)
    for (let i = 0; i < names.length-1; i++) {
      if (names[i] === names[i+1]) {
        error += "医生姓名不得重复，请重新填写后保存 "
        break
      }
    }
    return error
  }

  render() {

    let content = <div/>

    const occupationMap = {
      "1": "康复治疗师: 初级士",
      "2": "康复治疗师: 初级师",
      "3": "康复治疗师: 中级",
      "4": "医师: 主任医师",
      "5": "医师: 副主任医师",
      "6": "医师: 主治医师",
      "7": "医师: 住院医师",
    }

    if (this.state.content === 1) {
      content = <div>
        {/*<div className={classes.Title}>*/}
          {/*<Title>基本信息</Title>*/}
          {/*<div style={{padding: '15px 0'}}>*/}
            {/*{this.state.edit ? null*/}
              {/*: <Button btnType="New" clicked={this.editInfo}>编辑</Button>}*/}
          {/*</div>*/}
        {/*</div>*/}
        <table className={classes.Table}>
          <thead>
          <tr>
            <th className={classes.Key}/>
            <th className={classes.Value}/>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>用户名</td>
            <td>{this.props.username}</td>
          </tr>
          <tr>
            <td>姓名</td>
            {this.state.edit ? <td><Input
              onChange={this.inputChangeHandler}
              noMargin id="name"
              value={this.state.name}/></td> : <td>{this.props.name}</td>}
          </tr>
          {/*<tr>*/}
          {/*  <td>身份证号</td>*/}
          {/*  {this.state.edit ? <td><Input*/}
          {/*    onChange={this.inputChangeHandler}*/}
          {/*    noMargin id="idnum"*/}
          {/*    value={this.state.idnum}/></td> : <td>{this.props.idnum}</td>}*/}
          {/*</tr>*/}
          {this.props.admin !== 5 && this.props.admin !== 4 ? <tr>
            <td>收款方式</td>
            {this.state.edit ?
              <td>
                <input type="radio" name="account" value="bank" checked={this.state.account === "bank"}
                       onChange={event => this.inputChangeHandler(event, "account")}/>
                <span className={classes.Radio}>银行卡</span>
                <input type="radio" name="account" value="alipay" checked={this.state.account === "alipay"}
                       onChange={event => this.inputChangeHandler(event, "account")}/>
                <span className={classes.Radio}>支付宝</span>
              </td>
              : <td>{this.props.account === "alipay" ? "支付宝" : this.props.account === "bank" ? "银行卡" : ""}</td>}
          </tr> : null}
          {this.props.admin === 5 || this.props.admin === 4 ? null : this.state.edit ? this.state.account === "bank" ?
              <Aux>
                <tr>
                  <td>开户行名称</td>
                  <td><Input
                    onChange={this.inputChangeHandler}
                    noMargin id="bankName" value={this.state.bankName}/></td>
                </tr>
                <tr>
                  <td>银行账号</td>
                  <td><Input
                    onChange={this.inputChangeHandler}
                    noMargin id="bankAccount" value={this.state.bankAccount}/></td>
                </tr>
              </Aux> : this.state.account === "alipay" ?
                <tr>
                  <td>支付宝账号</td>
                  <td><Input
                    onChange={this.inputChangeHandler}
                    noMargin id="alipayAccount" value={this.state.alipayAccount}/></td>
                </tr> : null

            : this.props.account === "bank" ? <Aux>
            <tr>
              <td>开户行名称</td>
              <td>{this.props.bankName}</td>
            </tr>
            <tr>
              <td>银行账号</td>
              <td>{this.props.bankAccount}</td>
            </tr>
          </Aux> : this.props.account === "alipay" ?
            <tr>
              <td>支付宝账号</td>
              <td>{this.props.alipayAccount}</td>
            </tr> : null
          }
          <tr>
            <td>手机号</td>
            {(this.props.admin === 4 || this.props.admin === 5) && this.state.edit ? <td><Input
                onChange={this.inputChangeHandler}
                noMargin id="phone" value={this.state.phone}/></td> : <td>{this.props.phone}</td>}
          </tr>
          <tr>
            <td>微信号</td>
            {this.state.edit ? <td><Input
              onChange={this.inputChangeHandler}
              noMargin id="wechat" value={this.state.wechat}/></td> : <td>{this.props.wechat}</td>}
          </tr>
          <tr>
            <td>邮箱</td>
            {this.state.edit ? <td><Input
              onChange={this.inputChangeHandler}
              noMargin id="email" value={this.state.email}/></td> : <td>{this.props.email}</td>}
          </tr>
          {this.props.admin !== 5 && this.props.admin !== 4 ? <tr>
            <td>医院</td>
            {this.state.edit ? <td><Input
              onChange={this.inputChangeHandler}
              noMargin id="hospital" value={this.state.hospital}/></td> : <td>{this.props.hospital}</td>}
          </tr> : null}
          {this.props.admin !== 5 && this.props.admin !== 4 ? <tr>
            <td>科室</td>
            {this.state.edit ? <td><Input
              onChange={this.inputChangeHandler}
              noMargin id="room" value={this.state.room}/></td> : <td>{this.props.room}</td>}
          </tr> : null}
          {this.props.admin !== 5 && this.props.admin !== 4 ? <tr>
            <td>病区</td>
            {this.state.edit ? <td><Input
              onChange={this.inputChangeHandler}
              placeholder="选填"
              noMargin id="district" value={this.state.district}/></td> : <td>{this.props.district}</td>}
          </tr> : null}
          {this.props.admin !== 5 && this.props.admin !== 4 ? <tr>
            <td>职称</td>
            {this.state.edit ?
              <td>
                <select value={this.state.occupation} className={classes.Select} id="occupation" onChange={event => this.inputChangeHandler(event, "occupation")}>
                  <option value="0"/>
                  <optgroup label="康复治疗师">
                  <option value="1">初级士</option>
                  <option value="2">初级师</option>
                  <option value="3">中级</option>
                  </optgroup>
                  <optgroup label="医师">
                  <option value="4">主任医师</option>
                  <option value="5">副主任医师</option>
                  <option value="6">主治医师</option>
                  <option value="7">住院医师</option>
                  </optgroup>
                </select>
              </td>
              : <td>{occupationMap[this.props.occupation]}</td>}
          </tr> : null}
          {this.props.admin === 5 || this.props.admin === 4 ? null : this.state.edit ?
            this.state.occupation === "0" ? null :
              this.state.occupation === "1" || this.state.occupation === "2" || this.state.occupation === "3" ?
                <tr>
                  <td>康复医学治疗技术证管理号</td>
                  <td><Input noMargin id="certification" value={this.state.certification}
                             placeholder="选填"
                             onChange={this.inputChangeHandler}/></td>
                </tr> :
                <tr>
                  <td>执业医师资格证号</td>
                  <td><Input noMargin id="certification" value={this.state.certification}
                             placeholder="选填"
                             onChange={this.inputChangeHandler}/></td>
                </tr>
           :
          this.props.occupation === "1" || this.props.occupation === "2" || this.props.occupation === "3" ?
              <tr>
                <td>康复医学治疗技术证管理号</td>
                <td>{this.props.certification}</td>
              </tr> : this.props.occupation === "4" || this.props.occupation === "5" ||
            this.props.occupation === "6" || this.props.occupation === "7" ?
              <tr>
                <td>执业医师资格证号</td>
                <td>{this.props.certification}</td>
              </tr> : null}
          <tr>
            <td>账号创建日期</td>
            <td>{this.props.date.substring(0, 10)}</td>
          </tr>
          </tbody>
        </table>
        <ul className={classes.Errors} style={{margin: "15px 0"}}>
          {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
        </ul>
        {this.state.mode === 1 ? <SingleLineSpinner/> : null}
        {this.state.mode === 2 || this.state.mode === 3 ? <Question>{this.state.msg}</Question> : null}
        {this.state.edit ? <div>
          <Button btnType="Success" clicked={this.saveInfo}>保存</Button>
          <Button btnType="Grey" clicked={this.cancelEdit}>取消</Button>
        </div> : null}
      </div>
    }
    else if (this.state.content === 2) {
      content = <div>
        <Input id="old" label="原密码" onChange={this.inputChangeHandler} password/>
        <Input id="password" label="新密码" onChange={this.inputChangeHandler} password/>
        <Input id="passwordAgain" label="新密码确认" onChange={this.inputChangeHandler} password/>
        <ul className={classes.Errors} style={{margin: "15px 0"}}>
          {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
        </ul>
        {this.state.mode === 1 ? <SingleLineSpinner/> : null}
        {this.state.mode === 2 || this.state.mode === 3 ? <Question>{this.state.msg}</Question> : null}
        <Button clicked={this.updatePassword}
          largeFont disabled={this.state.mode === 3}
          largePadding btnType="Success">
          更改密码
        </Button>
      </div>
    }
    else if (this.state.content === 3) {
      content = <div>
        <Title>重置密码</Title>
        <Input id="username" label="用户名（请确保输入正确的用户名，并与该用户联系）" onChange={this.inputChangeHandler}/>
        <ul className={classes.Errors} style={{margin: "15px 0"}}>
          {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
        </ul>
        {this.state.mode === 1 ? <SingleLineSpinner/> : null}
        {this.state.mode === 2 || this.state.mode === 3 ? <Question>{this.state.msg}</Question> : null}
        <Button clicked={this.changePassword}
                largeFont disabled={this.state.mode === 3}
                largePadding btnType="Success">
          将其密码改为11111111
        </Button>
      </div>
    }
    else if (this.state.content === 4) {
      const current = this.props.childDoctors.length === 0 ? 1 : this.props.childDoctors.length

      content = <div>
        <Title>{"已创建医生（共" + current + "名）"}</Title>
        <ul className={classes.Current}>
          <li key="current0"> <Input id="current0" value="我的医生" readOnly/></li>
          {this.props.childDoctors.filter((child, index) => !child.original).map((child, index) => {
            return (
              <li key={"current"+(index+1)}><Input id={"current"+(index+1)} value={child.name} readOnly/></li>
            )
          })}
        </ul>

        <Title>{"新创建医生（共" + this.state.newDoctors.length + "名）"}</Title>
        <ul className={classes.New}>
          {this.state.newDoctors.map((child, index) =>
            <li key={"new"+index}>
              <Input id={"new"+index} value={child} deletable={index > 0} onClick={(event, id) => this.deleteNewDoctor(event, id)} onChange={this.inputChangeHandler}/>
            </li>
          )}
        </ul>

        <ul className={classes.Errors} style={{margin: "15px 0"}}>
          {this.state.errors.map((error, index) => <li key={index}>{error}</li>)}
        </ul>
        {this.state.mode === 1 ? <SingleLineSpinner/> : null}
        {this.state.mode === 2 || this.state.mode === 3 ? <Question>{this.state.msg}</Question> : null}
        {this.state.mode === 3 ? null : <div>
          <Button largeFont largePadding btnType="Success" clicked={this.addADoctor}>
            添加一个医生
          </Button>
          <Button largeFont largePadding btnType="Grey" clicked={this.saveNewDoctors}>
            保存
          </Button>
        </div>}
      </div>
    }



    return (
      <Content>
        <div className={classes.Content}>
          <div className={classes.Types}>
            <div className={[classes.Type, this.state.content === 1 ? classes.Chosen : ""].join(" ")} onClick={() => this.changeState(1)}>基本信息</div>
            <div className={[classes.Type, this.state.content === 2 ? classes.Chosen : ""].join(" ")} onClick={() => this.changeState(2)}>更改密码</div>
            {this.props.admin === 1 ?
              <div className={[classes.Type, this.state.content === 3 ? classes.Chosen : ""].join(" ")}
                   onClick={() => this.changeState(3)}>更改用户密码</div> : null}
            {this.props.admin === 1 || this.props.admin === 2 || this.props.admin === 0 ?
              <div className={[classes.Type, this.state.content === 4 ? classes.Chosen : ""].join(" ")}
                   onClick={() => this.changeState(4)}>增加医生</div> : null}
          </div>
          <div className={classes.First}>
            {this.state.content === 1 ? <div className={classes.FirstWidth}>
              {this.props.admin === 4 || this.props.admin === 5 ? null :
                <div className={classes.QRCode}>
                  {!this.state.base64 ? <Spinner/> :
                    <img className={classes.Img} src={"data:image/png;base64,"+this.state.base64} alt="qrcode"/>}
                </div>}
              {/*<div style={{"background": "black"}}><img src={edit} alt="edit"/></div>*/}
              {content}
              <div><Button largePadding largeFont btnType="Grey" clicked={this.editInfo}>编辑</Button></div>
            </div> : <div className={classes.SecondWidth}>
              {content}
            </div>}

          </div>
        </div>
      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    doctorId: state.authRoute.id,
    name: state.authRoute.name,
    username: state.authRoute.username,
    account: state.authRoute.account,
    bankName: state.authRoute.bankName,
    bankAccount: state.authRoute.bankAccount,
    alipayAccount: state.authRoute.alipayAccount,
    phone: state.authRoute.phone,
    email: state.authRoute.email,
    idnum: state.authRoute.idnum,
    hospital: state.authRoute.hospital,
    district: state.authRoute.district,
    wechat: state.authRoute.wechat,
    room: state.authRoute.room,
    certification: state.authRoute.certification,
    occupation: state.authRoute.occupation,
    date: state.authRoute.date,
    admin: state.authRoute.admin,
    childDoctors: state.authRoute.childDoctors
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuthenticate: (info, login, mode, error, admin) => dispatch(actions.authenticate(info, login, mode, error, admin))
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Account))