import React, { Component } from 'react'
import { connect } from 'react-redux';

import Content from '../../components/Content/Content'
import Title from "../../components/Title/Title";
import classes from './Cases.css'
import SearchInput from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import axios from "axios";
import Spinner from "../../components/UI/Spinner/Spinner";
import Question from "../../components/UI/Question/Question";

class Cases extends Component {

  componentDidMount = async () => {
    document.title = "VOIN 方案"
  }

  state = {
    keyWords: "",
    mode: 1,
    error: "",
    cases: []
  }

  search = async (keyWords) => {
    this.setState({mode: 2})
    try {
      const result = await axios.post('https://backend.voin-cn.com/cases', {keyWords})
      const res = result.data
      if (res.code === 1) {
        this.setState({mode: 1, cases: res.doc, error: ""})
      } else if (res.code === 2) {
        this.setState({mode: 3, error: ""})
      } else {
        this.setState({mode: 0, error: res.msg})
      }
    }
    catch (error) {
      this.setState({mode: 0, error: error})
    }
  }

  render() {
    return (
      <Content>
        <div className={classes.Content}>
          <Title>{this.props.admin === 3 ? "Cases" : "方案检索"}</Title>
          <div className={classes.Search}>
            <div className={classes.SearchButton}>
              <Button btnType="Success" clicked={() => {
                if (this.state.keyWords.trim().length > 0) this.search(this.state.keyWords)
              }}>搜索
              </Button></div>
            <SearchInput smallMargin search placeholder="搜索多个关键词可用空格分开" value={this.state.keyWords}
                         onChange={event => this.setState({keyWords: event.target.value})}
                         onKeyPress={event => {
              if (event.key === 'Enter') {
                if (this.state.keyWords.trim().length > 0) this.search(this.state.keyWords)
              }
            }}/>
          </div>
          <div className={classes.Claim}>声明：所有资源全部来自对互联网公共资源的收集和整理，仅供学习之用。</div>
          <div className={classes.Cases}>
          {this.state.mode === 2 ? <Spinner grey/> : this.state.mode === 3 ? <Question>未找到相关方案</Question> :
            this.state.error.length > 0 ? <Question>{this.state.error}</Question> :
          <ul className={classes.List}>
            {this.state.cases.map((oneCase, index) =>
              <a key={index} href={"https://backend.voin-cn.com/getcourse/"+oneCase+".pdf"} target="_blank">
                <li key={index} className={classes.Row}>
                  {oneCase}
                </li>
              </a>)}
          </ul>}
          </div>
        </div>
      </Content>
    )
  }
}

const mapStateToProps = state => {
  return {
    admin: state.authRoute.admin
  };
};

export default connect(mapStateToProps)(Cases)