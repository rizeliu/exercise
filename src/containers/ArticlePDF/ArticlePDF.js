import React, { Component } from 'react'
import axios from 'axios'


class ArticlePDF extends Component {

  state = {
    message: "",
    file: {},
    numPages: null,
    pageNumber: 1,
  }

  componentDidMount = async () => {
    document.title = "VOIN 文章预览"
    const file = await axios.get("https://backend.voin-cn.com/getarticle", {
      method: "GET",
      responseType: "blob"
    })
    console.log(file)
    const pdf = new Blob([file.data], {
      type: "application/pdf"
    });
    const fileURL = URL.createObjectURL(pdf);
    console.log(fileURL)
    window.open(fileURL);

  }



  render () {


    return (
      <div>

      </div>
    );
  }
}



export default ArticlePDF