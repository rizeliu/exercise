import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';

import exerciseReducer from './store/reducers/exercise';
import clientsReducer from './store/reducers/clients';
import addClientReducer from './store/reducers/addClient';
import categoriesReducer from './store/reducers/categories'
import tempCatesReducer from './store/reducers/tempCates'
import authRouteReducer from './store/reducers/authRoute';
import templateReducer from './store/reducers/templates';
import clientDetailReducer from './store/reducers/clientDetail'
import detailReducer from './store/reducers/detail'
import studentReducer from './store/reducers/students'
import homeworkReducer from './store/reducers/homework'

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const appReducer = combineReducers({
  exercise: exerciseReducer,
  clients: clientsReducer,
  addClient: addClientReducer,
  categories: categoriesReducer,
  tempCates: tempCatesReducer,
  authRoute: authRouteReducer,
  templates: templateReducer,
  clientDetail: clientDetailReducer,
  detail: detailReducer,
  student: studentReducer,
  homework: homeworkReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    state = undefined
  }
  return appReducer(state, action)
}

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
))

ReactDOM.render(<Provider store={store}><BrowserRouter><App /></BrowserRouter></Provider>, document.getElementById('root'));
registerServiceWorker();
