import * as actionTypes from './actionTypes'
import axios from 'axios'
import * as actions from '../../store/actions/index';


const fetchTemplatesStart = () => {
  return {
    type: actionTypes.FETCH_TEMPLATES_START
  }
}

const fetchTemplatesSuccess = (templates, categories) => {
  return {
    type: actionTypes.FETCH_TEMPLATES_SUCCESS,
    templates, categories
  };
};

const fetchTemplatesFail = error => {
  return {
    type: actionTypes.FETCH_TEMPLATES_FAIL,
    error
  };
};

export const fetchTemplates = (doctorId) => {
  return async dispatch => {
    dispatch(fetchTemplatesStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/templates', {params: {doctorId}})
      const res = result.data
      if (res.code === 1) {
        const templates = res.doc.map(template => {
          const coveredMoves = template.coveredMoves
          const articles = template.articles
          const templateId = template._id
          const templateName = template.name
          const count = template.count
          const category = template.category
          const date = template.date
          return {templateId, templateName, count, coveredMoves, articles, category, date}
        })
        const categories = await axios.get('https://backend.voin-cn.com/categories', {params: {doctorId}})
        if (categories.data.code === 1) {
          dispatch(fetchTemplatesSuccess(templates, categories.data.cats))
        } else {
          dispatch(fetchTemplatesFail(categories.data.msg))
        }
      } else {
        dispatch(fetchTemplatesFail(res.msg))
      }
    }
    catch (error) {
      console.log(error)
      dispatch(fetchTemplatesFail("内部服务器错误，请刷新"))
    }
  }
}

const fetchExpertTemplatesStart = () => {
  return {
    type: actionTypes.FETCH_EXPERT_TEMPLATES_START
  }
}

const fetchExpertTemplatesSuccess = templates => {
  return {
    type: actionTypes.FETCH_EXPERT_TEMPLATES_SUCCESS,
    templates
  };
};

const fetchExpertTemplatesFail = error => {
  return {
    type: actionTypes.FETCH_EXPERT_TEMPLATES_FAIL,
    error
  };
};

const fetchTeamTemplatesStart = () => {
  return {
    type: actionTypes.FETCH_TEAM_TEMPLATES_START
  }
}

const fetchTeamTemplatesSuccess = templates => {
  return {
    type: actionTypes.FETCH_TEAM_TEMPLATES_SUCCESS,
    templates
  };
};

const fetchTeamTemplatesFail = error => {
  return {
    type: actionTypes.FETCH_TEAM_TEMPLATES_FAIL,
    error
  };
};

export const fetchExpertTemplates = () => {
  return async dispatch => {
    dispatch(fetchExpertTemplatesStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/extemplates')
      const res = result.data
      if (res.code === 1) {
        const templates = res.doc.map(template => {
          const moveList = template.moveList
          const articles = template.articles
          const templateId = template._id
          const templateName = template.name
          const templateSummary = template.summary
          const totalInEach = template.totalInEach
          const periodDays = template.periodDays
          const count = template.moveList.length
          return {templateId, templateName, count, moveList, articles, templateSummary, totalInEach, periodDays}
        })
        dispatch(fetchExpertTemplatesSuccess(templates))
      } else {
        dispatch(fetchExpertTemplatesFail(res.msg))
      }
    }
    catch (error) {
      dispatch(fetchExpertTemplatesFail("内部服务器错误，请刷新"))
    }
  }
}

export const fetchTeamTemplate = (refer, referee, username) => {
  return async dispatch => {
    dispatch(fetchTeamTemplatesStart())
    if (refer || referee) {
      try {
        const result = await axios.get('https://backend.voin-cn.com/teamtemplates',
          {params: {refer: refer ? refer : "1", referee: referee ? username : "1"}})
        const res = result.data
        if (res.code === 1) {
          const templates = res.doc.map(template => {
            const moveList = template.moveList
            const articles = template.articles
            const templateId = template._id
            const templateName = template.name
            const templateSummary = template.summary
            const totalInEach = template.totalInEach
            const periodDays = template.periodDays
            const count = template.moveList.length
            const upload = template.upload
            return {templateId, templateName, count, moveList, articles,
              templateSummary, totalInEach, periodDays, upload}
          })
          dispatch(fetchTeamTemplatesSuccess(templates))
        } else {
          dispatch(fetchTeamTemplatesFail(res.msg))
        }
      }
      catch (error) {
        dispatch(fetchTeamTemplatesFail("内部服务器错误，请刷新"))
      }
    }
  }
}

export const fetchTeamsAndFirst = (doctorId, admin) => {
  return async dispatch => {
    dispatch(fetchTeamsAndFirstStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/teamtemplates', {params: {doctorId, admin}})
      const res = result.data
      if (res.code === 1) {
        dispatch(fetchTeamsAndFirstSuccess(admin, res.doc, res.classId))
      } else {
        dispatch(fetchTeamsAndFirstFail(res.msg))
      }
    }
    catch (error) {
      dispatch(fetchTeamsAndFirstFail("内部服务器错误，请刷新"))
    }
  }
}


const fetchTeamsAndFirstStart = () => {
  return {
    type: actionTypes.FETCH_TEAMS_AND_FIRST_START
  }
}

const fetchTeamsAndFirstSuccess = (admin, teams, classId) => {
  return {
    type: actionTypes.FETCH_TEAMS_AND_FIRST_SUCCESS,
    admin, teams, classId
  };
};

const fetchTeamsAndFirstFail = error => {
  return {
    type: actionTypes.FETCH_TEAMS_AND_FIRST_FAIL,
    error
  };
};

export const teamChange = (value) => {
  return async dispatch => {
    dispatch(fetchTeamsAndFirstStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/teamchange', {params: {teamId: value}})
      const res = result.data
      if (res.code === 1) {
        dispatch(teamChangeSuccess(res.doc))
      } else {
        dispatch(fetchTeamsAndFirstFail(res.msg))
      }
    }
    catch (error) {
      dispatch(fetchTeamsAndFirstFail("内部服务器错误，请刷新"))
    }
  }
}

const teamChangeSuccess = team => {
  return {
    type: actionTypes.TEAM_CHANGE_SUCCESS,
    team
  };
};

export const seeTeamMembers = (teamId) => {
  return async dispatch => {
    dispatch(seeTeamMembersStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/teammembers', {params: {teamId}})
      const res = result.data
      if (res.code === 1) {
        dispatch(seeTeamMembersSuccess(res.doc))
      } else {
        dispatch(seeTeamMembersFail(res.msg))
      }
    }
    catch (error) {
      dispatch(seeTeamMembersFail("内部服务器错误，请刷新"))
    }
  }
}

const seeTeamMembersStart = () => {
  return {
    type: actionTypes.SEE_TEAM_MEMBERS_START
  }
}

const seeTeamMembersSuccess = members => {
  return {
    type: actionTypes.SEE_TEAM_MEMBERS_SUCCESS,
    members
  };
};

const seeTeamMembersFail = error => {
  return {
    type: actionTypes.SEE_TEAM_MEMBERS_FAIL,
    error
  };
};

export const deleteTeamMember = (doctorId) => {
  return {
    type: actionTypes.DELETE_TEAM_MEMBER,
    doctorId
  };
}

export const realDeleteTeamMember = (teamId, doctorId) => {
  return async dispatch => {
    dispatch(realDeleteTeamMemberStart())
    try {
      const result = await axios.post('https://backend.voin-cn.com/deletemember', {teamId, doctorId})
      const res = result.data
      if (res.code === 1) {
        dispatch(realDeleteTeamMemberSuccess())
      } else {
        dispatch(realDeleteTeamMemberFail(res.msg))
      }
    }
    catch (error) {
      dispatch(realDeleteTeamMemberFail("内部服务器错误，请刷新"))
    }
  }
}

const realDeleteTeamMemberStart = () => {
  return {
    type: actionTypes.REAL_DELETE_TEAM_MEMBER_START
  }
}

const realDeleteTeamMemberSuccess = () => {
  return {
    type: actionTypes.REAL_DELETE_TEAM_MEMBER_SUCCESS,
  };
};

const realDeleteTeamMemberFail = error => {
  return {
    type: actionTypes.REAL_DELETE_TEAM_MEMBER_FAIL,
    error
  };
};

export const deleteTeam = () => {
  return {
    type: actionTypes.DELETE_TEAM
  };
}

export const realDeleteTeam = (teamId) => {
  return async dispatch => {
    dispatch(realDeleteTeamStart())
    try {
      const response = await axios.post('https://backend.voin-cn.com/deleteteam', {teamId})
      if (response.data.code === 1) {
        dispatch(realDeleteTeamSuccess())
      } else {
        dispatch(realDeleteTeamFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(realLeaveTeamFail("内部服务器错误，请刷新"))
    }
  }
}

const realDeleteTeamStart = () => {
  return {
    type: actionTypes.REAL_DELETE_TEAM_START
  }
}

const realDeleteTeamSuccess = () => {
  return {
    type: actionTypes.REAL_DELETE_TEAM_SUCCESS,
  };
};

const realDeleteTeamFail = error => {
  return {
    type: actionTypes.REAL_DELETE_TEAM_FAIL,
    error
  };
};

export const leaveTeam = () => {
  return {
    type: actionTypes.LEAVE_TEAM
  };
}

export const realLeaveTeam = (teamId, doctorId) => {
  return async dispatch => {
    dispatch(realLeaveTeamStart())
    try {
      const response = await axios.post('https://backend.voin-cn.com/deletemember', {teamId, doctorId})
      if (response.data.code === 1) {
        dispatch(realLeaveTeamSuccess())
      } else {
        dispatch(realLeaveTeamFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(realLeaveTeamFail("内部服务器错误，请刷新"))
    }
  }
}

const realLeaveTeamStart = () => {
  return {
    type: actionTypes.REAL_LEAVE_TEAM_START
  }
}

const realLeaveTeamSuccess = () => {
  return {
    type: actionTypes.REAL_LEAVE_TEAM_SUCCESS,
  };
};

const realLeaveTeamFail = error => {
  return {
    type: actionTypes.REAL_LEAVE_TEAM_FAIL,
    error
  };
};

export const downloadTemplate = (templateId, doctorId, expertOrTeam, teamId, admin) => {
  return async dispatch => {
    dispatch(downloadTemplateStart(templateId, expertOrTeam))
    try {
      const response = await axios.post('https://backend.voin-cn.com/downloadtemplate', {
        templateId, doctorId, expertOrTeam, teamId, admin
      })
      if (response.data.code === 1) {
        dispatch(downloadTemplateSuccess(templateId, expertOrTeam))
      } else {
        dispatch(downloadTemplateFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(downloadTemplateFail("内部服务器错误，请刷新"))
    }
  }
}

const downloadTemplateStart = (templateId, expertOrTeam) => {
  return {
    type: actionTypes.DOWNLOAD_TEMPLATE_START,
    templateId, expertOrTeam
  }
}

const downloadTemplateSuccess = (templateId, expertOrTeam) => {
  return {
    type: actionTypes.DOWNLOAD_TEMPLATE_SUCCESS,
    templateId, expertOrTeam
  }
}

const downloadTemplateFail = error => {
  return {
    type: actionTypes.DOWNLOAD_TEMPLATE_FAIL,
    error
  }
}

export const addExpert = (templateId, templateName) => {
  return {
    type: actionTypes.ADD_EXPERT,
    templateId, templateName
  }
}

export const addExpertOrTeam = (templateId, templateName) => {
  return {
    type: actionTypes.ADD_EXPERT_OR_TEAM,
    templateId, templateName
  }
}

export const addTeam = (doctorId, templateId, username, index) => {
  return async dispatch => {
    dispatch(addTeamStart(index))
    try {
      const response = await axios.post('https://backend.voin-cn.com/addteam', {
        doctorId, templateId, username
      })
      if (response.data.code === 1) {
        dispatch(addTeamSuccess())
      } else {
        dispatch(addTeamFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(addTeamFail("内部服务器错误，请刷新"))
    }
  }
}

const addTeamStart = (index) => {
  return {
    type: actionTypes.ADD_TEAM_START,
    index
  }
}

const addTeamSuccess = () => {
  return {
    type: actionTypes.ADD_TEAM_SUCCESS
  }
}

const addTeamFail = error => {
  return {
    type: actionTypes.ADD_TEAM_FAIL,
    error
  }
}

// export const realAddExpert = (doctorId, labels, addTemplateId) => {
//   return async dispatch => {
//     dispatch(addExpertStart())
//     try {
//       const response = await axios.post('https://backend.voin-cn.com/addexpert', {
//         doctorId, templateId: addTemplateId, labels
//       })
//       if (response.data.code === 1) {
//         dispatch(addExpertSuccess())
//       } else {
//         dispatch(addExpertFail(response.msg))
//       }
//     }
//     catch (error) {
//       dispatch(addExpertFail("内部服务器错误，请刷新"))
//     }
//   }
// }

export const realAddClass = (doctorId, addTemplateId, classId) => {
  return async dispatch => {
    dispatch(addExpertStart())
    try {
      const response = await axios.post('https://backend.voin-cn.com/addexpertorteam', {
        doctorId, templateId: addTemplateId, classId
      })
      if (response.data.code === 1) {
        dispatch(addExpertSuccess())
      } else {
        dispatch(addExpertFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(addExpertFail("内部服务器错误，请刷新"))
    }
  }
}

export const realAddExpertOrTeam = (doctorId, labelName, labels, addTemplateId) => {
  return async dispatch => {
    dispatch(addExpertStart())
    try {
      const response = await axios.post('https://backend.voin-cn.com/addexpertorteam', {
        doctorId, labelName, templateId: addTemplateId, labels
      })
      if (response.data.code === 1) {
        dispatch(addExpertSuccess())
      } else {
        dispatch(addExpertFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(addExpertFail("内部服务器错误，请刷新"))
    }
  }
}

const addExpertStart = () => {
  return {
    type: actionTypes.ADD_EXPERT_START
  }
}

const addExpertSuccess = () => {
  return {
    type: actionTypes.ADD_EXPERT_SUCCESS
  }
}

const addExpertFail = error => {
  return {
    type: actionTypes.ADD_EXPERT_FAIL,
    error
  }
}

export const sendTemplate = (templateId, name) => {
  return {
    type: actionTypes.SEND_TEMPLATE,
    templateId, name
  }
}

export const deleteTemplate = (templateId, name, myType) => {
  return {
    type: actionTypes.DELETE_TEMPLATE,
    templateId,
    name,
    myType
  }
}

export const createQRCode = (doctorId, templateId, templateName, mode) => {
  return async dispatch => {
    dispatch(createQRCodeStart(templateName))
    try {
      const response = await axios.get('https://backend.voin-cn.com/templateqr', {params: {doctorId: doctorId, templateId: templateId, mode}})
      if (response.data.code === 1) {
        const buffer = new Buffer(response.data.buffer)
        const base64Str = buffer.toString('base64')
        dispatch(createQRCodeSuccess(base64Str))
      } else {
        dispatch(createQRCodeFail(response.data.msg))
      }
    }
    catch (error) {
      dispatch(createQRCodeFail("内部服务器错误，请刷新"))
    }
  }
}


const createQRCodeStart = (templateName) => {
  return {
    type: actionTypes.CREATE_QR_CODE_START,
    templateName
  }
}

const createQRCodeSuccess = (base64Str) => {
  return {
    type: actionTypes.CREATE_QR_CODE_SUCCESS,
    base64Str
  }
}

const createQRCodeFail = error => {
  return {
    type: actionTypes.CREATE_QR_CODE_FAIL,
    error
  }
}

const realTemplateDeleteStart = () => {
  return {
    type: actionTypes.REAL_TEMPLATE_DELETE_START
  }
}

const realTemplateDeleteFail = error => {
  return {
    type: actionTypes.REAL_TEMPLATE_DELETE_FAIL,
    error
  }
}

const realTemplateDeleteSuccess = myType => {
  return {
    type: actionTypes.REAL_TEMPLATE_DELETE_SUCCESS,
    myType
  }
}

export const realTemplateDelete = (myType, teamId, doctorId, templateId, admin) => {
  return async dispatch => {
    dispatch(realTemplateDeleteStart())
    try {
      const deleteRes = await axios.post('https://backend.voin-cn.com/deletetemplate',
        {myType, teamId, doctorId, templateId, admin})
      if (deleteRes.data.code === 0) {
        dispatch(realTemplateDeleteFail(deleteRes.data.msg))
      } else if (deleteRes.data.code === 1) {
        dispatch(realTemplateDeleteSuccess(deleteRes.data.myType))
      }
    } catch (error) {
      dispatch(realTemplateDeleteFail("内部服务器错误，请刷新"))

    }
  }

}

export const deleteTemplateFinished = () => {
  return {
    type: actionTypes.DELETE_TEMPLATE_FINISHED
  }
}

export const editTemplate = (doctorId, template, index, page) => {
  return async dispatch => {
    dispatch(editTemplateStart(index))
    try {
      const fullTemplate= await axios.get('https://backend.voin-cn.com/templatebyid', {params: {doctorId, templateId: template.templateId}})
      const moveIds = fullTemplate.data.moveList.filter(move => move.move).map(move => move.move)
      const moveWithArticleIds = moveIds.concat(fullTemplate.data.articles)
      const allIds = moveWithArticleIds.concat(fullTemplate.data.therapyList.map(therapy => therapy.move))
      const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
      if (moveSet.data.code === 1) {
        const moveResult = moveSet.data.doc
        const planDetail = fullTemplate.data.moveList.map((move) => {
          if (!move.bar) {
            const thisMove = moveResult.filter(aMove => aMove._id === move.move)[0]
            return {
              ...move,
              moveId: move.move,
              moveName: thisMove.name
            }
          } else {
            return {
              ...move
            }
          }
        })
        const articleDetail = fullTemplate.data.articles.map(article => {
          const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
          return {
            moveId: article,
            moveName: thisArticle.name
          }
        })
        const therapyDetail = fullTemplate.data.therapyList.map(therapy => {
          const thisTherapy = moveResult.filter(aTherapy => aTherapy._id === therapy.move)[0]
          return {
            ...therapy,
            moveId: therapy.move,
            moveName: thisTherapy.name
          }
        })
        dispatch(editTemplateSuccess(fullTemplate.data, planDetail, therapyDetail, articleDetail, page))
        dispatch(actions.searchChosenVideos(planDetail, therapyDetail, articleDetail))
      } else {
        dispatch(editTemplateFail("后端出错了，请刷新"))
      }
    } catch (error) {
      dispatch(editTemplateFail("内部服务器错误，请刷新"))
    }
  }
}

const editTemplateStart = index => {
  return {
    type: actionTypes.EDIT_TEMPLATE_START,
    index
  }
}

const editTemplateSuccess = (template, moves, therapies, articles, page)  => {
  page.props.history.push('/exercises/template')
  return {
    type: actionTypes.EDIT_TEMPLATE_SUCCESS,
    template, moves, therapies, articles
  }
}

const editTemplateFail = msg => {
  return {
    type: actionTypes.EDIT_TEMPLATE_FAIL,
    msg
  }
}

export const createPlanByTemplate = (doctorId, template, index, page, type, from, clientId) => {
  return async dispatch => {
    if (type === "template") {
      dispatch(createPlanByTemplateStart(index))
    } else {
      dispatch(createPlanByRecommendTemplateStart(index, from))
    }
    try {
      const newDocId = type === "template" ? doctorId : from === "self" ? doctorId : "5b68bb0fde7068270a29d0fe"
      const fullTemplate= await axios.get('https://backend.voin-cn.com/templatebyid', {
        params: {doctorId: newDocId, templateId: type === "template" ? template.templateId : template._id}
      })
      const moveIds = fullTemplate.data.moveList.filter(move => move.move).map(move => move.move)
      const moveWithArticleIds = moveIds.concat(fullTemplate.data.articles)
      const allIds = moveWithArticleIds.concat(fullTemplate.data.therapyList.map(therapy => therapy.move))
      const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
      if (moveSet.data.code === 1) {
        const moveResult = moveSet.data.doc
        const planDetail = fullTemplate.data.moveList.map((move) => {
          if (!move.bar) {
            const thisMove = moveResult.filter(aMove => aMove._id === move.move)[0]
            return {
              ...move,
              moveId: move.move,
              moveName: thisMove.name
            }
          } else {
            return {
              ...move
            }
          }
        })
        const articleDetail = fullTemplate.data.articles.map(article => {
          const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
          return {
            moveId: article,
            moveName: thisArticle.name
          }
        })
        const therapyDetail = fullTemplate.data.therapyList.map(therapy => {
          const thisTherapy = moveResult.filter(aTherapy => aTherapy._id === therapy.move)[0]
          return {
            ...therapy,
            moveId: therapy.move,
            moveName: thisTherapy.name
          }
        })
        if (type === "template") {
          dispatch(createPlanByTemplateSuccess(fullTemplate.data, planDetail, therapyDetail, articleDetail, page))
        } else {
          dispatch(createPlanByTemplateSuccess(fullTemplate.data, planDetail, therapyDetail, articleDetail, page, clientId))
        }
        dispatch(actions.searchChosenVideos(planDetail, therapyDetail, articleDetail))
      } else {
        dispatch(createPlanByTemplateFail("后端出错了，请刷新"))
      }
    } catch (error) {
      dispatch(createPlanByTemplateFail("内部服务器错误，请刷新"))
    }
  }
}

const createPlanByTemplateStart = index => {
  return {
    type: actionTypes.CREATE_PLAN_BY_TEMPLATE_START,
    index
  }
}

const createPlanByRecommendTemplateStart = (index, from) => {
  return {
    type: actionTypes.CREATE_PLAN_BY_RECOMMEND_TEMPLATE_START,
    index, from
  }
}

const createPlanByTemplateSuccess = (template, moves, therapies, articles, page, clientId)  => {
  page.props.history.push('/exercises')
  return {
    type: actionTypes.CREATE_PLAN_BY_TEMPLATE_SUCCESS,
    template, moves, therapies, articles, clientId
  }
}

const createPlanByTemplateFail = msg => {
  return {
    type: actionTypes.CREATE_PLAN_BY_TEMPLATE_FAIL,
    msg
  }
}

export const fetchAllDoctors = () => {
  return async dispatch => {
    dispatch(fetchDoctorsStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/doctors')
      const res = result.data
      dispatch(fetchDoctorsSuccess(res.doc))
    } catch (e) {
      dispatch(fetchDoctorsFail("内部服务器错误，请刷新"))
    }

  }
}

const fetchDoctorsStart = () => {
  return {
    type: actionTypes.FETCH_DOCTORS_START,
  }
}

const fetchDoctorsSuccess = doctors => {
  return {
    type: actionTypes.FETCH_DOCTORS_SUCCESS,
    doctors
  };
};

const fetchDoctorsFail = error => {
  return {
    type: actionTypes.FETCH_DOCTORS_FAIL,
    error
  };
};

export const changeDoctor = doctorId => {
  return {
    type: actionTypes.CHANGE_DOCTOR,
    doctorId
  };
}

export const changeLabel = (name, value) => {
  return {
    type: actionTypes.CHANGE_LABEL,
    name, value
  };
}

export const searchDoctors = value => {
  return {
    type: actionTypes.SEARCH_DOCTORS,
    value
  };
}

export const realSendTemplate = (doctorId1, templateId, doctorIds) => {
  return async dispatch => {
    dispatch(realSendTemplateStart())
    try {
      const result = await axios.post('https://backend.voin-cn.com/sendtemplate', {doctorId1, templateId, doctorIds})
      const res = result.data
      if (res.code === 1) {
        dispatch(realSendTemplateSuccess(res.doc))
      } else {
        dispatch(realSendTemplateFail(res.msg))
      }
    } catch (e) {
      dispatch(realSendTemplateFail("内部服务器错误，请刷新"))
    }

  }
}

const realSendTemplateStart = () => {
  return {
    type: actionTypes.REAL_SEND_TEMPLATE_START
  }
}

const realSendTemplateSuccess = () => {
  return {
    type: actionTypes.REAL_SEND_TEMPLATE_SUCCESS
  };
};

const realSendTemplateFail = error => {
  return {
    type: actionTypes.REAL_SEND_TEMPLATE_FAIL,
    error
  };
};

export const searchTemplateChange = value => {
  return {
    type: actionTypes.SEARCH_TEMPLATE_CHANGE,
    value
  }
}

export const searchExpertTemplateChange = value => {
  return {
    type: actionTypes.SEARCH_EXPERT_TEMPLATE_CHANGE,
    value
  }
}

export const searchTeamTemplateChange = value => {
  return {
    type: actionTypes.SEARCH_TEAM_TEMPLATE_CHANGE,
    value
  }
}

export const switchTemplateFocusToMy = (value) => {
  return {
    type: actionTypes.SWITCH_TEMPLATE_FOCUS_TO_MY,
    value
  }
}

export const showAllTemplates = () => {
  return {
    type: actionTypes.SHOW_ALL_TEMPLATES
  }
}

export const changeContent = content => {
  return {
    type: actionTypes.CHANGE_CONTENT,
    content
  }
}

export const syncCircle = () => {
  return async dispatch => {
    try {
      const response = await axios.get('https://backend.voin-cn.com/synccircles')
      if (response.data.code === 1) {
        dispatch(syncCircleSuccess(response.data.doc))
      } else {
        dispatch(syncCircleFail(response.data.msg))
      }
    }
    catch (error) {
      console.log(error)
      dispatch(syncCircleFail("内部服务器错误，请刷新"))
    }
  }
}

const syncCircleSuccess = (circles) => {
  return {
    type: actionTypes.SYNC_CIRCLE_SUCCESS,
    circles
  };
};

const syncCircleFail = error => {
  return {
    type: actionTypes.SYNC_CIRCLE_FAIL,
    error
  };
};

export const getCircle = () => {
  return async dispatch => {
    try {
      const response = await axios.get('https://backend.voin-cn.com/getcircles')
      if (response.data.code === 1) {
        dispatch(getCircleSuccess(response.data.doc))
      } else {
        dispatch(getCircleFail(response.data.msg))
      }
    }
    catch (error) {
      console.log(error)
      dispatch(getCircleFail("内部服务器错误，请刷新"))
    }
  }
}

const getCircleSuccess = (circles) => {
  return {
    type: actionTypes.GET_CIRCLE_SUCCESS,
    circles
  };
};

const getCircleFail = error => {
  return {
    type: actionTypes.GET_CIRCLE_FAIL,
    error
  };
};

export const addTemplateToCircle = (templateId, templateName) => {
  return {
    type: actionTypes.ADD_TEMPLATE_TO_CIRCLE,
    templateId, templateName
  };
};

export const changeCircle = (name, id) => {
  return {
    type: actionTypes.CHANGE_CIRCLE,
    name, id
  };
};

export const realAddTemplateToCircle = (doctorId, templateId, circleId) => {
  return async dispatch => {
    dispatch(realAddTemplateToCircleStart())
    try {
      const response = await axios.post('https://backend.voin-cn.com/addemplatetocircle', {doctorId, templateId, circleId})
      if (response.data.code === 1) {
        dispatch(realAddTemplateToCircleSuccess(response.data.doc))
      } else {
        dispatch(realAddTemplateToCircleFail(response.data.msg))
      }
    }
    catch (error) {
      console.log(error)
      dispatch(realAddTemplateToCircleFail("内部服务器错误，请刷新"))
    }
  }
}

const realAddTemplateToCircleStart = () => {
  return {
    type: actionTypes.REAL_ADD_TEMPLATE_TO_CIRCLE_START,
  };
};

const realAddTemplateToCircleSuccess = () => {
  return {
    type: actionTypes.REAL_ADD_TEMPLATE_TO_CIRCLE_SUCCESS,
  };
};

const realAddTemplateToCircleFail = error => {
  return {
    type: actionTypes.REAL_ADD_TEMPLATE_TO_CIRCLE_FAIL,
    error
  };
};
