import * as actionTypes from './actionTypes'
import axios from 'axios'

export const fetchClassesAndStudents = (doctorId) => {
  return async dispatch => {
    dispatch(fetchClassesAndStudentsStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/classstudent', {params: {doctorId}})
      const res = result.data
      if (res.code === 1) {
        const titles = [...res.titles]
        titles.reverse()
        dispatch(fetchClassesAndStudentsSuccess(titles, res.students, res.classInfo))
      } else {
        dispatch(fetchClassesAndStudentsFail(res.msg))
      }
    }
    catch (error) {
      dispatch(fetchClassesAndStudentsFail("内部服务器错误，请刷新"))
    }
  }
}

const fetchClassesAndStudentsStart = () => {
  return {
    type: actionTypes.FETCH_CLASSES_AND_STUDENTS_START
  }
}

const fetchClassesAndStudentsSuccess = (titles, students, classInfo) => {
  return {
    type: actionTypes.FETCH_CLASSES_AND_STUDENTS_SUCCESS,
    titles, students, classInfo
  };
};

const fetchClassesAndStudentsFail = msg => {
  return {
    type: actionTypes.FETCH_CLASSES_AND_STUDENTS_FAIL,
    msg
  };
};

export const titleChange = titleId => {
  return async dispatch => {
    dispatch(titleChangeStart(titleId))
    if (!titleId) dispatch(titleChangeSuccess([]))
    try {
      const result = await axios.get('https://backend.voin-cn.com/homeworkfortitle', {params: {titleId}})
      const res = result.data
      if (res.code === 1) {
        dispatch(titleChangeSuccess(res.doc))
      } else {
        dispatch(titleChangeFail(res.msg))
      }
    }
    catch (error) {
      dispatch(titleChangeFail("内部服务器错误，请刷新"))
    }
  }
};

const titleChangeStart = (titleId) => {
  return {
    type: actionTypes.TITLE_CHANGE_START,
    titleId
  }
}

const titleChangeSuccess = (doc) => {
  return {
    type: actionTypes.TITLE_CHANGE_SUCCESS,
    doc
  };
};

const titleChangeFail = msg => {
  return {
    type: actionTypes.TITLE_CHANGE_FAIL,
    msg
  };
};

// const dataURLtoFile = (dataurl, filename) => {
//   const arr = dataurl.split(',')
//   const mime = arr[0].match(/:(.*?);/)[1]
//   const bstr = atob(arr[1])
//   let n = bstr.length
//   const u8arr = new Uint8Array(n)
//   while (n) {
//     u8arr[n - 1] = bstr.charCodeAt(n - 1)
//     n -= 1 // to make eslint happy
//   }
//   return new File([u8arr], filename, { type: mime })
// }

export const submitNewTitle = (classId, title, name, sex, age, history, smoke, description, doctorId, selectedFiles, names) => {
  return async dispatch => {
    dispatch(submitNewTitleStart())
    try {
      // const config = {
      //   headers: { 'Content-Type': 'multipart/form-data' }
      // }
      //
      // const data = new FormData()
      // console.log(images.length)
      // for (let i = 0; i < images.length; i++) {
      //   console.log(i)
      //   const file = dataURLtoFile(images[i], classId + "-" + i)
      //   data.append("file", file, file.name)
      //   const resultI = await axios.post('https://backend.voin-cn.com/submittitlerecord', data, config)
      //   console.log(resultI)
      //   if (resultI.data.code !== 1) {
      //     console.log("fail1")
      //     dispatch(submitNewTitleFail(resultI.data.msg))
      //     return
      //   }
      // }


      const dateTime = new Date().getTime()

      const images = selectedFiles ? selectedFiles.map((file, index) => classId + "_" + dateTime + "_" + index) : []

      const result = await axios.post('https://backend.voin-cn.com/submittitle',
        {classId, title, name, sex, age, history, smoke, description, images})
      const res = result.data
      if (res.code === 1) {


        if (selectedFiles.length > 0) {
          const formData = new FormData();

          // Update the formData object
          for (let i = 0; i < selectedFiles.length; i++) {
            formData.append(
                "myFile"+i,
                selectedFiles[i],
                names[i]
            );
          }

          formData.append("classId", classId)
          formData.append("titleId", res.titleId)
          formData.append("dateTime", dateTime)
          formData.append("size", selectedFiles.length)
          console.log("dao1")

          const result = await axios({
            url: "https://backend.voin-cn.com/homeworkupload",
            method: 'post',
            maxContentLength: Infinity,
            maxBodyLength: Infinity,
            data: formData
          })


          if (result.data.code === 1) {
            console.log(1)
          } else {
            console.log(2)
          }

        }


        dispatch(submitNewTitleSuccess(res.titleId))
      } else {
        console.log(res.msg)
        dispatch(submitNewTitleFail(res.msg))
      }
    }
    catch (error) {
      console.log(error)
      dispatch(submitNewTitleFail("内部服务器错误，请刷新"))
    }
  }
};

const submitNewTitleStart = () => {
  return {
    type: actionTypes.SUBMIT_NEW_TITLE_START
  }
}

const submitNewTitleSuccess = (titleId) => {
  return {
    type: actionTypes.SUBMIT_NEW_TITLE_SUCCESS,
    titleId
  };
};

const submitNewTitleFail = msg => {
  return {
    type: actionTypes.SUBMIT_NEW_TITLE_FAIL,
    msg
  };
};

export const submitReview = (studentId, titleId, score, review) => {
  return async dispatch => {
    dispatch(submitReviewStart())
    try {
      const result = await axios.post('https://backend.voin-cn.com/submitreview', {studentId, titleId, score, review})
      const res = result.data
      if (res.code === 1) {
        dispatch(submitReviewSuccess())
      } else {
        dispatch(submitReviewFail(res.msg))
      }
    }
    catch (error) {
      dispatch(submitReviewFail("内部服务器错误，请刷新"))
    }
  }
};

const submitReviewStart = () => {
  return {
    type: actionTypes.SUBMIT_REVIEW_START
  }
}

const submitReviewSuccess = () => {
  return {
    type: actionTypes.SUBMIT_REVIEW_SUCCESS,
  };
};

const submitReviewFail = msg => {
  return {
    type: actionTypes.SUBMIT_REVIEW_FAIL,
    msg
  };
};

const modalStudentLoading = () => {
  return {
    type: actionTypes.MODAL_STUDENT_LOADING
  }
}

export const closeStudentProposal = () => {
  return {
    type: actionTypes.CLOSE_STUDENT_PROPOSAL
  }
}

const seeProposalSuccess = (name, proposalContent, references) => {
  return {
    type: actionTypes.SEE_PROPOSAL_SUCCESS,
    name, proposalContent, references
  }
}

const seeProposalFail = msg => {
  return {
    type: actionTypes.SEE_PROPOSAL_FAIL,
    msg
  }
}


export const seeProposal = (doctorId, titleId) => {
  return async dispatch => {
    dispatch(modalStudentLoading())
    try {
      const plan = await axios.get('https://backend.voin-cn.com/proposalbyid', {params: {doctorId, titleId}})
      if (plan.data.code === 1) {
        const proposalContent = plan.data.proposalContent
        const references = plan.data.references
        const name = plan.data.name

        dispatch(seeProposalSuccess(
            name, proposalContent, references
        ))

      } else if (plan.data.code === 0) {
        dispatch(seeProposalFail(plan.data.msg))
      }
    } catch (error) {
      dispatch(seeProposalFail("内部服务器错误，请刷新"))
    }
  }
}