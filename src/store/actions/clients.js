import * as actionTypes from './actionTypes'
import axios from 'axios'

const fetchClientStart = () => {
  return {
    type: actionTypes.FETCH_CLIENTS_START
  }
}

const fetchClientsSuccess = (clients, categories) => {
  return {
    type: actionTypes.FETCH_CLIENTS_SUCCESS,
    clients, categories
  };
};

const fetchClientsFail = error => {
  return {
    type: actionTypes.FETCH_CLIENTS_FAIL,
    error
  };
};

export const fetchClients = (doctorId) => {
  return async dispatch => {
    dispatch(fetchClientStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/plans', {params: {doctorId}})
      const res = result.data
      if (res.code === 1) {
        const clientIds = res.doc.map(plan => plan.clientId)
        const clients = await axios.post('https://backend.voin-cn.com/clientsbyid', {clientIds})
        if (clients.data.code === 1) {
          const clientInfo = [...clients.data.doc]
          clientInfo.sort((a, b) =>  {
            return clientIds.indexOf(a._id) - clientIds.indexOf(b._id);
          });
          const realClients = clientInfo.map((client) => {
            const clientInRes = res.doc.filter(cl => cl.clientId === client._id)[0]
            return {
              clientId: client._id,
              clientName: client.name,
              sex: client.sex,
              age: client.age,
              refer: clientInRes.refer,
              allPlans: clientInRes.plans.map(plan => plan.paid),
              empty: clientInRes.empty,
              request: clientInRes.request,
              read: !!clientInRes.read,
              category: clientInRes.category,
              childDoctorId: clientInRes.childDoctorId
            }
          })
          const categories = await axios.get('https://backend.voin-cn.com/categories', {params: {doctorId}})
          if (categories.data.code === 1) {
            dispatch(fetchClientsSuccess(realClients, categories.data.cats))
          } else {
            dispatch(fetchClientsFail(categories.data.msg))
          }
        } else {
          dispatch(fetchClientsFail(clients.data.msg))
        }
      } else {
        dispatch(fetchClientsFail(res.msg))
      }
    }
    catch (error) {
      dispatch(fetchClientsFail("内部服务器错误，请刷新"))
    }
  }
}


export const addClientToCategoryDetail = (doctorId, clientId, name, category) => {
  return async dispatch => {
    dispatch(fetchCategoryStart())
    try {
      const categories = await axios.get('https://backend.voin-cn.com/categories', {params: {doctorId}})
      if (categories.data.code === 1) {
        dispatch(fetchCategorySuccess(categories.data.cats, clientId, name, category))
      } else {
        console.log(categories.data)
        dispatch(fetchCategoryFail(categories.data.msg))
      }
    }
    catch (error) {
      console.log(error)
      dispatch(fetchCategoryFail("内部服务器错误，请刷新"))
    }
  }
}

const fetchCategoryStart = () => {
  return {
    type: actionTypes.FETCH_CATEGORY_START
  }
}

const fetchCategorySuccess = (categories, clientId, name, category) => {
  return {
    type: actionTypes.FETCH_CATEGORY_SUCCESS,
    categories, clientId, name, category
  };
};

const fetchCategoryFail = error => {
  return {
    type: actionTypes.FETCH_CATEGORY_FAIL,
    error
  };
};

export const showAllClients = () => {
  return {
    type: actionTypes.SHOW_ALL_CLIENTS
  }
}

export const clientSearchChange = (value) => {
  return {
    type: actionTypes.CLIENT_SEARCH_CHANGE,
    value
  }
}

export const diseaseCodeSearchChange = (value) => {
  return {
    type: actionTypes.DISEASE_CODE_SEARCH_CHANGE,
    value
  }
}


export const deleteClient = (clientId, name) => {
  return {
    type: actionTypes.DELETE_CLIENT,
    clientId,
    name
  }
}

export const closeModal = () => {
  return {
    type: actionTypes.CLOSE_MODAL
  }
}

const realClientDeleteStart = () => {
  return {
    type: actionTypes.REAL_CLIENT_DELETE_START
  }
}

const realClientDeleteFail = error => {
  return {
    type: actionTypes.REAL_CLIENT_DELETE_FAIL,
    error
  }
}

const realClientDeleteSuccess = () => {
  return {
    type: actionTypes.REAL_CLIENT_DELETE_SUCCESS
  }
}

export const realClientDelete = (doctorId, clientId) => {
  return async dispatch => {
    dispatch(realClientDeleteStart())
    try {
      const deleteRes = await axios.post('https://backend.voin-cn.com/deleteclient', {doctorId, clientId})
      if (deleteRes.data.code === 0) {
        dispatch(realClientDeleteFail(deleteRes.data.msg))
      } else if (deleteRes.data.code === 1) {
        dispatch(realClientDeleteSuccess())
      }
    } catch (error) {
      dispatch(realClientDeleteFail("内部服务器错误，请刷新"))

    }
  }

}

export const deleteClientFinished = () => {
  return {
    type: actionTypes.DELETE_CLIENT_FINISHED
  }
}

export const seeRequest = (index, first, clientId, doctorId) => {
  if (first) {
    axios.post('https://backend.voin-cn.com/readrequest', {doctorId, clientId})
  }
  return {
    type: actionTypes.SEE_REQUEST,
    index
  }
}

export const sortClients = (category) => {
  return {
    type: actionTypes.SORT_CLIENTS,
    category
  }
}

export const addCategory = () => {
  return {
    type: actionTypes.ADD_CATEGORY
  }
}

export const addChildCategory = () => {
  return {
    type: actionTypes.ADD_CHILD_CATEGORY
  }
}

export const deleteChildCategory = () => {
  return {
    type: actionTypes.DELETE_CHILD_CATEGORY
  }
}

export const deleteCategory = () => {
  return {
    type: actionTypes.DELETE_CATEGORY
  }
}

export const deleteDiseaseCategory = () => {
  return {
    type: actionTypes.DELETE_DISEASE_CATEGORY
  }
}

export const deleteSecondCategory = () => {
  return {
    type: actionTypes.DELETE_SECOND_CATEGORY
  }
}

export const deleteOperationCategory = () => {
  return {
    type: actionTypes.DELETE_OPERATION_CATEGORY
  }
}

export const cancelAddCategory = () => {
  return {
    type: actionTypes.CANCEL_ADD_CATEGORY
  }
}

export const cancelAddChildCategory = () => {
  return {
    type: actionTypes.CANCEL_ADD_CHILD_CATEGORY
  }
}

export const cancelDeleteCategory = () => {
  return {
    type: actionTypes.CANCEL_DELETE_CATEGORY
  }
}

export const specialize = (catId) => {
  return async dispatch => {
    dispatch(specializeStart(catId))
    try {
      const response = await axios.post('https://backend.voin-cn.com/specialize', {catId})
      if (response.data.code === 0) {
        dispatch(specializeFail(response.data.msg))
      } else if (response.data.code === 1) {
        dispatch(specializeSuccess())
      }
    } catch (error) {
      dispatch(specializeFail("内部服务器错误，请刷新"))

    }
  }
}

const specializeStart = (catId) => {
  return {
    type: actionTypes.SPECIALIZE_START,
    catId
  }
}

const specializeFail = error => {
  return {
    type: actionTypes.SPECIALIZE_FAIL,
    error
  }
}

const specializeSuccess = () => {
  return {
    type: actionTypes.SPECIALIZE_SUCCESS
  }
}

export const seeQRCode = (catId) => {
  return async dispatch => {
    // dispatch(seeQRCodeStart(catId))
    try {
      const response = await axios.get('https://backend.voin-cn.com/categorycode', {params: {catId}})
      if (response.data.code === 0) {
        dispatch(seeQRCodeFail(response.data.msg))
      } else if (response.data.code === 1) {
        const buffer = new Buffer(response.data.buffer)
        const base64Str = buffer.toString('base64')
        dispatch(seeQRCodeSuccess(catId, base64Str))
      }
    } catch (error) {
      dispatch(seeQRCodeFail("内部服务器错误，请刷新"))

    }
  }
}

const seeQRCodeStart = (catId) => {
  return {
    type: actionTypes.SPECIALIZE_START,
    catId
  }
}

const seeQRCodeFail = error => {
  return {
    type: actionTypes.SEE_QR_CODE_FAIL,
    error
  }
}

const seeQRCodeSuccess = (catId, base64Str) => {
  return {
    type: actionTypes.SEE_QR_CODE_SUCCESS,
    base64Str, catId
  }
}


export const addCategoryChange = (value, mode) => {
  return {
    type: actionTypes.ADD_CATEGORY_CHANGE,
    value, mode
  }
}

export const realAddCategory = (name, doctorId, type, first) => {
  return async dispatch => {
    if (name.trim().length === 0) {
      if (type === "defined") {
        dispatch(addEmptyCategory())
      } else if (type === "second") {
        dispatch(addEmptyChildCategory())
      }
    } else if (name.includes("$")) {
      if (type === "defined") {
        dispatch(addDollarCategory())
      } else if (type === "second") {
        dispatch(addDollarChildCategory())
      }
    } else {
      if (type === "defined") dispatch(realAddCategoryStart())
      else if (type === "second") dispatch(realAddChildCategoryStart())
      else if (type === "disease") dispatch(searchDiseaseCategoryStart())
      else if (type === "operation") dispatch(searchDiseaseCategoryStart())
      try {
        const newName = type === "second" ? first + "$" + name : name
        const addResult = await axios.post('https://backend.voin-cn.com/addcategory', {doctorId, name: newName, type})
        if (addResult.data.code === 0) {
          if (type === "second") dispatch(realAddChildCategoryFail(addResult.data.msg))
          else if (type === "defined") dispatch(realAddCategoryFail(addResult.data.msg))
          else dispatch(realAddRestCategoryFail(addResult.data.msg))
        } else if (addResult.data.code === 1) {
          dispatch(realAddCategorySuccess())
        }
      } catch (error) {
        dispatch(realAddCategoryFail("内部服务器错误，请刷新"))

      }
    }
  }
}

export const realAddCategories = (names, doctorId, type) => {
  return async dispatch => {
    dispatch(searchDiseaseCategoryStart())
    try {
      const addResult = await axios.post('https://backend.voin-cn.com/addcategories', {doctorId, names, type})
      if (addResult.data.code === 0) {
        dispatch(realAddRestCategoryFail(addResult.data.msg))
      } else if (addResult.data.code === 1) {
        dispatch(realAddCategorySuccess())
      }
    } catch (error) {
      dispatch(realAddCategoryFail("内部服务器错误，请刷新"))

    }
  }
}

const addEmptyCategory = () => {
  return {
    type: actionTypes.ADD_EMPTY_CATEGORY,
  }
}

const addEmptyChildCategory = () => {
  return {
    type: actionTypes.ADD_EMPTY_CHILD_CATEGORY,
  }
}

const addDollarCategory = () => {
  return {
    type: actionTypes.ADD_DOLLAR_CATEGORY,
  }
}

const addDollarChildCategory = () => {
  return {
    type: actionTypes.ADD_DOLLAR_CHILD_CATEGORY,
  }
}

const realAddCategoryStart = () => {
  return {
    type: actionTypes.REAL_ADD_CATEGORY_START
  }
}

const realAddChildCategoryStart = () => {
  return {
    type: actionTypes.REAL_ADD_CHILD_CATEGORY_START
  }
}

const realAddCategoryFail = error => {
  return {
    type: actionTypes.REAL_ADD_CATEGORY_FAIL,
    error
  }
}

const realAddChildCategoryFail = error => {
  return {
    type: actionTypes.REAL_ADD_CHILD_CATEGORY_FAIL,
    error
  }
}

const realAddRestCategoryFail = error => {
  return {
    type: actionTypes.REAL_ADD_REST_CATEGORY_FAIL,
    error
  }
}

const realAddCategorySuccess = () => {
  return {
    type: actionTypes.REAL_ADD_CATEGORY_SUCCESS
  }
}

export const realDeleteCategory = (mode, doctorId, categoryId, seconds) => {
  return async dispatch => {
    if (mode === 1) dispatch(realDeleteCategoryStart())
    else if (mode === 2) dispatch(realDeleteDiseaseCategoryStart())
    else if (mode === 3) dispatch(realDeleteOperationCategoryStart())
    else if (mode === 4) dispatch(realDeleteSecondCategoryStart())
    try {
      if (mode === 1 && seconds[categoryId]) {
        const ids = [categoryId]
        ids.push(...seconds[categoryId].map(cat => cat._id))
        const deleteResult = await axios.post('https://backend.voin-cn.com/deletecategories', {doctorId, ids})
        if (deleteResult.data.code === 0) {
          dispatch(realDeleteCategoryFail(deleteResult.data.msg))
        } else if (deleteResult.data.code === 1) {
          dispatch(realDeleteCategorySuccess())
        }
      } else {
        const deleteResult = await axios.post('https://backend.voin-cn.com/deletecategory', {doctorId, categoryId})
        if (deleteResult.data.code === 0) {
          if (mode === 1) dispatch(realDeleteCategoryFail(deleteResult.data.msg))
          else if (mode === 2) dispatch(realDeleteDiseaseCategoryFail(deleteResult.data.msg))
          else if (mode === 3) dispatch(realDeleteOperationCategoryFail(deleteResult.data.msg))
          else if (mode === 4) dispatch(realDeleteSecondCategoryFail(deleteResult.data.msg))
        } else if (deleteResult.data.code === 1) {
          dispatch(realDeleteCategorySuccess())
        }
      }

    } catch (error) {
      if (mode === 1) dispatch(realDeleteCategoryFail("内部服务器错误，请刷新"))
      else if (mode === 2) dispatch(realDeleteDiseaseCategoryFail("内部服务器错误，请刷新"))
      else if (mode === 3) dispatch(realDeleteOperationCategoryFail("内部服务器错误，请刷新"))
      else if (mode === 4) dispatch(realDeleteSecondCategoryFail("内部服务器错误，请刷新"))
    }
  }

}


const realDeleteCategoryStart = () => {
  return {
    type: actionTypes.REAL_DELETE_CATEGORY_START
  }
}

const realDeleteSecondCategoryStart = () => {
  return {
    type: actionTypes.REAL_DELETE_SECOND_CATEGORY_START
  }
}

const realDeleteDiseaseCategoryStart = () => {
  return {
    type: actionTypes.REAL_DELETE_DISEASE_CATEGORY_START
  }
}

const realDeleteOperationCategoryStart = () => {
  return {
    type: actionTypes.REAL_DELETE_OPERATION_CATEGORY_START
  }
}

const realDeleteCategoryFail = error => {
  return {
    type: actionTypes.REAL_DELETE_CATEGORY_FAIL,
    error
  }
}

const realDeleteSecondCategoryFail = error => {
  return {
    type: actionTypes.REAL_DELETE_SECOND_CATEGORY_FAIL,
    error
  }
}

const realDeleteDiseaseCategoryFail = error => {
  return {
    type: actionTypes.REAL_DELETE_DISEASE_CATEGORY_FAIL,
    error
  }
}

const realDeleteOperationCategoryFail = error => {
  return {
    type: actionTypes.REAL_DELETE_OPERATION_CATEGORY_FAIL,
    error
  }
}

const realDeleteCategorySuccess = () => {
  return {
    type: actionTypes.REAL_DELETE_CATEGORY_SUCCESS
  }
}

export const chooseCategory = (id, ids) => {
  return {
    type: actionTypes.CHOOSE_CATEGORY,
    id, ids
  }
}

export const chooseSecondCategory = (id) => {
  return {
    type: actionTypes.CHOOSE_SECOND_CATEGORY,
    id
  }
}

export const addClientToCategory = (id, name, category, childDoctorId) => {
  return {
    type: actionTypes.ADD_CLIENT_TO_CATEGORY,
    id, name, category, childDoctorId
  }
}

export const addTemplateToCategory = (id, name, category) => {
  return {
    type: actionTypes.ADD_TEMPLATE_TO_CATEGORY,
    id, name, category
  }
}

export const deleteOneCategory = (mode, id) => {
  if (mode === 1) {
    return {
      type: actionTypes.DELETE_ONE_CATEGORY,
      id
    }
  } else if (mode === 2) {
    return {
      type: actionTypes.DELETE_ONE_DISEASE_CATEGORY,
      id
    }
  } else if (mode === 3) {
    return {
      type: actionTypes.DELETE_ONE_OPERATION_CATEGORY,
      id
    }
  } else if (mode === 4) {
    return {
      type: actionTypes.DELETE_ONE_SECOND_CATEGORY,
      id
    }
  }
}

export const clickCategory = (value) => {
  return {
    type: actionTypes.CLICK_CATEGORY,
    value
  }
}

export const clickChildDoctor = (value) => {
  return {
    type: actionTypes.CLICK_CHILD_DOCTOR,
    value
  }
}

export const checkDiseaseCategory = (value, label) => {
  return {
    type: actionTypes.CHECK_DISEASE_CATEGORY,
    value, label
  }
}

export const realAddClientToCategory = (doctorId, clientId, categoryIds, childDoctorId, type) => {
  return async dispatch => {
    dispatch(realAddClientToCategoryStart())
    try {
      let addResult
      if (type === "client") {
        addResult = await axios.post('https://backend.voin-cn.com/addclienttocategory', {doctorId, clientId, categoryIds, childDoctorId})
      } else {
        addResult = await axios.post('https://backend.voin-cn.com/addtemplatetocategory', {doctorId, templateId: clientId, categoryIds})
      }
      if (addResult.data.code === 0) {
        dispatch(realAddClientToCategoryFail(addResult.data.msg))
      } else if (addResult.data.code === 1) {
        dispatch(realAddClientToCategorySuccess())
      }
    } catch (error) {
      dispatch(realAddClientToCategoryFail("内部服务器错误，请刷新"))
    }

  }
}

const realAddClientToCategoryStart = () => {
  return {
    type: actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_START
  }
}

const realAddClientToCategoryFail = error => {
  return {
    type: actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_FAIL,
    error
  }
}

const realAddClientToCategorySuccess = () => {
  return {
    type: actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_SUCCESS
  }
}

export const searchDiseaseCategory = (mode, value, page) => {
  return async dispatch => {
    if (value.trim().length > 0) {
      dispatch(searchDiseaseCategoryStart())
      try {
        const result = await axios.get('https://backend.voin-cn.com/bothcode', {params: {mode, value, page}})
        if (result.data.code === 0) {
          dispatch(searchDiseaseCategoryFail(result.data.msg))
        } else if (result.data.code === 1) {
          dispatch(searchDiseaseCategorySuccess(result.data.doc, result.data.count, value, page))
        }
      } catch (error) {
        dispatch(searchDiseaseCategoryFail("内部服务器错误，请刷新"))
      }
    }
  }
}

const searchDiseaseCategoryStart = () => {
  return {
    type: actionTypes.SEARCH_DISEASE_CATEGORY_START
  }
}

const searchDiseaseCategoryFail = error => {
  return {
    type: actionTypes.SEARCH_DISEASE_CATEGORY_FAIL,
    error
  }
}

const searchDiseaseCategorySuccess = (doc, count, value, page) => {
  return {
    type: actionTypes.SEARCH_DISEASE_CATEGORY_SUCCESS,
    doc, count, value, page
  }
}

export const addClientToEmptyCategory = () => {
  return {
    type: actionTypes.ADD_CLIENT_TO_EMPTY_CATEGORY
  }
}

export const addDiseaseCategory = () => {
  return {
    type: actionTypes.ADD_DISEASE_CATEGORY
  }
}

export const addOperationCategory = () => {
  return {
    type: actionTypes.ADD_OPERATION_CATEGORY
  }
}

export const collapse = (mode) => {
  return {
    type: actionTypes.COLLAPSE,
    mode
  }
}

export const chooseChildDoctorCategory = id => {
  return {
    type: actionTypes.CHOOSE_CHILD_DOCTOR_CATEGORY,
    id
  }
}

