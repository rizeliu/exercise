import * as actionTypes from './actionTypes'
import axios from "axios";


export const playVideo = (videoUrl, videoName) => {
  return {
    type: actionTypes.PLAY_VIDEO,
    videoUrl,
    videoName
  }
}

export const completeMessage = () => {
  return {
    type: actionTypes.COMPLETE_MESSAGE
  }
}

export const closeVideo = () => {
  return {
    type: actionTypes.CLOSE_VIDEO
  }
}

export const closeMessage = () => {
  return {
    type: actionTypes.CLOSE_MESSAGE
  }
}

export const exerciseInit = () => {
  return {
    type: actionTypes.EXERCISE_INIT
  }
}

export const exerciseInitDone = () => {
  return {
    type: actionTypes.EXERCISE_INIT_DONE
  }
}

export const nameChange = event => {
  return {
    type: actionTypes.NAME_CHANGE,
    name: event.target.value
  }
}

export const clientNameBChange = event => {
  return {
    type: actionTypes.CLIENT_NAME_B_CHANGE,
    clientNameB: event.target.value
  }
}

export const clientEmailChange = event => {
  return {
    type: actionTypes.CLIENT_EMAIL_CHANGE,
    clientEmail: event.target.value
  }
}

export const introductionChange = event => {
  return {
    type: actionTypes.INTRODUCTION_CHANGE,
    introduction: event.target.value
  }
}

export const clientChange = (event) => {
  return {
    type: actionTypes.CLIENT_CHANGE,
    clientId: event.target.value,
  }
}

export const titleChangeStudent = (titleId) => {
  return {
    type: actionTypes.TITLE_CHANGE_STUDENT,
    titleId
  }
}

export const addMovement = (name, id, moveType, params) => {
  return {
    type: actionTypes.ADD_MOVEMENT,
    name, id, moveType, params
  }
}

export const addPeriod = () => {
  return {
    type: actionTypes.ADD_PERIOD
  }
}

export const changePeriod = index => {
  return {
    type: actionTypes.CHANGE_PERIOD,
    index
  }
}

export const deleteMove = (index, moveId, period, periodIndex) => {
  return {
    type: actionTypes.DELETE_MOVE,
    index, moveId, period, periodIndex
  }
}

export const deletePeriod = index => {
  return {
    type: actionTypes.DELETE_PERIOD,
    index
  }
}

export const displayError = (plan, template) => {
  return {
    type: actionTypes.DISPLAY_ERROR,
    plan, template
  }
}

export const createPlanForClient = (clientId) => {
  return {
    type: actionTypes.CREATE_PLAN_FOR_CLIENT,
    clientId
  }
}

export const exerciseUnmount = () => {
  return {
    type: actionTypes.EXERCISE_UNMOUNT
  }
}

export const previewInit = () => {
  return {
    type: actionTypes.PREVIEW_INIT
  }
}

// export const createPlanByTemplate = (templateId, templateName, coveredMoves) => {
//   return {
//     type: actionTypes.CREATE_PLAN_BY_TEMPLATE,
//     templateId,
//     templateName,
//     coveredMoves
//   }
// }

export const deleteTemplateInPlan = () => {
  return {
    type: actionTypes.DELETE_TEMPLATE_IN_PLAN
  }
}

export const allSearchChange = first5 => {
  return async dispatch => {
    dispatch(searchAllStart())
    const result = await axios.get('https://backend.voin-cn.com/searchall', {params: {first5}})
    const list = result.data
    dispatch(searchAllSuccess(list, first5))
  }
}

const searchAllStart = () => {
  return {
    type: actionTypes.SEARCH_ALL_START
  }
}

const searchAllSuccess = (list, first5) => {
  return {
    type: actionTypes.SEARCH_ALL_SUCCESS,
    list,
    first5
  }
}

export const searchChosenVideos = (plan, therapies, articles) => {
  return async dispatch => {
    dispatch(searchAllStart())
    try {
      const moveIds = plan.filter(move => !move.bar).map(move => move.moveId)
      const articleIds = articles.map(move => move.moveId)
      let allIds = moveIds.concat(articleIds)
      const therapyIds = therapies.map(move => move.moveId)
      allIds = allIds.concat(therapyIds)
      const idSet = new Set(allIds)
      let array = Array.from(idSet);

      const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: array})
      if (moveSet.data.code === 1) {
        const moveResult = [...moveSet.data.doc]
        moveResult.sort((a, b) =>  {
          return array.indexOf(a._id) - array.indexOf(b._id);
        });
        dispatch(searchChosenVideosSuccess(moveResult))
      } else {
        dispatch(searchChosenVideosFail("后端出错了，请刷新"))
      }
    } catch (error) {
      console.log(error)
      dispatch(searchChosenVideosFail("内部服务器错误，请刷新"))
    }
  }
}

const searchChosenVideosSuccess = (finalMoves) => {
  return {
    type: actionTypes.SEARCH_CHOSEN_VIDEOS_SUCCESS,
    finalMoves
  }
}

const searchChosenVideosFail = error => {
  return {
    type: actionTypes.SEARCH_CHOSEN_VIDEOS_FAIL,
    error
  }
}

export const changeOrder = (newPlan, index, source, dest) => {
  return {
    type: actionTypes.CHANGE_ORDER,
    newPlan, index, source, dest
  }
}

export const changePeriodOrder = (source, dest) => {
  return {
    type: actionTypes.CHANGE_PERIOD_ORDER,
    source, dest
  }
}

export const deleteAll = () => {
  return {
    type: actionTypes.DELETE_ALL
  }
}

export const createNewTemplate = page => {
  page.props.history.push('/exercises/template')
  return {
    type: actionTypes.DO_NOTHING
  }
}

export const addGroup = index1 => {
  return {
    type: actionTypes.ADD_GROUP,
    index1
  }
}

export const changeGroup = (index1, index) => {
  return {
    type: actionTypes.CHANGE_GROUP,
    index1, index
  }
}

export const deleteGroup = (index1, index) => {
  return {
    type: actionTypes.DELETE_GROUP,
    index1, index
  }
}

export const renameGroup = (index1, index) => {
  return {
    type: actionTypes.RENAME_GROUP,
    index1, index
  }
}

export const renameGroupDone = (index1, index) => {
  return {
    type: actionTypes.RENAME_GROUP_DONE,
    index1, index
  }
}

export const renamingGroup = (index1, index, event) => {
  return {
    type: actionTypes.RENAMING_GROUP,
    index1, index, value: event.target.value
  }
}

export const loseFocus = (index1, index) => {
  return {
    type: actionTypes.LOSE_FOCUS,
    index1, index
  }
}

export const switchTo = (mode, web) => {
  if (mode === "plan") web.props.history.push('/exercises')
  else if (mode === "template") web.props.history.push('/exercises/template')
  return {
    type: actionTypes.SWITCH_TO,
    mode
  }
}

export const introduceTemplate = (doctorId) => {
  return async dispatch => {
    dispatch(introduceTemplateStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/templates', {params: {doctorId}})
      if (result.data.code === 1) {
        dispatch(introduceTemplateSuccess(result.data.doc))
      } else {
        dispatch(introduceTemplateFail("后端出错了，请刷新"))
      }
    } catch (error) {
      dispatch(introduceTemplateFail("内部服务器错误，请刷新"))
    }
  }
}

const introduceTemplateStart = () => {
  return {
    type: actionTypes.INTRODUCE_TEMPLATE_START
  }
}


const introduceTemplateSuccess = (templates) => {
  return {
    type: actionTypes.INTRODUCE_TEMPLATE_SUCCESS,
    templates
  }
}

const introduceTemplateFail = error => {
  return {
    type: actionTypes.INTRODUCE_TEMPLATE_FAIL,
    error
  }
}

export const closeTemplates = () => {
  return {
    type: actionTypes.CLOSE_TEMPLATES
  }
}

export const closeProposal = () => {
  return {
    type: actionTypes.CLOSE_PROPOSAL
  }
}

export const deleteReference = (index) => {
  return {
    type: actionTypes.DELETE_REFERENCE,
    index
  }
}

export const changeReference = (index, value) => {
  return {
    type: actionTypes.CHANGE_REFERENCE,
    index, value
  }
}

export const addReference = () => {
  return {
    type: actionTypes.ADD_REFERENCE,
  }
}

export const changeProposal = value => {
  return {
    type: actionTypes.CHANGE_PROPOSAL,
    value
  }
}

export const introduceOneTemplate = (doctorId, templateId) => {
  return async dispatch => {
    dispatch(introduceOneTemplateStart())
    try {
      const fullTemplate= await axios.get('https://backend.voin-cn.com/templatebyid', {params: {doctorId, templateId}})
      const moveIds = fullTemplate.data.moveList.filter(move => move.move).map(move => move.move)
      const moveWithArticleIds = moveIds.concat(fullTemplate.data.articles)
      const allIds = moveWithArticleIds.concat(fullTemplate.data.therapyList.map(therapy => therapy.move))
      const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
      if (moveSet.data.code === 1) {
        const moveResult = moveSet.data.doc
        const planDetail = fullTemplate.data.moveList.map((move) => {
          if (!move.bar) {
            const thisMove = moveResult.filter(aMove => aMove._id === move.move)[0]
            return {
              ...move,
              moveId: move.move,
              moveName: thisMove.name
            }
          } else {
            return {
              ...move
            }
          }
        })
        const articleDetail = fullTemplate.data.articles.map(article => {
          const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
          return {
            moveId: article,
            moveName: thisArticle.name
          }
        })
        const therapyDetail = fullTemplate.data.therapyList.map(therapy => {
          const thisTherapy = moveResult.filter(aTherapy => aTherapy._id === therapy.move)[0]
          return {
            ...therapy,
            moveId: therapy.move,
            moveName: thisTherapy.name
          }
        })
        dispatch(introduceOneTemplateSuccess(fullTemplate.data, planDetail, therapyDetail, articleDetail))
      } else {
        dispatch(introduceOneTemplateFail("后端出错了，请刷新"))
      }
    } catch (error) {
      dispatch(introduceOneTemplateFail("内部服务器错误，请刷新"))
    }
  }
}


const introduceOneTemplateStart = () => {
  return {
    type: actionTypes.INTRODUCE_ONE_TEMPLATE_START
  }
}


const introduceOneTemplateSuccess = (template, plans, therapies, articles) => {
  return {
    type: actionTypes.INTRODUCE_ONE_TEMPLATE_SUCCESS,
    template, plans, therapies, articles
  }
}

const introduceOneTemplateFail = error => {
  return {
    type: actionTypes.INTRODUCE_ONE_TEMPLATE_FAIL,
    error
  }
}

export const changeTemplateId = (templateId) => {
  return {
    type: actionTypes.CHANGE_TEMPLATE_ID,
    templateId
  }
}

export const completeHomework = (titleId) => {
  return {
    type: actionTypes.COMPLETE_HOMEWORK,
    titleId
  }
}

export const editProposal = (doctorId, titleId) => {
  return async dispatch => {
    // dispatch(modalLoading())
    try {
      dispatch(showProposalSuccess())
    } catch (error) {
      // dispatch(showPlanDetailFail("内部服务器错误，请刷新"))
    }
  }
}

export const showProposalSuccess = () => {
  return {
    type: actionTypes.SHOW_PROPOSAL_SUCCESS
  }
}