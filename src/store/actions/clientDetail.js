import * as actionTypes from './actionTypes'
import axios from 'axios'
import * as actions from "./index";

export const clientDetailInit = (doctorId, clientId, self) => {
  return async dispatch => {
    dispatch(fetchClientDetailStart(clientId))
    try {
      if (self) {
        const result = await axios.get('https://backend.voin-cn.com/plansbyclient', {params: {doctorId, clientId}})
        const res = result.data
        if (res.code === 1) {
          const secondResult = await axios.get('https://backend.voin-cn.com/clientbyid', {params: {clientId}})
          if (secondResult.data.code === 1) {
            const third = await axios.get('https://backend.voin-cn.com/records', {params: {doctorId, clientId}})
            if (third.data.code === 1) {
              const roots = res.doc.categories.filter(cat => cat.rootId).map(cat => {
                return {type: cat.type, rootId: cat.rootId}
              })
              const fourth = await axios.post('https://backend.voin-cn.com/recommend', {doctorId, clientId, roots})
              if (fourth.data.code === 1) {
                dispatch(fetchClientDetailSuccess(res.doc, secondResult.data.doc,
                  third.data.description, third.data.records, fourth.data.selfTemps, fourth.data.expertTemps))
              } else {
                dispatch(fetchClientDetailFail(fourth.data.msg))
              }
            }
            else {
              dispatch(fetchClientDetailFail(third.data.msg))
            }
          } else if (secondResult.data.code === 0) {
            dispatch(fetchClientDetailFail(secondResult.data.msg))
          }
        } else {
          dispatch(fetchClientDetailFail(res.msg))
        }
      } else {
        const result = await axios.get('https://backend.voin-cn.com/plansbyclient', {params: {doctorId, clientId}})
        const res = result.data
        if (res.code === 1) {
          const docResult = await axios.get('https://backend.voin-cn.com/doctorbyid', {params: {doctorId}})
          if (docResult.data.code === 1) {
            const secondResult = await axios.get('https://backend.voin-cn.com/clientbyid', {params: {clientId}})
            if (secondResult.data.code === 1) {
              const third = await axios.get('https://backend.voin-cn.com/records', {params: {doctorId, clientId}})
              if (third.data.code === 1) {
                dispatch(fetchClientDetailSuccessAdmin(res.doc, secondResult.data.doc,
                  third.data.description, third.data.records, docResult.data.doc))
              }
              else {
                dispatch(fetchClientDetailFail(third.data.msg))
              }
            } else if (secondResult.data.code === 0) {
              dispatch(fetchClientDetailFail(secondResult.data.msg))
            }
          } else if (docResult.data.code === 0) {
            dispatch(fetchClientDetailFail(docResult.data.msg))
          }
        } else {
          dispatch(fetchClientDetailFail(res.msg))
        }
      }
    }
    catch (error) {
      dispatch(fetchClientDetailFail("内部服务器错误，请刷新"))
    }
  }
}

const fetchClientDetailStart = clientId => {
  return {
    type: actionTypes.FETCH_CLIENT_DETAIL_START,
    clientId
  }
}

const fetchClientDetailSuccess = (doc, secondDoc, description, records, selfTemps, expertTemps) => {
  return {
    type: actionTypes.FETCH_CLIENT_DETAIL_SUCCESS,
    doc,
    secondDoc,
    description,
    records,
    selfTemps,
    expertTemps
  }
}

const fetchClientDetailSuccessAdmin = (doc, secondDoc, description, records, doctor) => {
  return {
    type: actionTypes.FETCH_CLIENT_DETAIL_SUCCESS_ADMIN,
    doc,
    secondDoc,
    description,
    records,
    doctor
  }
}

const fetchClientDetailFail = msg => {
  return {
    type: actionTypes.FETCH_CLIENT_DETAIL_FAIL,
    msg
  }
}


export const deletePlan = (planId, planName) => {
  return {
    type: actionTypes.DELETE_PLAN,
    planId,
    planName
  }
}

const realPlanDeleteStart = () => {
  return {
    type: actionTypes.REAL_PLAN_DELETE_START
  }
}

const realPlanDeleteFail = error => {
  return {
    type: actionTypes.REAL_PLAN_DELETE_FAIL,
    error
  }
}

const realPlanDeleteSuccess = () => {
  return {
    type: actionTypes.REAL_PLAN_DELETE_SUCCESS
  }
}

export const realPlanDelete = (doctorId, clientId, planId) => {
  return async dispatch => {
    dispatch(realPlanDeleteStart())
    try {
      const deleteRes = await axios.post('https://backend.voin-cn.com/deleteplan', {doctorId, clientId, planId})
      if (deleteRes.data.code === 0) {
        dispatch(realPlanDeleteFail(deleteRes.data.msg))
      } else if (deleteRes.data.code === 1) {
        dispatch(realPlanDeleteSuccess())
      }
    } catch (error) {
      dispatch(realPlanDeleteFail("内部服务器错误，请刷新"))

    }
  }

}

export const deletePlanFinished = () => {
  return {
    type: actionTypes.DELETE_PLAN_FINISHED
  }
}

export const editPlan = (doctorId, clientId, planId, index, page) => {
  return async dispatch => {
    dispatch(editPlanStart(index))
    try {
      const fullPlan= await axios.get('https://backend.voin-cn.com/planbyid', {params: {doctorId, clientId, planId}})
      const moveIds = fullPlan.data.moveList.filter(move => move.move).map(move => move.move)
      const moveWithArticleIds = moveIds.concat(fullPlan.data.articles)
      const allIds = moveWithArticleIds.concat(fullPlan.data.therapyList.map(therapy => therapy.move))
      const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
      if (moveSet.data.code === 1) {
        const moveResult = moveSet.data.doc
        const planDetail = fullPlan.data.moveList.map((move) => {
          if (!move.bar) {
            const thisMove = moveResult.filter(aMove => aMove._id === move.move)[0]
            return {
              ...move,
              moveId: move.move,
              moveName: thisMove.name
            }
          } else {
            return {
              ...move
            }
          }
        })
        const articleDetail = fullPlan.data.articles.map(article => {
          const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
          return {
            moveId: article,
            moveName: thisArticle.name
          }
        })
        const therapyDetail = fullPlan.data.therapyList.map(therapy => {
          const thisTherapy = moveResult.filter(aTherapy => aTherapy._id === therapy.move)[0]
          return {
            ...therapy,
            moveId: therapy.move,
            moveName: thisTherapy.name
          }
        })
        dispatch(editPlanSuccess(fullPlan.data, clientId, planDetail, therapyDetail, articleDetail, page))
        dispatch(actions.searchChosenVideos(planDetail, therapyDetail, articleDetail))
      } else {
        dispatch(editPlanFail("后端出错了，请刷新"))
      }
    } catch (error) {
      dispatch(editPlanFail("内部服务器错误，请刷新"))
    }
  }
}

const editPlanStart = index => {
  return {
    type: actionTypes.EDIT_PLAN_START,
    index
  }
}

const editPlanSuccess = (plan, clientId, moves, therapyMoves, articleMoves, page)  => {
  page.props.history.push('/exercises')
  return {
    type: actionTypes.EDIT_PLAN_SUCCESS,
    clientId, plan, moves, therapyMoves, articleMoves
  }
}

const editPlanFail = msg => {
  return {
    type: actionTypes.EDIT_PLAN_FAIL,
    msg
  }
}

export const editClientDetail = () => {
  return {
    type: actionTypes.EDIT_CLIENT_DETAIL
  }
}

export const cancelEditClientDetail = () => {
  return {
    type: actionTypes.CANCEL_EDIT_CLIENT_DETAIL
  }
}

export const changeClientDetail = (value, id) => {
  return {
    type: actionTypes.CHANGE_CLIENT_DETAIL,
    value, id
  }
}

export const saveClientDetail = (doctorId, clientId, newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription) => {
  return async dispatch => {
    dispatch(saveClientDetailStart())
    const result = await axios.post('https://backend.voin-cn.com/editclientdetail',
      {doctorId, clientId, newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription})
    if (result.data.code === 1){
      dispatch(saveClientDetailSuccess(newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription))
    } else {
      dispatch(saveClientDetailFail())
    }
  }

}

const saveClientDetailStart = () => {
  return {
    type: actionTypes.SAVE_CLIENT_DETAIL_START,
  }
}


const saveClientDetailSuccess = (newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription) => {
  return {
    type: actionTypes.SAVE_CLIENT_DETAIL_SUCCESS,
    newAge, newSex, newRealhistory, newSmoke, newPhone, newDescription
  }
}

const saveClientDetailFail = () => {
  return {
    type: actionTypes.SAVE_CLIENT_DETAIL_FAIL
  }
}
