import * as actionTypes from './actionTypes'

export const authenticate = (info, login, mode, error, admin=0, refer=false) => {
  return {
    type: actionTypes.AUTHENTICATE,
    info,
    login,
    mode,
    error,
    admin,
    refer
  }
}

export const removeCookies = () => {
  const now = new Date()
  document.cookie = "userId=; expires="+now.toUTCString()
  document.cookie = "token=; expires="+now.toUTCString()
  return {type: actionTypes.DO_NOTHING}
}

export const changeExpand = () => {
  return {
    type: actionTypes.CHANGE_EXPAND
  }
}

export const logout = () => {
  removeCookies()
  return {type: actionTypes.LOGOUT}
}

export const changeEdu = () => {
  return {
    type: actionTypes.CHANGE_EDU
  }
}

