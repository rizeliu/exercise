import * as actionTypes from './actionTypes'

import axios from 'axios'

export const categoryClick = (name, index) => {
  return {
    type: actionTypes.CATEGORY_CLICK,
    name,
    index,
  }
}

export const bcSearchChange = value => {
  return {
    type: actionTypes.BC_SEARCH_CHANGE,
    value
  }
}

export const backClick = () => {
  return {
    type: actionTypes.BACK_CLICK,
  }
}

export const dropDown = (inside, name) => {
  return {
    type: actionTypes.DROP_DOWN,
    inside,
    name,
  }
}

const searchMovesStart = (a, b, c) => {
  return {
    type: actionTypes.SEARCH_MOVES_START,
    a, b, c
  }
}

const searchMovesFail = error => {
  return {
    type: actionTypes.SEARCH_MOVES_FAIL,
    error
  }
}

const searchMovesSuccess = (videoList, title) => {
  return {
    type: actionTypes.SEARCH_MOVES_SUCCESS,
    videoList,
    title
  }
}

export const searchMoves = (doctorId, a, b, c) => {
  window.scrollTo(0, 0)
  return async dispatch => {
    dispatch(searchMovesStart(a, b, c))
    try {
      let response
      if (c === 0) {
        response = await axios.get('https://backend.voin-cn.com/searchmoves', {params: {param0: doctorId, param1: a, param2: b}})
      } else {
        response = await axios.get('https://backend.voin-cn.com/searchmoves', {params: {param0: doctorId, param1: a, param2: b, param3: c}})
      }
      if (response.data.code === 0) {
        dispatch(searchMovesFail(response.data.msg))
      } else {
        const videoList = response.data.doc2
        dispatch(searchMovesSuccess(videoList, response.data.title))
      }
    } catch (error) {
      dispatch(searchMovesFail("内部服务器错误，请刷新"))
    }
  }
}

export const categoriesInit = () => {
  return {
    type: actionTypes.CATEGORIES_INIT,
  }
}
