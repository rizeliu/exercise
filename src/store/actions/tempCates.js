import * as actionTypes from './actionTypes'

import axios from 'axios'

export const tempCateClick = (name, index) => {
  return {
    type: actionTypes.TEMP_CATE_CLICK,
    name,
    index,
  }
}

export const tempBackClick = () => {
  return {
    type: actionTypes.TEMP_BACK_CLICK,
  }
}

export const tempDropDown = (inside, name) => {
  return {
    type: actionTypes.TEMP_DROP_DOWN,
    inside,
    name,
  }
}

const searchTempStart = () => {
  return {
    type: actionTypes.SEARCH_TEMPS_START
  }
}

const searchTempsFail = error => {
  return {
    type: actionTypes.SEARCH_TEMPS_FAIL,
    error
  }
}

const searchTempsSuccess = (templateList, title) => {
  return {
    type: actionTypes.SEARCH_TEMPS_SUCCESS,
    templateList,
    title
  }
}

export const searchTemps = (a, b, c) => {
  return async dispatch => {
    dispatch(searchTempStart())
    try {
      let response
      if (c === 0) {
        response = await axios.get('https://backend.voin-cn.com/searchtemps', {params: {label: b}})
      } else {
        response = await axios.get('https://backend.voin-cn.com/searchtemps', {params: {label: c}})
      }
      if (response.data.code === 0) {
        dispatch(searchTempsFail(response.data.msg))
      } else {
        dispatch(searchTempsSuccess(response.data.doc, response.data.title))
      }
    } catch (error) {
      dispatch(searchTempsFail("内部服务器错误，请刷新"))
    }
  }
}

export const tempCategoriesInit = () => {
  return {
    type: actionTypes.TEMP_CATEGORIES_INIT,
  }
}

export const searchAllTemplates = (value) => {
  return async dispatch => {
    dispatch(searchTempStart())
    try {
      const response = await axios.get('https://backend.voin-cn.com/searchexperttemps', {params: {value}})
      if (response.data.code === 0) {
        dispatch(searchTempsFail(response.data.msg))
      } else {
        dispatch(searchTempsSuccess(response.data.doc, response.data.title))
      }
    } catch (error) {
      dispatch(searchTempsFail("内部服务器错误，请刷新"))
    }
  }
}


