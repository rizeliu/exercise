export { fetchClients, deleteClient, closeModal, realClientDelete, deleteClientFinished, clientSearchChange,
  showAllClients, seeRequest, sortClients, addCategory, addCategoryChange, cancelAddCategory, realAddCategory, realAddCategories,
  chooseCategory, addClientToCategory, clickCategory, clickChildDoctor, realAddClientToCategory, addClientToEmptyCategory,
  deleteCategory, cancelDeleteCategory, deleteOneCategory, realDeleteCategory, addDiseaseCategory,
  diseaseCodeSearchChange, deleteDiseaseCategory, searchDiseaseCategory, checkDiseaseCategory,
  addOperationCategory, deleteOperationCategory, collapse, addChildCategory, cancelAddChildCategory,
  chooseSecondCategory, deleteChildCategory, deleteSecondCategory, addClientToCategoryDetail, addTemplateToCategory,
  chooseChildDoctorCategory, specialize, seeQRCode
} from './clients'

export {
  fetchTemplates, deleteTemplate, realTemplateDelete, deleteTemplateFinished, editTemplate,
  createPlanByTemplate, fetchExpertTemplates, downloadTemplate, addExpert, sendTemplate, fetchAllDoctors,
  changeDoctor, realSendTemplate, searchTemplateChange, searchExpertTemplateChange, switchTemplateFocusToMy,
  changeLabel, searchDoctors, fetchTeamsAndFirst, addTeam, searchTeamTemplateChange, addExpertOrTeam,
  realAddExpertOrTeam, teamChange, seeTeamMembers, deleteTeamMember, realDeleteTeamMember, leaveTeam, deleteTeam,
  realLeaveTeam, realDeleteTeam, realAddClass, showAllTemplates, fetchTeamTemplate, changeContent, createQRCode,
  syncCircle, getCircle, addTemplateToCircle, changeCircle, realAddTemplateToCircle
} from './templates'

export { searchNewClientInputChange, searchNewClient, addNewClient, addClientInit } from './addClient'

export {
  playVideo, closeVideo, exerciseInit, nameChange, introductionChange, clientChange, addMovement, deleteMove,
  createPlanForClient, exerciseUnmount, displayError, previewInit, deleteTemplateInPlan, searchChosenVideos,
  allSearchChange, changeOrder, clientNameBChange, clientEmailChange, completeMessage, closeMessage,
  addPeriod, changePeriod, deletePeriod, deleteAll, createNewTemplate, addGroup, changeGroup, deleteGroup,
  renameGroup, renameGroupDone, renamingGroup, loseFocus, switchTo, introduceTemplate, closeTemplates, closeProposal,
  introduceOneTemplate, changeTemplateId, completeHomework, titleChangeStudent, changePeriodOrder, editProposal,
  deleteReference, changeReference, addReference, changeProposal, exerciseInitDone
} from './exercise'

export { categoryClick, backClick, dropDown, searchMoves, categoriesInit, bcSearchChange } from './categories'

export {
  tempCateClick, tempBackClick, tempDropDown, searchTemps, tempCategoriesInit, searchAllTemplates
} from './tempCates'

export { authenticate, removeCookies, logout, changeExpand, changeEdu } from './authRoute'

export {
  clientDetailInit, deletePlan, realPlanDelete, deletePlanFinished, editPlan, editClientDetail,
  cancelEditClientDetail, changeClientDetail, saveClientDetail
} from './clientDetail'

export {
  closeDetailModal, changeAllDay, changeExtra, changeSingle, savingTemplate, savingPlan, changeToInt, notInt,
  showDetail, setClientName, seePlanDetail, showTemplateDetail, deleteOne, deleteArticle, noMoves, updatingPlan,
  updatingTemplate, deleteParameter, notComplete, showExpertTemplateDetail, showPlanModal, changePeriodDays,
  notBetween, notGreaterThan15, seeFeedback, currentPeriodShorter, seeHomeworkDetail, deleteTherapy,
  changeTherapySingle, addParameter, deleteTherapyParameter, changeParameterName, changeParameterValue
} from './detail'

export {
  fetchClassesAndStudents, titleChange, submitNewTitle, submitReview, seeProposal, closeStudentProposal
} from './students'

export {
  fetchClassesAndHomework, modalClosed
} from './homework'