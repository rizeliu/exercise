import * as actionTypes from './actionTypes'
import axios from "axios";

export const closeDetailModal = (success, template, web, editPlan, clientId, titleId) => {
  if (success && template) {
    web.props.history.push('/templates')
  } else if (success && editPlan) {
    web.props.history.push('/client/'+clientId)
  } else if (success && titleId) {
    web.props.history.push('/homework')
  } else if (success && !template) {
    web.props.history.push('/clients')
  }
  return {
    type: actionTypes.CLOSE_DETAIL_MODAL
  }

}

export const changeAllDay = value => {
  return {
    type: actionTypes.CHANGE_ALL_DAY,
    value
  }
}

export const changeExtra = value => {
  return {
    type: actionTypes.CHANGE_EXTRA,
    value
  }
}

export const changeSingle = (index1, index2, value) => {
  return {
    type: actionTypes.CHANGE_SINGLE,
    index1, index2, value
  }
}

export const changeTherapySingle = (index1, index2, value) => {
  return {
    type: actionTypes.CHANGE_THERAPY_SINGLE,
    index1, index2, value
  }
}

export const changeToInt = (newBetweens) => {
  return {
    type: actionTypes.CHANGE_TO_INT,
    newBetweens
  }
}

export const notInt = () => {
  return {
    type: actionTypes.NOT_INT
  }
}

export const currentPeriodShorter = () => {
  return {
    type: actionTypes.CURRENT_PERIOD_SHORTER
  }
}

export const notBetween = () => {
  return {
    type: actionTypes.NOT_BETWEEN
  }
}

export const notGreaterThan15 = () => {
  return {
    type: actionTypes.NOT_GREATER_THAN_15
  }
}

export const notComplete = () => {
  return {
    type: actionTypes.NOT_COMPLETE
  }
}

const savingStart = () => {
  return {
    type: actionTypes.SAVING_START
  }
}

export const changePeriodDays = (period, value) => {
  return {
    type: actionTypes.CHANGE_PERIOD_DAYS,
    value, period
  }
}

export const savingTemplate = (info, redirect, web) => {
  return async dispatch => {
    dispatch(savingStart())
    try {
      const result = await axios.post("https://backend.voin-cn.com/newsavetemplate", {
        doctorId: info.doctorId,
        name: info.name,
        summary: info.summary,
        times: info.times,
        sets: info.sets,
        timesPerSets: info.timesPerSets,
        weights: info.weights,
        betweens: info.betweens,
        parameter1s: info.parameter1s,
        parameter2s: info.parameter2s,
        value1s: info.value1s,
        value2s: info.value2s,
        infos: info.infos,
        minutes: info.minutes,
        timesPerDays: info.timesPerDays,
        time2s: info.time2s,
        periods: info.periods,
        therapyInfos: info.therapyInfos,
        addedParamCounts: info.addedParamCounts,
        allChosenParams: info.allChosenParams,
        therapyIds: info.therapyIds,
        ids: info.ids,
        bars: info.bars,
        starts: info.starts,
        barNames: info.barNames,
        totalInEach: info.totalInEach,
        periodDays: info.periodDays,
        articles: info.articles,
        proposalContent: info.proposalContent,
        references: info.references
      })
      if (result.data.code === 1) {
        if (redirect) {
          web.props.history.push('/templates')
        } else {
          dispatch(savingSuccess())
        }
      } else if (result.data.code === 0) {
        dispatch(savingFail(result.data.msg))
      }
    } catch (error) {
      dispatch(savingFail("内部服务器错误，请刷新"))
    }
  }
}

const savingFail = msg => {
  return {
    type: actionTypes.SAVING_FAIL,
    msg
  }
}

const savingSuccess = () => {
  return {
    type: actionTypes.SAVING_SUCCESS
  }
}

export const savingPlan = (info, web) => {
  return async dispatch => {
    dispatch(savingStart())
    try {
      const result = await axios.post("https://backend.voin-cn.com/saveplan", {
        doctorId: info.doctorId,
        clientId: info.clientId,
        name: info.name,
        allDay: info.allDay,
        times: info.times,
        sets: info.sets,
        timesPerSets: info.timesPerSets,
        weights: info.weights,
        betweens: info.betweens,
        parameter1s: info.parameter1s,
        parameter2s: info.parameter2s,
        value1s: info.value1s,
        value2s: info.value2s,
        infos: info.infos,
        minutes: info.minutes,
        timesPerDays: info.timesPerDays,
        time2s: info.time2s,
        periods: info.periods,
        therapyInfos: info.therapyInfos,
        addedParamCounts: info.addedParamCounts,
        allChosenParams: info.allChosenParams,
        therapyIds: info.therapyIds,
        extra: info.extra,
        ids: info.ids,
        bars: info.bars,
        starts: info.starts,
        barNames: info.barNames,
        totalInEach: info.totalInEach,
        periodDays: info.periodDays,
        articles: info.articles,
        titleId: info.titleId,
        proposalContent: info.proposalContent,
        references: info.references
      })
      if (result.data.code === 1) {
        dispatch(savingSuccess())
      } else if (result.data.code === 0) {
        console.log(result.data.msg)
        dispatch(savingFail(result.data.msg))
      }
    } catch (error) {
      console.log(error)
      dispatch(savingFail("内部服务器错误，请刷新"))
    }
  }
}

export const showDetail = info => {
  return {
    type: actionTypes.SHOW_DETAIL,
    info
  }
}

export const setClientName = clientName => {
  return {
    type: actionTypes.SET_CLIENT_NAME,
    clientName
  }
}

const modalLoading = () => {
  return {
    type: actionTypes.MODAL_LOADING
  }
}

const showPlanDetailSuccess = (planDetail, ids, sets, timesPerSets, times, weights, betweens, infos, parameter1s,
                               parameter2s, value1s, value2s, bars, starts, barNames, extra, totalInEach, periodDays,
                               allDay, name, articles, proposalContent, references, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
                               allChosenParams, therapyDetail, addedParamCounts, homework) => {
  return {
    type: actionTypes.SHOW_PLAN_DETAIL_SUCCESS,
    planDetail, ids, sets, timesPerSets, times, weights, betweens, infos, parameter1s, parameter2s, value1s, value2s,
    bars, starts, barNames, extra, allDay, name, articles, totalInEach, periodDays, proposalContent, references, therapyIds, minutes, timesPerDays,
    time2s, periods, therapyInfos, allChosenParams, therapyDetail, addedParamCounts, homework
  }
}

const showPlanDetailFail = msg => {
  return {
    type: actionTypes.SHOW_PLAN_DETAIL_FAIL,
    msg
  }
}

export const seePlanDetail = (doctorId, clientId, planId) => {
  return async dispatch => {
    dispatch(modalLoading())
    try {
      const plan = await axios.get('https://backend.voin-cn.com/planbyid', {params: {doctorId, clientId, planId}})
      if (plan.data.code === 1) {
        const ids = plan.data.moveList.map(move => move.move)
        const sets = plan.data.moveList.map(move => move.set)
        const timesPerSets = plan.data.moveList.map(move => move.timesPerSet)
        const weights = plan.data.moveList.map(move => move.weight)
        const betweens = plan.data.moveList.map(move => move.between)
        const times = plan.data.moveList.map(move => move.time)
        const infos = plan.data.moveList.map(move => move.info)
        const parameter1s = plan.data.moveList.map(move => move.parameter1)
        const parameter2s = plan.data.moveList.map(move => move.parameter2)
        const value1s = plan.data.moveList.map(move => move.value1)
        const value2s = plan.data.moveList.map(move => move.value2)
        const bars = plan.data.moveList.map(move => move.bar)
        const starts = plan.data.moveList.map(move => move.start)
        const barNames = plan.data.moveList.map(move => move.barName)
        const therapyIds = plan.data.therapyList.map(therapy => therapy.move)
        const minutes = plan.data.therapyList.map(therapy => therapy.minute)
        const timesPerDays = plan.data.therapyList.map(therapy => therapy.timesPerDay)
        const time2s = plan.data.therapyList.map(therapy => therapy.time2)
        const periods = plan.data.therapyList.map(therapy => therapy.period)
        const therapyInfos = plan.data.therapyList.map(therapy => therapy.therapyInfo)
        const allChosenParams = plan.data.therapyList.map(therapy => therapy.chosenParams)
        const addedParamCounts = plan.data.addedParamCounts
        const articles = plan.data.articles
        const name = plan.data.name
        const totalInEach = plan.data.totalInEach
        const periodDays = plan.data.periodDays
        const extra = plan.data.extra
        const allDay = plan.data.allDay
        const proposalContent = plan.data.proposalContent
        const references = plan.data.references

        const actualMoveIds = ids.filter(id => id)
        const allIds = actualMoveIds.concat(articles).concat(therapyIds)
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const planDetail = ids.map((move, index) => {
            if (move) {
              const thisMove = moveResult.filter(aMove => aMove._id === move)[0]
              return {
                _id: move,
                name: thisMove.name,
                screenshot: thisMove.screenshot,
                info: thisMove.info
              }
            } else {
              return {
                name: barNames[index],
                bar: bars[index],
                start: starts[index]
              }
            }
          })
          const therapyDetail = therapyIds.map((therapy, index) => {
            if (therapy) {
              const thisTherapy = moveResult.filter(aMove => aMove._id === therapy)[0]
              return {
                moveId: therapy,
                moveName: thisTherapy.name,
                params: thisTherapy.params,
              }
            } else {
              return {
              }
            }
          })
          const articleDetail = articles.map(article => {
            const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
            return {
              moveId: article,
              moveName: thisArticle.name
            }
          })
          dispatch(showPlanDetailSuccess(
            planDetail, ids, sets, timesPerSets, times, weights, betweens, infos,
            parameter1s, parameter2s, value1s, value2s, bars, starts, barNames, extra, totalInEach, periodDays,
            allDay, name, articleDetail, proposalContent, references, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos, allChosenParams,
            therapyDetail, addedParamCounts, false
          ))
        } else {
          dispatch(showPlanDetailFail("后端出错了，请刷新"))
        }
      } else if (plan.data.code === 0) {
        dispatch(showPlanDetailFail(plan.data.msg))
      }
    } catch (error) {
      dispatch(showPlanDetailFail("内部服务器错误，请刷新"))
    }
  }
}


export const seeHomeworkDetail = (doctorId, titleId) => {
  return async dispatch => {
    dispatch(modalLoading())
    try {
      const plan = await axios.get('https://backend.voin-cn.com/homeworkbyid', {params: {doctorId, titleId}})
      if (plan.data.code === 1) {
        const ids = plan.data.moveList.map(move => move.move)
        const sets = plan.data.moveList.map(move => move.set)
        const timesPerSets = plan.data.moveList.map(move => move.timesPerSet)
        const weights = plan.data.moveList.map(move => move.weight)
        const betweens = plan.data.moveList.map(move => move.between)
        const times = plan.data.moveList.map(move => move.time)
        const infos = plan.data.moveList.map(move => move.info)
        const parameter1s = plan.data.moveList.map(move => move.parameter1)
        const parameter2s = plan.data.moveList.map(move => move.parameter2)
        const value1s = plan.data.moveList.map(move => move.value1)
        const value2s = plan.data.moveList.map(move => move.value2)
        const bars = plan.data.moveList.map(move => move.bar)
        const starts = plan.data.moveList.map(move => move.start)
        const barNames = plan.data.moveList.map(move => move.barName)
        const therapyIds = plan.data.therapyList.map(therapy => therapy.move)
        const minutes = plan.data.therapyList.map(therapy => therapy.minute)
        const timesPerDays = plan.data.therapyList.map(therapy => therapy.timesPerDay)
        const time2s = plan.data.therapyList.map(therapy => therapy.time2)
        const periods = plan.data.therapyList.map(therapy => therapy.period)
        const therapyInfos = plan.data.therapyList.map(therapy => therapy.therapyInfo)
        const allChosenParams = plan.data.therapyList.map(therapy => therapy.chosenParams)
        const addedParamCounts = plan.data.addedParamCounts
        const articles = plan.data.articles
        const name = plan.data.name
        const totalInEach = plan.data.totalInEach
        const periodDays = plan.data.periodDays
        const extra = plan.data.extra
        const proposalContent = plan.data.proposalContent
        const references = plan.data.references

        const actualMoveIds = ids.filter(id => id)
        const allIds = actualMoveIds.concat(articles).concat(therapyIds)
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const planDetail = ids.map((move, index) => {
            if (move) {
              const thisMove = moveResult.filter(aMove => aMove._id === move)[0]
              return {
                _id: move,
                name: thisMove.name,
                screenshot: thisMove.screenshot,
                info: thisMove.info
              }
            } else {
              return {
                name: barNames[index],
                bar: bars[index],
                start: starts[index]
              }
            }
          })
          const articleDetail = articles.map(article => {
            const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
            return {
              moveId: article,
              moveName: thisArticle.name
            }
          })
          const therapyDetail = therapyIds.map((therapy, index) => {
            if (therapy) {
              const thisTherapy = moveResult.filter(aMove => aMove._id === therapy)[0]
              return {
                moveId: therapy,
                moveName: thisTherapy.name,
                params: thisTherapy.params,
              }
            } else {
              return {
              }
            }
          })
          dispatch(showPlanDetailSuccess(
            planDetail, ids, sets, timesPerSets, times, weights, betweens, infos,
            parameter1s, parameter2s, value1s, value2s, bars, starts, barNames, extra, totalInEach, periodDays,
            0, name, articleDetail, proposalContent, references, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
            allChosenParams, therapyDetail, addedParamCounts, true
          ))
        } else {
          dispatch(showPlanDetailFail("后端出错了，请刷新"))
        }
      } else if (plan.data.code === 0) {
        dispatch(showPlanDetailFail(plan.data.msg))
      }
    } catch (error) {
      dispatch(showPlanDetailFail("内部服务器错误，请刷新"))
    }
  }
}

export const seeFeedback = (doctorId, clientId, planId, status) => {
  return async dispatch => {
    dispatch(modalLoading())
    try {
      const plan = await axios.get('https://backend.voin-cn.com/planbyid', {params: {doctorId, clientId, planId}})
      if (plan.data.code === 1) {
        const ids = plan.data.moveList.map(move => move.move)
        const betweens = plan.data.moveList.map(move => move.between)
        const bars = plan.data.moveList.map(move => move.bar)
        const starts = plan.data.moveList.map(move => move.start)
        const barNames = plan.data.moveList.map(move => move.barName)
        const totalInEach = [...plan.data.totalInEach]
        const periodDays = [...plan.data.periodDays]
        const feedback = [...plan.data.feedback]
        const savedFeedback = [...plan.data.savedFeedback]
        const extra = plan.data.extra
        const completion = plan.data.completion
        const effect = plan.data.effect
        const description = plan.data.description
        const startDate = plan.data.startDate
        const name = plan.data.name

        const actualMoveIds = ids.filter(id => id)
        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: actualMoveIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const planDetail = ids.map((move, index) => {
            if (move) {
              const thisMove = moveResult.filter(aMove => aMove._id === move)[0]
              return {
                _id: move,
                name: thisMove.name,
                screenshot: thisMove.screenshot,
                info: thisMove.info
              }
            } else {
              return {
                name: barNames[index],
                bar: bars[index],
                start: starts[index]
              }
            }
          })

          dispatch(seeFeedbackSuccess(
            planDetail, ids, betweens, bars, starts, barNames, extra, totalInEach, periodDays,
            name, feedback, completion, effect, description, startDate, status, savedFeedback
          ))
        } else {
          dispatch(showPlanDetailFail("后端出错了，请刷新"))
        }
      } else {
        dispatch(showPlanDetailFail(plan.data.msg))
      }
    } catch (error) {
      dispatch(showPlanDetailFail("内部服务器错误，请刷新"))
    }
  }
}

const seeFeedbackSuccess = (planDetail, ids, betweens, bars, starts, barNames, extra, totalInEach, periodDays,
                            name, feedback, completion, effect, description, startDate, status, savedFeedback) => {
  return {
    type: actionTypes.SEE_FEEDBACK_SUCCESS,
    planDetail, ids, betweens, bars, starts, barNames, extra, totalInEach, periodDays,
    name, feedback, completion, effect, description, startDate, status, savedFeedback
  }
}

export const deleteOne = index => {
  return {
    type: actionTypes.DELETE_ONE,
    index
  }
}

export const deleteArticle = index => {
  return {
    type: actionTypes.DELETE_ARTICLE,
    index
  }
}

export const deleteTherapy = index => {
  return {
    type: actionTypes.DELETE_THERAPY,
    index
  }
}

export const addParameter = (index, params) => {
  return {
    type: actionTypes.ADD_PARAMETER,
    index, params
  }
}

export const deleteTherapyParameter = (index1, index2) => {
  return {
    type: actionTypes.DELETE_THERAPY_PARAMETER,
    index1, index2
  }
}

export const changeParameterName = (index1, index2, value) => {
  return {
    type: actionTypes.CHANGE_PARAMETER_NAME,
    index1, index2, value
  }
}

export const changeParameterValue = (index1, index2, value) => {
  return {
    type: actionTypes.CHANGE_PARAMETER_VALUE,
    index1, index2, value
  }
}

export const deleteParameter = (index1, index2) => {
  return {
    type: actionTypes.DELETE_PARAMETER,
    index1, index2
  }
}

export const noMoves = () => {
  return {
    type: actionTypes.NO_MOVES
  }
}

export const showTemplateDetail = (doctorId, templateId, teamId) => {
  return async dispatch => {
    dispatch(modalLoading())
    try {
      const template = await axios.get('https://backend.voin-cn.com/templatebyid', {params: {doctorId, templateId, teamId}})
      if (template.data.code === 1) {
        const ids = template.data.moveList.map(move => move.move)
        const sets = template.data.moveList.map(move => move.set)
        const timesPerSets = template.data.moveList.map(move => move.timesPerSet)
        const weights = template.data.moveList.map(move => move.weight)
        const betweens = template.data.moveList.map(move => move.between)
        const times = template.data.moveList.map(move => move.time)
        const infos = template.data.moveList.map(move => move.info)
        const parameter1s = template.data.moveList.map(move => move.parameter1)
        const parameter2s = template.data.moveList.map(move => move.parameter2)
        const value1s = template.data.moveList.map(move => move.value1)
        const value2s = template.data.moveList.map(move => move.value2)
        const bars = template.data.moveList.map(move => move.bar)
        const starts = template.data.moveList.map(move => move.start)
        const barNames = template.data.moveList.map(move => move.barName)
        const articles = template.data.articles
        const name = template.data.name
        const summary = template.data.summary
        const totalInEach = template.data.totalInEach
        const periodDays = template.data.periodDays
        const proposalContent = template.data.proposalContent
        const references = template.data.references
        const therapyIds = template.data.therapyList.map(therapy => therapy.move)
        const minutes = template.data.therapyList.map(therapy => therapy.minute)
        const timesPerDays = template.data.therapyList.map(therapy => therapy.timesPerDay)
        const time2s = template.data.therapyList.map(therapy => therapy.time2)
        const periods = template.data.therapyList.map(therapy => therapy.period)
        const therapyInfos = template.data.therapyList.map(therapy => therapy.therapyInfo)
        const allChosenParams = template.data.therapyList.map(therapy => therapy.chosenParams)
        const addedParamCounts = template.data.addedParamCounts

        const actualMoveIds = ids.filter(id => id)
        const tempIds = actualMoveIds.concat(articles)
        const allIds = tempIds.concat(therapyIds)

        const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
        if (moveSet.data.code === 1) {
          const moveResult = moveSet.data.doc
          const planDetail = ids.map((move, index) => {
            if (move) {
              const thisMove = moveResult.filter(aMove => aMove._id === move)[0]
              return {
                _id: move,
                name: thisMove.name,
                screenshot: thisMove.screenshot,
                info: thisMove.info
              }
            } else {
              return {
                name: barNames[index],
                bar: bars[index],
                start: starts[index]
              }
            }
          })
          const articleDetail = articles.map(article => {
            const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
            return {
              moveId: article,
              moveName: thisArticle.name
            }
          })
          const therapyDetail = therapyIds.map((therapy, index) => {
            if (therapy) {
              const thisTherapy = moveResult.filter(aMove => aMove._id === therapy)[0]
              return {
                moveId: therapy,
                moveName: thisTherapy.name,
                params: thisTherapy.params,
              }
            } else {
              return {
              }
            }
          })
          dispatch(showTemplateSuccess(planDetail, ids, sets, timesPerSets, times, weights, betweens, infos,
            parameter1s, parameter2s, value1s, value2s, bars, starts, barNames, articleDetail, totalInEach, periodDays,
            name, summary, proposalContent, references, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
            allChosenParams, therapyDetail, addedParamCounts)
          )

        } else {
          dispatch(showPlanDetailFail("后端出错了，请刷新"))
        }
      } else if (template.data.code === 0) {
        dispatch(showTemplateFail(template.data.msg))
      }
    } catch (error) {
      console.log(error)
      dispatch(showTemplateFail("内部服务器错误，请刷新"))
    }
  }
}

export const showExpertTemplateDetail = (templates, templateId) => {
  return async dispatch => {
    dispatch(modalLoading())
    try {
      const template = templates.filter(template => template.templateId === templateId)[0]
      const ids = template.moveList.map(move => move.move)
      const sets = template.moveList.map(move => move.set)
      const timesPerSets = template.moveList.map(move => move.timesPerSet)
      const weights = template.moveList.map(move => move.weight)
      const betweens = template.moveList.map(move => move.between)
      const times = template.moveList.map(move => move.time)
      const infos = template.moveList.map(move => move.info)
      const parameter1s = template.moveList.map(move => move.parameter1)
      const parameter2s = template.moveList.map(move => move.parameter2)
      const value1s = template.moveList.map(move => move.value1)
      const value2s = template.moveList.map(move => move.value2)
      const articles = template.articles
      const totalInEach = template.totalInEach
      const periodDays = template.periodDays
      const bars = template.moveList.map(move => move.bar)
      const starts = template.moveList.map(move => move.start)
      const barNames = template.moveList.map(move => move.barName)
      const name = template.templateName
      const summary = template.templateSummary

      const actualMoveIds = ids.filter(id => id)
      const allIds = actualMoveIds.concat(articles)
      const moveSet = await axios.post('https://backend.voin-cn.com/movements', {ids: allIds})
      if (moveSet.data.code === 1) {
        const moveResult = moveSet.data.doc
        const planDetail = ids.map((move, index) => {
          if (move) {
            const thisMove = moveResult.filter(aMove => aMove._id === move)[0]
            return {
              _id: move,
              name: thisMove.name,
              screenshot: thisMove.screenshot,
              info: thisMove.info
            }
          } else {
            return {
              name: barNames[index],
              bar: bars[index],
              start: starts[index]
            }
          }
        })
        const articleDetail = articles.map(article => {
          const thisArticle = moveResult.filter(anArticle => anArticle._id === article)[0]
          return {
            moveId: article,
            moveName: thisArticle.name
          }
        })
        dispatch(showTemplateSuccess(planDetail, ids, sets, timesPerSets, times, weights, betweens, infos,
          parameter1s, parameter2s, value1s, value2s, bars, starts, barNames, articleDetail, totalInEach, periodDays,
          name, summary, "", []))

      } else {
        dispatch(showPlanDetailFail("后端出错了，请刷新"))
      }
    } catch (error) {
      dispatch(showTemplateFail("内部服务器错误，请刷新"))
    }
  }
}

const showTemplateSuccess = (planDetail, ids, sets, timesPerSets, times, weights, betweens, infos,
                             parameter1s, parameter2s, value1s, value2s, bars, starts, barNames, articleDetail,
                             totalInEach, periodDays, name, summary, proposalContent, references, therapyIds, minutes,
                             timesPerDays, time2s, periods, therapyInfos, allChosenParams, therapyDetail,
                             addedParamCounts) => {
  return {
    type: actionTypes.SHOW_TEMPLATE_SUCCESS,
    planDetail, ids, sets, timesPerSets, times, weights, betweens, infos, name, summary,
    parameter1s, parameter2s, value1s, value2s, bars, starts, barNames, articleDetail, totalInEach, periodDays,
    proposalContent, references, therapyIds, minutes, timesPerDays, time2s, periods, therapyInfos,
    allChosenParams, therapyDetail, addedParamCounts
  }
}

const showTemplateFail = msg => {
  return {
    type: actionTypes.SHOW_TEMPLATE_FAIL,
    msg
  }
}

export const updatingPlan = (info, web) => {
  return async dispatch => {
    dispatch(updatingStart())
    try {
      const result = await axios.post("https://backend.voin-cn.com/updateplan", {
        doctorId: info.doctorId,
        clientId: info.clientId,
        planId: info.planId,
        name: info.name,
        summary: info.summary,
        times: info.times,
        sets: info.sets,
        timesPerSets: info.timesPerSets,
        weights: info.weights,
        betweens: info.betweens,
        parameter1s: info.parameter1s,
        parameter2s: info.parameter2s,
        value1s: info.value1s,
        value2s: info.value2s,
        infos: info.infos,
        minutes: info.minutes,
        timesPerDays: info.timesPerDays,
        time2s: info.time2s,
        periods: info.periods,
        therapyInfos: info.therapyInfos,
        addedParamCounts: info.addedParamCounts,
        allChosenParams: info.allChosenParams,
        therapyIds: info.therapyIds,
        ids: info.ids,
        bars: info.bars,
        starts: info.starts,
        barNames: info.barNames,
        totalInEach: info.totalInEach,
        periodDays: info.periodDays,
        articles: info.articles,
        extra: info.extra
      })
      if (result.data.code === 1) {
        dispatch(updatingSuccess())
        // setTimeout(() => {
        //   web.props.history.push('/templates')
        //   dispatch(closeDetailModal())
        // }, 500)
      } else if (result.data.code === 0) {
        dispatch(updatingFail(result.data.msg))
      }
    } catch (error) {
      dispatch(updatingFail("内部服务器错误，请刷新"))
    }
  }
}

export const updatingTemplate = (info, web) => {
  return async dispatch => {
    dispatch(updatingStart())
    try {
      const result = await axios.post("https://backend.voin-cn.com/newupdatetemplate", {
        doctorId: info.doctorId,
        templateId: info.templateId,
        name: info.name,
        summary: info.summary,
        times: info.times,
        sets: info.sets,
        timesPerSets: info.timesPerSets,
        weights: info.weights,
        betweens: info.betweens,
        parameter1s: info.parameter1s,
        parameter2s: info.parameter2s,
        value1s: info.value1s,
        value2s: info.value2s,
        infos: info.infos,
        minutes: info.minutes,
        timesPerDays: info.timesPerDays,
        time2s: info.time2s,
        periods: info.periods,
        therapyInfos: info.therapyInfos,
        addedParamCounts: info.addedParamCounts,
        allChosenParams: info.allChosenParams,
        therapyIds: info.therapyIds,
        ids: info.ids,
        bars: info.bars,
        starts: info.starts,
        barNames: info.barNames,
        totalInEach: info.totalInEach,
        periodDays: info.periodDays,
        articles: info.articles,
        proposalContent: info.proposalContent,
        references: info.references
      })
      if (result.data.code === 1) {
        dispatch(updatingSuccess())
        // setTimeout(() => {
        //   web.props.history.push('/templates')
        //   dispatch(closeDetailModal())
        // }, 500)
      } else if (result.data.code === 0) {
        dispatch(updatingFail(result.data.msg))
      }
    } catch (error) {
      dispatch(updatingFail("内部服务器错误，请刷新"))
    }
  }
}

const updatingStart = () => {
  return {
    type: actionTypes.UPDATING_START
  }
}

const updatingFail = msg => {
  return {
    type: actionTypes.UPDATING_FAIL,
    msg
  }
}

const updatingSuccess = () => {
  return {
    type: actionTypes.UPDATING_SUCCESS
  }
}

export const showPlanModal = info => {
  return {
    type: actionTypes.SHOW_PLAN_MODAL,
    info
  }
}