import * as actionTypes from './actionTypes'
import axios from 'axios'

export const fetchClassesAndHomework = (doctorId) => {
  return async dispatch => {
    dispatch(fetchClassesAndHomeworkStart())
    try {
      const result = await axios.get('https://backend.voin-cn.com/classhomework', {params: {doctorId}})
      const res = result.data
      if (res.code === 1) {
        const titles = [...res.titles]
        titles.reverse()
        dispatch(fetchClassesAndHomeworkSuccess(titles, res.submits, res.classInfo, res.teacher))
      } else {
        dispatch(fetchClassesAndHomeworkFail(res.msg))
      }
    }
    catch (error) {
      dispatch(fetchClassesAndHomeworkFail("内部服务器错误，请刷新"))
    }
  }
}

const fetchClassesAndHomeworkStart = () => {
  return {
    type: actionTypes.FETCH_CLASSES_AND_HOMEWORK_START
  }
}

const fetchClassesAndHomeworkSuccess = (titles, submits, classInfo, teacher) => {
  return {
    type: actionTypes.FETCH_CLASSES_AND_HOMEWORK_SUCCESS,
    titles: titles, submits, classInfo, teacher
  };
};

const fetchClassesAndHomeworkFail = msg => {
  return {
    type: actionTypes.FETCH_CLASSES_AND_HOMEWORK_FAIL,
    msg
  };
};

export const modalClosed = () => {
  return {
    type: actionTypes.CLOSE_MODAL
  }
}