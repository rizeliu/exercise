import * as actionTypes from './actionTypes'
import axios from 'axios'

export const searchNewClientInputChange = value => {
  return {
    type: actionTypes.SEARCH_NEW_CLIENT_INPUT_CHANGE,
    value
  }
}

const searchOrAddStart = () => {
  return {
    type: actionTypes.SEARCH_OR_ADD_START
  }
}

const findOneClient = (name, clientId) => {
  return {
    type: actionTypes.FIND_ONE_CLIENT,
    name,
    clientId
  }
}

const findNoOne = () => {
  return {
    type: actionTypes.FIND_NO_ONE
  }
}

const searchOrAddFail = error => {
  return {
    type: actionTypes.SEARCH_OR_ADD_FAIL,
    error
  }
}

export const searchNewClient = username => {
  return async dispatch => {
    dispatch(searchOrAddStart())
    try {
      const client = await axios.get("https://backend.voin-cn.com/searchclient", {params: {username}})
      const res = client.data
      if (res.code === 1) {
        if (res.doc) {
          dispatch(findOneClient(res.doc.name, res.doc._id))
        } else {
          dispatch(findNoOne())
        }
      } else {
        dispatch(searchOrAddFail(res.doc.msg))
      }
    } catch (error) {
      dispatch(searchOrAddFail("内部服务器错误，请刷新"))
    }
  }
}

const addNewClientSuccess = () => {
  return {
    type: actionTypes.ADD_NEW_CLIENT_SUCCESS
  }
}

const alreadyAClient = () => {
  return {
    type: actionTypes.ALREADY_A_CLIENT
  }
}

export const addNewClient = (doctorId, clientId, doctorName, clientName) => {
  return async dispatch => {
    dispatch(searchOrAddStart())
    try{
      const response = await axios.post('https://backend.voin-cn.com/addclient', {doctorId, clientId, doctorName, clientName})
      if (response.data.code === 1) {
        dispatch(addNewClientSuccess())
      } else if (response.data.code === 0) {
        dispatch(searchOrAddFail(response.doc.msg))
      } else if (response.data.code === 2) {
        dispatch(alreadyAClient())
      }
    } catch (error) {
      dispatch(searchOrAddFail("内部服务器错误，请刷新"))
    }
  }
}

export const addClientInit = () => {
  return {
    type: actionTypes.ADD_CLIENT_INIT
  }
}