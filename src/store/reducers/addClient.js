import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  username: "",
  name: "",
  clientId: "",
  mode: 0,
  error: ""
}

const searchNewClientInputChange = (state, action) => updateObject(state, {
  mode: 0,
  username: action.value
})

const searchNewClientStart = state => updateObject(state, {mode: 1})

const findOneClient = (state, action) => updateObject(state, {
  mode: 3,
  name: action.name,
  clientId: action.clientId
})

const searchNewClientFail = (state, action) => updateObject(state, {
  mode: 6,
  error: action.error
})

const findNoOne = state => updateObject(state, {mode: 2})

const addNewClientSuccess = state => updateObject(state, {mode: 5})

const alreadyAClient = state => updateObject(state, {mode: 4})

const addClientInit = () => {return {...initialState}}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEARCH_NEW_CLIENT_INPUT_CHANGE:
      return searchNewClientInputChange(state, action)
    case actionTypes.SEARCH_OR_ADD_START:
      return searchNewClientStart(state)
    case actionTypes.FIND_ONE_CLIENT:
      return findOneClient(state, action)
    case actionTypes.FIND_NO_ONE:
      return findNoOne(state)
    case actionTypes.SEARCH_OR_ADD_FAIL:
      return searchNewClientFail(state, action)
    case actionTypes.ADD_NEW_CLIENT_SUCCESS:
      return addNewClientSuccess(state)
    case actionTypes.ALREADY_A_CLIENT:
      return alreadyAClient(state)
    case actionTypes.ADD_CLIENT_INIT:
      return addClientInit()
    default: return state
  }
}

export default reducer