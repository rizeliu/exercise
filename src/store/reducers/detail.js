import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  showDetailModal: false,
  template: false,
  clientName: "",
  clientEmail: "",
  allDay: "",
  planDetail: [],
  infos: [],
  sets: [],
  times: [],
  timesPerSets: [],
  weights: [],
  betweens: [],
  ids: [],
  articles: [],
  therapies: [],
  parameter1s: [],
  value1s: [],
  parameter2s: [],
  value2s: [],
  minutes: [],
  timesPerDays: [],
  time2s: [],
  periods: [],
  therapyInfos: [],
  addedParamCounts: [],
  allChosenParams: [],
  therapyIds: [],
  bars: [],
  starts: [],
  barNames: [],
  feedback: [],
  savedFeedback: [],
  showFeedback: false,
  completion: 0,
  effect: 0,
  description: "",
  startDate: "",
  status: "",
  overallValid: false,
  saving: false,
  error: 0,
  errorMsg: "",
  savingSuccess: false,
  modalLoading: false,
  editing: false,
  toClient: false,
  doctorNameToClient: "",
  planName: "",
  introduction: "",
  extra: "",
  period: 1,
  totalInEach: [0],
  periodDays: [],
  newPeriodDays: [],
  savedPlan: {},
  savedTemplate: {},
  fromEditPlan: false,
  homeworkMode: false,
  proposalContent: "",
  references: [""]
}

const closeModalDetail = state => updateObject(state, {
  showDetailModal: false,
  allDay: "",
  error: 0,
  errorMsg: "",
  savingSuccess: false,
  modalLoading: false
})

const changeAllDay = (state, action) => updateObject(state, {
  allDay: action.value, error: 0
})

const changeExtra = (state, action) => updateObject(state, {
  extra: action.value, error: 0
})

const changeSingle = (state, action) => {
  if (action.index1 === 0) {
    const betweens = [...state.betweens]
    betweens[action.index2] = action.value
    return updateObject(state, {betweens, error: 0})
  } else if (action.index1 === 1) {
    const times = [...state.times]
    times[action.index2] = action.value
    return updateObject(state, {times, error: 0})
  } else if (action.index1 === 2) {
    const sets = [...state.sets]
    sets[action.index2] = action.value
    return updateObject(state, {sets, error: 0})
  } else if (action.index1 === 3) {
    const timesPerSets = [...state.timesPerSets]
    timesPerSets[action.index2] = action.value
    return updateObject(state, {timesPerSets, error: 0})
  } else if (action.index1 === 4) {
    const infos = [...state.infos]
    infos[action.index2] = action.value
    return updateObject(state, {infos, error: 0})
  } else if (action.index1 === 5) {
    const parameter1s = [...state.parameter1s]
    parameter1s[action.index2] = action.value
    return updateObject(state, {parameter1s, error: 0})
  } else if (action.index1 === 6) {
    const value1s = [...state.value1s]
    value1s[action.index2] = action.value
    return updateObject(state, {value1s, error: 0})
  } else if (action.index1 === 7) {
    const parameter2s = [...state.parameter2s]
    parameter2s[action.index2] = action.value
    return updateObject(state, {parameter2s, error: 0})
  } else if (action.index1 === 8) {
    const value2s = [...state.value2s]
    value2s[action.index2] = action.value
    return updateObject(state, {value2s, error: 0})
  }
  return state
}

const changeTherapySingle = (state, action) => {
  if (action.index1 === 0) {
    const minutes = [...state.minutes]
    minutes[action.index2] = action.value
    return updateObject(state, {minutes, error: 0})
  } else if (action.index1 === 1) {
    const timesPerDays = [...state.timesPerDays]
    timesPerDays[action.index2] = action.value
    return updateObject(state, {timesPerDays, error: 0})
  } else if (action.index1 === 2) {
    const time2s = [...state.time2s]
    time2s[action.index2] = action.value
    return updateObject(state, {time2s, error: 0})
  } else if (action.index1 === 3) {
    const periods = [...state.periods]
    periods[action.index2] = action.value
    return updateObject(state, {periods, error: 0})
  } else if (action.index1 === 4) {
    const therapyInfos = [...state.therapyInfos]
    therapyInfos[action.index2] = action.value
    return updateObject(state, {therapyInfos, error: 0})
  }
  return state
}

const changeToInt = (state, action) => updateObject(state, {
  betweens: action.newBetweens,
})

const savingStart = state => updateObject(state, {
  saving: true
})

const notInt = state => updateObject(state, {
  error: 1
})

const currentPeriodShorter = state => updateObject(state, {
  error: 6
})

const notBetween = state => updateObject(state, {
  error: 2
})

const notComplete = state => updateObject(state, {
  error: 5
})

const notGreaterThan15 = state => updateObject(state, {
  error: 7
})

const savingSuccess = state => updateObject(state, {
  saving: false, savingSuccess: true
})

const savingFail = (state, action) => updateObject(state, {
  saving: false, error: 3, errorMsg: action.msg
})

const updatingStart = state => updateObject(state, {
  saving: true
})

const updatingSuccess = state => updateObject(state, {
  saving: false, savingSuccess: true
})

const updatingFail = (state, action) => updateObject(state, {
  saving: false, error: 3, errorMsg: action.msg
})

const showDetail = (state, action) => updateObject(state, {
  planDetail: action.info.planDetail, template: action.info.template, modalLoading: false,
  allDay: action.info.allDay, times: action.info.times, infos: action.info.infos,
  sets: action.info.sets, weights: action.info.weights, betweens: action.info.betweens, timesPerSets: action.info.timesPerSets,
  parameter1s: action.info.parameter1s.length > 0 ? action.info.parameter1s : Array(action.info.sets.length).join(".").split("."),
  value1s: action.info.value1s.length > 0 ? action.info.value1s : Array(action.info.sets.length).join(".").split("."),
  parameter2s: action.info.parameter2s.length > 0 ? action.info.parameter2s : Array(action.info.sets.length).join(".").split("."),
  value2s: action.info.value2s.length > 0 ? action.info.value2s : Array(action.info.sets.length).join(".").split("."),
  minutes: action.info.minutes, timesPerDays: action.info.timesPerDays, time2s: action.info.time2s, periods: action.info.periods,
  therapyInfos: action.info.therapyInfos, addedParamCounts: action.info.addedParamCounts, allChosenParams: action.info.allChosenParams,
  showDetailModal: true, savingSuccess: false, ids: action.info.ids, editing: true,
  planName: action.info.planName, introduction: action.info.introduction, extra: action.info.extra,
  articles: action.info.articles, period: action.info.totalInEach.length, therapies: action.info.therapies,
  totalInEach: action.info.totalInEach && action.info.totalInEach.length > 0 ? action.info.totalInEach : [action.info.planDetail.length],
  bars: action.info.bars, starts: action.info.starts,
  barNames: action.info.planDetail.map(move => move.name ? move.name : ""),
  periodDays: action.info.totalInEach && action.info.totalInEach.length > 0
    ? action.info.periodDays : action.info.allDay ? [action.info.allDay] : [""],
  newPeriodDays: action.info.totalInEach && action.info.totalInEach.length > 0
    ? action.info.periodDays : action.info.allDay ? [action.info.allDay] : [""],
  showFeedback: false, start: action.info.start, startDate: action.info.startDate ? action.info.startDate : "",
  fromEditPlan: action.info.fromEditPlan, therapyIds: action.info.therapyIds
})

const setClientName = (state, action) => updateObject(state, {
  clientName: action.clientName
})

const modalLoading = state => updateObject(state, {
  modalLoading: true, showDetailModal: true
})

const showPlanDetailSuccess = (state, action) => updateObject(state, {
  planDetail: action.planDetail, template: false, editing: false, weights: action.weights, betweens: action.betweens,
  timesPerSets: action.timesPerSets, times: action.times, sets: action.sets, infos: action.infos,
  parameter1s: action.parameter1s, parameter2s: action.parameter2s, value1s: action.value1s, value2s: action.value2s,
  modalLoading: false, showDetailModal: true, savingSuccess: false, ids: action.ids, extra: action.extra,
  planName: action.name, allDay: action.allDay, therapyIds: action.therapyIds, minutes: action.minutes,
  timesPerDays: action.timesPerDays, time2s: action.time2s, periods: action.periods, therapyInfos: action.therapyInfos,
  allChosenParams: action.allChosenParams, therapies: action.therapyDetail, addedParamCounts: action.addedParamCounts,
  articles: action.articles, proposalContent: action.proposalContent ? action.proposalContent : "",
  references: action.references && action.references.length > 0 ? [...action.references] : [""],
  totalInEach: action.totalInEach && action.totalInEach.length > 0 ? action.totalInEach : [action.planDetail.length],
  periodDays: action.totalInEach && action.totalInEach.length > 0
    ? action.periodDays : action.allDay ? [action.allDay] : [""],
  bars: action.bars, starts: action.starts, barNames: action.barNames, showFeedback: false,
  homeworkMode: action.homework
})

const seeFeedbackSuccess = (state, action) => updateObject(state, {
  planDetail: action.planDetail, template: false, editing: false, betweens: action.betweens,
  modalLoading: false, showDetailModal: true, savingSuccess: false, ids: action.ids, extra: action.extra,
  planName: action.name, allDay: action.allDay,
  totalInEach: action.totalInEach && action.totalInEach.length > 0 ? action.totalInEach : [action.planDetail.length],
  periodDays: action.totalInEach && action.totalInEach.length > 0
    ? action.periodDays : action.allDay ? [action.allDay] : [""],
  bars: action.bars, starts: action.starts, barNames: action.barNames, showFeedback: true, feedback: action.feedback,
  completion: action.completion, effect: action.effect, description: action.description,
  startDate: action.startDate, status: action.status, savedFeedback: action.savedFeedback
})

const showPlanDetailFail = (state, action) => updateObject(state, {
  modalLoading: false, showDetailModal: true, template: false, savingSuccess: false, error: 3, errorMsg: action.msg
})

const showTemplateSuccess = (state, action) => updateObject(state, {
  planDetail: action.planDetail, template: true, editing: false, weights: action.weights, betweens: action.betweens,
  timesPerSets: action.timesPerSets, times: action.times, sets: action.sets, infos: action.infos,
  modalLoading: false, showDetailModal: true, savingSuccess: false, ids: action.ids,
  clientName: action.clientName, planName: action.name, introduction: action.summary,
  parameter1s: action.parameter1s, parameter2s: action.parameter2s, value1s: action.value1s,
  value2s: action.value2s, articles: action.articleDetail,
  totalInEach: action.totalInEach && action.totalInEach.length > 0 ? action.totalInEach : [action.planDetail.length],
  periodDays: action.totalInEach && action.totalInEach.length > 0
    ? action.periodDays : action.allDay ? [action.allDay] : [""],
  bars: action.bars, starts: action.starts, barNames: action.barNames, showFeedback: false,
  proposalContent: action.proposalContent ? action.proposalContent : "",
  references: action.references && action.references.length > 0 ? [...action.references] : [""],
  therapyIds: action.therapyIds, minutes: action.minutes,
  timesPerDays: action.timesPerDays, time2s: action.time2s, periods: action.periods, therapyInfos: action.therapyInfos,
  allChosenParams: action.allChosenParams, therapies: action.therapyDetail, addedParamCounts: action.addedParamCounts,
})

const showTemplateFail = (state, action) => updateObject(state, {
  modalLoading: false, showDetailModal: true, template: true, savingSuccess: false, error: 3, errorMsg: action.msg
})

const changePeriodDays = (state, action) => updateObject(state, {
  newPeriodDays: [...state.newPeriodDays.slice(0, action.period), action.value, ...state.newPeriodDays.slice(action.period+1)],
  error: 0
})

const deleteOne = (state, action) => {
  const newPlanDetail = [
    ...state.planDetail.slice(0, action.index),
    ...state.planDetail.slice(action.index + 1)
  ]
  const newInfos = [
    ...state.infos.slice(0, action.index),
    ...state.infos.slice(action.index + 1)
  ]
  const newTimesPerSets = [
    ...state.timesPerSets.slice(0, action.index),
    ...state.timesPerSets.slice(action.index + 1)
  ]
  const newSets = [
    ...state.sets.slice(0, action.index),
    ...state.sets.slice(action.index + 1)
  ]
  const newWeights = [
    ...state.weights.slice(0, action.index),
    ...state.weights.slice(action.index + 1)
  ]
  const newBetweens = [
    ...state.betweens.slice(0, action.index),
    ...state.betweens.slice(action.index + 1)
  ]
  const newTimes = [
    ...state.times.slice(0, action.index),
    ...state.times.slice(action.index + 1)
  ]
  const newParameter1s = [
    ...state.parameter1s.slice(0, action.index),
    ...state.parameter1s.slice(action.index + 1)
  ]
  const newValue1s = [
    ...state.value1s.slice(0, action.index),
    ...state.value1s.slice(action.index + 1)
  ]
  const newParameter2s = [
    ...state.parameter2s.slice(0, action.index),
    ...state.parameter2s.slice(action.index + 1)
  ]
  const newValue2s = [
    ...state.value2s.slice(0, action.index),
    ...state.value2s.slice(action.index + 1)
  ]
  const newIds = [
    ...state.ids.slice(0, action.index),
    ...state.ids.slice(action.index + 1)
  ]
  const newBars = [
    ...state.bars.slice(0, action.index),
    ...state.bars.slice(action.index + 1)
  ]
  const newStarts = [
    ...state.starts.slice(0, action.index),
    ...state.starts.slice(action.index + 1)
  ]
  const newBarNames = [
    ...state.barNames.slice(0, action.index),
    ...state.barNames.slice(action.index + 1)
  ]
  const newTotal = state.totalInEach.map(num => num > action.index ? num-1 : num)
  return updateObject(state, {
    planDetail: newPlanDetail,
    infos: newInfos,
    times: newTimes,
    timesPerSets: newTimesPerSets,
    weights: newWeights,
    betweens: newBetweens,
    sets: newSets,
    parameter1s: newParameter1s,
    value1s: newValue1s,
    parameter2s: newParameter2s,
    value2s: newValue2s,
    ids: newIds,
    totalInEach: newTotal,
    bars: newBars,
    starts: newStarts,
    barNames: newBarNames
  })
}

const deleteArticle = (state, action) => {
  const newArticles = [
    ...state.articles.slice(0, action.index),
    ...state.articles.slice(action.index + 1)
  ]
  return updateObject(state, {
    articles: newArticles
  })
}

const addParameter = (state, action) => {
  const index = action.index
  const currentCount = state.addedParamCounts[index]
  const chosenNames = state.allChosenParams[index].map(param => param.name)
  const allNames = action.params.map(param => param.name)
  const newChosenParams = [...state.allChosenParams[index]]
  for (let i = 0; i < allNames.length; i++) {
    if (chosenNames.indexOf(allNames[i]) < 0) {
      newChosenParams.push({name: allNames[i], value: ""})
      break
    }
  }
  const newAllChosenParams = [
    ...state.allChosenParams.slice(0, index), newChosenParams, ...state.allChosenParams.slice(index+1)
  ]
  const newAddedParamCounts = [
    ...state.addedParamCounts.slice(0, index), currentCount+1, ...state.addedParamCounts.slice(index+1)
  ]
  return updateObject(state, {
    allChosenParams: newAllChosenParams, addedParamCounts: newAddedParamCounts
  })
}

const deleteTherapyParameter = (state, action) => {
  const index1 = action.index1
  const index2 = action.index2
  const currentCount = state.addedParamCounts[index1]
  const currentChosenParams = state.allChosenParams[index1]
  const newCurrentChosenParams = [...currentChosenParams.slice(0, index2), ...currentChosenParams.slice(index2+1)]
  const newAddedParamCounts = [
    ...state.addedParamCounts.slice(0, index1), currentCount-1, ...state.addedParamCounts.slice(index1+1)
  ]
  const newAllChosenParams = [...state.allChosenParams.slice(0, index1), newCurrentChosenParams, ...state.allChosenParams.slice(index1+1)]
  return updateObject(state, {
    allChosenParams: newAllChosenParams, addedParamCounts: newAddedParamCounts
  })
}

const changeParameterName = (state, action) => {
  const index1 = action.index1
  const index2 = action.index2
  const value = action.value
  const newChosenParams = [...state.allChosenParams[index1].slice(0, index2),
    {name: value, value: ""}, ...state.allChosenParams[index1].slice(index2+1)]
  const newAllChosenParams = [...state.allChosenParams.slice(0, index1), newChosenParams, ...state.allChosenParams.slice(index1+1)]
  return updateObject(state, {
    allChosenParams: newAllChosenParams
  })
}

const changeParameterValue = (state, action) => {
  const index1 = action.index1
  const index2 = action.index2
  const value = action.value
  const newChosenParams = [...state.allChosenParams[index1].slice(0, index2),
    {name: state.allChosenParams[index1][index2].name, value: value}, ...state.allChosenParams[index1].slice(index2+1)]
  const newAllChosenParams = [...state.allChosenParams.slice(0, index1), newChosenParams, ...state.allChosenParams.slice(index1+1)]
  return updateObject(state, {
    allChosenParams: newAllChosenParams
  })
}

const deleteTherapy = (state, action) => {
  const newTherapies = [
    ...state.therapies.slice(0, action.index),
    ...state.therapies.slice(action.index + 1)
  ]
  const newMinutes = [
    ...state.minutes.slice(0, action.index),
    ...state.minutes.slice(action.index + 1)
  ]
  const newTimesPerDays = [
    ...state.timesPerDays.slice(0, action.index),
    ...state.timesPerDays.slice(action.index + 1)
  ]
  const newTime2s = [
    ...state.time2s.slice(0, action.index),
    ...state.time2s.slice(action.index + 1)
  ]
  const newPeriods = [
    ...state.periods.slice(0, action.index),
    ...state.periods.slice(action.index + 1)
  ]
  const newTherapyInfos = [
    ...state.therapyInfos.slice(0, action.index),
    ...state.therapyInfos.slice(action.index + 1)
  ]
  const newAddedParamsCounts = [
    ...state.addedParamCounts.slice(0, action.index),
    ...state.addedParamCounts.slice(action.index + 1)
  ]
  const newAllChosenParams = [
    ...state.allChosenParams.slice(0, action.index),
    ...state.allChosenParams.slice(action.index + 1)
  ]
  const newTherapyIds = [
    ...state.therapyIds.slice(0, action.index),
    ...state.therapyIds.slice(action.index + 1)
  ]


  return updateObject(state, {
    therapies: newTherapies, minutes: newMinutes, timesPerDays: newTimesPerDays, time2s: newTime2s, periods: newPeriods,
    therapyInfos: newTherapyInfos, addedParamCounts: newAddedParamsCounts, allChosenParams: newAllChosenParams,
    therapyIds: newTherapyIds
  })
}

const showPlanModal = (state, action) => {
  return updateObject(state, {toClient: true, editing: false, showDetailModal: true, template: false,
    clientName: action.info.clientName, allDay: action.info.allDay, planDetail: action.info.planDetail,
    infos: action.info.infos, sets: action.info.sets, times: action.info.times, timesPerSets: action.info.timesPerSets,
    weights: action.info.weights, betweens: action.info.betweens, articles: action.info.articles,
    parameter1s: action.info.parameter1s, value1s: action.info.value1s,
    parameter2s: action.info.parameter2s, value2s: action.info.value2s, planName: action.info.planName,
    extra: action.info.extra, doctorNameToClient: action.info.doctorName
  })
}

const deleteParameter = (state, action) => {
  if (action.index1 === 1) {
    const newParameter1 = state.parameter2s[action.index2]
    const newValue1 = state.value2s[action.index2]
    const newParameter1s = [
      ...state.parameter1s.slice(0, action.index2),
      newParameter1,
      ...state.parameter1s.slice(action.index2 + 1)
    ]
    const newValue1s = [
      ...state.value1s.slice(0, action.index2),
      newValue1,
      ...state.value1s.slice(action.index2 + 1)
    ]
    const newParameter2s = [
      ...state.parameter2s.slice(0, action.index2),
      "",
      ...state.parameter2s.slice(action.index2 + 1)
    ]
    const newValue2s = [
      ...state.value2s.slice(0, action.index2),
      "",
      ...state.value2s.slice(action.index2 + 1)
    ]
    return updateObject(state, {
      parameter1s: newParameter1s,
      value1s: newValue1s,
      parameter2s: newParameter2s,
      value2s: newValue2s
    })
  } else if (action.index1 === 2) {
    const newParameter2s = [
      ...state.parameter2s.slice(0, action.index2),
      "",
      ...state.parameter2s.slice(action.index2 + 1)
    ]
    const newValue2s = [
      ...state.value2s.slice(0, action.index2),
      "",
      ...state.value2s.slice(action.index2 + 1)
    ]
    return updateObject(state, {
      parameter2s: newParameter2s,
      value2s: newValue2s
    })
  }
  return state
}

const noMoves = state => updateObject(state, {
  error: 4
})

const editPlanSuccess = (state, action) => updateObject(state, {savedPlan: action.plan})

const editTemplateSuccess = (state, action) => updateObject(state, {savedTemplate: action.template})

const createPlanFromTemplateSuccess = (state, action) => updateObject(state, {savedTemplate: action.template})

const introduceOneTemplateSuccess = (state, action) => updateObject(state, {savedTemplate: action.template})

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CLOSE_DETAIL_MODAL:
      return closeModalDetail(state)
    case actionTypes.CHANGE_ALL_DAY:
      return changeAllDay(state, action)
    case actionTypes.CHANGE_EXTRA:
      return changeExtra(state, action)
    case actionTypes.CHANGE_SINGLE:
      return changeSingle(state, action)
    case actionTypes.CHANGE_THERAPY_SINGLE:
      return changeTherapySingle(state, action)
    case actionTypes.NOT_INT:
      return notInt(state)
    case actionTypes.CURRENT_PERIOD_SHORTER:
      return currentPeriodShorter(state)
    case actionTypes.NOT_BETWEEN:
      return notBetween(state)
    case actionTypes.NOT_COMPLETE:
      return notComplete(state)
    case actionTypes.NOT_GREATER_THAN_15:
      return notGreaterThan15(state)
    case actionTypes.CHANGE_TO_INT:
      return changeToInt(state, action)
    case actionTypes.SAVING_START:
      return savingStart(state)
    case actionTypes.SAVING_SUCCESS:
      return savingSuccess(state)
    case actionTypes.SAVING_FAIL:
      return savingFail(state, action)
    case actionTypes.UPDATING_START:
      return updatingStart(state)
    case actionTypes.UPDATING_SUCCESS:
      return updatingSuccess(state)
    case actionTypes.UPDATING_FAIL:
      return updatingFail(state, action)
    case actionTypes.SHOW_DETAIL:
      return showDetail(state, action)
    case actionTypes.CHANGE_PERIOD_DAYS:
      return changePeriodDays(state, action)
    case actionTypes.SET_CLIENT_NAME:
      return setClientName(state, action)
    case actionTypes.MODAL_LOADING:
      return modalLoading(state)
    case actionTypes.SHOW_PLAN_DETAIL_SUCCESS:
      return showPlanDetailSuccess(state, action)
    case actionTypes.SHOW_PLAN_DETAIL_FAIL:
      return showPlanDetailFail(state, action)
    case actionTypes.SHOW_TEMPLATE_SUCCESS:
      return showTemplateSuccess(state, action)
    case actionTypes.SHOW_TEMPLATE_FAIL:
      return showTemplateFail(state, action)
    case actionTypes.DELETE_ONE:
      return deleteOne(state, action)
    case actionTypes.DELETE_ARTICLE:
      return deleteArticle(state, action)
    case actionTypes.DELETE_THERAPY:
      return deleteTherapy(state, action)
    case actionTypes.ADD_PARAMETER:
      return addParameter(state, action)
    case actionTypes.DELETE_THERAPY_PARAMETER:
      return deleteTherapyParameter(state, action)
    case actionTypes.CHANGE_PARAMETER_NAME:
      return changeParameterName(state, action)
    case actionTypes.CHANGE_PARAMETER_VALUE:
      return changeParameterValue(state, action)
    case actionTypes.DELETE_PARAMETER:
      return deleteParameter(state, action)
    case actionTypes.SHOW_PLAN_MODAL:
      return showPlanModal(state, action)
    case actionTypes.NO_MOVES:
      return noMoves(state)
    case actionTypes.EXERCISE_UNMOUNT:
      return {...initialState}
    case actionTypes.EDIT_PLAN_SUCCESS:
      return editPlanSuccess(state, action)
    case actionTypes.EDIT_TEMPLATE_SUCCESS:
      return editTemplateSuccess(state, action)
    case actionTypes.CREATE_PLAN_BY_TEMPLATE_SUCCESS:
      return createPlanFromTemplateSuccess(state, action)
    case actionTypes.SEE_FEEDBACK_SUCCESS:
      return seeFeedbackSuccess(state, action)
    case actionTypes.INTRODUCE_ONE_TEMPLATE_SUCCESS:
      return introduceOneTemplateSuccess(state, action)
    default: return state
  }
}

export default reducer