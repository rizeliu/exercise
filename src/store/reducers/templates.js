import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  templates: [],
  allTemplates: [],
  expertTemplates: [],
  teamTemplates: [],
  teamId: "",
  teams: [],
  teamMembers: [],
  teamMembersError: "",
  leaveTeamError: "",
  deleteTeamError: "",
  deleteMemberSuccess: false,
  deleteMemberError: "",
  allExpertTemplates: [],
  allTeamTemplates: [],
  deleteDoctorId: "",
  deleteTemplateId: "",
  deleteTemplateName: "",
  createQRTemplateId: "",
  createQRTemplateName: "",
  base64Str: "",
  createQRError: "",
  sendTemplateId: "",
  sendTemplateName: "",
  addTemplateId: "",
  addTemplateName: "",
  doctors: [],
  allDoctors: [],
  sendError: "",
  realSendError: "",
  sendingDoctorIds: [],
  deleteMy: true,
  mode: 1,
  expertMode: 1,
  teamMode: 1,
  labels: [],
  labelName: "",
  error: "",
  expertError: "",
  teamError: "",
  realAddExpertError: "",
  deleteError: "",
  templateSearchValue: "",
  expertTemplateSearchValue: "",
  teamTemplateSearchValue: "",
  focusOnMy: true,
  doctorSearchValue: "",
  templateTitle: "",
  addCategoryMode: 0,
  content: 1,
  circles: [],
  addCircleTemplateId: "",
  addCircleTemplateName: "",
  addCircleTemplateError: "",
  chosenCircleName: "",
  chosenCircleId: "",
}

const fetchTemplatesStart = state => updateObject(state, {
  mode: 1, templateSearchValue: ""
})

const fetchTemplatesSuccess = (state, action) => {
  if (action.templates.length > 0 && action.templates[0].date) {
    const newTemplates = action.templates.sort((a, b) => {
      if (a.date > b.date) return -1
      else return 1
    })
    return updateObject( state, {
      templates: newTemplates,
      allTemplates: newTemplates,
      templateSearchValue: "",
      mode: 0,
      addCategoryMode: 0
    });
  } else {
    return updateObject( state, {
      templates: [...action.templates],
      allTemplates: [...action.templates],
      templateSearchValue: "",
      mode: 0,
      addCategoryMode: 0
    });
  }

}

const fetchTemplatesFail = (state, action) => updateObject(state, {
  templateSearchValue: "",
  error: action.error,
  mode: 0
})

const fetchExpertTemplatesStart = state => updateObject(state, {
  expertMode: 1,
  expertTemplateSearchValue: "",
  focusOnMy: true
})

const fetchExpertTemplatesSuccess = (state, action) => updateObject( state, {
  expertTemplates: [...action.templates],
  allExpertTemplates: [...action.templates],
  expertTemplateSearchValue: "",
  expertMode: 0
});

const fetchExpertTemplatesFail = (state, action) => updateObject(state, {
  expertTemplateSearchValue: "",
  expertError: action.error,
  expertMode: 0
})

const fetchTeamsAndFirstStart = state => updateObject(state, {
  teamMode: 1,
})

const fetchTeamsAndFirstSuccess = (state, action) => {
  if (action.admin === 4) {
    return updateObject(state, {
      teamTemplates: [...action.teams],
      teamMode: 0,
      teamId: action.classId,
      teams: [{teamId: action.classId, creator: true}]
    });
  } else if (action.admin === 5) {
    return updateObject(state, {
      teamTemplates: [...action.teams],
      teamMode: 0,
      teamId: action.classId,
      teams: [{teamId: action.classId, creator: false}]
    });
  }
  if (action.teams.length > 0) {
    return updateObject( state, {
      teamTemplates: [...action.teams[0].templates],
      teamId: action.teams[0]._id,
      teams: [...action.teams.map(team => {
        return {teamId: team._id, name: team.name, username: team.username, creator: team.creator}
      })],
      teamMode: 0
    });
  } else {
    return updateObject( state, {
      teamMode: 0
    });
  }
}

const teamChangeSuccess = (state, action) => {
  return updateObject( state, {
    teamMode: 0,
    teamId: action.team._id,
    teamTemplates: [...action.team.templates]
  });
}

const fetchTeamsAndFirstFail = (state, action) => updateObject(state, {
  teamError: action.error,
  teamMode: 0
})

const fetchTeamTemplatesStart = state => updateObject(state, {
  teamMode: 1,
  teamTemplateSearchValue: "",
})

const fetchTeamTemplatesSuccess = (state, action) => updateObject( state, {
  teamTemplates: [...action.templates],
  allTeamTemplates: [...action.templates],
  teamTemplateSearchValue: "",
  teamMode: 0
});

const fetchTeamTemplatesFail = (state, action) => updateObject(state, {
  teamTemplateSearchValue: "",
  teamError: action.error,
  teamMode: 0
})

const downloadTemplateStart = (state, action) => {
  if (action.expertOrTeam === "team") {
    const newTemplates = [...state.teamTemplates]
    const index = newTemplates.map(template => template.templateId).indexOf(action.templateId)
    newTemplates[index]["loading4"] = true
    return updateObject(state, {teamTemplates: newTemplates})
  }
  return state
}

const downloadTemplateSuccess = (state, action) => {
  if (action.expertOrTeam === "team") {
    const newTemplates = [...state.teamTemplates]
    newTemplates.forEach(template => {
      template["loading4"] = false
      template["done"] = true
    })
    return updateObject( state, {
      teamTemplates: newTemplates, mode: 27
    });
  } else return updateObject( state, {mode: 27});
}

const downloadTemplateFail = (state, action) => updateObject(state, {
  expertError: action.error,
  expertMode: 0
})

const addExpert = (state, action) => updateObject(state, {
  addTemplateId: action.templateId,
  addTemplateName: action.templateName,
  mode: 13, labels: []
})

const addExpertOrTeam = (state, action) => updateObject(state, {
  addTemplateId: action.templateId,
  addTemplateName: action.templateName,
  mode: 13, labels: []
})

const addExpertStart = (state) => updateObject(state, {
  mode: 14
})

const addExpertSuccess = state => updateObject(state, {
  mode: 15
})

const addExpertFail = (state, action) => updateObject(state, {
  realAddExpertError: action.error,
  mode: 15
})

const seeTeamMembersStart = (state) => updateObject(state, {
  mode: 16
})

const seeTeamMembersSuccess = (state, action) => updateObject(state, {
  mode: 17, teamMembers: action.members
})

const seeTeamMembersFail = (state, action) => updateObject(state, {
  mode: 18, teamMembersError: action.error
})

const addTeamStart = (state, action) => {
  const newTemplates = [...state.templates]
  newTemplates[action.index]["loading3"] = true
  return updateObject(state, {templates: newTemplates})
}

const addTeamSuccess = (state) => {
  const newTemplates = [...state.templates]
  newTemplates.forEach(template => {
    template.loading3 = false
  })
  return updateObject(state, {templates: newTemplates, mode: 5})
}

const addTeamFail = (state) => {
  const newTemplates = [...state.templates]
  newTemplates.forEach(template => {
    template.loading3 = false
  })
  return updateObject(state, {templates: newTemplates})
}

const sendTemplate = (state, action) => updateObject(state, {
  sendTemplateId: action.templateId,
  sendTemplateName: action.name,
  mode: 8
})

const fetchDoctorsStart = (state) => updateObject(state, {
  mode: 10,
  sendError: "",
})

const fetchDoctorsSuccess = (state, action) => updateObject(state, {
  doctors: [...action.doctors],
  allDoctors: [...action.doctors],
  sendError: "",
  mode: 9
})

const fetchDoctorsFail = (state, action) => updateObject(state, {
  sendError: action.error,
  mode: 9
})

const changeDoctor = (state, action) => {
  if (state.sendingDoctorIds.indexOf(action.doctorId) >= 0) {
    return updateObject(state, {
      sendingDoctorIds: state.sendingDoctorIds.filter(id => id !== action.doctorId)
    })
  } else {
    return updateObject(state, {
      sendingDoctorIds: [...state.sendingDoctorIds, action.doctorId]
    })
  }
}

const changeLabel = (state, action) => {
  if (action.name === state.labelName) {
    if (action.name === "team") {
      return updateObject(state, {labels: [action.value]})
    } else {
      if (state.labels.indexOf(action.value) >= 0) {
        return updateObject(state, {
          labels: state.labels.filter(label => label !== action.value)
        })
      } else {
        return updateObject(state, {
          labels: [...state.labels, action.value]
        })
      }
    }
  } else {
    if (action.name === "team") {
      return updateObject(state, {labels: [action.value], labelName: "team"})
    } else {
      return updateObject(state, {labels: [action.value], labelName: "expert"})
    }
  }

}

const realSendTemplateStart = (state) => updateObject(state, {
  mode: 11
})

const realSendTemplateSuccess = (state) => updateObject(state, {
  mode: 12
})

const realSendTemplateFail = (state, action) => updateObject(state, {
  realSendError: action.error,
  mode: 12
})

const realDeleteTeamMemberStart = (state) => updateObject(state, {
  deleteDoctorId: "spin"
})

const realDeleteTeamMemberSuccess = (state) => updateObject(state, {
  deleteMemberSuccess: true, deleteDoctorId: ""
})

const realDeleteTeamMemberFail = (state, action) => updateObject(state, {
  deleteMemberError: action.error, deleteDoctorId: ""
})

const realLeaveTeamStart = (state) => updateObject(state, {
  mode: 20
})

const realLeaveTeamSuccess = (state) => updateObject(state, {
  mode: 21
})

const realLeaveTeamFail = (state, action) => updateObject(state, {
  mode: 22, leaveTeamError: action.error
})

const leaveTeam = (state) => updateObject(state, {
  mode: 19
})

const deleteTeam = (state) => updateObject(state, {
  mode: 23
})

const realDeleteTeamStart = (state) => updateObject(state, {
  mode: 24
})

const realDeleteTeamSuccess = (state) => updateObject(state, {
  mode: 25
})

const realDeleteTeamFail = (state, action) => updateObject(state, {
  mode: 26, deleteTeamError: action.error
})

const deleteTemplate = (state, action) => updateObject(state, {
  deleteTemplateId: action.templateId,
  deleteTemplateName: action.name,
  deleteMy: action.myType,
  mode: 2
})

const createQRCodeStart = (state, action) => updateObject(state, {
  createQRTemplateId: action.templateId,
  createQRTemplateName: action.templateName,
  mode: 28
})

const createQRCodeSuccess = (state, action) => {
  if (state.mode === 28) {
    return updateObject(state, {
      base64Str: action.base64Str,
      mode: 29
    })
  } else {
    return state
  }
}

const createQRCodeFail = (state, action) => updateObject(state, {
  mode: 30, createQRError: action.error
})



const closeModal = state => {
  if (state.addCategoryMode === 4) return updateObject(state, {mode: 5, doctors: [], allDoctors: [],
    sendingDoctorIds: [], labels: [], doctorSearchValue: "", teamMembersError: "", deleteDoctorId: "",
    deleteMemberSuccess: false, deleteMemberError: "", leaveTeamError: "", deleteTeamError: "", addCategoryMode: 0,
    addCircleTemplateId: "", addCircleTemplateName: "", addCircleTemplateError: "", chosenCircleName: "",
    chosenCircleId: ""
  })
  else return updateObject(state, {mode: 0, doctors: [], allDoctors: [],
    sendingDoctorIds: [], labels: [], doctorSearchValue: "", teamMembersError: "", deleteDoctorId: "",
    deleteMemberSuccess: false, deleteMemberError: "", leaveTeamError: "", deleteTeamError: "", addCategoryMode: 0,
    addCircleTemplateId: "", addCircleTemplateName: "", addCircleTemplateError: "", chosenCircleName: "",
    chosenCircleId: ""})
}

const realTemplateDeleteStart = state => updateObject(state, {mode: 3})

const realTemplateDeleteFail = (state, action) => updateObject(state, {
  deleteError: action.error,
  mode: 6
})

const realTemplateDeleteSuccess = (state, action) => {
  if (action.myType === "my") return updateObject(state, {mode: 4})
  else if (action.myType === "expert") return updateObject(state, {mode: 7})
  else if (action.myType === "team") return updateObject(state, {mode: 7})
}

const deleteTemplateFinished = state => updateObject(state, {mode: 5, doctors: [], allDoctors: [],
  sendingDoctorIds: [], labels: [], doctorSearchValue: ""})

const editTemplateStart = (state, action) => {
  const newTemplates = [...state.templates]
  newTemplates[action.index]["loading2"] = true
  return updateObject(state, {templates: newTemplates})
}

const createPlanByTemplateStart = (state, action) => {
  const newTemplates = [...state.templates]
  newTemplates[action.index]["loading1"] = true
  return updateObject(state, {templates: newTemplates})
}

const editTemplateFail = (state, action) => updateObject(state,{
  error: action.msg,
  mode: 0
})

const createPlanByTemplateFail = (state, action) => updateObject(state,{
  error: action.msg,
  mode: 0
})

const searchTemplateChange = (state, action) => {
  if (action.value.length === 0) {
    return updateObject(state,{
      templateSearchValue: action.value, templates: [...state.allTemplates], templateTitle: ""
    })
  } else {
    const newTemplates = state.allTemplates.filter(template => template.templateName.includes(action.value))
    return updateObject(state, {
      templateSearchValue: action.value, templates: newTemplates, templateTitle: "全部模板中搜索"+action.value
    })
  }
}

const searchExpertTemplateChange = (state, action) => {
  return updateObject(state,{
    expertTemplateSearchValue: action.value
  })
}

const searchTeamTemplateChange = (state, action) => {
  if (action.value.length === 0) {
    return updateObject(state,{
      teamTemplateSearchValue: action.value, teamTemplates: [...state.allTeamTemplates]
    })
  } else {
    const newTemplates = state.allTeamTemplates.filter(template => template.templateName.includes(action.value))
    return updateObject(state, {
      teamTemplateSearchValue: action.value, teamTemplates: newTemplates
    })
  }
}

const switchTemplateFocusToMy = (state, action) => updateObject(state, {
  focusOnMy: action.value
})

const searchDoctors = (state, action) => {
  const newDoctors = state.allDoctors.filter(doctor =>
    doctor.name.includes(action.value) || doctor.username.includes(action.value))
  return updateObject(state, {
    doctorSearchValue: action.value,
    doctors: newDoctors
  })
}


const deleteTeamMember = (state, action) => updateObject(state, {
  deleteDoctorId: action.doctorId
})

const realAddCategorySuccess = (state) => updateObject( state, {
  mode: 5
});

const realDeleteCategorySuccess = (state) => updateObject( state, {
  mode: 5
});


const chooseCategory = (state, action) => {
  const templates = state.allTemplates.filter(template => {
    for (let i = 0; i < action.ids.length; i++) {
      if (template.category.indexOf(action.ids[i]) > -1) {
        return true
      }
    }
    return false
  })
  return updateObject(state, {templates: [...templates], templateTitle: "", templateSearchValue: ""})
}

const chooseSecondCategory = (state, action) => {
  const templates = state.allTemplates.filter(template =>
    template.category.indexOf(action.id) > -1)
  templates.forEach(temp => {
    console.log(temp.category.indexOf(action.id))
  })
  console.log(templates)
  return updateObject(state, {templates: [...templates], templateTitle: "", templateSearchValue: ""})
}

const showAllTemplates = (state, action) => updateObject(state, {templates: [...state.allTemplates],
  templateSearchValue: "", templateTitle: ""})

const realAddClientToCategorySuccess = (state) => updateObject( state, {
  addCategoryMode: 4
});

const changeContent = (state, action) => updateObject( state, {
  content: action.content
});

const syncCircleSuccess = (state, action) => {
  return updateObject( state, {
    circles: [...action.circles]
  });
}

const syncCircleFail = (state, action) => updateObject( state, {
});

const getCircleSuccess = (state, action) => {
  return updateObject( state, {
    circles: [...action.circles]
  });
}

const getCircleFail = (state, action) => updateObject( state, {
});

const addTemplateToCircle = (state, action) => {
  return updateObject( state, {
    addCircleTemplateId: action.templateId, mode: 31,
    addCircleTemplateName: action.templateName
  });
}

const changeCircle = (state, action) => {
  return updateObject( state, {
    chosenCircleName: action.name,
    chosenCircleId: action.id
  });
}

const realAddTemplateToCircleStart = (state, action) => updateObject( state, {
  mode: 32
});

const realAddTemplateToCircleSuccess = (state, action) => {
  return updateObject( state, {
    mode: 33
  });
}

const realAddTemplateToCircleFail = (state, action) => {
  return updateObject( state, {
    mode: 34, addCircleTemplateError: action.error
  });
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_TEMPLATES_START:
      return fetchTemplatesStart(state, action)
    case actionTypes.FETCH_TEMPLATES_SUCCESS:
      return fetchTemplatesSuccess(state, action)
    case actionTypes.FETCH_TEMPLATES_FAIL:
      return fetchTemplatesFail(state, action)
    case actionTypes.FETCH_EXPERT_TEMPLATES_START:
      return fetchExpertTemplatesStart(state, action)
    case actionTypes.FETCH_EXPERT_TEMPLATES_SUCCESS:
      return fetchExpertTemplatesSuccess(state, action)
    case actionTypes.FETCH_EXPERT_TEMPLATES_FAIL:
      return fetchExpertTemplatesFail(state, action)
    case actionTypes.FETCH_TEAM_TEMPLATES_START:
      return fetchTeamTemplatesStart(state, action)
    case actionTypes.FETCH_TEAM_TEMPLATES_SUCCESS:
      return fetchTeamTemplatesSuccess(state, action)
    case actionTypes.FETCH_TEAM_TEMPLATES_FAIL:
      return fetchTeamTemplatesFail(state, action)
    case actionTypes.FETCH_TEAMS_AND_FIRST_START:
      return fetchTeamsAndFirstStart(state, action)
    case actionTypes.FETCH_TEAMS_AND_FIRST_SUCCESS:
      return fetchTeamsAndFirstSuccess(state, action)
    case actionTypes.FETCH_TEAMS_AND_FIRST_FAIL:
      return fetchTeamsAndFirstFail(state, action)
    case actionTypes.DOWNLOAD_TEMPLATE_START:
      return downloadTemplateStart(state, action)
    case actionTypes.DOWNLOAD_TEMPLATE_SUCCESS:
      return downloadTemplateSuccess(state, action)
    case actionTypes.DOWNLOAD_TEMPLATE_FAIL:
      return downloadTemplateFail(state, action)
    case actionTypes.ADD_EXPERT:
      return addExpert(state, action)
    case actionTypes.ADD_EXPERT_OR_TEAM:
      return addExpertOrTeam(state, action)
    case actionTypes.ADD_EXPERT_START:
      return addExpertStart(state, action)
    case actionTypes.ADD_EXPERT_SUCCESS:
      return addExpertSuccess(state)
    case actionTypes.ADD_EXPERT_FAIL:
      return addExpertFail(state, action)
    case actionTypes.SEE_TEAM_MEMBERS_START:
      return seeTeamMembersStart(state, action)
    case actionTypes.SEE_TEAM_MEMBERS_SUCCESS:
      return seeTeamMembersSuccess(state, action)
    case actionTypes.SEE_TEAM_MEMBERS_FAIL:
      return seeTeamMembersFail(state, action)
    case actionTypes.ADD_TEAM_START:
      return addTeamStart(state, action)
    case actionTypes.ADD_TEAM_SUCCESS:
      return addTeamSuccess(state)
    case actionTypes.ADD_TEAM_FAIL:
      return addTeamFail(state, action)
    case actionTypes.REAL_DELETE_TEAM_MEMBER_START:
      return realDeleteTeamMemberStart(state, action)
    case actionTypes.REAL_DELETE_TEAM_MEMBER_SUCCESS:
      return realDeleteTeamMemberSuccess(state)
    case actionTypes.REAL_DELETE_TEAM_MEMBER_FAIL:
      return realDeleteTeamMemberFail(state, action)
    case actionTypes.TEAM_CHANGE_SUCCESS:
      return teamChangeSuccess(state, action)
    case actionTypes.DELETE_TEAM_MEMBER:
      return deleteTeamMember(state, action)
    case actionTypes.SEND_TEMPLATE:
      return sendTemplate(state, action)
    case actionTypes.FETCH_DOCTORS_START:
      return fetchDoctorsStart(state, action)
    case actionTypes.FETCH_DOCTORS_SUCCESS:
      return fetchDoctorsSuccess(state, action)
    case actionTypes.FETCH_DOCTORS_FAIL:
      return fetchDoctorsFail(state, action)
    case actionTypes.CHANGE_DOCTOR:
      return changeDoctor(state, action)
    case actionTypes.CHANGE_LABEL:
      return changeLabel(state, action)
    case actionTypes.REAL_SEND_TEMPLATE_START:
      return realSendTemplateStart(state)
    case actionTypes.REAL_SEND_TEMPLATE_SUCCESS:
      return realSendTemplateSuccess(state)
    case actionTypes.REAL_SEND_TEMPLATE_FAIL:
      return realSendTemplateFail(state, action)
    case actionTypes.DELETE_TEAM:
      return deleteTeam(state)
    case actionTypes.LEAVE_TEAM:
      return leaveTeam(state)
    case actionTypes.REAL_LEAVE_TEAM_START:
      return realLeaveTeamStart(state)
    case actionTypes.REAL_LEAVE_TEAM_SUCCESS:
      return realLeaveTeamSuccess(state)
    case actionTypes.REAL_LEAVE_TEAM_FAIL:
      return realLeaveTeamFail(state, action)
    case actionTypes.REAL_DELETE_TEAM_START:
      return realDeleteTeamStart(state)
    case actionTypes.REAL_DELETE_TEAM_SUCCESS:
      return realDeleteTeamSuccess(state)
    case actionTypes.REAL_DELETE_TEAM_FAIL:
      return realDeleteTeamFail(state, action)
    case actionTypes.DELETE_TEMPLATE:
      return deleteTemplate(state, action)
    case actionTypes.CREATE_QR_CODE_START:
      return createQRCodeStart(state, action)
    case actionTypes.CREATE_QR_CODE_SUCCESS:
      return createQRCodeSuccess(state, action)
    case actionTypes.CREATE_QR_CODE_FAIL:
      return createQRCodeFail(state, action)
    case actionTypes.CLOSE_MODAL:
      return closeModal(state)
    case actionTypes.REAL_TEMPLATE_DELETE_START:
      return realTemplateDeleteStart(state)
    case actionTypes.REAL_TEMPLATE_DELETE_FAIL:
      return realTemplateDeleteFail(state, action)
    case actionTypes.REAL_TEMPLATE_DELETE_SUCCESS:
      return realTemplateDeleteSuccess(state, action)
    case actionTypes.DELETE_TEMPLATE_FINISHED:
      return deleteTemplateFinished(state)
    case actionTypes.EDIT_TEMPLATE_START:
      return editTemplateStart(state, action)
    case actionTypes.EDIT_TEMPLATE_FAIL:
      return editTemplateFail(state, action)
    case actionTypes.CREATE_PLAN_BY_TEMPLATE_START:
      return createPlanByTemplateStart(state, action)
    case actionTypes.CREATE_PLAN_BY_TEMPLATE_FAIL:
      return createPlanByTemplateFail(state, action)
    case actionTypes.SEARCH_TEMPLATE_CHANGE:
      return searchTemplateChange(state, action)
    case actionTypes.SEARCH_EXPERT_TEMPLATE_CHANGE:
      return searchExpertTemplateChange(state, action)
    case actionTypes.SEARCH_TEAM_TEMPLATE_CHANGE:
      return searchTeamTemplateChange(state, action)
    case actionTypes.SWITCH_TEMPLATE_FOCUS_TO_MY:
      return switchTemplateFocusToMy(state, action)
    case actionTypes.REAL_ADD_CATEGORY_SUCCESS:
      return realAddCategorySuccess(state, action)
    case actionTypes.REAL_DELETE_CATEGORY_SUCCESS:
      return realDeleteCategorySuccess(state, action)
    case actionTypes.CHOOSE_CATEGORY:
      return chooseCategory(state, action)
    case actionTypes.CHOOSE_SECOND_CATEGORY:
      return chooseSecondCategory(state, action)
    case actionTypes.SEARCH_DOCTORS:
      return searchDoctors(state, action)
    case actionTypes.SHOW_ALL_TEMPLATES:
      return showAllTemplates(state, action)
    case actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_SUCCESS:
    return realAddClientToCategorySuccess(state, action)
    case actionTypes.CHANGE_CONTENT:
      return changeContent(state, action)
    case actionTypes.SYNC_CIRCLE_SUCCESS:
      return syncCircleSuccess(state, action)
    case actionTypes.SYNC_CIRCLE_FAIL:
      return syncCircleFail(state, action)
    case actionTypes.GET_CIRCLE_SUCCESS:
      return getCircleSuccess(state, action)
    case actionTypes.GET_CIRCLE_FAIL:
      return getCircleFail(state, action)
    case actionTypes.ADD_TEMPLATE_TO_CIRCLE:
      return addTemplateToCircle(state, action)
    case actionTypes.CHANGE_CIRCLE:
      return changeCircle(state, action)
    case actionTypes.REAL_ADD_TEMPLATE_TO_CIRCLE_START:
      return realAddTemplateToCircleStart(state, action)
    case actionTypes.REAL_ADD_TEMPLATE_TO_CIRCLE_SUCCESS:
      return realAddTemplateToCircleSuccess(state, action)
    case actionTypes.REAL_ADD_TEMPLATE_TO_CIRCLE_FAIL:
      return realAddTemplateToCircleFail(state, action)
    default: return state
  }
}

export default reducer