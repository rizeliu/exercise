import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  root: true,
  inside: "",
  unchangedCurrent: [],
  overallInfo: {
    a: {
      a01: [],
      a02: [],
      a03: [],
      a04: []
    },
    b: {
      b01: [],
      b02: [],
    },
    c: {
      c01: [],
      c02: [],
      c03: [],
    },
    d: {
      d01: []
    }
  },
  current: [],
  initial: ['a', 'b', 'c', 'd'],
  open: "",
  cList: [],
  mode: -1,
  newMode: 1,
  templateList: [],
  title: "",
  error: "",
  map: {
    a: "肌肉骨骼康复",
    a01: "脊柱外科",
    a02: "关节外科",
    a03: "创伤外科",
    a04: "运动医学",
    b: "神经康复",
    b01: "中枢神经",
    b02: "周围神经",
    c: "产前产后康复",
    c01: "备孕期",
    c02: "孕期",
    c03: "产后",
    d: "其他",
    d01: "其他"
  }
}

const tempCateClick = (state, action) => {
  const newCurrent = state.overallInfo[action.name]
  let updated = Object.keys(newCurrent).sort()
  return updateObject(state, {
    inside: state.current[action.index],
    current: updated,
    unchangedCurrent: [...updated],
    root: false
  })
}

const tempBackClick = (state, action) => {
  return updateObject(state, {
    inside: "",
    current: [...state.initial],
    root: true,
    open: "",
    cList: []
  })

}

const tempDropDown = (state, action) => {
  if (state.cList.length > 0 && state.open === action.name) {
    return updateObject(state, {cList: [], open: ""})
  } else {
    let newCList = [...state.overallInfo[action.inside][action.name]]
    return updateObject(state, {cList: newCList, open: action.name})
  }
}

const searchTempsStart = state => updateObject(state, {mode: 1})

const searchTempsFail = (state, action) => updateObject(state, {mode: 2, error: action.error})

const searchTempsSuccess = (state, action) => updateObject(state,
  {inSearch: true, mode: 0, templateList: action.templateList, title: action.title})

const downloadTemplateStart = (state, action) => {
  if (action.expertOrTeam === "expert") {
    const newTemplates = [...state.templateList]
    const index = newTemplates.map(template => template.templateId).indexOf(action.templateId)
    newTemplates[index]["loading4"] = true
    return updateObject(state, {templateList: newTemplates})
  }
  return state
}

const downloadTemplateSuccess = (state, action) => {
  if (action.expertOrTeam === "expert") {
    const newTemplates = [...state.templateList]
    newTemplates.forEach(template => {
      if (template.templateId === action.templateId) {
        template["loading4"] = false
        template["done"] = true
      }
    })
    return updateObject( state, {
      templateList: newTemplates
    });
  } else return state
}



const tempCategoriesInit = () => {
  return {...initialState, current: [...initialState.initial], newMode: 1}
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEARCH_TEMPS_START:
      return searchTempsStart(state)
    case actionTypes.SEARCH_TEMPS_FAIL:
      return searchTempsFail(state, action)
    case actionTypes.SEARCH_TEMPS_SUCCESS:
      return searchTempsSuccess(state, action)
    case actionTypes.TEMP_CATE_CLICK:
      return tempCateClick(state, action)
    case actionTypes.TEMP_BACK_CLICK:
      return tempBackClick(state, action)
    case actionTypes.TEMP_DROP_DOWN:
      return tempDropDown(state, action)
    case actionTypes.DOWNLOAD_TEMPLATE_START:
      return downloadTemplateStart(state, action)
    case actionTypes.DOWNLOAD_TEMPLATE_SUCCESS:
      return downloadTemplateSuccess(state, action)
    case actionTypes.DELETE_TEMPLATE_FINISHED:
      return tempCategoriesInit()
    case actionTypes.TEMP_CATEGORIES_INIT:
      return tempCategoriesInit()
    default: return state
  }
}

export default reducer