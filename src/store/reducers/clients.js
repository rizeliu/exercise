import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  allClients: [],
  clients: [],
  deleteClientId: "",
  title: "",
  deleteClientName: "",
  mode: 1,
  requestIndex: -1,
  error: "",
  deleteError: "",
  searchValue: "",
  clientCategories: [],
  order: "",
  clientCategoryMode: 0,
  addCategoryMode: 0,
  addCategoryModeError: "",
  newCategory: "",
  deleteCategoryId: "",
  clickCategoryIds: [],
  clickParentIds: [],
  checkedDiseaseCategories: [],
  chosenChildDoctorId: "",
  chosen: "",
  titleId: "",
  diseaseCodeValue: "",
  diseaseCategories: [],
  collapse1: false,
  collapse2: false,
  collapse3: false,
  collapse4: false,
  firstCollapse: false,
  categoryCount: 0,
  currentPage: 1,
  childCategoryMode: 0,
  clientCategoryError: "",
  seconds: [],
  detailCategoryMode: 0,
  addCategoryClientId: "",
  addCategoryClientName: "",
  specialCatId: "",
  specialMode: 0,
  seeQRCode: 0,
  base64Str: "",
  seeQRCodeCatId: "",
  seeQRCodeError: ""
}

const fetchClientsStart = state => updateObject(state, {mode: 1, requestIndex: -1, searchValue: "", order: "", clientCategoryMode: 0})

const fetchClientsSuccess = (state, action) => {
  const seconds = {}
  action.categories.filter(cat => cat.type === "second").forEach(cat => {
    const root = cat.parentId
    const name = cat.childName
    if (seconds[cat.parentId] === undefined) seconds[root] = [{...cat, name}]
    else seconds[root].push({...cat, name})
  })
  return updateObject( state, {
    clients: action.clients,
    allClients: [...action.clients],
    searchAll: "",
    mode: 0,
    title: "",
    fetchClientsSuccess: [],
    order: "",
    clientCategoryMode: 0,
    newCategory: "",
    clientCategories: [...action.categories],
    chosen: "",
    titleId: "",
    diseaseCategories: [],
    diseaseCodeValue: "",
    addCategoryMode: 0,
    childCategoryMode: 0,
    seconds: seconds,
    collapse1: false,
    collapse2: false,
    collapse3: false,
    collapse4: false,
  });
}

const fetchTemplatesStart = state => updateObject(state, {
  mode: 0
})

const fetchTemplateSuccess = (state, action) => {
  const seconds = {}
  action.categories.filter(cat => cat.type === "second").forEach(cat => {
    const root = cat.parentId
    const name = cat.childName
    if (seconds[cat.parentId] === undefined) seconds[root] = [{...cat, name}]
    else seconds[root].push({...cat, name})
  })
  return updateObject( state, {
    mode: 0,
    title: "",
    fetchClientsSuccess: [],
    order: "",
    clientCategoryMode: 0,
    newCategory: "",
    clientCategories: [...action.categories],
    chosen: "",
    titleId: "",
    diseaseCategories: [],
    diseaseCodeValue: "",
    addCategoryMode: 0,
    childCategoryMode: 0,
    seconds: seconds,
    collapse1: false,
    collapse2: false,
    collapse3: false,
    collapse4: false,
  });
}

const fetchClientsFail = (state, action) => updateObject(state, {
  error: action.error,
  mode: 0,
  searchAll: "",
  order: "",
  clientCategoryMode: 0,
  newCategory: ""
})

const fetchCategoryStart = state => updateObject(state, {
  addCategoryMode: 2
})

const fetchClientDetailStart = state => {
  return updateObject(state, {
    mode: 1
  })
}

const fetchCategorySuccess = (state, action) => {
  const clientCategories = [...action.categories].sort((a, b) => {
    return a.name < b.name ? -1 : 1
  })
  const catIds = clientCategories.map(cat => cat._id)
  const clickParentIds = []
  action.category.forEach(cat => {
    const index = catIds.indexOf(cat)
    if (index > -1) {
      if (clientCategories[index].type === "second") {
        clickParentIds.push(clientCategories[index].parentId)
      } else if (state.seconds[cat]) {
        clickParentIds.push(cat)
      }
    }
  })
  const seconds = {}
  clientCategories.filter(cat => cat.type === "second").forEach(cat => {
    const root = cat.parentId
    const name = cat.childName
    if (seconds[cat.parentId] === undefined) seconds[root] = [{...cat, name}]
    else seconds[root].push({...cat, name})
  })
  return updateObject( state, {
    addCategoryMode: 1,
    clientCategories: [...clientCategories],
    addCategoryClientId: action.clientId,
    addCategoryClientName: action.name,
    clickCategoryIds: action.category,
    clickParentIds: clickParentIds,
    seconds: seconds
  });
}

const fetchCategoryFail = (state, action) => updateObject(state, {
  addCategoryMode: 3,
  addCategoryModeError: action.error
})

const deleteClient = (state, action) => updateObject(state, {
  deleteClientId: action.clientId,
  deleteClientName: action.name,
  mode: 2
})

const closeModal = state => {
  if (state.addCategoryMode === 4) return updateObject(state, {mode: 5, diseaseCodeValue: "",
    requestIndex: -1, addCategoryMode: 0, clickCategoryIds: [], clickParentIds: [], clientCategoryMode: 0,
    checkedDiseaseCategories: []})
  else return updateObject(state, {mode: 0, diseaseCodeValue: "",
    requestIndex: -1, addCategoryMode: 0, clickCategoryIds: [], clickParentIds: [], clientCategoryMode: 0,
    checkedDiseaseCategories: [], })
}

const realClientDeleteStart = state => updateObject(state, {mode: 3})

const realClientDeleteFail = (state, action) => updateObject(state, {
  deleteError: action.error,
  mode: 6
})

const realClientDeleteSuccess = state => updateObject(state, {mode: 4})

const deleteClientFinished = state => updateObject(state, {mode: 5})

const clientSearchChange = (state, action) => {
  if (action.value === "") {
    return updateObject(state, {clients: [...state.allClients], title: "",
      searchValue: action.value, order: "", chosen: "", titleId: ""})
  } else {
    const clients = state.allClients.filter(client =>
      client.clientName.includes(action.value) || (client.refer && client.refer.includes(action.value)))
    return updateObject(state, {clients: [...clients], order: "",
      title: "全部患者中搜索"+action.value, searchValue: action.value, chosen: "", titleId: ""})
  }
}

const diseaseCodeSearchChange = (state, action) => {
  return updateObject(state, {diseaseCodeValue: action.value})
}

const showAllClients = state => updateObject(state, {clients: [...state.allClients],
  title: "", searchValue: "", order: "", chosen: "", titleId: ""})

const seeRequest = (state, action) => updateObject(state, {
  requestIndex: action.index,
  clients: [...state.clients.slice(0, action.index),
    {...state.clients[action.index], read: true}, ...state.clients.slice(action.index+1)]
})

const sortClients = (state, action) => {
  const newClients = [...state.clients]
  if (action.category === 'refer') {
    newClients.sort((a, b) => a.refer > b.refer ? -1
      : a.refer === b.refer ? 0 : 1)
  } else if (action.category === 'total') {
    newClients.sort((a, b) => a.allPlans.length > b.allPlans.length ? -1
      : a.allPlans.length === b.allPlans.length ? 0 : 1)
  } else if (action.category === 'paid') {
    newClients.sort((a, b) => a.allPlans.filter(plan=>plan).length > b.allPlans.filter(plan=>plan).length ? -1
      : a.allPlans.filter(plan=>plan).length === b.allPlans.filter(plan=>plan).length ? 0 : 1)
  } else if (action.category === 'unpaid') {
    newClients.sort((a, b) => a.allPlans.filter(plan=>!plan).length > b.allPlans.filter(plan=>!plan).length ? -1
      : a.allPlans.filter(plan=>!plan).length === b.allPlans.filter(plan=>!plan).length ? 0 : 1)
  }
  return updateObject(state, {
    order: action.category,
    clients: newClients
  })
}

const addCategory = (state) => updateObject(state, {
  clientCategoryMode: 1, newCategory: "", childCategoryMode: 0
})

const addChildCategory = (state) => updateObject(state, {
  childCategoryMode: 1, newCategory: ""
})

const deleteChildCategory = (state) => updateObject(state, {
  childCategoryMode: 6, newCategory: ""
})

const addEmptyCategory = (state) => updateObject(state, {
  clientCategoryMode: 2, newCategory: ""
})

const addEmptyChildCategory = (state) => updateObject(state, {
  childCategoryMode: 2, newCategory: ""
})

const addDollarCategory = (state) => updateObject(state, {
  clientCategoryMode: 1.5
})

const addDollarChildCategory = (state) => updateObject(state, {
  childCategoryMode: 3
})

const cancelAddCategory = (state) => updateObject(state, {
  clientCategoryMode: 0, newCategory: ""
})

const cancelAddChildCategory = (state) => updateObject(state, {
  clientCategoryMode: 0, childCategoryMode: 0, newCategory: ""
})

const cancelDeleteCategory = (state) => updateObject(state, {
  clientCategoryMode: 0, deleteCategoryId: ""
})

const addCategoryChange = (state, action) => {
  if (action.mode === 1) {
    return updateObject(state, {
      newCategory: action.value, clientCategoryMode: 1
    })
  } else if (action.mode === 2) {
    return updateObject(state, {
      newCategory: action.value, childCategoryMode: 1
    })
  } else return state

}

const realAddCategoryStart = state => updateObject(state, {clientCategoryMode: 3})

const realAddChildCategoryStart = state => updateObject(state, {childCategoryMode: 4})

const realAddCategorySuccess = (state) => updateObject( state, {
  mode: 5
});

const realAddCategoryFail = (state, action) => updateObject(state, {
  clientCategoryMode: 4,
  clientCategoryError: action.error
})

const specializeStart = (state, action) => updateObject(state, {specialMode: 1, specialCatId: action.catId})

const specializeSuccess = (state) => updateObject( state, {
  specialMode: 0, specialCatId: "", mode: 5
});

const specializeFail = (state) => updateObject(state, {
  specialMode: 0, specialCatId: ""
})

const seeQRCodeFail = (state, action) => updateObject(state, {
  seeQRCode: 2, seeQRCodeError: action.error
})

const seeQRCodeSuccess = (state, action) => updateObject(state, {
  seeQRCode: 1, base64Str: action.base64Str, seeQRCodeCatId: action.catId
})

const realAddChildCategoryFail = (state, action) => updateObject(state, {
  childCategoryMode: 5,
  clientCategoryError: action.error
})

const realAddRestCategoryFail = (state, action) => updateObject(state, {
  addCategoryMode: 9,
  clientCategoryError: action.error
})

const realDeleteCategoryStart = state => updateObject(state, {clientCategoryMode: 7.5})

const realDeleteDiseaseCategoryStart = state => updateObject(state, {clientCategoryMode: 11})

const realDeleteOperationCategoryStart = state => updateObject(state, {clientCategoryMode: 16})

const realDeleteSecondCategoryStart = state => updateObject(state, {childCategoryMode: 8})

const realDeleteCategorySuccess = (state) => updateObject( state, {
  mode: 5
});

const realDeleteCategoryFail = (state, action) => updateObject(state, {
  clientCategoryMode: 8,
  clientCategoryError: action.error
})

const realDeleteDiseaseCategoryFail = (state, action) => updateObject(state, {
  clientCategoryMode: 12,
  clientCategoryError: action.error
})

const realDeleteSecondCategoryFail = (state, action) => updateObject(state, {
  childCategoryMode: 9,
  clientCategoryError: action.error
})

const realDeleteOperationCategoryFail = (state, action) => updateObject(state, {
  clientCategoryMode: 17,
  clientCategoryError: action.error
})

const chooseCategory = (state, action) => {
  const clients = state.allClients.filter(client => {
    for (let i = 0; i < action.ids.length; i++) {
      if (client.category.indexOf(action.ids[i]) > -1) {
        return true
      }
    }
    return false
  })
  const firstCollapse = state.chosen === action.id ? !state.firstCollapse : true
  return updateObject(state, {clients: [...clients], title: "", searchValue: "", order: "",
    chosen: action.id, childCategoryMode: 0, clientCategoryMode: 0, titleId: action.id, firstCollapse: firstCollapse,
    seeQRCode: 0, base64Str: "",})

}

const chooseChildDoctorCategory = (state, action) => {
  const clients = state.allClients.filter(client => {
    return client.childDoctorId === action.id;
  })
  return updateObject(state, {clients: [...clients], title: "", searchValue: "", order: "",
    chosen: action.id, childCategoryMode: 0, clientCategoryMode: 0, titleId: action.id})

}

const chooseSecondCategory = (state, action) => {
  const clients = state.allClients.filter(client =>
    client.category.indexOf(action.id) > -1)
  return updateObject(state, {clients: [...clients], title: "", searchValue: "", order: "",
    childCategoryMode: 0, clientCategoryMode: 0, titleId: action.id})
}


const addClientToCategory = (state, action) => {
  const catIds = state.clientCategories.map(cat => cat._id)
  const clickParentIds = []
  action.category.forEach(cat => {
    const index = catIds.indexOf(cat)
    if (index > -1) {
      if (state.clientCategories[index].type === "second") {
        clickParentIds.push(state.clientCategories[index].parentId)
      } else if (state.seconds[cat]) {
        clickParentIds.push(cat)
      }
    }
  })
  return updateObject(state, {
    addCategoryMode: 1,
    addCategoryClientId: action.id,
    addCategoryClientName: action.name,
    clickCategoryIds: action.category,
    clickParentIds: clickParentIds,
    chosenChildDoctorId: action.childDoctorId
  })
}

const addTemplateToCategory = (state, action) => {
  const catIds = state.clientCategories.map(cat => cat._id)
  const clickParentIds = []
  action.category.forEach(cat => {
    const index = catIds.indexOf(cat)
    if (index > -1) {
      if (state.clientCategories[index].type === "second") {
        clickParentIds.push(state.clientCategories[index].parentId)
      } else if (state.seconds[cat]) {
        clickParentIds.push(cat)
      }
    }
  })
  return updateObject(state, {
    addCategoryMode: 1,
    addCategoryClientId: action.id,
    addCategoryClientName: action.name,
    clickCategoryIds: action.category,
    clickParentIds: clickParentIds
  })
}

const clickCategory = (state, action) => {
  const index = state.clickCategoryIds.indexOf(action.value)
  if (index > -1) {
    const ids = state.clientCategories.map(cat => cat._id)
    const index1 = ids.indexOf(action.value)
    if (state.clientCategories[index1].type === "second") {
      return updateObject(state, {
        clickCategoryIds: [...state.clickCategoryIds.slice(0, index), ...state.clickCategoryIds.slice(index+1)],
        clickParentIds: state.clickParentIds.filter(id => id !== state.clientCategories[index1].parentId),
        addCategoryMode: 1})
    } else if (state.seconds[action.value]) {
      return updateObject(state, {
        clickCategoryIds: [...state.clickCategoryIds.slice(0, index), ...state.clickCategoryIds.slice(index+1)],
        clickParentIds: state.clickParentIds.filter(id => id !== action.value),
        addCategoryMode: 1})
    } else return updateObject(state, {
        clickCategoryIds: [...state.clickCategoryIds.slice(0, index), ...state.clickCategoryIds.slice(index+1)],
        addCategoryMode: 1})
  } else {
    const ids = state.clientCategories.map(cat => cat._id)
    const index1 = ids.indexOf(action.value)
    if (state.clientCategories[index1].type === "second") {
      return updateObject(state, {
        clickCategoryIds: [...state.clickCategoryIds, action.value],
        addCategoryMode: 1,
        clickParentIds: [...state.clickParentIds, state.clientCategories[index1].parentId]
      })
    }
    else if (state.seconds[action.value]) {
      return updateObject(state, {
        clickCategoryIds: [...state.clickCategoryIds, action.value],
        addCategoryMode: 1,
        clickParentIds: [...state.clickParentIds, action.value]
      })
    } else return updateObject(state, {
      clickCategoryIds: [...state.clickCategoryIds, action.value],
      addCategoryMode: 1,
    })
  }
}

const clickChildDoctor = (state, action) => {
  return updateObject(state, {chosenChildDoctorId: action.value})
}

const checkDiseaseCategory = (state, action) => {
  const index = state.checkedDiseaseCategories.map(item => item.id).indexOf(action.value)
  if (index >= 0) {
    const newCheckedDiseaseCategories = [...state.checkedDiseaseCategories.slice(0, index), ...state.checkedDiseaseCategories.slice(index+1)]
    return updateObject(state, {checkedDiseaseCategories: newCheckedDiseaseCategories})
  } else {
    const newCheckedDiseaseCategories = [...state.checkedDiseaseCategories, {id: action.value, name: action.label}]
    return updateObject(state, {checkedDiseaseCategories: newCheckedDiseaseCategories})
  }

}

const realAddClientToCategoryStart = state => updateObject(state, {addCategoryMode: 2})

const realAddClientToCategorySuccess = (state) => updateObject( state, {
  addCategoryMode: 4
});

const realAddClientToCategoryFail = (state, action) => updateObject(state, {
  addCategoryMode: 3,
  addCategoryModeError: action.error
})

const searchDiseaseCategoryStart = state => updateObject(state, {addCategoryMode: 6})

const searchDiseaseCategorySuccess = (state, action) => updateObject( state, {
  addCategoryMode: 8,
  diseaseCategories: [...action.doc],
  categoryCount: action.count,
  currentDiseaseCodeValue: action.value,
  currentPage: action.page
});

const searchDiseaseCategoryFail = (state, action) => updateObject(state, {
  addCategoryMode: 7,
  addCategoryModeError: action.error
})

const addClientToEmptyCategory = (state) => updateObject(state, {
  addCategoryMode: 5
})

const deleteCategory = (state) => updateObject(state, {
  clientCategoryMode: 5, deleteCategoryId: "", childCategoryMode: 0
})

const deleteDiseaseCategory = (state) => updateObject(state, {
  clientCategoryMode: 9, deleteCategoryId: "", childCategoryMode: 0
})

const deleteSecondCategory = (state) => updateObject(state, {
  deleteCategoryId: "", childCategoryMode: 6
})

const deleteOneCategory = (state, action) => updateObject(state, {
  clientCategoryMode: 6, deleteCategoryId: action.id
})

const deleteOneSecondCategory = (state, action) => updateObject(state, {
  childCategoryMode: 7, deleteCategoryId: action.id
})

const deleteOneDiseaseCategory = (state, action) => updateObject(state, {
  clientCategoryMode: 10, deleteCategoryId: action.id
})

const deleteOneOperationCategory = (state, action) => updateObject(state, {
  clientCategoryMode: 15, deleteCategoryId: action.id
})

const addDiseaseCategory = (state) => updateObject(state, {
  clientCategoryMode: 7, childCategoryMode: 0,
})

const deleteOperationCategory = (state) => updateObject(state, {
  clientCategoryMode: 14, childCategoryMode: 0
})

const addOperationCategory = (state) => updateObject(state, {
  clientCategoryMode: 13, childCategoryMode: 0
})

const collapse = (state, action) => {
  switch (action.mode) {
    case 1: {
      return updateObject(state, {collapse1: !state.collapse1})
    }
    case 2: {
      return updateObject(state, {collapse2: !state.collapse2})
    }
    case 3: {
      return updateObject(state, {collapse3: !state.collapse3})
    }
    case 4: {
      return updateObject(state, {collapse4: !state.collapse4})
    }
    default: {
      return state
    }
  }
}

const showAllTemplates = (state, action) => updateObject(state, {chosen: "", titleId: ""})

const searchTemplateChange = (state, action) => updateObject(state, {chosen: "", titleId: ""})



const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CLIENTS_START:
      return fetchClientsStart(state, action)
    case actionTypes.FETCH_CLIENTS_SUCCESS:
      return fetchClientsSuccess(state, action)
    case actionTypes.FETCH_CLIENTS_FAIL:
      return fetchClientsFail(state, action)
    case actionTypes.FETCH_CATEGORY_START:
      return fetchCategoryStart(state, action)
    case actionTypes.FETCH_CATEGORY_SUCCESS:
      return fetchCategorySuccess(state, action)
    case actionTypes.FETCH_TEMPLATES_SUCCESS:
      return fetchTemplateSuccess(state, action)
    case actionTypes.FETCH_CATEGORY_FAIL:
      return fetchCategoryFail(state, action)
    case actionTypes.DELETE_CLIENT:
      return deleteClient(state, action)
    case actionTypes.CLOSE_MODAL:
      return closeModal(state)
    case actionTypes.REAL_CLIENT_DELETE_START:
      return realClientDeleteStart(state)
    case actionTypes.REAL_CLIENT_DELETE_FAIL:
      return realClientDeleteFail(state, action)
    case actionTypes.REAL_CLIENT_DELETE_SUCCESS:
      return realClientDeleteSuccess(state)
    case actionTypes.DELETE_CLIENT_FINISHED:
      return deleteClientFinished(state)
    case actionTypes.CLIENT_SEARCH_CHANGE:
      return clientSearchChange(state, action)
    case actionTypes.DISEASE_CODE_SEARCH_CHANGE:
      return diseaseCodeSearchChange(state, action)
    case actionTypes.SHOW_ALL_CLIENTS:
      return showAllClients(state)
    case actionTypes.SEE_REQUEST:
      return seeRequest(state, action)
    case actionTypes.SORT_CLIENTS:
      return sortClients(state, action)
    case actionTypes.ADD_CATEGORY:
      return addCategory(state)
    case actionTypes.ADD_CHILD_CATEGORY:
      return addChildCategory(state)
    case actionTypes.DELETE_CHILD_CATEGORY:
      return deleteChildCategory(state)
    case actionTypes.ADD_EMPTY_CATEGORY:
      return addEmptyCategory(state)
    case actionTypes.ADD_DOLLAR_CATEGORY:
      return addDollarCategory(state)
    case actionTypes.ADD_EMPTY_CHILD_CATEGORY:
      return addEmptyChildCategory(state)
    case actionTypes.ADD_DOLLAR_CHILD_CATEGORY:
      return addDollarChildCategory(state)
    case actionTypes.CANCEL_ADD_CATEGORY:
      return cancelAddCategory(state)
    case actionTypes.CANCEL_ADD_CHILD_CATEGORY:
      return cancelAddChildCategory(state)
    case actionTypes.CANCEL_DELETE_CATEGORY:
      return cancelDeleteCategory(state)
    case actionTypes.ADD_CATEGORY_CHANGE:
      return addCategoryChange(state, action)
    case actionTypes.REAL_ADD_CATEGORY_START:
      return realAddCategoryStart(state, action)
    case actionTypes.REAL_ADD_CHILD_CATEGORY_START:
      return realAddChildCategoryStart(state, action)
    case actionTypes.REAL_ADD_CATEGORY_FAIL:
      return realAddCategoryFail(state, action)
    case actionTypes.SPECIALIZE_START:
      return specializeStart(state, action)
    case actionTypes.SPECIALIZE_SUCCESS:
      return specializeSuccess(state, action)
    case actionTypes.SPECIALIZE_FAIL:
      return specializeFail(state, action)
    case actionTypes.SEE_QR_CODE_FAIL:
      return seeQRCodeFail(state, action)
    case actionTypes.SEE_QR_CODE_SUCCESS:
      return seeQRCodeSuccess(state, action)
    case actionTypes.FETCH_TEMPLATES_START:
      return fetchTemplatesStart(state, action)
    case actionTypes.REAL_ADD_CHILD_CATEGORY_FAIL:
      return realAddChildCategoryFail(state, action)
    case actionTypes.REAL_ADD_REST_CATEGORY_FAIL:
      return realAddRestCategoryFail(state, action)
    case actionTypes.REAL_ADD_CATEGORY_SUCCESS:
      return realAddCategorySuccess(state, action)
    case actionTypes.REAL_DELETE_CATEGORY_START:
      return realDeleteCategoryStart(state, action)
    case actionTypes.REAL_DELETE_CATEGORY_FAIL:
      return realDeleteCategoryFail(state, action)
    case actionTypes.REAL_DELETE_DISEASE_CATEGORY_START:
      return realDeleteDiseaseCategoryStart(state, action)
    case actionTypes.REAL_DELETE_DISEASE_CATEGORY_FAIL:
      return realDeleteDiseaseCategoryFail(state, action)
    case actionTypes.REAL_DELETE_SECOND_CATEGORY_FAIL:
      return realDeleteSecondCategoryFail(state, action)
    case actionTypes.REAL_DELETE_OPERATION_CATEGORY_START:
      return realDeleteOperationCategoryStart(state, action)
    case actionTypes.REAL_DELETE_SECOND_CATEGORY_START:
      return realDeleteSecondCategoryStart(state, action)
    case actionTypes.REAL_DELETE_OPERATION_CATEGORY_FAIL:
      return realDeleteOperationCategoryFail(state, action)
    case actionTypes.REAL_DELETE_CATEGORY_SUCCESS:
      return realDeleteCategorySuccess(state, action)
    case actionTypes.CHOOSE_CATEGORY:
      return chooseCategory(state, action)
    case actionTypes.CHOOSE_CHILD_DOCTOR_CATEGORY:
      return chooseChildDoctorCategory(state, action)
    case actionTypes.CHOOSE_SECOND_CATEGORY:
      return chooseSecondCategory(state, action)
    case actionTypes.ADD_CLIENT_TO_CATEGORY:
      return addClientToCategory(state, action)
    case actionTypes.ADD_TEMPLATE_TO_CATEGORY:
      return addTemplateToCategory(state, action)
    case actionTypes.CLICK_CATEGORY:
      return clickCategory(state, action)
    case actionTypes.CLICK_CHILD_DOCTOR:
      return clickChildDoctor(state, action)
    case actionTypes.CHECK_DISEASE_CATEGORY:
      return checkDiseaseCategory(state, action)
    case actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_START:
      return realAddClientToCategoryStart(state, action)
    case actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_FAIL:
      return realAddClientToCategoryFail(state, action)
    case actionTypes.REAL_ADD_CLIENT_TO_CATEGORY_SUCCESS:
      return realAddClientToCategorySuccess(state, action)
    case actionTypes.SEARCH_DISEASE_CATEGORY_START:
      return searchDiseaseCategoryStart(state, action)
    case actionTypes.SEARCH_DISEASE_CATEGORY_FAIL:
      return searchDiseaseCategoryFail(state, action)
    case actionTypes.SEARCH_DISEASE_CATEGORY_SUCCESS:
      return searchDiseaseCategorySuccess(state, action)
    case actionTypes.ADD_CLIENT_TO_EMPTY_CATEGORY:
      return addClientToEmptyCategory(state)
    case actionTypes.DELETE_CATEGORY:
      return deleteCategory(state)
    case actionTypes.DELETE_DISEASE_CATEGORY:
      return deleteDiseaseCategory(state)
    case actionTypes.DELETE_SECOND_CATEGORY:
      return deleteSecondCategory(state)
    case actionTypes.DELETE_OPERATION_CATEGORY:
      return deleteOperationCategory(state)
    case actionTypes.DELETE_ONE_CATEGORY:
      return deleteOneCategory(state, action)
    case actionTypes.DELETE_ONE_SECOND_CATEGORY:
      return deleteOneSecondCategory(state, action)
    case actionTypes.DELETE_ONE_DISEASE_CATEGORY:
      return deleteOneDiseaseCategory(state, action)
    case actionTypes.DELETE_ONE_OPERATION_CATEGORY:
      return deleteOneOperationCategory(state, action)
    case actionTypes.ADD_DISEASE_CATEGORY:
      return addDiseaseCategory(state, action)
    case actionTypes.ADD_OPERATION_CATEGORY:
      return addOperationCategory(state, action)
    case actionTypes.COLLAPSE:
      return collapse(state, action)
    case actionTypes.FETCH_CLIENT_DETAIL_START:
      return fetchClientDetailStart(state, action)
    case actionTypes.SHOW_ALL_TEMPLATES:
      return showAllTemplates(state, action)
    case actionTypes.SEARCH_TEMPLATE_CHANGE:
      return searchTemplateChange(state, action)
    default: return state
  }
}

export default reducer