import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  id: "",
  name: "",
  username: "",
  account: "",
  token: "",
  bankName: "",
  bankAccount: "",
  alipayAccount: "",
  phone: "",
  email: "",
  room: "",
  hospital: "",
  idnum: "",
  district: "",
  wechat: "",
  certification: "",
  occupation: "",
  date: "",
  login: false,
  mode: 0,
  error: "",
  done: false,
  admin: 0,
  referee: false,
  refer: "",
  expand: true,
  edu: true,
  free30: false,
  qrcode: false,
  circle: false,
  childDoctors: []
}

const authenticate = (state, action) => {
  if (action.info.code === 0) return updateObject(state, {
    id: action.info.doc._id,
    name: action.info.doc.name,
    username: action.info.doc.username,
    account: action.info.doc.account,
    token: action.info.doc.token,
    bankName: action.info.doc.bankName,
    bankAccount: action.info.doc.bankAccount,
    alipayAccount: action.info.doc.alipayAccount,
    phone: action.info.doc.phone,
    email: action.info.doc.email,
    room: action.info.doc.room,
    certification: action.info.doc.certification,
    occupation: action.info.doc.occupation,
    idnum: action.info.doc.idnum,
    wechat: action.info.doc.wechat,
    hospital: action.info.doc.hospital,
    district: action.info.doc.district,
    date: action.info.doc.date,
    free30: !!action.info.doc.free30,
    qrcode: !!action.info.doc.qrcode,
    circle: !!action.info.doc.circle,
    login: action.login,
    mode: action.mode,
    error: action.error,
    admin: action.admin,
    referee: action.refer,
    refer: action.info.doc.refer ? action.info.doc.refer : "",
    edu: action.admin === 4 || action.admin === 5,
    childDoctors: action.info.doc.childDoctors,
    done: true,
  })
  else return updateObject(state, {
    login: action.login,
    mode: action.mode,
    error: action.error,
    admin: action.admin,
    done: true,
  })
}

const changeExpand = (state) => {
  const originalExpand = state.expand
  return updateObject(state, {expand: !originalExpand})
}

const changeEdu = (state) => {
  return updateObject(state, {edu: false})
}

const logout = state => updateObject(state, {done: true, expand: true})

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTHENTICATE:
      return authenticate(state, action)
    case actionTypes.LOGOUT:
      return logout(state)
    case actionTypes.CHANGE_EXPAND:
      return changeExpand(state)
    case actionTypes.CHANGE_EDU:
      return changeEdu(state)
    default:
      return state
  }
}

export default reducer