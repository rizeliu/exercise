import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  clientId: "",
  mode: 0,
  name: "",
  age: "",
  sex: "",
  phone: "",
  history: "",
  smoke: "",
  description: "",
  doctorDesc: "",
  newAge: "",
  newSex: "",
  newPhone: "",
  newRealhistory: "",
  newSmoke: "",
  newDescription: "",
  recordDes:"",
  records: [],
  date: "",
  plans: [],
  error: "",
  deletePlanId: "",
  deletePlanName: "",
  deleteError: "",
  self: true,
  doctorName: "",
  doctorUsername: "",
  edit: false,
  saving: false,
  categories: [],
  childDoctor: {},
  selfTemps: [],
  expertTemps: []
}

const fetchClientDetailStart = (state, action) => updateObject(state, {
  clientId: action.clientId,
  error: "",
  mode: 1,
})

const fetchClientDetailSuccess = (state, action) => {
  const plans = action.doc.plans.reverse().map((plan, index) => {
    return {
      ...plan,
      loading: false
    }
  })
  return updateObject(state, {
    mode: 0,
    edit: false,
    name: action.secondDoc.name,
    age: action.secondDoc.age,
    sex: action.secondDoc.sex,
    history: action.secondDoc.history,
    smoke: action.secondDoc.smoke,
    phone: action.secondDoc.phone,
    description: action.secondDoc.description,
    doctorDesc: action.doc.description,
    date: action.doc.date,
    plans: plans,
    records: action.records,
    recordDes: action.description,
    categories: action.doc.categories,
    childDoctor: action.doc.childDoctor,
    selfTemps: action.selfTemps,
    expertTemps: action.expertTemps,
    self: true
  })
}

const fetchClientDetailSuccessAdmin = (state, action) => {
  const plans = action.doc.plans.reverse().map((plan, index) => {
    return {
      ...plan,
      loading: false
    }
  })
  return updateObject(state, {
    mode: 0,
    edit: false,
    name: action.secondDoc.name,
    age: action.secondDoc.age,
    sex: action.secondDoc.sex,
    history: action.secondDoc.history,
    smoke: action.secondDoc.smoke,
    phone: action.secondDoc.phone,
    description: action.secondDoc.description,
    doctorDesc: action.doc.description,
    date: action.doc.date,
    plans: plans,
    records: action.records,
    recordDes: action.description,
    categories: action.doc.categories,
    childDoctor: action.doc.childDoctor,
    self: false,
    doctorName: action.doctor.name,
    doctorUsername: action.doctor.username
  })
}

const fetchClientDetailFail = (state, action) => updateObject(state, {
  mode: 0,
  error: action.msg
})

const deletePlan = (state, action) => updateObject(state, {
  deletePlanId: action.planId,
  deletePlanName: action.planName,
  mode: 2
})

const closeModal = state => updateObject(state, {mode: 0})

const realPlanDeleteStart = state => updateObject(state, {mode: 3})

const realPlanDeleteFail = (state, action) => updateObject(state, {
  deleteError: action.error,
  mode: 6
})

const realPlanDeleteSuccess = state => updateObject(state, {mode: 4})

const deletePlanFinished = state => updateObject(state, {mode: 5})

const editPlanStart = (state, action) => {
  const newPlans = [...state.plans]
  newPlans[action.index]["loading"] = true
  return updateObject(state, {plans: newPlans})
}

const editPlanFail = (state, action) => updateObject(state,{
  error: action.msg,
  mode: 0
})

const editClientDetail = (state) => updateObject(state,{
  edit: true, newAge: state.age,
  newSex: state.sex,
  newPhone: state.phone,
  newRealhistory: state.history,
  newSmoke: state.smoke,
  newDescription: state.doctorDesc ? state.doctorDesc : "",
})

const cancelEditClientDetail = (state) => updateObject(state,{
  edit: false, newAge: "",
  newSex: "",
  newPhone: "",
  newRealhistory: "",
  newSmoke: "",
  newDescription: "",
})

const changeClientDetail = (state, action) => updateObject(state, {
  [action.id]: action.value
})

const saveClientDetail = (state, action) => updateObject(state, {
  [action.id]: action.value
})

const saveClientDetailSuccess = (state, action) => updateObject(state, {
  edit: false, age: action.newAge,
  sex: action.newSex,
  phone: action.newPhone,
  history: action.newRealhistory,
  smoke: action.newSmoke,
  doctorDesc: action.newDescription,
  saving: false
})

const saveClientDetailFail = (state) => updateObject(state, {
  edit: false, saving: false
})

const saveClientDetailStart = (state) => updateObject(state, {
  saving: true
})

const createPlanByRecommendTemplateStart = (state, action) => {
  if (action.from === "self") {
    const newTemplates = [...state.selfTemps]
    newTemplates[action.index]["loading1"] = true
    return updateObject(state, {selfTemps: newTemplates})
  } else {
    const newTemplates = [...state.expertTemps]
    newTemplates[action.index]["loading1"] = true
    return updateObject(state, {expertTemps: newTemplates})
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CLIENT_DETAIL_START:
      return fetchClientDetailStart(state, action)
    case actionTypes.FETCH_CLIENT_DETAIL_SUCCESS:
      return fetchClientDetailSuccess(state, action)
    case actionTypes.FETCH_CLIENT_DETAIL_SUCCESS_ADMIN:
      return fetchClientDetailSuccessAdmin(state, action)
    case actionTypes.FETCH_CLIENT_DETAIL_FAIL:
      return fetchClientDetailFail(state, action)
    case actionTypes.DELETE_PLAN:
      return deletePlan(state, action)
    case actionTypes.CLOSE_MODAL:
      return closeModal(state)
    case actionTypes.REAL_PLAN_DELETE_START:
      return realPlanDeleteStart(state)
    case actionTypes.REAL_PLAN_DELETE_FAIL:
      return realPlanDeleteFail(state, action)
    case actionTypes.REAL_PLAN_DELETE_SUCCESS:
      return realPlanDeleteSuccess(state)
    case actionTypes.DELETE_PLAN_FINISHED:
      return deletePlanFinished(state)
    case actionTypes.EDIT_PLAN_START:
      return editPlanStart(state, action)
    case actionTypes.EDIT_PLAN_FAIL:
      return editPlanFail(state, action)
    case actionTypes.EDIT_CLIENT_DETAIL:
      return editClientDetail(state, action)
    case actionTypes.CANCEL_EDIT_CLIENT_DETAIL:
      return cancelEditClientDetail(state, action)
    case actionTypes.CHANGE_CLIENT_DETAIL:
      return changeClientDetail(state, action)
    case actionTypes.SAVE_CLIENT_DETAIL:
      return saveClientDetail(state, action)
    case actionTypes.SAVE_CLIENT_DETAIL_SUCCESS:
      return saveClientDetailSuccess(state, action)
    case actionTypes.SAVE_CLIENT_DETAIL_FAIL:
      return saveClientDetailFail(state, action)
    case actionTypes.SAVE_CLIENT_DETAIL_START:
      return saveClientDetailStart(state, action)
    case actionTypes.CREATE_PLAN_BY_RECOMMEND_TEMPLATE_START:
      return createPlanByRecommendTemplateStart(state, action)
    default: return state
  }
}

export default reducer