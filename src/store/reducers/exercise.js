import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  showVideo: false,
  showCompleteMessage: false,
  list: [],
  videoUrl: "",
  error: "",
  videoName: "",
  clientId: "",
  clientName: "",
  clientNameB: "",
  clientEmail: "",
  planName: "",
  introduction: "",
  plan: [],
  totalInEach: [0],
  period: 1,
  currentPeriod: 0,
  currentGroupStart: "",
  currentGroupEnd: "",
  periodDays: [""],
  articles: [],
  therapies: [],
  planId: "",
  templateId: "",
  coveredMoves: [],
  savedTemplateId: "",
  savedTemplateName: "",
  noExerciseError: false,
  noClientError: false,
  noTitleError: false,
  noNameError: false,
  start: false,
  empty: false,
  fromEditPlan: false,
  fromTemplate: false,
  fromTemplateToPlan: false,
  templates: [],
  introducing: false,
  introducingLoading: false,
  introducingOneLoading: false,
  introduceTemplateId: "",
  introducedTemplate: "",
  titleId: "",
  proposalState: 0,
  proposalContent: "",
  references: [""],
  initDone: false
}

const playVideo = (state, action) => {
  return updateObject(state, {
    showVideo: true,
    videoUrl: action.videoUrl,
    videoName: action.videoName
  })
}

const completeMessage = (state) => updateObject(state, {
  showCompleteMessage: true,
})

const closeVideo = state => updateObject(state, {showVideo: false})

const closeMessage = state => updateObject(state, {showCompleteMessage: false})

const exerciseInit = state => {
  if (state && state.fromEditPlan) {
    return {...initialState, fromEditPlan: true, clientId: state.clientId, planName: state.planName, plan: state.plan,
      articles: [...state.articles], therapies: [...state.therapies], initDone: state.initDone,
      period: state.period, totalInEach: [...state.totalInEach], periodDays: [...state.periodDays]
    }
  }
  if (state && state.clientId ) {
    return {...initialState, clientId: state.clientId, initDone: state.initDone}
  }
  if (state && state.titleId ) {
    return {...initialState, titleId: state.titleId, initDone: state.initDone}
  }
  // if (state && state.clientNameB && state.clientEmail)
  //   return {...initialState, clientNameB: state.clientNameB, clientEmail: state.clientEmail}
  // if (state && state.savedTemplateId && state.savedTemplateName) {
  //   return {
  //     ...initialState, savedTemplateId: state.savedTemplateId, savedTemplateName: state.savedTemplateName,
  //     coveredMoves: state.coveredMoves
  //   }
  // }
  if (state) {
    if (state && (state.plan.length > 0 || state.articles.length > 0)) {
      return {...initialState, plan: [...state.plan], articles: [...state.articles], therapies: [...state.therapies],
        period: state.period, totalInEach: [...state.totalInEach], periodDays: [...state.periodDays],
        introduction: state.introduction, titleId: state.titleId, initDone: state.initDone}
    } else {
      return {...initialState, introduction: state.introduction, titleId: state.titleId, initDone: state.initDone}
    }
  }
  return {...initialState, initDone: state.initDone}
}

const exerciseInitDone = state => {
  return updateObject(state, {initDone: true})
}

const nameChange = (state, action) => updateObject(state, {
  planName: action.name,
  noNameError: false
})

const clientNameBChange = (state, action) => updateObject(state, {
  clientNameB: action.clientNameB,
  noClientError: false
})

const clientEmailChange = (state, action) => updateObject(state, {
  clientEmail: action.clientEmail,
  noClientError: false
})

const introductionChange = (state, action) => updateObject(state, {
  introduction: action.introduction
})

const clientChange = (state, action) => updateObject(state, {
  clientId: action.clientId,
  noClientError: false
})

const titleChange = (state, action) => updateObject(state, {
  titleId: action.titleId,
  noTitleError: false
})

const addMovement = (state, action) => {
  if (action.moveType === 1) {
    let newPlan = [...state.plan]
    let newCurrentGroupEnd = state.currentGroupEnd
    if (state.currentGroupStart === "") {
      newPlan = [...state.plan.slice(0, state.totalInEach[state.currentPeriod]),
        {moveName: action.name, moveId: action.id}, ...state.plan.slice(state.totalInEach[state.currentPeriod])]
    } else {
      const startNum = Number(state.currentGroupStart.split(" ")[1])
      const endNum = Number(state.currentGroupEnd.split(" ")[1])
      const endIndex = state.currentPeriod === 0 ? endNum : endNum + state.totalInEach[state.currentPeriod-1]
      const startIndex = state.currentPeriod === 0 ? startNum : startNum + state.totalInEach[state.currentPeriod-1]
      newPlan = [...state.plan.slice(0, startIndex),
        {...state.plan[startIndex], contains: state.plan[startIndex].contains+1},
        ...state.plan.slice(startIndex+1, endIndex),
        {moveName: action.name, moveId: action.id},
        ...state.plan.slice(endIndex)]
      newCurrentGroupEnd = state.currentPeriod+" "+(endNum+1)
    }
    const newTotalInEach = state.totalInEach.map((num, index) => index >= state.currentPeriod ? num + 1 : num)
    return updateObject(state, {
      plan: [...newPlan],
      totalInEach: [...newTotalInEach],
      currentGroupEnd: newCurrentGroupEnd,
      noExerciseError: false
    })
  } else if (action.moveType === 3){
    return updateObject(state, {
      articles: [...state.articles, {moveName: action.name, moveId: action.id}],
      noExerciseError: false
    })
  } else if (action.moveType === 2){
    return updateObject(state, {
      therapies: [...state.therapies, {moveName: action.name, moveId: action.id, params: [...action.params]}],
      noExerciseError: false
    })
  } else return state
}

const addPeriod = state => {
  const newPeriod = state.period + 1
  return updateObject(state, {
    period: newPeriod,
    currentPeriod: newPeriod - 1,
    totalInEach: [...state.totalInEach, state.plan.length],
    noExerciseError: false,
    periodDays: [...state.periodDays, ""],
    currentGroupStart: "",
    currentGroupEnd: ""
  })
}

const changePeriod = (state, action) => {
  return updateObject(state, {
    currentPeriod: action.index,
    noExerciseError: false,
    currentGroupStart: "",
    currentGroupEnd: "",
  })
}

const deleteMove = (state, action) => {
  if (action.index === 1) {
    const totalIndex = action.period === 0 ?
      action.periodIndex : action.periodIndex + state.totalInEach[action.period-1]
    const stop = action.period === 0 ? 0 : state.totalInEach[action.period-1]
    let barIndex = -1
    for (let i = totalIndex-1; i >= stop; i = i-1) {
      if (state.plan[i].bar && state.plan[i].start) {
        barIndex = i
        break
      } else if (state.plan[i].bar && !state.plan[i].start) break
    }
    let newPlanWithChange = [...state.plan]
    if (barIndex >= 0) {
      newPlanWithChange = [
        ...state.plan.slice(0, barIndex),
        {...state.plan[barIndex], contains: state.plan[barIndex].contains-1},
        ...state.plan.slice(barIndex+1),
      ]
    }
    const newPlan = newPlanWithChange.filter((move, index) => index !== totalIndex)
    const newTotalInEach = state.totalInEach.map((num, index) => index >= action.period ? num - 1 : num)
    const startChange = state.currentGroupStart !== "" &&
      action.period === Number(state.currentGroupStart.split(" ")[0]) &&
      action.periodIndex < Number(state.currentGroupStart.split(" ")[1])
    const endChange = state.currentGroupEnd !== "" &&
      action.period === Number(state.currentGroupEnd.split(" ")[0]) &&
      action.periodIndex < Number(state.currentGroupEnd.split(" ")[1])
    return updateObject(state, {
      plan: [...newPlan],
      totalInEach: [...newTotalInEach],
      noExerciseError: false,
      currentGroupStart: startChange ? state.currentGroupStart.split(" ")[0] + " "
        + Number(state.currentGroupStart.split(" ")[1]-1) : state.currentGroupStart,
      currentGroupEnd: endChange ? state.currentGroupEnd.split(" ")[0] + " "
        + Number(state.currentGroupEnd.split(" ")[1]-1) : state.currentGroupEnd
    })
  } else if (action.index === 2) {
    const newTherapies = [...state.therapies].filter(move => move.moveId !== action.moveId)
    return updateObject(state, {therapies: newTherapies, noExerciseError: false})
  } else if (action.index === 3) {
    const newArticles = [...state.articles].filter(move => move.moveId !== action.moveId)
    return updateObject(state, {articles: newArticles, noExerciseError: false})
  }
}

const deletePeriod = (state, action) => {
  const newPeriod = state.period - 1
  const newPlan = [...state.plan.slice(0, action.index === 0 ? 0 : state.totalInEach[action.index-1]),
    ...state.plan.slice(state.totalInEach[action.index])]
  const newTotalInEach = []
  for (let i = 0; i < state.totalInEach.length; i = i+1) {
    if (i < action.index) newTotalInEach.push(state.totalInEach[i])
    else if (i > action.index) {
      if (action.index === 0) {
        newTotalInEach.push(state.totalInEach[i] - state.totalInEach[action.index])
      } else {
        newTotalInEach.push(state.totalInEach[i] -
          (state.totalInEach[action.index] - state.totalInEach[action.index-1]))
      }

    }
  }
  const newCurrentPeriod = state.currentPeriod >= action.index ?
    state.currentPeriod === 0 ?
      0 : state.currentPeriod-1 : state.currentPeriod
  const newPeriodDays = [...state.periodDays.slice(0, action.index), ...state.periodDays.slice(action.index+1)]
  return updateObject(state, {period: newPeriod, currentPeriod: newCurrentPeriod,
    plan: [...newPlan], totalInEach: [...newTotalInEach], noExerciseError: false,
    periodDays: newPeriodDays
  })
}

const displayError = (state, action) => {
  updateObject(state, {noExerciseError: false, noClientError: false, noNameError: false, noTitleError: false})
  if (action.template) {
    const noExerciseError = !action.plan.every(period => period !== undefined
      && period.filter(move => move.moveId).length > 0)
    return updateObject(state, {noExerciseError, noClientError: false, noNameError: false, noTitleError: false})
  } else {
    const noExerciseError = !action.plan.every(period => period !== undefined
      && period.filter(move => move.moveId).length > 0)
    const noClientError = !state.clientId
    const noNameError = !state.planName
    const noTitleError = !state.titleId
    return updateObject(state, {noExerciseError, noClientError, noNameError, noTitleError})
  }
}

// const deleteTemplateInPlan = state => updateObject(state, {
//   savedTemplateId: "",
//   savedTemplateName: "",
//   coveredMoves: []
// })

const editPlanSuccess = (state, action) => {
  const plan = []
  let contains = 0
  let start = 0
  for (let i = 0; i < action.moves.length; i += 1) {
    if (action.moves[i].bar) {
      if (action.moves[i].start) {
        contains = 0
        start = i
        plan.push({...action.moves[i], name: action.moves[i].barName})
      }
      if (!action.moves[i].start) {
        plan[start].contains = contains
        contains = 0
        plan.push({...action.moves[i]})
      }
    } else {
      contains += 1
      plan.push({...action.moves[i]})
    }
  }
  return updateObject(state, {
    plan,
    empty: action.plan.empty,
    start: action.plan.start,
    startDate: action.plan.startDate,
    planName: action.plan.name,
    introduction: action.plan.summary,
    planId: action.plan.planId,
    articles: action.articleMoves,
    therapies: action.therapyMoves,
    fromEditPlan: true,
    clientId: action.clientId,
    period: action.plan.totalInEach.length === 0 ? 1 : action.plan.totalInEach.length,
    totalInEach: action.plan.totalInEach.length === 0 ?
      [action.moves.length] : [...action.plan.totalInEach],
    periodDays: action.plan.totalInEach.length === 0 ? [""] : [...action.plan.periodDays],
    currentPeriod: action.plan.totalInEach.length === 0 ? 0 : action.plan.totalInEach.length - 1
  })
}

const editTemplateSuccess = (state, action) => {
  const plan = []
  let contains = 0
  let start = 0
  for (let i = 0; i < action.moves.length; i += 1) {
    if (action.moves[i].bar) {
      if (action.moves[i].start) {
        contains = 0
        start = i
        plan.push({...action.moves[i], name: action.moves[i].barName})
      }
      if (!action.moves[i].start) {
        plan[start].contains = contains
        contains = 0
        plan.push({...action.moves[i]})
      }
    } else {
      contains += 1
      plan.push({...action.moves[i]})
    }
  }
  return updateObject(state, {
    plan,
    planName: action.template.name,
    introduction: action.template.summary,
    templateId: action.template.templateId,
    articles: action.articles,
    fromTemplate: true,
    period: action.template.totalInEach.length === 0 ? 1 : action.template.totalInEach.length,
    totalInEach: action.template.totalInEach.length === 0 ?
      [action.moves.length] : [...action.template.totalInEach],
    therapies: action.therapies,
    periodDays: action.template.totalInEach.length === 0 ? [""] : [...action.template.periodDays],
    proposalContent: action.template.proposalContent && action.template.proposalContent.length > 0 ? action.template.proposalContent : "",
    references: action.template.references.length > 0 ? action.template.references : [""]
  })
}

const createPlanByTemplateSuccess = (state, action) => {
  const plan = []
  let contains = 0
  let start = 0
  for (let i = 0; i < action.moves.length; i += 1) {
    if (action.moves[i].bar) {
      if (action.moves[i].start) {
        contains = 0
        start = i
        plan.push({...action.moves[i], name: action.moves[i].barName})
      }
      if (!action.moves[i].start) {
        plan[start].contains = contains
        contains = 0
        plan.push({...action.moves[i]})
      }
    } else {
      contains += 1
      plan.push({...action.moves[i]})
    }
  }
  return updateObject(state, {
    plan: plan,
    articles: action.articles,
    planName: action.template.name,
    introduction: action.template.summary,
    templateId: action.template.templateId,
    fromTemplateToPlan: true,
    period: action.template.totalInEach.length === 0 ? 1 : action.template.totalInEach.length,
    totalInEach: action.template.totalInEach.length === 0 ?
      [action.moves.length] : [...action.template.totalInEach],
    periodDays: action.template.totalInEach.length === 0 ? [""] : [...action.template.periodDays],
    clientId: action.clientId, therapies: action.therapies,
    proposalContent: action.template.proposalContent && action.template.proposalContent.length > 0 ? action.template.proposalContent : "",
    references: action.template.references.length > 0 ? action.template.references : [""]
  })
}

const createPlanForClient = (state, action) => {
  return updateObject(state, {
    clientId: action.clientId
  })
}

const changeOrder = (state, action) => {
  let currentGroupStart = -1
  let currentGroupEnd = -1
  if (state.currentGroupStart !== "" && action.index === state.currentPeriod) {
    currentGroupStart = Number(state.currentGroupStart.split(" ")[1])
    currentGroupEnd = Number(state.currentGroupEnd.split(" ")[1])
    const startDiff1 = action.source - currentGroupStart
    const startDiff2 = action.dest - currentGroupStart
    const endDiff1 = action.source - currentGroupEnd
    const endDiff2 = action.dest - currentGroupEnd
    if (startDiff1 > 0 && startDiff2 <= 0) currentGroupStart += 1
    if (startDiff1 < 0 && startDiff2 >= 0) currentGroupStart -= 1
    if (endDiff1 > 0 && endDiff2 <= 0) currentGroupEnd += 1
    if (endDiff1 < 0 && endDiff2 >= 0) currentGroupEnd -= 1
  }
  let currentContain = -1
  for (let i = action.newPlan.length-1; i >= 0; i--) {
    const move = action.newPlan[i]
    if (move.bar && !move.start) {
      currentContain = 0
    } else if (move.bar && move.start) {
      move.contains = currentContain
    } else {
      currentContain += 1
    }
  }
  return updateObject(state, {
    plan: [...state.plan.slice(0, action.index === 0 ? 0 : state.totalInEach[action.index-1]),
      ...action.newPlan,
      ...state.plan.slice(state.totalInEach[action.index])],
    noExerciseError: false,
    currentGroupStart: currentGroupStart >= 0 ? action.index+" "+currentGroupStart : state.currentGroupStart,
    currentGroupEnd: currentGroupEnd >= 0 ? action.index+" "+currentGroupEnd : state.currentGroupEnd,
  })
}

const changePeriodOrder = (state, action) => {
  const {source, dest} = action
  let newPlan = []
  let newEachNum = []
  const eachNum = state.totalInEach.map((num, index) => num - (index === 0 ? 0 : state.totalInEach[index-1]))
  const newTotalInEach = []
  let newPeriodDays = []
  if (source < dest) {
    newPlan = [
      ...state.plan.slice(0, source === 0 ? 0 : state.totalInEach[source-1]),
      ...state.plan.slice(state.totalInEach[source], state.totalInEach[dest]),
      ...state.plan.slice(source === 0 ? 0 : state.totalInEach[source-1], state.totalInEach[source]),
      ...state.plan.slice(state.totalInEach[dest]),
    ]
    newEachNum = [
      ...eachNum.slice(0, source),
      ...eachNum.slice(source+1, dest+1),
      ...eachNum.slice(source, source+1),
      ...eachNum.slice(dest+1),
    ]
    newPeriodDays = [
      ...state.periodDays.slice(0, source),
      ...state.periodDays.slice(source+1, dest+1),
      ...state.periodDays.slice(source, source+1),
      ...state.periodDays.slice(dest+1),
    ]
  } else if (source > dest) {
    newPlan = [
      ...state.plan.slice(0, dest === 0 ? 0 : state.totalInEach[dest-1]),
      ...state.plan.slice(state.totalInEach[source-1], state.totalInEach[source]),
      ...state.plan.slice(dest === 0 ? 0 : state.totalInEach[dest-1], state.totalInEach[source-1]),
      ...state.plan.slice(state.totalInEach[source]),
    ]
    newEachNum = [
      ...eachNum.slice(0, dest),
      ...eachNum.slice(source, source+1),
      ...eachNum.slice(dest, source),
      ...eachNum.slice(source+1),
    ]
    newPeriodDays = [
      ...state.periodDays.slice(0, dest),
      ...state.periodDays.slice(source, source+1),
      ...state.periodDays.slice(dest, source),
      ...state.periodDays.slice(source+1),
    ]
  } else {
    return state
  }
  let current = 0
  newEachNum.forEach(num => {
    current = current + num
    newTotalInEach.push(current)
  })
  return updateObject(state, {
    plan: [...newPlan],
    totalInEach: newTotalInEach,
    periodDays: newPeriodDays,
    currentPeriod: source === state.currentPeriod ? dest : state.currentPeriod,
    noExerciseError: false,
  })
}

const deleteAll = state => updateObject(state, {
  plan: [], articles: [], planName: "", introduction: "", therapies: [],
  period: 1, totalInEach: [0],
  currentPeriod: 0,
  currentGroupStart: "", currentGroupEnd: ""
})

const addGroup = (state, action) => {
  const newPlanWithoutRenaming = state.plan.map(move => {
    if (move.bar && move.editing) return {...move, editing: false}
    else return move
  })
  const endpoint = state.totalInEach[action.index1]
  const newPlan = [...newPlanWithoutRenaming.slice(0, endpoint),
    {bar: true, index1: action.index1, start: true, contains: 0, editing: true, name: "新分类"},
    {bar: true, index1: action.index1, start: false},
    ...newPlanWithoutRenaming.slice(endpoint)]
  const index = action.index1 === 0 ? endpoint : endpoint - state.totalInEach[action.index1-1]
  return updateObject(state, {
    currentPeriod: action.index1,
    plan: newPlan,
    totalInEach: state.totalInEach.map((num, index) => index < action.index1 ? num : num+2),
    currentGroupStart: action.index1+" "+index,
    currentGroupEnd: action.index1+" "+(index+1),
    noExerciseError: false
  })
}

const changeGroup = (state, action) => {
  const theOne = action.index1 === 0 ? action.index : state.totalInEach[action.index1-1] + action.index
  return updateObject(state, {
    currentPeriod: action.index1,
    currentGroupStart: action.index1+" "+action.index,
    currentGroupEnd: action.index1+" "+(action.index+state.plan[theOne].contains+1)
  })
}

const deleteGroup = (state, action) => {
  const start = action.index1 === 0 ? action.index : state.totalInEach[action.index1-1] + action.index
  const end = start + state.plan[start].contains + 1
  let newCurrentGroupStart = state.currentGroupStart
  let newCurrentGroupEnd = state.currentGroupEnd
  if (state.currentGroupStart === action.index1 + " " + action.index) {
    newCurrentGroupStart = ""
    newCurrentGroupEnd = ""
  } else if (state.currentPeriod === action.index1 && Number(state.currentGroupStart.split(" ")[1]) > action.index) {
    const previousGroupStartIndex = Number(state.currentGroupStart.split(" ")[1])
    const previousGroupEndIndex = Number(state.currentGroupEnd.split(" ")[1])
    newCurrentGroupStart = action.index1 + " " + (previousGroupStartIndex-state.plan[start].contains-2)
    newCurrentGroupEnd = action.index1 + " " + (previousGroupEndIndex-state.plan[start].contains-2)
  }
  return updateObject(state, {
    plan: [...state.plan.slice(0, start), ...state.plan.slice(end+1)],
    totalInEach: state.totalInEach.map((num, i) => i < action.index1 ? num : num - state.plan[start].contains - 2),
    currentGroupStart: newCurrentGroupStart,
    currentGroupEnd: newCurrentGroupEnd,
  })
}

const renameGroup = (state, action) => {
  const newPlan = state.plan.map(move => {
    if (move.bar && move.editing) return {...move, editing: false}
    else return move
  })
  const theOne = action.index1 === 0 ? action.index : state.totalInEach[action.index1-1] + action.index
  return updateObject(state, {
    plan: [...newPlan.slice(0, theOne), {...newPlan[theOne], editing: true}, ...newPlan.slice(theOne+1)]
  })
}

const renameGroupDone = (state, action) => {
  const theOne = action.index1 === 0 ? action.index : state.totalInEach[action.index1-1] + action.index
  return updateObject(state, {
    plan: [...state.plan.slice(0, theOne), {...state.plan[theOne], editing: false}, ...state.plan.slice(theOne+1)]
  })
}

const renamingGroup = (state, action) => {
  const theOne = action.index1 === 0 ? action.index : state.totalInEach[action.index1-1] + action.index
  return updateObject(state, {
    plan: [...state.plan.slice(0, theOne), {...state.plan[theOne], name: action.value}, ...state.plan.slice(theOne+1)]
  })
}

const loseFocus = (state, action) => {
  const theOne = action.index1 === 0 ? action.index : state.totalInEach[action.index1-1] + action.index
  return updateObject(state, {
    plan: [...state.plan.slice(0, theOne), {...state.plan[theOne], editing: false}, ...state.plan.slice(theOne+1)]
  })
}

const switchTo = (state) => updateObject(state, {
  fromTemplate: false, fromTemplateToPlan: false, fromEditPlan: false, start: false
})

const introduceTemplateStart = (state) => updateObject(state, {
  introducing: true, introducingLoading: true
})

const introduceTemplateSuccess = (state, action) => updateObject(state, {
  templates: action.templates, introducingLoading: false
})

const introduceTemplateFail = (state) => updateObject(state, {
  introducingLoading: false
})

const introduceOneTemplateStart = (state) => updateObject(state, {
  introducingOneLoading: true
})

const introduceOneTemplateSuccess = (state, action) => {
  const newTotal = action.template.totalInEach.map(num => num+state.totalInEach[state.totalInEach.length-1])
  console.log(state)
  console.log(action)
  const actionArticleIds = action.articles.map(article => article.moveId)
  const stateArticles = [...state.articles]
  console.log(actionArticleIds)
  const newStateArticles = stateArticles.filter(article => actionArticleIds.indexOf(article.moveId) < 0)
  console.log(newStateArticles)

  const actionTherapyIds = action.therapies.map(therapy => therapy.moveId)
  const stateTherapies = [...state.therapies]
  const newStateTherapies = stateTherapies.filter(therapy => actionTherapyIds.indexOf(therapy.moveId) < 0)
  console.log(newStateTherapies)


  return updateObject(state, {
    introducing: false, introducingLoading: false, introducingOneLoading: false,
    introducedTemplate: action.template.templateId,
    plan: state.plan.length === 0 ? [...action.plans] : [...state.plan, ...action.plans],
    periodDays: state.plan.length === 0 ? [...action.template.periodDays] : [...state.periodDays, ...action.template.periodDays],
    period: state.plan.length === 0 ? action.template.totalInEach.length : state.period + action.template.totalInEach.length,
    totalInEach: state.plan.length === 0 ? [...action.template.totalInEach] : [...state.totalInEach, ...newTotal],
    articles: [...newStateArticles, ...action.articles], planName: action.template.name, therapies: [...newStateTherapies, ...action.therapies],
    proposalContent: action.template.proposalContent && action.template.proposalContent.length > 0 ? action.template.proposalContent : "",
    references: action.template.references.length > 0 ? action.template.references : [""]
  })
}

const introduceOneTemplateFail = (state, action) => updateObject(state, {
  introducingOneLoading: false
})

const closeTemplates = (state) => updateObject(state, {
  introducing: false, introducingLoading: false, templates: [], introducingOneLoading: false
})

const closeProposal = (state) => updateObject(state, {
  proposalState: 0
})

const changeTemplateId = (state, action) => updateObject(state, {
  introduceTemplateId: action.templateId
})

const exerciseUnmount = state => updateObject(initialState, {
  plan: [...state.plan], articles: [...state.articles], period: state.period, totalInEach: [...state.totalInEach],
  introduction: state.introduction, therapies: [...state.therapies], initDone: state.initDone
})

const previewInit = state => updateObject(initialState, {
  plan: [...state.plan], articles: [...state.articles], period: state.period, totalInEach: [...state.totalInEach],
  introduction: state.introduction, therapies: [...state.therapies]
})

const completeHomework = (state, action) => updateObject(initialState, {
  titleId: action.titleId
})

const showProposalSuccess = (state, action) => updateObject(state, {
  proposalState: 1,
})

const deleteReference = (state, action) => {
  const index = action.index
  if (index < state.references.length) {
    const newReferences = [...state.references.slice(0, index), ...state.references.slice(index+1)]
    return updateObject(state, {
      references: newReferences,
    })
  } else {
    return state
  }
}

const changeReference = (state, action) => {
  const {index, value} = action
  if (index < state.references.length) {
    const newReferences = [...state.references.slice(0, index), value, ...state.references.slice(index+1)]
    return updateObject(state, {
      references: newReferences,
    })
  } else {
    return state
  }
}

const addReference = (state, action) => {
  return updateObject(state, {
    references: [...state.references, ""],
  })
}

const changeProposal = (state, action) => {
  return updateObject(state, {
    proposalContent: action.value,
  })
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PLAY_VIDEO:
      return playVideo(state, action)
    case actionTypes.COMPLETE_MESSAGE:
      return completeMessage(state, action)
    case actionTypes.CLOSE_VIDEO:
      return closeVideo(state)
    case actionTypes.CLOSE_MESSAGE:
      return closeMessage(state)
    case actionTypes.EXERCISE_INIT:
      return exerciseInit(state)
    case actionTypes.EXERCISE_INIT_DONE:
      return exerciseInitDone(state)
    case actionTypes.NAME_CHANGE:
      return nameChange(state, action)
    case actionTypes.CLIENT_NAME_B_CHANGE:
      return clientNameBChange(state, action)
    case actionTypes.CLIENT_EMAIL_CHANGE:
      return clientEmailChange(state, action)
    case actionTypes.CLIENT_CHANGE:
      return clientChange(state, action)
    case actionTypes.TITLE_CHANGE_STUDENT:
      return titleChange(state, action)
    case actionTypes.ADD_MOVEMENT:
      return addMovement(state, action)
    case actionTypes.ADD_PERIOD:
      return addPeriod(state)
    case actionTypes.CHANGE_PERIOD:
      return changePeriod(state, action)
    case actionTypes.DELETE_MOVE:
      return deleteMove(state, action)
    case actionTypes.DELETE_PERIOD:
      return deletePeriod(state, action)
    // case actionTypes.DELETE_TEMPLATE_IN_PLAN:
    //   return deleteTemplateInPlan(state)
    case actionTypes.DISPLAY_ERROR:
      return displayError(state, action)
    case actionTypes.EXERCISE_UNMOUNT:
      return exerciseUnmount(state)
    case actionTypes.PREVIEW_INIT:
      return previewInit(state)
    case actionTypes.INTRODUCTION_CHANGE:
      return introductionChange(state, action)
    case actionTypes.EDIT_PLAN_SUCCESS:
      return editPlanSuccess(state, action)
    case actionTypes.EDIT_TEMPLATE_SUCCESS:
      return editTemplateSuccess(state, action)
    case actionTypes.CREATE_PLAN_BY_TEMPLATE_SUCCESS:
      return createPlanByTemplateSuccess(state, action)
    case actionTypes.CREATE_PLAN_FOR_CLIENT:
      return createPlanForClient(state, action)
    case actionTypes.CHANGE_ORDER:
      return changeOrder(state, action)
    case actionTypes.CHANGE_PERIOD_ORDER:
      return changePeriodOrder(state, action)
    case actionTypes.DELETE_ALL:
      return deleteAll(state)
    case actionTypes.ADD_GROUP:
      return addGroup(state, action)
    case actionTypes.CHANGE_GROUP:
      return changeGroup(state, action)
    case actionTypes.DELETE_GROUP:
      return deleteGroup(state, action)
    case actionTypes.RENAME_GROUP:
      return renameGroup(state, action)
    case actionTypes.RENAME_GROUP_DONE:
      return renameGroupDone(state, action)
    case actionTypes.RENAMING_GROUP:
      return renamingGroup(state, action)
    case actionTypes.LOSE_FOCUS:
      return loseFocus(state, action)
    case actionTypes.SWITCH_TO:
      return switchTo(state, action)
    case actionTypes.INTRODUCE_TEMPLATE_START:
      return introduceTemplateStart(state, action)
    case actionTypes.INTRODUCE_TEMPLATE_SUCCESS:
      return introduceTemplateSuccess(state, action)
    case actionTypes.INTRODUCE_TEMPLATE_FAIL:
      return introduceTemplateFail(state, action)
    case actionTypes.CLOSE_TEMPLATES:
      return closeTemplates(state, action)
    case actionTypes.CLOSE_PROPOSAL:
      return closeProposal(state, action)
    case actionTypes.CHANGE_TEMPLATE_ID:
      return changeTemplateId(state, action)
    case actionTypes.INTRODUCE_ONE_TEMPLATE_START:
      return introduceOneTemplateStart(state, action)
    case actionTypes.INTRODUCE_ONE_TEMPLATE_SUCCESS:
      return introduceOneTemplateSuccess(state, action)
    case actionTypes.INTRODUCE_ONE_TEMPLATE_FAIL:
      return introduceOneTemplateFail(state, action)
    case actionTypes.COMPLETE_HOMEWORK:
      return completeHomework(state, action)
    case actionTypes.SHOW_PROPOSAL_SUCCESS:
      return showProposalSuccess(state, action)
    case actionTypes.DELETE_REFERENCE:
      return deleteReference(state, action)
    case actionTypes.CHANGE_REFERENCE:
      return changeReference(state, action)
    case actionTypes.ADD_REFERENCE:
      return addReference(state, action)
    case actionTypes.CHANGE_PROPOSAL:
      return changeProposal(state, action)
    default: return state
  }
}

export default reducer