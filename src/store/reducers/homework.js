import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  mode: 0,
  teacher: {},
  titles: [],
  classInfo: {},
  submits: [],
  fetchError: "",
  proposalState: 0,
}

const fetchClassesAndHomeworkStart = (state) => updateObject(state, {
  mode: 1
})

const fetchClassesAndHomeworkSuccess = (state, action) => updateObject(state, {
  teacher: action.teacher,
  titles: action.titles,
  classInfo: {...action.classInfo},
  submits: action.submits,
  mode: 0,
})

const fetchClassesAndHomeworkFail = (state, action) => updateObject(state, {
  fetchError: action.msg,
  mode: 0
})


const closeModal = (state, action) => updateObject(state, {

})




const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CLASSES_AND_HOMEWORK_START:
      return fetchClassesAndHomeworkStart(state)
    case actionTypes.FETCH_CLASSES_AND_HOMEWORK_SUCCESS:
      return fetchClassesAndHomeworkSuccess(state, action)
    case actionTypes.FETCH_CLASSES_AND_HOMEWORK_FAIL:
      return fetchClassesAndHomeworkFail(state, action)
    case actionTypes.CLOSE_MODAL:
      return closeModal(state, action)

    default: return state
  }
}

export default reducer