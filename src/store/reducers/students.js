import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
  mode: 0,
  students: [],
  fetchError: "",
  classInfo: {},
  titles: [],
  selectedTitleId: "",
  titleMode: 0,
  titleError: "",
  homework: [],
  newTitleMode: 0,
  newTitleError: "",
  reviewMode: 0,
  reviewError: "",
  newTitleId: "",
  proposalState: 0,
  name: "",
  proposalContent: "",
  references: [],
  proposalErr: ""
}

const fetchClassesAndStudentsStart = (state) => updateObject(state, {
  mode: 1
})

const fetchClassesAndStudentsSuccess = (state, action) => updateObject(state, {
  students: action.students,
  titles: action.titles,
  classInfo: {...action.classInfo},
  mode: 0,
  newTitleMode: 0,
  newTitleError: ""
})

const fetchClassesAndStudentsFail = (state, action) => updateObject(state, {
  fetchError: action.msg,
  mode: 0
})

const titleChangeStart = (state, action) => updateObject(state, {
  selectedTitleId: action.titleId,
  titleMode: 1
})

const titleChangeSuccess = (state, action) => updateObject(state, {
  titleMode: 0,
  homework: action.doc,
  newTitleMode: 0
})

const titleChangeFail = (state, action) => updateObject(state, {
  titleMode: 0,
  titleError: action.msg
})

const submitNewTitleStart = (state, action) => updateObject(state, {
  newTitleMode: 1
})

const submitNewTitleSuccess = (state, action) => updateObject(state, {
  newTitleMode: 2,
  newTitleId: action.titleId
})

const submitNewTitleFail = (state, action) => updateObject(state, {
  newTitleMode: 2,
  newTitleError: action.msg
})

const submitReviewStart = (state, action) => updateObject(state, {
  reviewMode: 1
})

const submitReviewSuccess = (state, action) => updateObject(state, {
  reviewMode: 2,
})

const submitReviewFail = (state, action) => updateObject(state, {
  reviewMode: 2,
  reviewError: action.msg
})

const modalLoading = (state, action) => updateObject(state, {
  // proposalState: 1,
})

const closeStudentProposal = (state, action) => updateObject(state, {
  proposalState: 0,
})

const seeProposalSuccess = (state, action) => updateObject(state, {
  proposalState: 3,
  name: action.name,
  proposalContent: action.proposalContent,
  references: [...action.references]
})

const seeProposalFail = (state, action) => updateObject(state, {
  proposalState: 2, proposalErr: action.msg
})

const modalStudentLoading = (state, action) => updateObject(state, {
  proposalState: 1,
})



const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CLASSES_AND_STUDENTS_START:
      return fetchClassesAndStudentsStart(state)
    case actionTypes.FETCH_CLASSES_AND_STUDENTS_SUCCESS:
      return fetchClassesAndStudentsSuccess(state, action)
    case actionTypes.FETCH_CLASSES_AND_STUDENTS_FAIL:
      return fetchClassesAndStudentsFail(state, action)
    case actionTypes.TITLE_CHANGE_START:
      return titleChangeStart(state, action)
    case actionTypes.TITLE_CHANGE_SUCCESS:
      return titleChangeSuccess(state, action)
    case actionTypes.TITLE_CHANGE_FAIL:
      return titleChangeFail(state, action)
    case actionTypes.SUBMIT_NEW_TITLE_START:
      return submitNewTitleStart(state, action)
    case actionTypes.SUBMIT_NEW_TITLE_SUCCESS:
      return submitNewTitleSuccess(state, action)
    case actionTypes.SUBMIT_NEW_TITLE_FAIL:
      return submitNewTitleFail(state, action)
    case actionTypes.SUBMIT_REVIEW_START:
      return submitReviewStart(state, action)
    case actionTypes.SUBMIT_REVIEW_SUCCESS:
      return submitReviewSuccess(state, action)
    case actionTypes.SUBMIT_REVIEW_FAIL:
      return submitReviewFail(state, action)
    case actionTypes.MODAL_LOADING:
      return modalLoading(state, action)
    case actionTypes.SEE_PROPOSAL_SUCCESS:
      return seeProposalSuccess(state, action)
    case actionTypes.SEE_PROPOSAL_FAIL:
      return seeProposalFail(state, action)
    case actionTypes.CLOSE_STUDENT_PROPOSAL:
      return closeStudentProposal(state, action)
    case actionTypes.MODAL_STUDENT_LOADING:
      return modalStudentLoading(state, action)
    default: return state
  }
}

export default reducer